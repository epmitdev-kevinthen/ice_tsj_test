﻿
Partial Class strPeriode
    Inherits System.Web.UI.Page
    Dim myChef As New Chef

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click


        Try

            infoerror.Text = ""
            infoerror.Text = myChef.SQLDelMSTPERIODE(TxtKode_prd2.Text)
            Response.Redirect(HttpContext.Current.Request.Url.ToString(), True)
        Catch ex As Exception
            infoerror.Text = ex.Message
        End Try


    End Sub

    Private Sub btnInsert_Click(sender As Object, e As EventArgs) Handles btnInsert.Click

        Try

            infoerror.Text = ""

            Dim mpLabel As Label
            Dim myusername As String = ""
            mpLabel = CType(Master.FindControl("lblusername"), Label)
            If Not mpLabel Is Nothing Then
                myusername = mpLabel.Text
            End If
            infoerror.Text = myChef.SQLNewMSTPERIODE(TxtKode_prd3.Text, TxtNama_prd3.Text,
                               txtStartDate3.Text, TxtEndDate3.Text, myusername, ddlNotifSelfAsses_New.SelectedValue, txt_start_date_selasses_new.Text, txt_end_date_selasses_new.Text)
            Response.Redirect(HttpContext.Current.Request.Url.ToString(), True)
        Catch ex As Exception
            infoerror.Text = ex.Message
        End Try

    End Sub

    Private Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click

        Try

            infoerror.Text = ""

            Dim mpLabel As Label
            Dim myusername As String = ""
            mpLabel = CType(Master.FindControl("lblusername"), Label)
            If Not mpLabel Is Nothing Then
                myusername = mpLabel.Text
            End If
            infoerror.Text = myChef.SQLEditMSTPERIODE(TxtKode_prd.Text, TxtNama_prd.Text,
                               txtStartDate.Text, TxtEndDate.Text, myusername, TxtFlagAktif.Text,
                                ddlNotifSelfAsses_Edit.SelectedValue, txt_start_date_selasses_edit.Text, txt_end_date_selasses_edit.Text)
            Response.Redirect(HttpContext.Current.Request.Url.ToString(), True)
        Catch ex As Exception
            infoerror.Text = ex.Message
        End Try


    End Sub

    Private Sub strCheckList_Init(sender As Object, e As EventArgs) Handles Me.Init
        Dim myData As Data.DataSet
        Dim myhtml As New StringBuilder

        If Session("sUsername") Is Nothing Then
            Response.Redirect("Default.aspx")
        End If


        If Session("levelakses") Is Nothing Then
            Response.Redirect("hakakses.aspx")
        ElseIf IsDBNull(Session("levelakses")) Then
            Response.Redirect("hakakses.aspx")
        ElseIf Session("levelakses") <> "1" And Session("levelakses") <> "2" Then
            Response.Redirect("hakakses.aspx")
        End If


        TxtFlagAktif.Items.Add("A")
        TxtFlagAktif.Items.Add("I")


        myData = myChef.SQLMSTPERIODE
        myhtml.Append(" <table id='mytable' class='stripe row-border order-column' cellspacing='0' width='100%'> ")
        'myhtml.Append(" <table id='mytable' class='cell-border' cellspacing='0' width='100%'> ")
        myhtml.Append("   <thead>")
        myhtml.Append("      <tr>")
        myhtml.Append("         <th>Kode</th>")
        myhtml.Append("         <th></th>")
        myhtml.Append("         <th>Nama Periode</th>")
        myhtml.Append("         <th>Start Date</th>")
        myhtml.Append("         <th>End Date</th>")
        myhtml.Append("         <th>Status</th>")
        myhtml.Append("         <th>Flag Notification Self Assessment</th>")
        myhtml.Append("         <th>Start Date Self Assessment</th>")
        myhtml.Append("         <th>End Date Self Assessment</th>")
        myhtml.Append("         <th></th>")
        myhtml.Append("      </tr>")
        myhtml.Append("   </thead>")
        myhtml.Append("   <tbody>")
        For i = 0 To myData.Tables(0).Rows.Count - 1
            myhtml.Append("      <tr>")

            Dim mydate1 As String = Format(myData.Tables(0).Rows(i).Item("start_date"), "d-MMM-yyyy")
            Dim mydate2 As String = Format(myData.Tables(0).Rows(i).Item("end_date"), "d-MMM-yyyy")

            Dim Temps As String = myData.Tables(0).Rows(i).Item("kode_prd") & "|" &
                                  myData.Tables(0).Rows(i).Item("nama_prd") & "|" &
                                  mydate1 & "|" & mydate2 & "|" &
                                  myData.Tables(0).Rows(i).Item("flag_aktif") & "|" &
                                  myData.Tables(0).Rows(i).Item("flag_notif_selasses") & "|" &
                                  If(IsDBNull(myData.Tables(0).Rows(i).Item("start_date_selasses")), "", DateTime.Parse(myData.Tables(0).Rows(i).Item("start_date_selasses")).ToString("yyyy-MM-dd")) & "|" &
                                  If(IsDBNull(myData.Tables(0).Rows(i).Item("end_date_selasses")), "", DateTime.Parse(myData.Tables(0).Rows(i).Item("end_date_selasses")).ToString("yyyy-MM-dd"))

            myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("kode_prd") & "</td>")

            myhtml.Append("<td class='text-right'>")
            myhtml.Append("<button type='button' class='button2xx' data-book-id='" & Temps & "' data-target='#formModalEdit' data-toggle='modal'>Edit</button>")
            myhtml.Append("</td>")

            myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("nama_prd") & "</td>")
            myhtml.Append("         <td>" & mydate1 & "</td>")
            myhtml.Append("         <td>" & mydate2 & "</td>")
            myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("flag_aktif") & "</td>")
            myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("flag_notif_selasses") & "</td>")
            myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("start_date_selasses") & "</td>")
            myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("end_date_selasses") & "</td>")



            myhtml.Append("<td class='text-right'>")
            myhtml.Append("<button type='button' class='button2xx' data-book-id='" & Temps & "' data-target='#formModalDelete' data-toggle='modal'>Delete</button>")
            myhtml.Append("</td>")

            myhtml.Append("      </tr>")
        Next
        myhtml.Append("   </tbody>")
        myhtml.Append(" </table> ")

        myPlaceH.Controls.Add(New Literal() With {
          .Text = myhtml.ToString()
         })
    End Sub




End Class
