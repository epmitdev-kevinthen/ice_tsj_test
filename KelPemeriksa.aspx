﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="KelPemeriksa.aspx.vb" Inherits="KelPemeriksa" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="script/jquery-1.4.1.min.js"></script>
    <script>
        $(document).ready(function () {
            var table = $("#ContentPlaceHolder1_GridView1").DataTable({
                paging: true,
                searching: true,
                lengthChange: false,
            });
            $('#ContentPlaceHolder1_GridView1 tbody').on('click', 'tr', function () {
                var data = table.row(this).data();

                $("#ContentPlaceHolder1_txtKodeKel").val(data[0]);
                $("#ContentPlaceHolder1_txtNamaKel").val(data[1]);
                $("#<%=ddlApprover1.ClientID%>").val(data[2]).change();
                $("#<%=ddlApprover2.ClientID%>").val(data[3]).change();
                $("#ContentPlaceHolder1_createdBy").text(data[4]);
                $("#ContentPlaceHolder1_createdDate").text(data[5]);
                $("#ContentPlaceHolder1_lastUpdateBy").text(data[6]);
                $("#ContentPlaceHolder1_lastUpdateDate").text(data[7]);
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <asp:Label ID="lblHead" runat="server" Text="Master Kelompok Pemeriksa ()"
        Style="font-size: 20px;"></asp:Label>
    <div>
        <asp:Label ID="userEdit" runat="server"></asp:Label>
    </div>
    <div style="display: flex;">
        <table>
            <tr>
                <td>No Kelompok</td>
                <td>:</td>
                <td>
                    <asp:TextBox ID="txtKodeKel" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Nama Kelompok</td>
                <td>:</td>
                <td>
                    <asp:TextBox ID="txtNamaKel" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Approver 1</td>
                <td>:</td>
                <td>
                    <asp:DropDownList ID="ddlApprover1" runat="server"></asp:DropDownList></td>
            </tr>
            <tr>
                <td>Approver 2</td>
                <td>:</td>
                <td>
                    <asp:DropDownList ID="ddlApprover2" runat="server"></asp:DropDownList></td>
            </tr>
        </table>
        <div style="float: right; margin-left: 15px;">
            Created By: <span id="createdBy" runat="server"></span>
            <br />
            Creation Date: <span id="createdDate" runat="server"></span>
            <br />
            Last Update By: <span id="lastUpdateBy" runat="server"></span>
            <br />
            Last Update Date:<span id="lastUpdateDate" runat="server"></span><br />
        </div>
    </div>
    <br />
    <br />
    <div id="div_lbl" runat="server">
        <asp:Label ID="lbl_msg" runat="server"></asp:Label>
    </div>
    <asp:Button ID="clearData" runat="server" Text="Clear Data" OnClick="clearData_Click" CssClass="button2"/>
    <asp:Button ID="insertData" runat="server" Text="Insert Data" OnClick="insertData_Click" CssClass="button2"/>
    <asp:Button ID="updateData" runat="server" Text="Update Data" OnClick="updateData_Click" CssClass="button2"/>
    <asp:Button ID="deleteData" runat="server" Text="Delete Data" OnClick="deleteData_Click" CssClass="button2"/>

    <div>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" AllowPaging="false" AllowSorting="false"
            PageSize="10" CssClass="dataTable">
            <Columns>
                <asp:BoundField DataField="kodekel" HeaderText="Kode Kelompok" />
                <asp:BoundField DataField="namakel" HeaderText="Nama Kelompok" />
                <asp:BoundField DataField="approver1" HeaderText="Approver 1" />
                <asp:BoundField DataField="approver2" HeaderText="Approver 2" />
                <asp:Boundfield DataField="createdby" HeaderText="RBM" >
                    <HeaderStyle CssClass="hidden"></HeaderStyle>

                        <ItemStyle CssClass="hidden"></ItemStyle>
                </asp:Boundfield>
                <asp:Boundfield DataField="createdat" HeaderText="RBM" >
                    <HeaderStyle CssClass="hidden"></HeaderStyle>

                        <ItemStyle CssClass="hidden"></ItemStyle>
                </asp:Boundfield>
                <asp:Boundfield DataField="lasteditby" HeaderText="RBM" >
                    <HeaderStyle CssClass="hidden"></HeaderStyle>

                        <ItemStyle CssClass="hidden"></ItemStyle>
                </asp:Boundfield>
                <asp:Boundfield DataField="lasteditat" HeaderText="RBM" >
                    <HeaderStyle CssClass="hidden"></HeaderStyle>

                        <ItemStyle CssClass="hidden"></ItemStyle>
                </asp:Boundfield>
            </Columns>
            <PagerStyle CssClass="paginate_button" />
        </asp:GridView>
    </div>
</asp:Content>


