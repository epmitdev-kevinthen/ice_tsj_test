﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class cabang
    Inherits System.Web.UI.Page

    Public Shared ConnStrAudit As String = ConfigurationManager.ConnectionStrings("SQLICE").ConnectionString
    Public Shared cmd As New SqlCommand
    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        If Not Page.IsPostBack Then
            cabangEdit.Text = Request.QueryString("kode")
            If cabangEdit.Text = "" Then
                loadRegion()
                loadRBM()
                loadClass()
                tampilTable()
            Else
                loadRegion()
                loadRBM()
                loadClass()
                tampilTable()
                tampil()
            End If

        End If
    End Sub

    Protected Sub loadClass()
        Using con As New SqlConnection(ConnStrAudit)
            con.Open()
            Dim da As New SqlDataAdapter("select class from MSTCLASS order by class", con)
            Dim dt As New DataTable
            da.Fill(dt)
            For i = 0 To dt.Rows.Count - 1
                ddlClass.Items.Add(New ListItem(dt.Rows(i).Item("class"), dt.Rows(i).Item("class")))
            Next

        End Using
    End Sub

    Protected Sub loadRegion()
        Using con As New SqlConnection(ConnStrAudit)
            con.Open()
            Dim da As New SqlDataAdapter("select region from MSTREGION order by region", con)
            Dim dt As New DataTable
            da.Fill(dt)
            For i = 0 To dt.Rows.Count - 1
                ddlRegion.Items.Add(New ListItem(dt.Rows(i).Item("region"), dt.Rows(i).Item("region")))
            Next

        End Using
    End Sub
    Protected Sub loadRBM()
        Using con As New SqlConnection(ConnStrAudit)
            con.Open()
            Dim da As New SqlDataAdapter("select * from MSTUSER order by fullusername", con)
            Dim dt As New DataTable
            da.Fill(dt)
            For i = 0 To dt.Rows.Count - 1
                ddlRBM.Items.Add(New ListItem(dt.Rows(i).Item("fullusername") & "-" & dt.Rows(i).Item("username"), dt.Rows(i).Item("username")))
                ddlABM.Items.Add(New ListItem(dt.Rows(i).Item("fullusername") & "-" & dt.Rows(i).Item("username"), dt.Rows(i).Item("username")))
            Next

        End Using
    End Sub
    Protected Sub tampilTable()
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                Dim da As New SqlDataAdapter("select * from mstcab order by kodecab", con)
                Dim dt As New DataTable
                da.Fill(dt)
                GridView1.DataSource = dt
                GridView1.DataBind()
                GridView1.HeaderRow.TableSection = TableRowSection.TableHeader
                con.Close()
            End Using
        Catch ex As Exception

        End Try

    End Sub
    Protected Sub tampil()
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                Dim da As New SqlDataAdapter("select * from mstcab where kodecab = '" & cabangEdit.Text & "'", con)
                Dim dt As New DataTable
                da.Fill(dt)
                If dt.Rows.Count > 0 Then
                    txtKodeCab.Text = dt.Rows(0).Item("kodecab")
                    txtNamaCab.Text = dt.Rows(0).Item("namacab")
                    ddlRegion.SelectedValue = dt.Rows(0).Item("region")
                    ddlRBM.SelectedValue = dt.Rows(0).Item("rbm")
                    ddlABM.SelectedValue = dt.Rows(0).Item("abm")
                    txtAlamat1.Text = dt.Rows(0).Item("alamat")
                    txtAlamat2.Text = dt.Rows(0).Item("alamat2")
                    createdBy.InnerText = dt.Rows(0).Item("created_by")
                    createdDate.InnerText = dt.Rows(0).Item("creation_date")
                    lastUpdateBy.InnerText = dt.Rows(0).Item("last_update_by")
                    lastUpdateDate.InnerText = dt.Rows(0).Item("last_update_date")
                End If
                con.Close()
            End Using
        Catch ex As Exception

        End Try

    End Sub
    Protected Sub GridView1_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        tampilTable()
    End Sub
    Protected Sub OnSorting(ByVal sender As Object, ByVal e As GridViewSortEventArgs) Handles GridView1.Sorting

    End Sub
    Protected Sub clearData_Click(sender As Object, e As EventArgs)
        Response.Redirect("cabang.aspx")
    End Sub
    Protected Sub insertData_Click(sender As Object, e As EventArgs)
        Dim rbmnm As String = ddlRBM.SelectedItem.Text.Substring(0, ddlRBM.SelectedItem.Text.IndexOf("-"))
        Dim abmnm As String = ddlABM.SelectedItem.Text.Substring(0, ddlABM.SelectedItem.Text.IndexOf("-"))
        Dim sqlstr As String = "insert into mstcab(kodecab, namacab, class, region, rbm, rbmnm, abm, abmnm, alamat, alamat2, created_by, creation_date, last_update_by, last_update_date) values ('" & txtKodeCab.Text & "', '" & txtNamaCab.Text & "', '" & ddlClass.SelectedValue & "', '" & ddlRegion.SelectedValue & "', '" & ddlRBM.SelectedValue & "', '" & rbmnm & "', '" & ddlABM.SelectedValue & "', '" & abmnm & "', '" & txtAlamat1.Text & "', '" & txtAlamat2.Text & "', '" & Session("sUsername") & "', current_timestamp, '" & Session("sUsername") & "', current_timestamp)"
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                cmd = New SqlCommand(sqlstr, con)
                cmd.ExecuteNonQuery()
                con.Close()
                div_lbl.Visible = True
                lbl_msg.Text = "Insert Successful!"
                tampilTable()
            End Using
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub updateData_Click(sender As Object, e As EventArgs)
        Dim rbmnm As String = ddlRBM.SelectedItem.Text.Substring(0, ddlRBM.SelectedItem.Text.IndexOf("-"))
        Dim abmnm As String = ddlABM.SelectedItem.Text.Substring(0, ddlABM.SelectedItem.Text.IndexOf("-"))
        Dim sqlstr As String = "UPDATE MSTCAB SET kodecab = '" & txtKodeCab.Text & "',namacab = '" & txtNamaCab.Text & "', class='" & ddlClass.SelectedValue & "',region = '" & ddlRegion.SelectedValue & "',rbm = '" & ddlRBM.SelectedValue & "' ,rbmnm = '" & rbmnm & "',abm = '" & ddlABM.SelectedValue & "',abmnm = '" & abmnm & "',alamat = '" & txtAlamat1.Text & "',alamat2 = '" & txtAlamat2.Text & "',last_update_by = '" & Session("sUsername") & "',last_update_date = current_timestamp WHERE kodecab = '" & txtKodeCab.Text & "'"
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                cmd = New SqlCommand(sqlstr, con)
                cmd.ExecuteNonQuery()
                con.Close()
                div_lbl.Visible = True
                lbl_msg.Text = "Update Successful!"
                tampilTable()
            End Using
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub deleteData_Click(sender As Object, e As EventArgs)
        Dim sqlstr As String = "delete from mstcab where kodecab = '" & txtKodeCab.Text & "'"
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                cmd = New SqlCommand(sqlstr, con)
                cmd.ExecuteNonQuery()
                con.Close()
            End Using
            div_lbl.Visible = True
            lbl_msg.Text = "Delete Successful!"
            tampilTable()
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub GridView1_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GridView1.RowCommand
        If e.CommandName = "cmdsubmit" Then
            Dim kode As String = e.CommandArgument
            Response.Redirect("cabang.aspx?kode=" & kode & "")
        ElseIf e.CommandName = "cmddel" Then


        End If
    End Sub
End Class
