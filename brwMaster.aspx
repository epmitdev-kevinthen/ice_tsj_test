﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="brwMaster.aspx.vb" Inherits="brwMaster" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Internal Control Effectiveness</title>

    <link rel="icon" href="image/Logoplikasi_Layer 2.png">

<script type="text/javascript">

    function fncSelect(p)
                { 
                    window.opener.document.getElementById("txtkode_activity1" + p).value = document.getElementById('txtCommand').value ;                       
                    window.opener.document.getElementById("txtactivity1" + p).value = document.getElementById('txtCommand1').value;
                    window.close(); 
                 }  


    function fncSelect1(p) {
        var ti = window.opener.document.getElementsByTagName("input");
        var i = 0;
        var tr = "";
        var br = "";
        var j = 0;
        var pn = "";
        for (i = 0; i < ti.length; i++) {
            tr = ti[i].id;
            br = tr;
            j = br.indexOf("txtkode_activity1" + p, 0);
            if (j != -1) {
                window.opener.document.getElementById(br).value = document.getElementById('txtCommand').value;
            }
            j = br.indexOf("txtactivity1" + p, 0);
            if (j != -1) {
                window.opener.document.getElementById(br).value = document.getElementById('txtCommand1').value;
            }
        }
        window.close();
    }


</script>


</head>
<body>
    <form id="form1" runat="server">
    <div>
       <asp:GridView ID="myGrid" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="kode,kelperiksa" DataSourceID="SQLData1">
           <Columns>
                <asp:CommandField ShowSelectButton="True" SelectText="Pilih" ButtonType="Button"  />
               <asp:BoundField DataField="kode" HeaderText="Kode" ReadOnly="True" SortExpression="kode" />
               <asp:BoundField DataField="kelperiksa" HeaderText="Keterangan" SortExpression="kelperiksa" />
           </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SQLData1" runat="server" ConnectionString="<%$ ConnectionStrings:SQLICE %>" SelectCommand="select kode,kelperiksa from MSTKELPERIKSA order by kode"></asp:SqlDataSource>

        <asp:TextBox ID="txtCommand" runat="server" Height="0px" Width="0px"></asp:TextBox>
        <asp:TextBox ID="txtCommand1" runat="server" Height="0px" Width="0px"></asp:TextBox>

    </div>
    </form>
</body>
</html>
