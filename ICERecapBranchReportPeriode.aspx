﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ICERecapBranchReportPeriode.aspx.vb" Inherits="ICERecapBranchReportPeriode" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <title>Internal Control Effectiveness</title>

    <link rel="icon" href="image/Logoplikasi_Layer 2.png">

     <script src= "https://cdn.zingchart.com/zingchart.min.js"></script>
		<script> zingchart.MODULESDIR = "https://cdn.zingchart.com/modules/";
		ZC.LICENSE = ["569d52cefae586f634c54f86dc99e6a9","ee6b7db5b51705a13dc2339db3edaf6d"];</script>


    <script>

        function fnExcelReport() {
            var tab_text = "<table border='2px'><tr bgcolor='#87AFC6'>";
            var textRange; var j = 0;
            tab = document.getElementById('TabReport'); // id of table

            for (j = 0 ; j < tab.rows.length ; j++) {
                tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
                //tab_text=tab_text+"</tr>";
            }

            tab_text = tab_text + "</table>";
            tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
            tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
            tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");

            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
            {
                txtArea1.document.open("txt/html", "replace");
                txtArea1.document.write(tab_text);
                txtArea1.document.close();
                txtArea1.focus();
                sa = txtArea1.document.execCommand("SaveAs", true, "Say Thanks to Sumit.xls");
            }
            else                 //other browser not tested on IE 11
                sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

            return (sa);
        }

    </script>



    <style>
table {
    border-collapse: collapse;
    border: 1px solid black;
    width: 100%;
}

th, td {
    text-align: left;
    border: 1px solid black;    
    padding: 8px;
}


th {
    background-color: white ;
    color: black;
}

#myChart {
  height:500px;
  width:400%;
  min-height:300px;
}


.zc-ref {
  display: none;
}

</style>

    <!-- style
        tr:nth-child(even){background-color: #f2f2f2}
-->


</head>
<body>
    <form id="form1" runat="server">

        <iframe id="txtArea1" style="display:none"></iframe>

        <table style="width: 400px">
            <tr>
               <td>Nama Laporan</td>
               <td>
               <asp:Label ID="Label1" runat="server">ICE-Recap By Branch (Internal Control Effectiveness Recapitulation By Branch)</asp:Label> 
               </td>
                <td rowspan="4">

                <asp:Label ID="dohist" runat="server"></asp:Label><br />
                <asp:Button ID="btnback" Text="Back" runat="server" />
                </td>

                <td rowspan="4">
                <button id="btnExport" onclick="fnExcelReport();">Export to Excel</button>
                    </td>
            </tr>
            <tr>
               <td>Cabang</td>
               <td>
               <asp:Label ID="_CabangKode" runat="server"></asp:Label> - 
               <asp:Label ID="_Cabang" runat="server"></asp:Label>
               </td>
            </tr>
            <tr>
               <td>CheckList</td>
               <td>
               <asp:Label ID="_tmpkode" runat="server"></asp:Label> - 
               <asp:Label ID="_tmp" runat="server"></asp:Label>
               </td>
            </tr>
            <tr>
               <td>Periode</td>
               <td>
                   <div style="display: none;">
               <asp:Label ID="_periodekd" runat="server" with="0px" Height="0px"></asp:Label> - 
                       </div>
               <asp:Label ID="_periode" runat="server"></asp:Label>
               </td>
            </tr>
        </table>
        <br />
    <div style="width: 1600px">
<asp:PlaceHolder ID="myPlaceH" runat="server"></asp:PlaceHolder>    
    </div>


<br /><br />
          
<!--
		<div id='myChart'><a class="zc-ref" href="https://www.zingchart.com/">Charts by ZingChart</a></div>

        <script>

            var myConfig = {
                "graphset": [
                    {
                        "type": "bar",
                        "background-color": "white",
                        "title": {
                            "text": "Internal Control Effectiveness Recapitulation by Branch",
                            "font-color": "#7E7E7E",
                            "backgroundColor": "none",
                            "font-size": "22px",
                            "alpha": 1,
                            "adjust-layout": true,
                        },
                        "plotarea": {
                            "margin": "dynamic"
                        },
                        "legend": {
                            "layout": "x3",
                            "overflow": "page",
                            "alpha": 0.05,
                            "shadow": false,
                            "align": "center",
                            "adjust-layout": true,
                            "marker": {
                                "type": "circle",
                                "border-color": "none",
                                "size": "10px"
                            },
                            "border-width": 0,
                            "maxItems": 3,
                            "toggle-action": "hide",
                            "pageOn": {
                                "backgroundColor": "#000",
                                "size": "10px",
                                "alpha": 0.65
                            },
                            "pageOff": {
                                "backgroundColor": "#7E7E7E",
                                "size": "10px",
                                "alpha": 0.65
                            },
                            "pageStatus": {
                                "color": "black"
                            }
                        },
                        "plot": {
                            "bars-space-left": 0.15,
                            "bars-space-right": 0.15,
                            "animation": {
                                "effect": "ANIMATION_SLIDE_BOTTOM",
                                "sequence": 0,
                                "speed": 800,
                                "delay": 800
                            }
                        },
                        "scale-y": {
                            "line-color": "#7E7E7E",
                            "item": {
                                "font-color": "#7e7e7e"
                            },
                            "values": <%= v7 %>,
                            "guide": {
                                "visible": true
                            },
                            "label": {
                                "text": "Control",
                                "font-family": "arial",
                                "bold": true,
                                "font-size": "14px",
                                "font-color": "#7E7E7E",
                            },
                        },
                        "scaleX": {
                            "values": [
                                <%= v4 %>
                            ],
                            "placement": "default",
                            "tick": {
                                "size": 58,
                                "placement": "cross"
                            },
                            "itemsOverlap": true,
                            "item": {
                                "offsetY": -55
                            }
                        },
                        "scaleX2": {
                            "values": [ <%= v1 %> ],
                            "placement": "default",
                            "tick": {
                                "size": 20,
                            },
                            "item": {
                                "offsetY": -15
                            }
                        },
                        "tooltip": {
                            "visible": false
                        },
                        "crosshair-x": {
                            "line-width": "100%",
                            "alpha": 0.18,
                            "plot-label": {
                                "header-text": "%kv Sales"
                            }
                        },
                        "series": [
                            {
                                "values": [
                                    <%= v2 %>
                                ],
                                "alpha": 0.95,
                                "borderRadiusTopLeft": 7,
                                "background-color": "#8993c7",
                                "text": "Element Control",
                            },
                            {
                                "values": [
                                    <%= v3 %>
                                ],
                                "borderRadiusTopLeft": 7,
                                "alpha": 0.95,
                                "background-color": "#fdb462",
                                "text": "Done Control"
                            }
                        ]
                    }
                ]
            };

            zingchart.render({
                id: 'myChart',
                data: myConfig,
                height: '100%',
                width: <%= v8 %>
            });
        </script>
-->


    </form>
</body>
</html>
