﻿
Imports System.Data
Imports System.Data.SqlClient

Partial Class ICERecapBranchReportPeriode
    Inherits System.Web.UI.Page
    Dim myChef As New Chef

    Public v1 As String
    Public v2 As String
    Public v3 As String
    Public v4 As String

    Public v5 As Long = 0
    Public v6 As Long = 0
    Public v7 As String

    Public v8 As String = "'100%'"

    Public arrayKodeHasil(0) As String
    Public arrayKetHasil(0) As String
    Public arrayN1Hasil(0) As Long
    Public arrayN2Hasil(0) As Long
    Public arrayWarnaHasil(0) As String

    Public Shared strConn As String = ConfigurationManager.ConnectionStrings("SQLICE").ConnectionString

    Dim dt_kriteria_penilaian As DataTable = GetDataSql("select * from MST_KRITERIA_PENILAIAN_DETAIL")
    Private Sub ICEDetailReport_Init(sender As Object, e As EventArgs) Handles Me.Init

        _CabangKode.Text = Session("cabangRpt")
        _Cabang.Text = Session("cabangR")
        _periodekd.Text = Session("periodeRpt")
        _periode.Text = Session("periodeR")
        _tmpkode.Text = Session("templateRpt")
        _tmp.Text = Session("templateR")

        If Session("unconfirm") Is Nothing Then
            dohist.Text = ""
        Else
            dohist.Text = Session("unconfirm")
        End If

        Call sub_refresh()
    End Sub

    Sub sub_refresh()
        Dim myData As Data.DataSet
        Dim myData1 As Data.DataSet
        Dim myData2 As Data.DataSet
        Dim myData3 As Data.DataSet
        Dim myhtml As New StringBuilder
        Dim i As Integer
        Dim j As Integer
        Dim x As Integer

        Dim vv1 As String = ""
        Dim vv2 As String = ""
        Dim vv3 As String = ""
        Dim vv4 As String = ""

        myData = myChef.dataHNILAI(_tmpkode.Text)
        If myData.Tables(0).Rows.Count > 0 Then
            ReDim arrayKodeHasil(myData.Tables(0).Rows.Count - 1)
            ReDim arrayKetHasil(myData.Tables(0).Rows.Count - 1)
            ReDim arrayN1Hasil(myData.Tables(0).Rows.Count - 1)
            ReDim arrayN2Hasil(myData.Tables(0).Rows.Count - 1)
            ReDim arrayWarnaHasil(myData.Tables(0).Rows.Count - 1)
            For i = 0 To myData.Tables(0).Rows.Count - 1
                arrayKodeHasil(i) = myData.Tables(0).Rows(i).Item("nilai_code")
                arrayKetHasil(i) = myData.Tables(0).Rows(i).Item("Keterangan")
                arrayN1Hasil(i) = myData.Tables(0).Rows(i).Item("nilai1")
                arrayN2Hasil(i) = myData.Tables(0).Rows(i).Item("nilai2")
                arrayWarnaHasil(i) = myData.Tables(0).Rows(i).Item("warna")
            Next
        End If

        myhtml.Append(" <table id='TabReport' cellspacing='0' cssclass='a' width='100%' style='border-color: black; border-width: 1px;'> ")
        myhtml.Append("   <thead>")
        myhtml.Append("      <tr><td colspan='16'>ICE-Recap By Branch (Internal Control Effectiveness Recapitulation By Branch)</br>" & _tmp.Text & "<br/>" & _Cabang.Text & "<br/>" & _periode.Text)
        myhtml.Append("      </td></tr>")
        myhtml.Append("      <tr>")
        myhtml.Append("         <th rowspan='3' style='text-align:center'><B>No</B></th>")
        myhtml.Append("         <th rowspan='3' style='text-align:center'><B>Periode</B></th>")
        myhtml.Append("         <th rowspan='3' style='text-align:center'><B>Branches</B></th>")
        myhtml.Append("         <th colspan='11' style='text-align:center'><B>Internal Control Effectiveness</B></th>")
        myhtml.Append("         <th rowspan='3' style='text-align:center'><B>Category</B></th>")
        myhtml.Append("         <th rowspan='3' style='text-align:center'><B>Status</B></th>")
        myhtml.Append("      </tr>")
        myhtml.Append("      <tr>")
        myhtml.Append("         <th colspan='4' style='text-align:center'><B>1st Defense</B></th>")
        myhtml.Append("         <th colspan='4' style='text-align:center'><B>2nd Defense</B></th>")
        myhtml.Append("         <th colspan='3' style='text-align:center'><B>Total</B></th>")
        myhtml.Append("      </tr>")
        myhtml.Append("      <tr>")
        myhtml.Append("         <th style='text-align:center'><B>Poin Element<br />Control</B></th>")
        myhtml.Append("         <th style='text-align:center'><B>Poin Done<br />Control</B></th>")
        myhtml.Append("         <th style='text-align:center'><B>Score</B></th>")
        myhtml.Append("         <th style='text-align:center'><B>Category</B></th>")
        myhtml.Append("         <th style='text-align:center'><B>Poin Element<br />Control</B></th>")
        myhtml.Append("         <th style='text-align:center'><B>Poin Done<br />Control</B></th>")
        myhtml.Append("         <th style='text-align:center'><B>Score</B></th>")
        myhtml.Append("         <th style='text-align:center'><B>Category</B></th>")
        myhtml.Append("         <th style='text-align:center'><B>Poin Element<br />Control</B></th>")
        myhtml.Append("         <th style='text-align:center'><B>Poin Done<br />Control</B></th>")
        myhtml.Append("         <th style='text-align:center'><B>Score</B></th>")
        myhtml.Append("      </tr>")
        myhtml.Append("   </thead>")
        myhtml.Append("   <tbody>")


        '--dari sini
        Dim aaa
        Dim bbb
        Dim xxx As Integer = 0
        aaa = Split(_periodekd.Text, ",")
        bbb = Split(_periode.Text, ",")
        For iii = 0 To aaa.Length - 1
            If Trim(aaa(iii)) <> "" Then


                'myData3 = myChef.SQLLISTCABENTRY(_tmpkode.Text, _periodekd.Text, _CabangKode.Text)
                myData3 = myChef.SQLLISTCABENTRY(_tmpkode.Text, aaa(iii), _CabangKode.Text)
                If myData3.Tables(0).Rows.Count <> 0 Then

                    If myData3.Tables(0).Rows.Count > 40 Then
                        v8 = "'100%'"
                    ElseIf myData3.Tables(0).Rows.Count > 35 Then
                        v8 = "'87.5%'"
                    ElseIf myData3.Tables(0).Rows.Count > 30 Then
                        v8 = "'75%'"
                    ElseIf myData3.Tables(0).Rows.Count > 25 Then
                        v8 = "'62.5%'"
                    ElseIf myData3.Tables(0).Rows.Count > 20 Then
                        v8 = "'50%'"
                    ElseIf myData3.Tables(0).Rows.Count > 15 Then
                        v8 = "'37.5%'"
                    ElseIf myData3.Tables(0).Rows.Count > 10 Then
                        v8 = "'25%'"
                    Else
                        v8 = "'12.5%'"
                    End If

                    For x = 0 To myData3.Tables(0).Rows.Count - 1

                        myhtml.Append("      <tr>")
                        xxx = xxx + 1
                        myhtml.Append("         <td style='text-align:right'>" & (xxx) & "</td>")
                        myhtml.Append("         <td style='text-align:left'>" & bbb(iii) & "</td>")
                        myhtml.Append("         <td style='text-align:left'>" & myData3.Tables(0).Rows(x).Item("nmcab") & "</td>")

                        vv1 = vv1 & "'" & myData3.Tables(0).Rows(x).Item("nmcab") & "' , "

                        Dim tvtc1 As Double = 0
                        Dim tvdc1 As Double = 0
                        Dim tvpc1 As Double = 0
                        Dim tvtc2 As Double = 0
                        Dim tvdc2 As Double = 0
                        Dim tvpc2 As Double = 0
                        Dim tvtc3 As Double = 0
                        Dim tvdc3 As Double = 0
                        Dim tvpc3 As Double = 0

                        Dim tjumlahsub As Double = 0


                        myData = myChef.SQLICE1(_tmpkode.Text)
                        For i = 0 To myData.Tables(0).Rows.Count - 1

                            Dim vtc1 As Double = 0
                            Dim vdc1 As Double = 0
                            Dim vpc1 As Double = 0
                            Dim vtc2 As Double = 0
                            Dim vdc2 As Double = 0
                            Dim vpc2 As Double = 0
                            Dim vtc3 As Double = 0
                            Dim vdc3 As Double = 0
                            Dim vpc3 As Double = 0

                            Dim vbobot1 As Double = 0
                            Dim vbobot2 As Double = 0

                            'myhtml.Append("      <tr>")
                            'myhtml.Append("         <td style='text-align:right'>" & (i + 1) & "</td>")
                            'myhtml.Append("         <td style='text-align:left'>" & myData.Tables(0).Rows(i).Item("activity1") & "</td>")

                            myData1 = myChef.SQLICE2(_tmpkode.Text, myData.Tables(0).Rows(i).Item("no"))
                            For j = 0 To myData1.Tables(0).Rows.Count - 1
                                'If j = 1 Then
                                'myhtml.Append("      <tr>")
                                'End If
                                'myhtml.Append("         <td style='text-align:center'>" & Chr(97 + j) & "</td>")
                                'myhtml.Append("         <td style='text-align:left'>" & myData1.Tables(0).Rows(j).Item("activity1") & "</td>")
                                'If j = 0 Then
                                'myhtml.Append("         <td rowspan='" & myData.Tables(0).Rows(i).Item("rowspan") & "' style='text-align:right'>" & myData.Tables(0).Rows(i).Item("bobot") & "</td>")
                                'End If
                                'myhtml.Append("         <td style='text-align:right'>" & myData1.Tables(0).Rows(j).Item("bobot") & "</td>")

                                If dohist.Text <> "" Then
                                    'myData2 = myChef.SQLICE3HIST(_tmpkode.Text, _periodekd.Text, myData1.Tables(0).Rows(j).Item("no"), myData3.Tables(0).Rows(x).Item("cab"), dohist.Text)
                                    myData2 = myChef.SQLICE3HIST(_tmpkode.Text, aaa(iii), myData1.Tables(0).Rows(j).Item("no"), myData3.Tables(0).Rows(x).Item("cab"), dohist.Text)
                                Else
                                    'myData2 = myChef.SQLICE3(_tmpkode.Text, _periodekd.Text, myData1.Tables(0).Rows(j).Item("no"), myData3.Tables(0).Rows(x).Item("cab"))
                                    myData2 = myChef.SQLICE3(_tmpkode.Text, aaa(iii), myData1.Tables(0).Rows(j).Item("no"), myData3.Tables(0).Rows(x).Item("cab"))
                                End If

                                Dim tc1 As Double
                                Dim dc1 As Double
                                Dim pc1 As Double
                                Dim tc2 As Double
                                Dim dc2 As Double
                                Dim pc2 As Double
                                Dim tc3 As Double
                                Dim dc3 As Double
                                Dim pc3 As Double
                                If IsDBNull(myData2.Tables(0).Rows(0).Item("tc1")) Then
                                    tc1 = 0
                                Else
                                    tc1 = myData2.Tables(0).Rows(0).Item("tc1")
                                End If
                                If IsDBNull(myData2.Tables(0).Rows(0).Item("dc1")) Then
                                    dc1 = 0
                                Else
                                    dc1 = myData2.Tables(0).Rows(0).Item("dc1")
                                End If
                                If IsDBNull(myData2.Tables(0).Rows(0).Item("tc2")) Then
                                    tc2 = 0
                                Else
                                    tc2 = myData2.Tables(0).Rows(0).Item("tc2")
                                End If
                                If IsDBNull(myData2.Tables(0).Rows(0).Item("dc2")) Then
                                    dc2 = 0
                                Else
                                    dc2 = myData2.Tables(0).Rows(0).Item("dc2")
                                End If
                                If tc1 = 0 Then
                                    pc1 = 0
                                Else
                                    pc1 = dc1 / tc1 * myData1.Tables(0).Rows(j).Item("bobot")
                                    vbobot1 = vbobot1 + myData1.Tables(0).Rows(j).Item("bobot")
                                End If
                                If tc2 = 0 Then
                                    pc2 = 0
                                Else
                                    pc2 = dc2 / tc2 * myData1.Tables(0).Rows(j).Item("bobot")
                                    vbobot2 = vbobot2 + myData1.Tables(0).Rows(j).Item("bobot")
                                End If
                                tc3 = tc2 + tc1
                                dc3 = dc2 + dc1
                                pc3 = (pc2 + pc1) / 2

                                'tc1 = Math.Round(tc1, 1)
                                'dc1 = Math.Round(dc1, 1)
                                'pc1 = Math.Round(pc1, 1)
                                'tc2 = Math.Round(tc2, 1)
                                'dc2 = Math.Round(dc2, 1)
                                'pc2 = Math.Round(pc2, 1)
                                'tc3 = Math.Round(tc3, 1)
                                'dc3 = Math.Round(dc3, 1)
                                'pc3 = Math.Round(pc3, 1)


                                vtc1 = vtc1 + tc1
                                vdc1 = vdc1 + dc1
                                vpc1 = vpc1 + pc1

                                vtc2 = vtc2 + tc2
                                vdc2 = vdc2 + dc2
                                vpc2 = vpc2 + pc2

                                vtc3 = vtc3 + tc3
                                vdc3 = vdc3 + dc3
                                vpc3 = vpc3 + pc3

                                'myhtml.Append("         <td style='text-align:right'>" & Math.Round(tc1, 1) & "</td>")
                                'myhtml.Append("         <td style='text-align:right'>" & Math.Round(dc1, 1) & "</td>")
                                'myhtml.Append("         <td style='text-align:right'>" & Math.Round(pc1, 1) & "</td>")
                                'myhtml.Append("         <td style='text-align:right'>" & Math.Round(tc2, 1) & "</td>")
                                'myhtml.Append("         <td style='text-align:right'>" & Math.Round(dc2, 1) & "</td>")
                                'myhtml.Append("         <td style='text-align:right'>" & Math.Round(pc2, 1) & "</td>")
                                'myhtml.Append("         <td style='text-align:right'>" & Math.Round(tc3, 1) & "</td>")
                                'myhtml.Append("         <td style='text-align:right'>" & Math.Round(dc3, 1) & "</td>")
                                'myhtml.Append("         <td style='text-align:right'>" & Math.Round(pc3, 1) & "</td>")
                                'myhtml.Append("         <td style='text-align:right'></td>")

                                'If j < myData1.Tables(0).Rows.Count - 1 Then
                                'myhtml.Append("      </tr>")
                                'End If
                            Next

                            'tjumlahsub = tjumlahsub + 1
                            If vtc3 <> 0 Then
                                tjumlahsub = tjumlahsub + 1
                            End If


                            'myhtml.Append("      </tr>")
                            'myhtml.Append("      <tr>")
                            'myhtml.Append("         <td colspan='4' style='text-align:center'>Sub Total</td>")
                            'myhtml.Append("         <td style='text-align:center'></td>")
                            'myhtml.Append("         <td style='text-align:center'></td>")

                            'If vtc1 = 0 Then
                            '    vpc1 = 0
                            'Else
                            '    vpc1 = vdc1 / vtc1 * myData.Tables(0).Rows(i).Item("bobot")
                            'End If
                            'If vtc2 = 0 Then
                            '    vpc2 = 0
                            'Else
                            '    vpc2 = vdc2 / vtc2 * myData.Tables(0).Rows(i).Item("bobot")
                            'End If

                            If vbobot1 = 0 Then
                                vpc1 = 0
                            ElseIf vbobot1 <> 100 Then
                                vpc1 = vpc1 * 100 / vbobot1
                                '                               vpc1 = Math.Round(vpc1 * 100 / vbobot1, 0)
                            End If
                            If vbobot2 = 0 Then
                                vpc2 = 0
                            ElseIf vbobot2 <> 100 Then
                                vpc2 = vpc2 * 100 / vbobot2
                                '                                vpc2 = Math.Round(vpc2 * 100 / vbobot2, 0)
                            End If

                            'vtc1 = Math.Round(vtc1, 0)
                            'vdc1 = Math.Round(vdc1, 0)
                            'vpc1 = Math.Round(vpc1, 0)
                            'vtc2 = Math.Round(vtc2, 0)
                            'vdc2 = Math.Round(vdc2, 0)
                            'vpc2 = Math.Round(vpc2, 0)
                            'vtc3 = Math.Round(vtc3, 0)
                            'vdc3 = Math.Round(vdc3, 0)
                            'vpc3 = Math.Round((vpc2 + vpc1) / 2, 0)

                            vpc3 = (vpc2 + vpc1) / 2


                            'tvtc1 = tvtc1 + Math.Round(vtc1, 0)
                            'tvdc1 = tvdc1 + Math.Round(vdc1, 0)
                            'tvpc1 = tvpc1 + Math.Round(vpc1, 0)
                            'tvtc2 = tvtc2 + Math.Round(vtc2, 0)
                            'tvdc2 = tvdc2 + Math.Round(vdc2, 0)
                            'tvpc2 = tvpc2 + Math.Round(vpc2, 0)
                            'tvtc3 = tvtc3 + Math.Round(vtc3, 0)
                            'tvdc3 = tvdc3 + Math.Round(vdc3, 0)

                            'tvpc3 = tvpc3 + Math.Round(vpc3, 0)


                            tvtc1 = tvtc1 + vtc1
                            tvdc1 = tvdc1 + vdc1
                            tvpc1 = tvpc1 + vpc1
                            tvtc2 = tvtc2 + vtc2
                            tvdc2 = tvdc2 + vdc2
                            tvpc2 = tvpc2 + vpc2
                            tvtc3 = tvtc3 + vtc3
                            tvdc3 = tvdc3 + vdc3

                            tvpc3 = tvpc3 + vpc3



                            'myhtml.Append("         <td style='text-align:right'>" & Math.Round(vtc1, 0) & "</td>")
                            'myhtml.Append("         <td style='text-align:right'>" & Math.Round(vdc1, 0) & "</td>")
                            'myhtml.Append("         <td style='text-align:right'>" & Math.Round(vpc1, 0) & "</td>")

                            'If vpc1 < 61 Then
                            '    myhtml.Append("         <td style='text-align:center; background-color: red'>Kurang</td>")
                            'ElseIf vpc1 < 76 Then
                            '    myhtml.Append("         <td style='text-align:center; background-color: yellow'>Cukup</td>")
                            'ElseIf vpc1 < 86 Then
                            '    myhtml.Append("         <td style='text-align:center; background-color: lightgreen'>Baik</td>")
                            'Else
                            '    myhtml.Append("         <td style='text-align:center; background-color: lightblue'>Sangat Baik</td>")
                            'End If


                            'myhtml.Append("         <td style='text-align:right'>" & Math.Round(vtc2, 0) & "</td>")
                            'myhtml.Append("         <td style='text-align:right'>" & Math.Round(vdc2, 0) & "</td>")
                            'myhtml.Append("         <td style='text-align:right'>" & Math.Round(vpc2, 0) & "</td>")

                            'If vpc2 < 61 Then
                            '    myhtml.Append("         <td style='text-align:center; background-color: red'>Kurang</td>")
                            'ElseIf vpc2 < 76 Then
                            '    myhtml.Append("         <td style='text-align:center; background-color: yellow'>Cukup</td>")
                            'ElseIf vpc2 < 86 Then
                            '    myhtml.Append("         <td style='text-align:center; background-color: lightgreen'>Baik</td>")
                            'Else
                            '    myhtml.Append("         <td style='text-align:center; background-color: lightblue'>Sangat Baik</td>")
                            'End If

                            'myhtml.Append("         <td style='text-align:right'>" & Math.Round(vtc3, 0) & "</td>")
                            'myhtml.Append("         <td style='text-align:right'>" & Math.Round(vdc3, 0) & "</td>")
                            'myhtml.Append("         <td style='text-align:right'>" & Math.Round(vpc3, 0) & "</td>")

                            'If vpc3 < 61 Then
                            '    myhtml.Append("         <td style='text-align:center; background-color: red'>Kurang</td>")
                            'ElseIf vpc3 < 76 Then
                            '    myhtml.Append("         <td style='text-align:center; background-color: yellow'>Cukup</td>")
                            'ElseIf vpc3 < 86 Then
                            '    myhtml.Append("         <td style='text-align:center; background-color: lightgreen'>Baik</td>")
                            'Else
                            '    myhtml.Append("         <td style='text-align:center; background-color: lightblue'>Sangat Baik</td>")
                            'End If

                            'If vpc3 < 61 Then
                            '    myhtml.Append("         <td style='text-align:center'>Kurang</td>")
                            'ElseIf vpc3 < 76 Then
                            '    myhtml.Append("         <td style='text-align:center'>Cukup</td>")
                            'ElseIf vpc3 < 86 Then
                            '    myhtml.Append("         <td style='text-align:center'>Baik</td>")
                            'Else
                            '    myhtml.Append("         <td style='text-align:center'>Sangat Baik</td>")
                            'End If

                            'myhtml.Append("      </tr>")
                        Next


                        'myhtml.Append("      </tr>")
                        'myhtml.Append("      <tr>")
                        'myhtml.Append("         <td colspan='2' style='text-align:center'><B>T o t a l</B></td>")
                        'myhtml.Append("         <td style='text-align:center'></td>")
                        'myhtml.Append("         <td style='text-align:center'></td>")



                        'tvtc1 = Math.Round(tvtc1, 0)
                        'tvdc1 = Math.Round(tvdc1, 0)
                        'tvpc1 = Math.Round(tvpc1 / tjumlahsub, 0)
                        'tvtc2 = Math.Round(tvtc2, 0)
                        'tvdc2 = Math.Round(tvdc2, 0)
                        'tvpc2 = Math.Round(tvpc2 / tjumlahsub, 0)
                        'tvtc3 = Math.Round(tvtc3, 0)
                        'tvdc3 = Math.Round(tvdc3, 0)
                        'tvpc3 = Math.Round((tvpc2 + tvpc1) / 2, 0)


                        tvpc1 = tvpc1 / tjumlahsub
                        tvpc2 = tvpc2 / tjumlahsub
                        tvpc3 = (tvpc2 + tvpc1) / 2




                        myhtml.Append("         <td style='text-align:right'><B>" & Math.Round(tvtc1, 0) & "</B></td>")
                        myhtml.Append("         <td style='text-align:right'><B>" & Math.Round(tvdc1, 0) & "</B></td>")
                        myhtml.Append("         <td style='text-align:right'><B>" & Math.Round(tvpc1, 0) & "</B></td>")

                        For ii = 0 To arrayKodeHasil.Length - 1
                            If tvpc1 <= 0 And arrayN1Hasil(ii) = 0 Then
                                myhtml.Append("         <td style='text-align:center; background-color: " & arrayWarnaHasil(ii) & "'><B>" & arrayKetHasil(ii) & "</B></td>")
                            ElseIf (arrayN1Hasil(ii) - 1) < Math.Round(tvpc1, 0) And arrayN2Hasil(ii) >= Math.Round(tvpc1, 0) Then
                                myhtml.Append("         <td style='text-align:center; background-color: " & arrayWarnaHasil(ii) & "'><B>" & arrayKetHasil(ii) & "</B></td>")
                                'Else
                                '    myhtml.Append("         <td style='text-align:center; background-color: white'><B>&nbsp;</B></td>")
                            End If
                        Next

                        'If tvpc1 < 61 Then
                        '    myhtml.Append("         <td style='text-align:center; background-color: red'><B>Kurang</B></td>")
                        'ElseIf tvpc1 < 76 Then
                        '    myhtml.Append("         <td style='text-align:center; background-color: yellow'<B>Cukup</B></td>")
                        'ElseIf tvpc1 < 86 Then
                        '    myhtml.Append("         <td style='text-align:center; background-color: lightgreen'><B>Baik</B></td>")
                        'Else
                        '    myhtml.Append("         <td style='text-align:center; background-color: lightblue'><B>Sangat Baik</B></td>")
                        'End If

                        myhtml.Append("         <td style='text-align:right'><B>" & Math.Round(tvtc2, 0) & "</B></td>")
                        myhtml.Append("         <td style='text-align:right'><B>" & Math.Round(tvdc2, 0) & "</B></td>")
                        myhtml.Append("         <td style='text-align:right'><B>" & Math.Round(tvpc2, 0) & "</B></td>")


                        For ii = 0 To arrayKodeHasil.Length - 1
                            If tvpc2 <= 0 And arrayN1Hasil(ii) = 0 Then
                                myhtml.Append("         <td style='text-align:center; background-color: " & arrayWarnaHasil(ii) & "'><B>" & arrayKetHasil(ii) & "</B></td>")
                            ElseIf (arrayN1Hasil(ii) - 1) < Math.Round(tvpc2, 0) And arrayN2Hasil(ii) >= Math.Round(tvpc2, 0) Then
                                myhtml.Append("         <td style='text-align:center; background-color: " & arrayWarnaHasil(ii) & "'><B>" & arrayKetHasil(ii) & "</B></td>")
                                'Else
                                '    myhtml.Append("         <td style='text-align:center; background-color: white'><B>&nbsp;</B></td>")
                            End If
                        Next

                        'If tvpc2 < 61 Then
                        '    myhtml.Append("         <td style='text-align:center; background-color: red'><B>Kurang</B></td>")
                        'ElseIf tvpc2 < 76 Then
                        '    myhtml.Append("         <td style='text-align:center; background-color: yellow'><B>Cukup</B></td>")
                        'ElseIf tvpc2 < 86 Then
                        '    myhtml.Append("         <td style='text-align:center; background-color: lightgreen'><B>Baik</B></td>")
                        'Else
                        '    myhtml.Append("         <td style='text-align:center; background-color: lightblue'><B>Sangat Baik</B></td>")
                        'End If


                        myhtml.Append("         <td style='text-align:right'><B>" & Math.Round(tvtc3, 0) & "</B></td>")
                        myhtml.Append("         <td style='text-align:right'><B>" & Math.Round(tvdc3, 0) & "</B></td>")
                        myhtml.Append("         <td style='text-align:right'><B>" & Math.Round(tvpc3, 0) & "</B></td>")

                        For ii = 0 To arrayKodeHasil.Length - 1
                            If tvpc3 <= 0 And arrayN1Hasil(ii) = 0 Then
                                myhtml.Append("         <td style='text-align:center; background-color: " & arrayWarnaHasil(ii) & "'><B>" & arrayKetHasil(ii) & "</B></td>")
                            ElseIf (arrayN1Hasil(ii) - 1) < Math.Round(tvpc3, 0) And arrayN2Hasil(ii) >= Math.Round(tvpc3, 0) Then
                                myhtml.Append("         <td style='text-align:center; background-color: " & arrayWarnaHasil(ii) & "'><B>" & arrayKetHasil(ii) & "</B></td>")
                                'Else
                                '    myhtml.Append("         <td style='text-align:center; background-color: white'><B>&nbsp;</B></td>")
                            End If
                        Next


                        'If tvpc3 < 61 Then
                        '    myhtml.Append("         <td style='text-align:center; background-color: red'><B>Kurang</B></td>")
                        'ElseIf tvpc3 < 76 Then
                        '    myhtml.Append("         <td style='text-align:center; background-color: yellow'><B>Cukup</B></td>")
                        'ElseIf tvpc3 < 86 Then
                        '    myhtml.Append("         <td style='text-align:center; background-color: lightgreen'><B>Baik</B></td>")
                        'Else
                        '    myhtml.Append("         <td style='text-align:center; background-color: lightblue'><B>Sangat Baik</B></td>")
                        'End If

                        vv4 = vv4 & "'1st Def' , '2nd Def' , "
                        vv2 = vv2 & tvtc1 & " , " & tvtc2 & " , "
                        vv3 = vv3 & tvdc1 & " , " & tvdc2 & " , "

                        If v5 < tvtc1 Then
                            v5 = tvtc1
                        End If
                        If v5 < tvtc2 Then
                            v5 = tvtc2
                        End If
                        If v5 < tvdc1 Then
                            v5 = tvdc1
                        End If
                        If v5 < tvdc2 Then
                            v5 = tvdc2
                        End If

                        If myData3.Tables(0).Rows(x).Item("jml_conf") = 0 Then
                            myhtml.Append("         <td style='text-align:left;background-color: red'>Belum Konfirm</td>")
                        Else
                            myhtml.Append("         <td style='text-align:left'>Sudah Konfirm</td>")
                        End If

                        myhtml.Append("      </tr>")


                    Next

                    'v6 = Math.Round(v5 / 10, 0)
                    v6 = v5 / 10

                    'vv1 = vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1
                    'vv2 = vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2
                    'vv3 = vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3
                    'vv4 = vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4

                    v1 = Left(vv1, vv1.Length - 2)
                    v2 = Left(vv2, vv2.Length - 2)
                    v3 = Left(vv3, vv3.Length - 2)
                    v4 = Left(vv4, vv4.Length - 2)

                    v7 = "'0:" & v5.ToString().Trim() & ":" & v6.ToString().Trim() & "'"


                    '--sampai sini
                End If
            End If
        Next


        myhtml.Append("   </tbody>")
        myhtml.Append(" </table> ")


        'myhtml.Append("      <tr>")
        'myhtml.Append("      <td colspan='13'>")
        'myhtml.Append("      <div id='chartDiv'></div>")
        'myhtml.Append("      <td></tr>")

        myPlaceH.Controls.Add(New Literal() With {
          .Text = myhtml.ToString()
         })


    End Sub



    Sub sub_refresh_bak()
        Dim myData As Data.DataSet
        Dim myData1 As Data.DataSet
        Dim myData2 As Data.DataSet
        Dim myData3 As Data.DataSet
        Dim myhtml As New StringBuilder
        Dim i As Integer
        Dim j As Integer
        Dim x As Integer

        Dim vv1 As String = ""
        Dim vv2 As String = ""
        Dim vv3 As String = ""
        Dim vv4 As String = ""



        myhtml.Append(" <table id='TabReport' cellspacing='0' cssclass='a' width='100%' style='border-color: black; border-width: 1px;'> ")
        myhtml.Append("   <thead>")
        myhtml.Append("      <tr><td colspan='16'>ICE-Recap By Branch (Internal Control Effectiveness Recapitulation By Branch)</br>" & _tmp.Text & "<br/>" & _Cabang.Text & "<br/>" & _periode.Text)
        myhtml.Append("      </td></tr>")
        myhtml.Append("      <tr>")
        myhtml.Append("         <th rowspan='3' style='text-align:center'><B>No</B></th>")
        myhtml.Append("         <th rowspan='3' style='text-align:center'><B>Periode</B></th>")
        myhtml.Append("         <th rowspan='3' style='text-align:center'><B>Branches</B></th>")
        myhtml.Append("         <th colspan='11' style='text-align:center'><B>Internal Control Effectiveness</B></th>")
        myhtml.Append("         <th rowspan='3' style='text-align:center'><B>Category</B></th>")
        myhtml.Append("         <th rowspan='3' style='text-align:center'><B>Status</B></th>")
        myhtml.Append("      </tr>")
        myhtml.Append("      <tr>")
        myhtml.Append("         <th colspan='4' style='text-align:center'><B>1st Defense</B></th>")
        myhtml.Append("         <th colspan='4' style='text-align:center'><B>2nd Defense</B></th>")
        myhtml.Append("         <th colspan='3' style='text-align:center'><B>Total</B></th>")
        myhtml.Append("      </tr>")
        myhtml.Append("      <tr>")
        myhtml.Append("         <th style='text-align:center'><B>Element<br />Control</B></th>")
        myhtml.Append("         <th style='text-align:center'><B>Done<br />Control</B></th>")
        myhtml.Append("         <th style='text-align:center'><B>Poin of<br />Control</B></th>")
        myhtml.Append("         <th style='text-align:center'><B>Category</B></th>")
        myhtml.Append("         <th style='text-align:center'><B>Element<br />Control</B></th>")
        myhtml.Append("         <th style='text-align:center'><B>Done<br />Control</B></th>")
        myhtml.Append("         <th style='text-align:center'><B>Poin of<br />Control</B></th>")
        myhtml.Append("         <th style='text-align:center'><B>Category</B></th>")
        myhtml.Append("         <th style='text-align:center'><B>Element<br />Control</B></th>")
        myhtml.Append("         <th style='text-align:center'><B>Done<br />Control</B></th>")
        myhtml.Append("         <th style='text-align:center'><B>Poin of<br />Control</B></th>")
        myhtml.Append("      </tr>")
        myhtml.Append("   </thead>")
        myhtml.Append("   <tbody>")


        '--dari sini
        Dim aaa
        Dim bbb
        Dim xxx As Integer = 0
        aaa = Split(_periodekd.Text, ",")
        bbb = Split(_periode.Text, ",")
        For iii = 0 To aaa.Length - 1
            If Trim(aaa(iii)) <> "" Then


                'myData3 = myChef.SQLLISTCABENTRY(_tmpkode.Text, _periodekd.Text, _CabangKode.Text)
                myData3 = myChef.SQLLISTCABENTRY(_tmpkode.Text, aaa(iii), _CabangKode.Text)
                If myData3.Tables(0).Rows.Count <> 0 Then

                    If myData3.Tables(0).Rows.Count > 40 Then
                        v8 = "'100%'"
                    ElseIf myData3.Tables(0).Rows.Count > 35 Then
                        v8 = "'87.5%'"
                    ElseIf myData3.Tables(0).Rows.Count > 30 Then
                        v8 = "'75%'"
                    ElseIf myData3.Tables(0).Rows.Count > 25 Then
                        v8 = "'62.5%'"
                    ElseIf myData3.Tables(0).Rows.Count > 20 Then
                        v8 = "'50%'"
                    ElseIf myData3.Tables(0).Rows.Count > 15 Then
                        v8 = "'37.5%'"
                    ElseIf myData3.Tables(0).Rows.Count > 10 Then
                        v8 = "'25%'"
                    Else
                        v8 = "'12.5%'"
                    End If

                    For x = 0 To myData3.Tables(0).Rows.Count - 1

                        myhtml.Append("      <tr>")
                        xxx = xxx + 1
                        myhtml.Append("         <td style='text-align:right'>" & (xxx) & "</td>")
                        myhtml.Append("         <td style='text-align:left'>" & bbb(iii) & "</td>")
                        myhtml.Append("         <td style='text-align:left'>" & myData3.Tables(0).Rows(x).Item("nmcab") & "</td>")

                        vv1 = vv1 & "'" & myData3.Tables(0).Rows(x).Item("nmcab") & "' , "

                        Dim tvtc1 As Double = 0
                        Dim tvdc1 As Double = 0
                        Dim tvpc1 As Double = 0
                        Dim tvtc2 As Double = 0
                        Dim tvdc2 As Double = 0
                        Dim tvpc2 As Double = 0
                        Dim tvtc3 As Double = 0
                        Dim tvdc3 As Double = 0
                        Dim tvpc3 As Double = 0

                        Dim tjumlahsub As Double = 0


                        myData = myChef.SQLICE1(_tmpkode.Text)
                        For i = 0 To myData.Tables(0).Rows.Count - 1

                            Dim vtc1 As Double = 0
                            Dim vdc1 As Double = 0
                            Dim vpc1 As Double = 0
                            Dim vtc2 As Double = 0
                            Dim vdc2 As Double = 0
                            Dim vpc2 As Double = 0
                            Dim vtc3 As Double = 0
                            Dim vdc3 As Double = 0
                            Dim vpc3 As Double = 0

                            Dim vbobot1 As Double = 0
                            Dim vbobot2 As Double = 0

                            'myhtml.Append("      <tr>")
                            'myhtml.Append("         <td style='text-align:right'>" & (i + 1) & "</td>")
                            'myhtml.Append("         <td style='text-align:left'>" & myData.Tables(0).Rows(i).Item("activity1") & "</td>")

                            myData1 = myChef.SQLICE2(_tmpkode.Text, myData.Tables(0).Rows(i).Item("no"))
                            For j = 0 To myData1.Tables(0).Rows.Count - 1
                                'If j = 1 Then
                                'myhtml.Append("      <tr>")
                                'End If
                                'myhtml.Append("         <td style='text-align:center'>" & Chr(97 + j) & "</td>")
                                'myhtml.Append("         <td style='text-align:left'>" & myData1.Tables(0).Rows(j).Item("activity1") & "</td>")
                                'If j = 0 Then
                                'myhtml.Append("         <td rowspan='" & myData.Tables(0).Rows(i).Item("rowspan") & "' style='text-align:right'>" & myData.Tables(0).Rows(i).Item("bobot") & "</td>")
                                'End If
                                'myhtml.Append("         <td style='text-align:right'>" & myData1.Tables(0).Rows(j).Item("bobot") & "</td>")

                                If dohist.Text <> "" Then
                                    'myData2 = myChef.SQLICE3HIST(_tmpkode.Text, _periodekd.Text, myData1.Tables(0).Rows(j).Item("no"), myData3.Tables(0).Rows(x).Item("cab"), dohist.Text)
                                    myData2 = myChef.SQLICE3HIST(_tmpkode.Text, aaa(iii), myData1.Tables(0).Rows(j).Item("no"), myData3.Tables(0).Rows(x).Item("cab"), dohist.Text)
                                Else
                                    'myData2 = myChef.SQLICE3(_tmpkode.Text, _periodekd.Text, myData1.Tables(0).Rows(j).Item("no"), myData3.Tables(0).Rows(x).Item("cab"))
                                    myData2 = myChef.SQLICE3(_tmpkode.Text, aaa(iii), myData1.Tables(0).Rows(j).Item("no"), myData3.Tables(0).Rows(x).Item("cab"))
                                End If

                                Dim tc1 As Double
                                Dim dc1 As Double
                                Dim pc1 As Double
                                Dim tc2 As Double
                                Dim dc2 As Double
                                Dim pc2 As Double
                                Dim tc3 As Double
                                Dim dc3 As Double
                                Dim pc3 As Double
                                If IsDBNull(myData2.Tables(0).Rows(0).Item("tc1")) Then
                                    tc1 = 0
                                Else
                                    tc1 = myData2.Tables(0).Rows(0).Item("tc1")
                                End If
                                If IsDBNull(myData2.Tables(0).Rows(0).Item("dc1")) Then
                                    dc1 = 0
                                Else
                                    dc1 = myData2.Tables(0).Rows(0).Item("dc1")
                                End If
                                If IsDBNull(myData2.Tables(0).Rows(0).Item("tc2")) Then
                                    tc2 = 0
                                Else
                                    tc2 = myData2.Tables(0).Rows(0).Item("tc2")
                                End If
                                If IsDBNull(myData2.Tables(0).Rows(0).Item("dc2")) Then
                                    dc2 = 0
                                Else
                                    dc2 = myData2.Tables(0).Rows(0).Item("dc2")
                                End If
                                If tc1 = 0 Then
                                    pc1 = 0
                                Else
                                    pc1 = dc1 / tc1 * myData1.Tables(0).Rows(j).Item("bobot")
                                    vbobot1 = vbobot1 + myData1.Tables(0).Rows(j).Item("bobot")
                                End If
                                If tc2 = 0 Then
                                    pc2 = 0
                                Else
                                    pc2 = dc2 / tc2 * myData1.Tables(0).Rows(j).Item("bobot")
                                    vbobot2 = vbobot2 + myData1.Tables(0).Rows(j).Item("bobot")
                                End If
                                tc3 = tc2 + tc1
                                dc3 = dc2 + dc1
                                pc3 = (pc2 + pc1) / 2

                                tc1 = Math.Round(tc1, 1)
                                dc1 = Math.Round(dc1, 1)
                                pc1 = Math.Round(pc1, 1)
                                tc2 = Math.Round(tc2, 1)
                                dc2 = Math.Round(dc2, 1)
                                pc2 = Math.Round(pc2, 1)
                                tc3 = Math.Round(tc3, 1)
                                dc3 = Math.Round(dc3, 1)
                                pc3 = Math.Round(pc3, 1)


                                vtc1 = vtc1 + tc1
                                vdc1 = vdc1 + dc1
                                vpc1 = vpc1 + pc1

                                vtc2 = vtc2 + tc2
                                vdc2 = vdc2 + dc2
                                vpc2 = vpc2 + pc2

                                vtc3 = vtc3 + tc3
                                vdc3 = vdc3 + dc3
                                vpc3 = vpc3 + pc3

                                'myhtml.Append("         <td style='text-align:right'>" & Math.Round(tc1, 1) & "</td>")
                                'myhtml.Append("         <td style='text-align:right'>" & Math.Round(dc1, 1) & "</td>")
                                'myhtml.Append("         <td style='text-align:right'>" & Math.Round(pc1, 1) & "</td>")
                                'myhtml.Append("         <td style='text-align:right'>" & Math.Round(tc2, 1) & "</td>")
                                'myhtml.Append("         <td style='text-align:right'>" & Math.Round(dc2, 1) & "</td>")
                                'myhtml.Append("         <td style='text-align:right'>" & Math.Round(pc2, 1) & "</td>")
                                'myhtml.Append("         <td style='text-align:right'>" & Math.Round(tc3, 1) & "</td>")
                                'myhtml.Append("         <td style='text-align:right'>" & Math.Round(dc3, 1) & "</td>")
                                'myhtml.Append("         <td style='text-align:right'>" & Math.Round(pc3, 1) & "</td>")
                                'myhtml.Append("         <td style='text-align:right'></td>")

                                'If j < myData1.Tables(0).Rows.Count - 1 Then
                                'myhtml.Append("      </tr>")
                                'End If
                            Next

                            'tjumlahsub = tjumlahsub + 1
                            If vtc3 <> 0 Then
                                tjumlahsub = tjumlahsub + 1
                            End If


                            'myhtml.Append("      </tr>")
                            'myhtml.Append("      <tr>")
                            'myhtml.Append("         <td colspan='4' style='text-align:center'>Sub Total</td>")
                            'myhtml.Append("         <td style='text-align:center'></td>")
                            'myhtml.Append("         <td style='text-align:center'></td>")

                            'If vtc1 = 0 Then
                            '    vpc1 = 0
                            'Else
                            '    vpc1 = vdc1 / vtc1 * myData.Tables(0).Rows(i).Item("bobot")
                            'End If
                            'If vtc2 = 0 Then
                            '    vpc2 = 0
                            'Else
                            '    vpc2 = vdc2 / vtc2 * myData.Tables(0).Rows(i).Item("bobot")
                            'End If

                            If vbobot1 = 0 Then
                                vpc1 = 0
                            ElseIf vbobot1 <> 100 Then
                                vpc1 = Math.Round(vpc1 * 100 / vbobot1, 0)
                            End If
                            If vbobot2 = 0 Then
                                vpc2 = 0
                            ElseIf vbobot2 <> 100 Then
                                vpc2 = Math.Round(vpc2 * 100 / vbobot2, 0)
                            End If

                            vtc1 = Math.Round(vtc1, 0)
                            vdc1 = Math.Round(vdc1, 0)
                            vpc1 = Math.Round(vpc1, 0)
                            vtc2 = Math.Round(vtc2, 0)
                            vdc2 = Math.Round(vdc2, 0)
                            vpc2 = Math.Round(vpc2, 0)
                            vtc3 = Math.Round(vtc3, 0)
                            vdc3 = Math.Round(vdc3, 0)
                            vpc3 = Math.Round((vpc2 + vpc1) / 2, 0)



                            tvtc1 = tvtc1 + Math.Round(vtc1, 0)
                            tvdc1 = tvdc1 + Math.Round(vdc1, 0)
                            tvpc1 = tvpc1 + Math.Round(vpc1, 0)
                            tvtc2 = tvtc2 + Math.Round(vtc2, 0)
                            tvdc2 = tvdc2 + Math.Round(vdc2, 0)
                            tvpc2 = tvpc2 + Math.Round(vpc2, 0)
                            tvtc3 = tvtc3 + Math.Round(vtc3, 0)
                            tvdc3 = tvdc3 + Math.Round(vdc3, 0)

                            tvpc3 = tvpc3 + Math.Round(vpc3, 0)

                            'myhtml.Append("         <td style='text-align:right'>" & Math.Round(vtc1, 0) & "</td>")
                            'myhtml.Append("         <td style='text-align:right'>" & Math.Round(vdc1, 0) & "</td>")
                            'myhtml.Append("         <td style='text-align:right'>" & Math.Round(vpc1, 0) & "</td>")

                            'If vpc1 < 61 Then
                            '    myhtml.Append("         <td style='text-align:center; background-color: red'>Kurang</td>")
                            'ElseIf vpc1 < 76 Then
                            '    myhtml.Append("         <td style='text-align:center; background-color: yellow'>Cukup</td>")
                            'ElseIf vpc1 < 86 Then
                            '    myhtml.Append("         <td style='text-align:center; background-color: lightgreen'>Baik</td>")
                            'Else
                            '    myhtml.Append("         <td style='text-align:center; background-color: lightblue'>Sangat Baik</td>")
                            'End If


                            'myhtml.Append("         <td style='text-align:right'>" & Math.Round(vtc2, 0) & "</td>")
                            'myhtml.Append("         <td style='text-align:right'>" & Math.Round(vdc2, 0) & "</td>")
                            'myhtml.Append("         <td style='text-align:right'>" & Math.Round(vpc2, 0) & "</td>")

                            'If vpc2 < 61 Then
                            '    myhtml.Append("         <td style='text-align:center; background-color: red'>Kurang</td>")
                            'ElseIf vpc2 < 76 Then
                            '    myhtml.Append("         <td style='text-align:center; background-color: yellow'>Cukup</td>")
                            'ElseIf vpc2 < 86 Then
                            '    myhtml.Append("         <td style='text-align:center; background-color: lightgreen'>Baik</td>")
                            'Else
                            '    myhtml.Append("         <td style='text-align:center; background-color: lightblue'>Sangat Baik</td>")
                            'End If

                            'myhtml.Append("         <td style='text-align:right'>" & Math.Round(vtc3, 0) & "</td>")
                            'myhtml.Append("         <td style='text-align:right'>" & Math.Round(vdc3, 0) & "</td>")
                            'myhtml.Append("         <td style='text-align:right'>" & Math.Round(vpc3, 0) & "</td>")

                            'If vpc3 < 61 Then
                            '    myhtml.Append("         <td style='text-align:center; background-color: red'>Kurang</td>")
                            'ElseIf vpc3 < 76 Then
                            '    myhtml.Append("         <td style='text-align:center; background-color: yellow'>Cukup</td>")
                            'ElseIf vpc3 < 86 Then
                            '    myhtml.Append("         <td style='text-align:center; background-color: lightgreen'>Baik</td>")
                            'Else
                            '    myhtml.Append("         <td style='text-align:center; background-color: lightblue'>Sangat Baik</td>")
                            'End If

                            'If vpc3 < 61 Then
                            '    myhtml.Append("         <td style='text-align:center'>Kurang</td>")
                            'ElseIf vpc3 < 76 Then
                            '    myhtml.Append("         <td style='text-align:center'>Cukup</td>")
                            'ElseIf vpc3 < 86 Then
                            '    myhtml.Append("         <td style='text-align:center'>Baik</td>")
                            'Else
                            '    myhtml.Append("         <td style='text-align:center'>Sangat Baik</td>")
                            'End If

                            'myhtml.Append("      </tr>")
                        Next


                        'myhtml.Append("      </tr>")
                        'myhtml.Append("      <tr>")
                        'myhtml.Append("         <td colspan='2' style='text-align:center'><B>T o t a l</B></td>")
                        'myhtml.Append("         <td style='text-align:center'></td>")
                        'myhtml.Append("         <td style='text-align:center'></td>")



                        tvtc1 = Math.Round(tvtc1, 0)
                        tvdc1 = Math.Round(tvdc1, 0)
                        tvpc1 = Math.Round(tvpc1 / tjumlahsub, 0)
                        tvtc2 = Math.Round(tvtc2, 0)
                        tvdc2 = Math.Round(tvdc2, 0)
                        tvpc2 = Math.Round(tvpc2 / tjumlahsub, 0)
                        tvtc3 = Math.Round(tvtc3, 0)
                        tvdc3 = Math.Round(tvdc3, 0)
                        tvpc3 = Math.Round((tvpc2 + tvpc1) / 2, 0)



                        myhtml.Append("         <td style='text-align:right'><B>" & Math.Round(tvtc1, 0) & "</B></td>")
                        myhtml.Append("         <td style='text-align:right'><B>" & Math.Round(tvdc1, 0) & "</B></td>")
                        myhtml.Append("         <td style='text-align:right'><B>" & Math.Round(tvpc1, 0) & "</B></td>")

                        For ii = 0 To arrayKodeHasil.Length - 1
                            If tvpc1 <= 0 And arrayN1Hasil(ii) = 0 Then
                                myhtml.Append("         <td style='text-align:center; background-color: " & arrayWarnaHasil(ii) & "'><B>" & arrayKetHasil(ii) & "</B></td>")
                            ElseIf (arrayN1Hasil(ii) - 1) < tvpc1 And arrayN2Hasil(ii) >= tvpc1 Then
                                myhtml.Append("         <td style='text-align:center; background-color: " & arrayWarnaHasil(ii) & "'><B>" & arrayKetHasil(ii) & "</B></td>")
                                'Else
                                '    myhtml.Append("         <td style='text-align:center; background-color: white'><B>&nbsp;</B></td>")
                            End If
                        Next


                        'If tvpc1 < 61 Then
                        '    myhtml.Append("         <td style='text-align:center; background-color: red'><B>Kurang</B></td>")
                        'ElseIf tvpc1 < 76 Then
                        '    myhtml.Append("         <td style='text-align:center; background-color: yellow'<B>Cukup</B></td>")
                        'ElseIf tvpc1 < 86 Then
                        '    myhtml.Append("         <td style='text-align:center; background-color: lightgreen'><B>Baik</B></td>")
                        'Else
                        '    myhtml.Append("         <td style='text-align:center; background-color: lightblue'><B>Sangat Baik</B></td>")
                        'End If

                        myhtml.Append("         <td style='text-align:right'><B>" & Math.Round(tvtc2, 0) & "</B></td>")
                        myhtml.Append("         <td style='text-align:right'><B>" & Math.Round(tvdc2, 0) & "</B></td>")
                        myhtml.Append("         <td style='text-align:right'><B>" & Math.Round(tvpc2, 0) & "</B></td>")

                        For ii = 0 To arrayKodeHasil.Length - 1
                            If tvpc2 <= 0 And arrayN1Hasil(ii) = 0 Then
                                myhtml.Append("         <td style='text-align:center; background-color: " & arrayWarnaHasil(ii) & "'><B>" & arrayKetHasil(ii) & "</B></td>")
                            ElseIf (arrayN1Hasil(ii) - 1) < tvpc2 And arrayN2Hasil(ii) >= tvpc2 Then
                                myhtml.Append("         <td style='text-align:center; background-color: " & arrayWarnaHasil(ii) & "'><B>" & arrayKetHasil(ii) & "</B></td>")
                                'Else
                                '    myhtml.Append("         <td style='text-align:center; background-color: white'><B>&nbsp;</B></td>")
                            End If
                        Next


                        'If tvpc2 < 61 Then
                        '    myhtml.Append("         <td style='text-align:center; background-color: red'><B>Kurang</B></td>")
                        'ElseIf tvpc2 < 76 Then
                        '    myhtml.Append("         <td style='text-align:center; background-color: yellow'><B>Cukup</B></td>")
                        'ElseIf tvpc2 < 86 Then
                        '    myhtml.Append("         <td style='text-align:center; background-color: lightgreen'><B>Baik</B></td>")
                        'Else
                        '    myhtml.Append("         <td style='text-align:center; background-color: lightblue'><B>Sangat Baik</B></td>")
                        'End If


                        myhtml.Append("         <td style='text-align:right'><B>" & Math.Round(tvtc3, 0) & "</B></td>")
                        myhtml.Append("         <td style='text-align:right'><B>" & Math.Round(tvdc3, 0) & "</B></td>")
                        myhtml.Append("         <td style='text-align:right'><B>" & Math.Round(tvpc3, 0) & "</B></td>")


                        For ii = 0 To arrayKodeHasil.Length - 1
                            If tvpc3 <= 0 And arrayN1Hasil(ii) = 0 Then
                                myhtml.Append("         <td style='text-align:center; background-color: " & arrayWarnaHasil(ii) & "'><B>" & arrayKetHasil(ii) & "</B></td>")
                            ElseIf (arrayN1Hasil(ii) - 1) < tvpc3 And arrayN2Hasil(ii) >= tvpc3 Then
                                myhtml.Append("         <td style='text-align:center; background-color: " & arrayWarnaHasil(ii) & "'><B>" & arrayKetHasil(ii) & "</B></td>")
                                'Else
                                '    myhtml.Append("         <td style='text-align:center; background-color: white'><B>&nbsp;</B></td>")
                            End If
                        Next


                        'If tvpc3 < 61 Then
                        '    myhtml.Append("         <td style='text-align:center; background-color: red'><B>Kurang</B></td>")
                        'ElseIf tvpc3 < 76 Then
                        '    myhtml.Append("         <td style='text-align:center; background-color: yellow'><B>Cukup</B></td>")
                        'ElseIf tvpc3 < 86 Then
                        '    myhtml.Append("         <td style='text-align:center; background-color: lightgreen'><B>Baik</B></td>")
                        'Else
                        '    myhtml.Append("         <td style='text-align:center; background-color: lightblue'><B>Sangat Baik</B></td>")
                        'End If

                        vv4 = vv4 & "'1st Def' , '2nd Def' , "
                        vv2 = vv2 & tvtc1 & " , " & tvtc2 & " , "
                        vv3 = vv3 & tvdc1 & " , " & tvdc2 & " , "

                        If v5 < tvtc1 Then
                            v5 = tvtc1
                        End If
                        If v5 < tvtc2 Then
                            v5 = tvtc2
                        End If
                        If v5 < tvdc1 Then
                            v5 = tvdc1
                        End If
                        If v5 < tvdc2 Then
                            v5 = tvdc2
                        End If

                        If myData3.Tables(0).Rows(x).Item("jml_conf") = 0 Then
                            myhtml.Append("         <td style='text-align:left;background-color: red'>Belum Konfirm</td>")
                        Else
                            myhtml.Append("         <td style='text-align:left'>Sudah Konfirm</td>")
                        End If

                        myhtml.Append("      </tr>")


                    Next

                    v6 = Math.Round(v5 / 10, 0)

                    'vv1 = vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1 & vv1
                    'vv2 = vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2 & vv2
                    'vv3 = vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3 & vv3
                    'vv4 = vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4 & vv4

                    v1 = Left(vv1, vv1.Length - 2)
                    v2 = Left(vv2, vv2.Length - 2)
                    v3 = Left(vv3, vv3.Length - 2)
                    v4 = Left(vv4, vv4.Length - 2)

                    v7 = "'0:" & v5.ToString().Trim() & ":" & v6.ToString().Trim() & "'"


                    '--sampai sini
                End If
            End If
        Next


        myhtml.Append("   </tbody>")
        myhtml.Append(" </table> ")


        'myhtml.Append("      <tr>")
        'myhtml.Append("      <td colspan='13'>")
        'myhtml.Append("      <div id='chartDiv'></div>")
        'myhtml.Append("      <td></tr>")

        myPlaceH.Controls.Add(New Literal() With {
          .Text = myhtml.ToString()
         })


    End Sub





    Private Sub btnback_Click(sender As Object, e As EventArgs) Handles btnback.Click
        Response.Redirect("ICERecapBranchPeriode.aspx")
    End Sub

    Protected Function GetDataSql(ByVal query As String) As DataTable
        Try
            Using con As New SqlConnection(strConn)
                con.Open()
                Dim da As New SqlDataAdapter(query, con)
                Dim dt As New DataTable
                da.Fill(dt)
                Return dt
                con.Close()
            End Using
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
End Class
