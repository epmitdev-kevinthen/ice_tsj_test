﻿
Imports System.Data
Imports System.Data.SqlClient

Partial Class _Default
    Inherits System.Web.UI.Page
    Public Shared strConn As String = ConfigurationManager.ConnectionStrings("SQLICE").ConnectionString
    Dim myChef As New Chef

    'Private Sub txtUsername_TextChanged(sender As Object, e As EventArgs) Handles txtUserName.TextChanged


    '    'Dim max As Integer = 1
    '    'Dim min As Integer = 6
    '    'Dim my_num, my_num1 As Integer
    '    'pKeyDisable()

    '    'Randomize()
    '    'my_num = Int((max - min + 1) * Rnd() + min)
    '    'pKeyEnable(my_num)
    '    'my_num1 = my_num
    '    'While my_num = my_num1
    '    '    Randomize()
    '    '    my_num = Int((max - min + 1) * Rnd() + min)
    '    '    pKeyEnable(my_num)
    '    'End While

    '    'lblErrMsg.Text = ""
    '    txtPassword.Value = ""
    '    'txtPassword.Focus()
    'End Sub

    'Private Sub pKeyDisable()
    '    txtKey1.Enabled = False
    '    txtKey2.Enabled = False
    '    txtKey3.Enabled = False
    '    txtKey4.Enabled = False
    '    txtKey5.Enabled = False
    '    txtKey6.Enabled = False
    '    txtKey1.Text = ""
    '    txtKey2.Text = ""
    '    txtKey3.Text = ""
    '    txtKey4.Text = ""
    '    txtKey5.Text = ""
    '    txtKey6.Text = ""

    'End Sub
    'Private Sub pKeyEnable(ByVal myNum As Integer)
    '    Select Case myNum
    '        Case 1
    '            txtKey1.Enabled = True
    '        Case 2
    '            txtKey2.Enabled = True
    '        Case 3
    '            txtKey3.Enabled = True
    '        Case 4
    '            txtKey4.Enabled = True
    '        Case 5
    '            txtKey5.Enabled = True
    '        Case 6
    '            txtKey6.Enabled = True
    '    End Select
    'End Sub


    Private Sub BtnLogiin_Click(sender As Object, e As EventArgs) Handles BtnLogiin.Click

        Session("FULLNAME") = ""
        If UCase(txtUserName.Value) = "SUPERADMIN" And UCase(txtPassword.Value) = "BAYUGANTENG" Then
            Session("sUsername") = UCase(txtUserName.Value)
            Session("sCabang") = "PST"
            Session("levelakses") = "1"
            Session("kodedept") = ""
            Session("region") = ""
            Session("kodejabatan") = "0"
            Session("jabatan") = "SUPERADMIN"

            myChef.SQLLog(UCase(txtUserName.Value), UCase(txtPassword.Value), "1", "")

            'System.Net.Dns.GetHostEntry(Request.ServerVariables("REMOTE_HOST")).HostName)
            Response.Redirect("main.aspx")
        Else
            Dim mycheck As String = myChef.SQLGETUSER(txtUserName.Value, txtPassword.Value)
            If mycheck = "1" Then
                Session("sUsername") = UCase(txtUserName.Value)

                Dim mycheckdata As Data.DataSet = myChef.SQLGETUSERDATA(txtUserName.Value, txtPassword.Value)
                Session("sCabang") = mycheckdata.Tables(0).Rows(0).Item("kodecab")
                Session("levelakses") = mycheckdata.Tables(0).Rows(0).Item("levelakses")
                Session("FULLNAME") = mycheckdata.Tables(0).Rows(0).Item("fullusername")
                Session("kodedept") = mycheckdata.Tables(0).Rows(0).Item("kodedept")

                If IsDBNull(mycheckdata.Tables(0).Rows(0).Item("jabatan")) Then
                    Session("jabatan") = ""
                Else
                    Session("jabatan") = mycheckdata.Tables(0).Rows(0).Item("jabatan")
                End If

                Session("kodejabatan") = mycheckdata.Tables(0).Rows(0).Item("kodejabatan")
                Session("nik") = mycheckdata.Tables(0).Rows(0).Item("nik")
                Session("region") = mycheckdata.Tables(0).Rows(0).Item("region")

                myChef.SQLLog(UCase(txtUserName.Value), UCase(txtPassword.Value), "1", "")
                'System.Net.Dns.GetHostEntry(Request.ServerVariables("REMOTE_HOST")).HostName)
                Response.Redirect("main.aspx")
            Else
                myChef.SQLLog(UCase(txtUserName.Value), UCase(txtPassword.Value), "0", "")

                Dim script As String = "window.onload = function() { nodatauser(); };"

                ClientScript.RegisterStartupScript(Me.GetType(), "nodatauser", script, True)
                'System.Net.Dns.GetHostEntry(Request.ServerVariables("REMOTE_HOST")).HostName)
                '  lblErrMsg.Text = "Akses tidak valid"
            End If
        End If
    End Sub

    'Private Sub _Default_Init(sender As Object, e As EventArgs) Handles Me.Init
    '    lblepm.Text = "<div style=" & Chr(34) & "position:fixed ; bottom: 10px; left: 80px;" & Chr(34) & " & chr(34) & " & Chr(34) & "><table style=" & Chr(34) & "border-width: 0px; max-width: 100%; background-color: none;" & Chr(34) & "><tr><td style=" & Chr(34) & "text-align: center; font-size: 15px; color: black; font-family: Verdana; max-width: 100%" & Chr(34) & ">Copyright &#169; 2017 - PT. Enseval Putera MegaTrading Tbk.&nbsp;|&nbsp;Internal Audit & Information Technology Department Collaboration<br />Enseval Building - Jl. Pulo Lentut No. 10, Kawasan Industri Pulogadung, Jakarta - Telp. 46822422<br />System Support : <a href=" & Chr(34) & "mailto:hendro.wibowo@enseval.com" & Chr(34) & ">Audit Admin</a> | <a href=" & Chr(34) & "mailto:bbayuaji@enseval.com" & Chr(34) & ">System Admin</a>" &
    '        "<br/>http://audit.enseval.com:8010</td></tr></table></div>"

    'End Sub

    Protected Function cekNIK() As String
        Dim nik As String = ""
        Dim username As String = Session("FULLNAME")
        Dim username_string As String = "'%" + Session("FULLNAME") + "'"
        If username.Contains(".") Then
            If username.Split(" ").Length > 1 Then
                username_string = "'%" + username.Split(" ")(0) + "' and '%" + username.Split(" ")(1) + +"'"
            End If
        Else

        End If
        Dim dt As DataTable = GetDataSql("select nik from MSTAUDITOR where nama like " & username_string & "")
        If dt.Rows.Count > 0 Then
            nik = dt.Rows(0).Item(0)
        End If
        Return nik
    End Function

    Protected Function GetDataSql(ByVal query As String) As DataTable
        Try
            Using con As New SqlConnection(strConn)
                con.Open()
                Dim da As New SqlDataAdapter(query, con)
                Dim dt As New DataTable
                da.Fill(dt)
                Return dt
                con.Close()
            End Using
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Private Sub _Default_Load(sender As Object, e As EventArgs) Handles Me.Load
        Session("sUsername") = ""
    End Sub
End Class
