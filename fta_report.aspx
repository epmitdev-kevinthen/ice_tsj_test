﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="fta_report.aspx.vb" Inherits="fta_report" MasterPageFile="~/MasterPage.master" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <style>
        /* The Modal (background) */
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        /* Modal Content */
        .modal-content {
            position: relative;
            background-color: #fefefe;
            margin: auto;
            padding: 0;
            border: 1px solid #888;
            width: 80%;
            box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
            -webkit-animation-name: animatetop;
            -webkit-animation-duration: 0.4s;
            animation-name: animatetop;
            animation-duration: 0.4s
        }

        /* Add Animation */
        @-webkit-keyframes animatetop {
            from {
                top: -300px;
                opacity: 0
            }

            to {
                top: 0;
                opacity: 1
            }
        }

        @keyframes animatetop {
            from {
                top: -300px;
                opacity: 0
            }

            to {
                top: 0;
                opacity: 1
            }
        }

        /* The Close Button */
        .close {
            color: white;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

            .close:hover,
            .close:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }

        .modal-header {
            padding: 2px 16px;
            background-color: #5cb85c;
            color: white;
        }

        .modal-body {
            padding: 2px 16px;
        }

        .modal-footer {
            padding: 2px 16px;
            background-color: #5cb85c;
            color: white;
        }

        .none {
            display: none;
        }

        .bg-red {
            background-color: red;
        }

        .bg-yellow {
            background-color: yellow;
        }

        .bg-purple {
            background-color: purple;
            color: white;
        }

        .bg-green {
            background-color: green;
            color: white;
        }

        .bg-deepskyblue {
            background-color: deepskyblue;
        }

        .bg-pink {
            background-color: pink;
        }

        .bg-greenyellow {
            background-color: greenyellow;
        }

        .align-right {
            text-align: right;
        }

        .select2-selection__rendered, .select2-results__options{
            background-color:white !important;
        }

        .large-width {
            min-width: 800px !important;
        }

        .modal-loading {
            position: fixed;
            top: 0;
            left: 0;
            background-color: black;
            z-index: 99;
            opacity: 0.8;
            filter: alpha(opacity=80);
            -moz-opacity: 0.8;
            min-height: 100%;
            width: 100%;
        }

        .loading {
            font-family: Arial;
            font-size: 10pt;
            background-image: url(~/Images/Spinner-1s-200px.gif);
            background-position: center;
            /*border-radius: 25px;
            border: 5px solid #71bd1f;*/
            width: 300px;
            height: 150px;
            display: none;
            position: fixed;
            background-color: White;
            z-index: 999;
        }

        .draw {
            overflow: hidden;
        }

            .draw::before, .draw::after {
                content: '';
                box-sizing: border-box;
                position: absolute;
                border: 4px solid transparent;
                width: 0;
                height: 0;
            }

            .draw::before {
                top: 0;
                left: 0;
                border-top-color: #71bd1f;
                border-right-color: #71bd1f;
                animation: border 2s infinite;
            }

            .draw::after {
                bottom: 0;
                right: 0;
                animation: border 2s 1s infinite, borderColor 2s 1s infinite;
            }

        @keyframes border {
            0% {
                width: 0;
                height: 0;
            }

            25% {
                width: 100%;
                height: 0;
            }

            50% {
                width: 100%;
                height: 100%;
            }

            100% {
                width: 100%;
                height: 100%;
            }
        }

        @keyframes borderColor {
            0% {
                border-bottom-color: #71bd1f;
                border-left-color: #71bd1f;
            }

            50% {
                border-bottom-color: #71bd1f;
                border-left-color: #71bd1f;
            }

            51% {
                border-bottom-color: transparent;
                border-left-color: transparent;
            }

            100% {
                border-bottom-color: transparent;
                border-left-color: transparent;
            }
        }
    </style>
    <script>
        $(document).ready(function () {
            var table = $('#ContentPlaceHolder1_GridView1').DataTable({
                paging: true,
                bProcessing: true, // shows 'processing' label
                bStateSave: true, // presumably saves state for reloads
            });
        });
    </script>

    <asp:Label ID="lblHead" runat="server" Text="Rekap FTA by All Branch"
        Style="font-size: 20px;"></asp:Label>
    <asp:Label ID="lblMsg" runat="server" Style="display: none;"></asp:Label>
    <br />
    <table>
        <tr>
            <td>Branch</td>
            <td>:</td>
            <td>
                <asp:DropDownList multiple ID="ddlBranch" runat="server"></asp:DropDownList><asp:HiddenField ID="hdnSelectedBranch" runat="server" />
            </td>
        </tr>
        <tr>
            <td>Case Category</td>
            <td>:</td>
            <td>
                <asp:DropDownList multiple ID="ddlCaseCategory" runat="server"></asp:DropDownList><asp:HiddenField runat="server" ID="hdnSelectedCaseCategory" />
            </td>
        </tr>
        <tr>
            <td>Finding Category</td>
            <td>:</td>
            <td>
                <asp:DropDownList multiple ID="ddlFindingCategory" runat="server"></asp:DropDownList><asp:HiddenField runat="server" ID="hdnSelectedFindingCategory" />
            </td>
        </tr>
        <tr>
            <td>Periode</td>
            <td>:</td>
            <td>
                <asp:DropDownList multiple ID="ddlPeriode" runat="server"></asp:DropDownList><asp:HiddenField runat="server" ID="hdnSelectedPeriode" />
            </td>
        </tr>
        <tr>
            <td>Status</td>
            <td>:</td>
            <td>
                <asp:DropDownList multiple ID="ddlStatus" runat="server"></asp:DropDownList><asp:HiddenField runat="server" ID="hdnSelectedStatus" />
            </td>
        </tr>
        <tr>
            <td>Kelompok Pemeriksa</td>
            <td>:</td>
            <td>
                <asp:DropDownList multiple ID="ddlKelompokPemeriksa" runat="server"></asp:DropDownList><asp:HiddenField runat="server" ID="hdnSelectedKelompokPemeriksa" />
            </td>
        </tr>
    </table>
    <asp:Button ID="btnFind" runat="server" Text="Find" CssClass="button2" OnClick="btnFind_Click" OnClientClick="ShowProgress();"/>
    <asp:Button ID="btnExport" runat="server" CssClass="button2" Text="Export to Excel" Visible="false" OnClientClick="fnExcelReport(); return false;" />
    <asp:HiddenField ID="hdnIsPostBack" runat="server" Value="N" />
    <div style="margin: 30px;">
        <div id="divTable" runat="server">
        </div>
        <div id="divTableHidden" runat="server" style="display: none;">
            
        </div>
        <table id='tblHidden' style="display:none">
            <thead>
                <tr>
                    <th rowspan='2'>FTA No.</th>
                    <th rowspan='2'>Branch</th>
                    <th colspan='2'>Period</th>
                    <th rowspan='2'>Report No</th>
                    <th rowspan='2'>Main Activity</th>
                    <th rowspan='2'>Sub Activity</th>
                    <th rowspan='2'>Findings Category</th>
                    <th rowspan='2'>Case Category</th>
                    <th rowspan='2' class='large-width'>Audit Result</th>
                    <th rowspan='2'>Bukti Obyektif</th>
                    <th rowspan='2'>Suspect</th>
                    <th rowspan='2'> </th>
                    <th rowspan='2'>Responsibility</th>
                    <th colspan='3'>Follow Up by Auditee</th>
                    <th rowspan='2' class='large-width'>Recommendation</th>
                    <th rowspan='2'>Start Date</th>
                    <th rowspan='2'>Due Date</th>
                    <th colspan='5'>Verification by Auditor</th>
                </tr>
                <tr>
                    <th>Month</th>
                    <th>Year</th>
                    <th class='large-width'>Corrective Action</th>
                    <th>PIC</th>
                    <th>Evidence</th>
                    <th class='large-width'>Verification Result-1</th>
                    <th class='large-width'>Verification Result-2</th>
                    <th>Status*</th>
                    <th>Finish Date</th>
                    <th>PIC</th>
                </tr>
            </thead>
            <tbody>
                </tbody>
                </table>
    </div>
    <div class="loading draw" align="center">
                <h5 style="margin-top: 10px">Generating data. Please wait.</h5>
                <br />
                <br />
                <img src="/image/loading.gif" height="80px" alt="" />
            </div>
    <script type="text/javascript">        
        $(document).ready(function () {
            var table = $('#mytable').DataTable({
                scrollY: "1000px",
                scrollX: "2000px",
                scrollCollapse: true,
                bProcessing: true, // shows 'processing' label
                bStateSave: true, // presumably saves state for reloads
                paging: true,
                pagelangth: 5,
                lengthMenu: [
                    [5, 10, 25, 50, -1],
                    ['5 rows', '10 rows', '25 rows', '50 rows', 'Show all']
                ],
                fixedColumns: {
                    leftColumns: 2
                },
                fixedHeader: true,
                "fnDrawCallback": function () {

                    $table = $(this);

                    // only apply this to specific tables
                    if ($table.closest(".datatable-multi-row").length) {

                        // for each row in the table body...
                        $table.find("tbody>tr").each(function () {
                            var $tr = $(this);

                            // get the "extra row" content from the <script> tag.
                            // note, this could be any DOM object in the row.
                            var extra_row = $tr.find(".extra-row-content").html();

                            // in case draw() fires multiple times, 
                            // we only want to add new rows once.
                            if (!$tr.next().hasClass('dt-added')) {
                                $tr.after(extra_row);
                                $tr.find("td").each(function () {

                                    // for each cell in the top row,
                                    // set the "rowspan" according to the data value.
                                    var $td = $(this);
                                    var rowspan = parseInt($td.data("datatable-multi-row-rowspan"), 10);
                                    if (rowspan) {
                                        $td.attr('rowspan', rowspan);
                                    }
                                });
                            }

                        });

                    } // end if the table has the proper class
                } // end fnDrawCallback()
            });


            var table_hidden = $('#mytable1').DataTable({
                paging: false,
                "fnDrawCallback": function () {

                    $table = $(this);

                    // only apply this to specific tables
                    if ($table.closest(".datatable-multi-row").length) {

                        // for each row in the table body...
                        $table.find("tbody>tr").each(function () {
                            var $tr = $(this);

                            // get the "extra row" content from the <script> tag.
                            // note, this could be any DOM object in the row.
                            var extra_row = $tr.find(".extra-row-content").html();

                            // in case draw() fires multiple times, 
                            // we only want to add new rows once.
                            if (!$tr.next().hasClass('dt-added')) {
                                $tr.after(extra_row);
                                $tr.find("td").each(function () {

                                    // for each cell in the top row,
                                    // set the "rowspan" according to the data value.
                                    var $td = $(this);
                                    var rowspan = parseInt($td.data("datatable-multi-row-rowspan"), 10);
                                    if (rowspan) {
                                        $td.attr('rowspan', rowspan);
                                    }
                                });
                            }

                        });

                    } // end if the table has the proper class
                } // end fnDrawCallback()
            });
            // Get the modal

            $("#mytable1 > tbody").clone().appendTo($("#tblHidden"));
            $('#tblHidden > tbody a').each(function () {
                $(this).attr('href', this.href);
            });
            $('#mytable1').remove("class");
        });
        $(function () {

            $("#<%= ddlBranch.ClientID %>").select2();
            $("#<%= ddlCaseCategory.ClientID %>").select2();
            $("#<%= ddlFindingCategory.ClientID %>").select2();
            $("#<%= ddlPeriode.ClientID %>").select2();
            $("#<%= ddlStatus.ClientID %>").select2();
            $("#<%= ddlKelompokPemeriksa.ClientID %>").select2();

            var hdnSelectedBranch = $("#<%= hdnSelectedBranch.ClientID %>").val();
            var hdnSelectedCaseCategory = $("#<%= hdnSelectedCaseCategory.ClientID %>").val();
            var hdnSelectedFindingCategory = $("#<%= hdnSelectedFindingCategory.ClientID %>").val();
            var hdnSelectedPeriode = $("#<%= hdnSelectedPeriode.ClientID %>").val();
            var hdnSelectedStatus = $("#<%= hdnSelectedStatus.ClientID %>").val();
            var hdnSelectedKelompokPemeriksa = $("#<%= hdnSelectedKelompokPemeriksa.ClientID %>").val();
            console.log(hdnSelectedBranch.split(","));
            $("#<%= ddlBranch.ClientID %>").val(hdnSelectedBranch.split(",")).change();
            $("#<%= ddlCaseCategory.ClientID %>").val(hdnSelectedCaseCategory.split(",")).change();
            $("#<%= ddlFindingCategory.ClientID %>").val(hdnSelectedFindingCategory.split(",")).change();
            $("#<%= ddlPeriode.ClientID %>").val(hdnSelectedPeriode.split(",")).change();
            $("#<%= ddlStatus.ClientID %>").val(hdnSelectedStatus.split(",")).change();
            $("#<%= ddlKelompokPemeriksa.ClientID %>").val(hdnSelectedKelompokPemeriksa.split(",")).change();
        });
        <%--$(function () {
            $('#<%= ddlBranch.ClientID %>').click(function (e) {
                var selected = $(e.target).val();
                console.log(selected);
                //if (selected == 'all') {
                //    $('#fruits > option').prop("selected", false);
                //    $(e.target).prop("selected", true);
                //};
            });
        });--%>
        $('#<%= ddlBranch.ClientID %>').on('change', function (e) {
            var optionSelected = $("option:selected", this);
            var valueSelected = $(this).val();
            var selectedBefore = $("#<%= hdnSelectedBranch.ClientID %>").val();
            console.log(selectedBefore);
            console.log(valueSelected);
            if (valueSelected.indexOf("%") > -1 && selectedBefore.indexOf("%") == -1) {
                //console.log($("option",e.target));
                //console.log($(e.target).select2("val"));
                $(optionSelected).prop("selected", false);
                $(e.target).find('[value="%"]').prop("selected", true);
                //$(e.target).prop("selected", true);
            } else {
                $(e.target).find('[value="%"]').prop("selected", false);
            }
            $("#<%= hdnSelectedBranch.ClientID %>").val($(this).val());
        });

        $('#<%= ddlCaseCategory.ClientID %>').on('change', function (e) {
            var optionSelected = $("option:selected", this);
            var valueSelected = $(this).val();
            var selectedBefore = $("#<%= hdnSelectedCaseCategory.ClientID %>").val();
            //console.log(selectedBefore);
            //console.log(optionSelected);
            if (valueSelected.indexOf("%") > -1 && selectedBefore.indexOf("%") == -1) {
                //console.log($("option",e.target));
                //console.log($(e.target).select2("val"));
                $(optionSelected).prop("selected", false);
                $(e.target).find('[value="%"]').prop("selected", true);
                //$(e.target).prop("selected", true);
            } else {
                $(e.target).find('[value="%"]').prop("selected", false);
            }
            $("#<%= hdnSelectedCaseCategory.ClientID %>").val($(this).val());
        });

        $('#<%= ddlFindingCategory.ClientID %>').on('change', function (e) {
            var optionSelected = $("option:selected", this);
            var valueSelected = $(this).val();
            var selectedBefore = $("#<%= hdnSelectedFindingCategory.ClientID %>").val();
            //console.log(selectedBefore);
            //console.log(optionSelected);
            if (valueSelected.indexOf("%") > -1 && selectedBefore.indexOf("%") == -1) {
                //console.log($("option",e.target));
                //console.log($(e.target).select2("val"));
                $(optionSelected).prop("selected", false);
                $(e.target).find('[value="%"]').prop("selected", true);
                //$(e.target).prop("selected", true);
            } else {
                $(e.target).find('[value="%"]').prop("selected", false);
            }
            $("#<%= hdnSelectedFindingCategory.ClientID %>").val($(this).val());
        });

        $('#<%= ddlPeriode.ClientID %>').on('change', function (e) {
            var optionSelected = $("option:selected", this);
            var valueSelected = $(this).val();
            var selectedBefore = $("#<%= hdnSelectedPeriode.ClientID %>").val();
            //console.log(selectedBefore);
            //console.log(optionSelected);
            if (valueSelected.indexOf("%") > -1 && selectedBefore.indexOf("%") == -1) {
                //console.log($("option",e.target));
                //console.log($(e.target).select2("val"));
                $(optionSelected).prop("selected", false);
                $(e.target).find('[value="%"]').prop("selected", true);
                //$(e.target).prop("selected", true);
            } else {
                $(e.target).find('[value="%"]').prop("selected", false);
            }
            $("#<%= hdnSelectedPeriode.ClientID %>").val($(this).val());
        });

        $('#<%= ddlStatus.ClientID %>').on('change', function (e) {
            var optionSelected = $("option:selected", this);
            var valueSelected = $(this).val();
            var selectedBefore = $("#<%= hdnSelectedStatus.ClientID %>").val();
            //console.log(selectedBefore);
            //console.log(optionSelected);
            if (valueSelected.indexOf("%") > -1 && selectedBefore.indexOf("%") == -1) {
                //console.log($("option",e.target));
                //console.log($(e.target).select2("val"));
                $(optionSelected).prop("selected", false);
                $(e.target).find('[value="%"]').prop("selected", true);
                //$(e.target).prop("selected", true);
            } else {
                $(e.target).find('[value="%"]').prop("selected", false);
            }
            $("#<%= hdnSelectedStatus.ClientID %>").val($(this).val());
        });

        $('#<%= ddlKelompokPemeriksa.ClientID %>').on('change', function (e) {
            var optionSelected = $("option:selected", this);
            var valueSelected = $(this).val();
            var selectedBefore = $("#<%= hdnSelectedKelompokPemeriksa.ClientID %>").val();
            //console.log(selectedBefore);
            //console.log(optionSelected);
            if (valueSelected.indexOf("%") > -1 && selectedBefore.indexOf("%") == -1) {
                //console.log($("option",e.target));
                //console.log($(e.target).select2("val"));
                $(optionSelected).prop("selected", false);
                $(e.target).find('[value="%"]').prop("selected", true);
                //$(e.target).prop("selected", true);
            } else {
                $(e.target).find('[value="%"]').prop("selected", false);
            }
            $("#<%= hdnSelectedKelompokPemeriksa.ClientID %>").val($(this).val());
        });

        function fnExcelReport() {
            //var tab_text = "<table border='2px'><tr bgcolor='#87AFC6'>";
            //var textRange; var j = 0;
            //tab = document.getElementById('mytable1'); // id of table

            //var tab_header = $("table.cell-border.table.table-striped.table-bordered.datatable-multi-row.dataTable.no-footer")[0];

            //for (j = 0; j < tab_header.rows.length; j++) {
            //    tab_text = tab_text + tab_header.rows[j].innerHTML + "</tr>";
            //    //tab_text=tab_text+"</tr>";
            //}

            //for (j = 2; j < tab.rows.length; j++) {
            //    tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
            //    //tab_text=tab_text+"</tr>";
            //}

            //tab_text = tab_text + "</table>";
            //tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
            //tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
            //tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params
            //tab_text = tab_text.replace(/<button[^>]*>|<\/button>/gi, ""); // reomves input params
            //tab_text = tab_text.replace(/<br>Update/g, ""); // reomves input params
            ////console.log(tab_text);
            //var ua = window.navigator.userAgent;
            //var msie = ua.indexOf("MSIE ");

            //if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
            //{
            //    txtArea1.document.open("txt/html", "replace");
            //    txtArea1.document.write(tab_text);
            //    txtArea1.document.close();
            //    txtArea1.focus();
            //    sa = txtArea1.document.execCommand("SaveAs", true, "Say Thanks to Sumit.xls");
            //}
            //else                 //other browser not tested on IE 11
            //    sa = window.open('data:application/vnd.ms-excel,filename=FTA_Report.xls' + encodeURIComponent(tab_text));

            //return (sa);

            var table = $("#tblHidden");
            if (table && table.length) {
                $(table).table2excel({
                    exclude: ".noExl",
                    name: "Excel Document Name",
                    filename: "FTA Report.xls",
                    fileext: ".xls",
                    exclude_img: true,
                    exclude_links: false,
                    exclude_inputs: true,
                    preserveColors: true
                });
            }
            return false;
        }

        function ShowProgress() {
            setTimeout(function () {
                var modal = $('<div />');
                modal.addClass("modal-loading");
                $('body').append(modal);
                var loading = $(".loading");
                loading.show();
                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                loading.css({ top: top, left: left });
            }, 200);
        }
    </script>

    <asp:Label ID="infoerror" runat="server" Style="color: red"></asp:Label>

</asp:Content>
