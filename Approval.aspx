﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Approval.aspx.vb" Inherits="Approval" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css"/>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>

    <br />
    

    <div>

    </div>

    <script>

        $(document).ready(function () {
            $('#mytable').DataTable({
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                "order": [[0, "desc"]]
            });
        });

    </script>

    <div style="width:80%;margin:auto">
        <asp:Label ID="myTable" runat="server" Text="Approval Data Customer" style="font-size:20px"></asp:Label>
        <asp:PlaceHolder ID="tblApproval" runat="server"></asp:PlaceHolder>
    </div>

    

    <div id="divDetail" style="display:none;width:80%;margin:auto">

        <asp:Label ID="lblDetail" runat="server" Text="Detail Data Approval" style="font-size:20px"></asp:Label>
        <br />
        <asp:Label ID="lblID" runat="server" Text="ID : " style="font-size:20px"></asp:Label>
        <%--<asp:Label ID="ID" runat="server" style="font-size:20px"></asp:Label>--%>
        <asp:TextBox ID="ID" runat="server"></asp:TextBox>
        <asp:Button ID="btnDetail" runat="server" Text="Show Detail" CssClass="button2"/>
        <br />

    </div>
    <div id="div2" style="display:none;width:80%;margin:auto">
        <table>
            <tr>
                <td><asp:Label ID="lblCusNum" runat="server" Text="Customer Number" style="font-size:20px"></asp:Label></td>
                <%--<td style="font-size:20px">:</td>--%>
                <td style="font-size:20px">: <asp:Label ID="cusNum" runat="server" style="font-size:20px"></asp:Label></td>
            </tr>
            <tr>
                <td><asp:Label ID="lblCusName" runat="server" Text="Customer Name" style="font-size:20px"></asp:Label></td>
                <%--<td style="font-size:20px">:</td>--%>
                <td style="font-size:20px">: <asp:Label ID="cusName" runat="server" style="font-size:20px"></asp:Label></td>
            </tr>
            <tr>
                <td><asp:Label ID="lblShipToId" runat="server" Text="Ship To ID" style="font-size:20px"></asp:Label></td>
                <%--<td style="font-size:20px">:</td>--%>
                <td style="font-size:20px">: <asp:Label ID="shipToId" runat="server" style="font-size:20px"></asp:Label></td>
            </tr>
            <tr>
                <td><asp:Label ID="lblAlamat" runat="server" Text="Alamat" style="font-size:20px"></asp:Label></td>
                <%--<td style="font-size:20px">:</td>--%>
                <td style="font-size:20px">: <asp:Label ID="alamat" runat="server" style="font-size:20px"></asp:Label></td>
            </tr>
            <tr>
                <td><asp:Label ID="lblNoTlp" runat="server" Text="No. Telp" style="font-size:20px"></asp:Label></td>
                <%--<td style="font-size:20px">:</td>--%>
                <td style="font-size:20px">: <asp:Label ID="noTlp" runat="server" style="font-size:20px"></asp:Label></td>
            </tr>
            <tr>
                <td><asp:Label ID="lblNmPmlk" runat="server" Text="Nama Pemilik" style="font-size:20px"></asp:Label></td>
                <%--<td style="font-size:20px">:</td>--%>
                <td style="font-size:20px">: <asp:Label ID="nmPmlk" runat="server" style="font-size:20px"></asp:Label></td>
            </tr>
            <tr>
                <td><asp:Label ID="lblNoHpPmlk" runat="server" Text="No. HP Pemilik" style="font-size:20px"></asp:Label></td>
                <%--<td style="font-size: 20px">:</td>--%>
                <td style="font-size:20px">: <asp:Label ID="noHpPmlk" runat="server" style="font-size: 20px"></asp:Label></td>
            </tr>
            <tr>
                <td><asp:Label ID="lblPicByr" runat="server" Text="Nama PIC Pembayaran" style="font-size:20px"></asp:Label></td>
                <%--<td style="font-size: 20px">:</td>--%>
                <td style="font-size:20px">: <asp:Label ID="picByr" runat="server" style="font-size:20px"></asp:Label></td>
            </tr>
            <tr>
                <td><asp:Label ID="lblHpPicByr" runat="server" Text="No. HP PIC Pembayaran" style="font-size:20px"></asp:Label></td>
                <%--<td style="font-size: 20px">:</td>--%>
                <td style="font-size:20px">: <asp:Label ID="hpPicByr" runat="server" style="font-size:20px"></asp:Label></td>
            </tr>
            <tr>
                <td><asp:Label ID="lblPicBeli" runat="server" Text="Nama PIC Pembelian" style="font-size:20px"></asp:Label></td>
                <%--<td style="font-size: 20px">:</td>--%>
                <td style="font-size:20px">: <asp:Label ID="picBeli" runat="server" style="font-size:20px"></asp:Label></td>
            </tr>
            <tr>
                <td><asp:Label ID="lblHpPicBeli" runat="server" Text="No. HP PIC Pembelian" style="font-size:20px"></asp:Label></td>
                <%--<td style="font-size: 20px">:</td>--%>
                <td style="font-size:20px">: <asp:Label ID="hpPicBeli" runat="server" style="font-size:20px"></asp:Label></td>
            </tr>
            <tr>
                <td><asp:Label ID="lblPicGdg" runat="server" Text="Nama PIC Gudang" style="font-size:20px"></asp:Label></td>
                <%--<td style="font-size:20px">:</td>--%>
                <td style="font-size:20px">: <asp:Label ID="picgdg" runat="server" style="font-size:20px"></asp:Label></td>
            </tr>
            <tr>
                <td><asp:Label ID="lblHpPicGdg" runat="server" Text="No. Hp PIC Gudang" style="font-size:20px"></asp:Label></td>
                <%--<td style="font-size:20px">:</td>--%>
                <td style="font-size:20px">: <asp:Label ID="hpPicGdg" runat="server" style="font-size:20px"></asp:Label></td>
            </tr>
            <tr>
                <td><asp:Label ID="lblPicApj" runat="server" Text="Nama PIC APJ" style="font-size:20px"></asp:Label></td>
                <%--<td style="font-size:20px">:</td>--%>
                <td style="font-size:20px">: <asp:Label ID="picApj" runat="server" style="font-size:20px"></asp:Label></td>
            </tr>
            <tr>
                <td><asp:Label ID="lblHpPicApj" runat="server" Text="No. HP PIC APJ" style="font-size:20px"></asp:Label></td>
                <%--<td style="font-size:20px">:</td>--%>
                <td style="font-size:20px">: <asp:Label ID="hpPicApj" runat="server" style="font-size:20px"></asp:Label></td>
            </tr>
            <tr>
                <td><asp:Label ID="lblPicTax" runat="server" Text="Nama PIC Tax" style="font-size:20px"></asp:Label></td>
                <%--<td style="font-size:20px">:</td>--%>
                <td style="font-size:20px">: <asp:Label ID="picTax" runat="server" style="font-size:20px"></asp:Label></t>
            </tr>
            <tr>
                <td><asp:Label ID="lblHpPicTax" runat="server" Text="No. HP PIC Tax" style="font-size:20px"></asp:Label></td>
                <%--<td style="font-size:20px">:</td>--%>
                <td style="font-size:20px">: <asp:Label ID="hpPicTax" runat="server" style="font-size:20px"></asp:Label></td>
            </tr>
            <tr>
                <td><asp:Label ID="lblPrsdrByr" runat="server" Text="Prosedur Pembayaran" style="font-size:20px"></asp:Label></td>
                <%--<td style="font-size:20px">:</td>--%>
                <td style="font-size:20px">: <asp:Label ID="prsdrByr" runat="server" style="font-size:20px"></asp:Label></td>
            </tr>
            <tr>
                <td><asp:Label ID="lblCrByr" runat="server" Text="Cara Pembayaran" style="font-size:20px"></asp:Label></td>
                <%--<td style="font-size:20px">:</td>--%>
                <td style="font-size:20px">: <asp:Label ID="crByr" runat="server" style="font-size:20px"></asp:Label></td>
            </tr>
            <tr>
                <td><asp:Label ID="lblOrgCode" runat="server" Text="ORG Code" style="font-size:20px"></asp:Label></td>
                <td style="font-size:20px">: <asp:Label ID="orgCode" runat="server" style="font-size:20px"></asp:Label></td>
            </tr>
            <tr>
                <td><asp:Label ID="lblSubDt" runat="server" Text="Submit Date" style="font-size:20px" ></asp:Label></td>
                <td style="font-size:20px">: <asp:Label ID="subDate" runat="server" style="font-size:20px" ></asp:Label></td>
            </tr>
            <tr>
                <td><asp:Label ID="lblInsertFrom" runat="server" Text="Insert From" style="font-size:20px"></asp:Label> </td>
                <td style="font-size:20px">: <asp:Label ID="inFrom" runat="server" style="font-size:20px"></asp:Label></td>
            </tr>
             <tr>
                <td><asp:Label ID="lblInsertFromHistory" runat="server" Text="Insert History" style="font-size:20px"></asp:Label> </td>
                <td style="font-size:20px">: <asp:Label ID="inFromHis" runat="server" style="font-size:20px"></asp:Label></td>
            </tr>
             <tr>
                <td><asp:Label ID="Label1" runat="server" Text="History Submit Date" style="font-size:20px"></asp:Label> </td>
                <td style="font-size:20px">: <asp:Label ID="HisDate" runat="server" style="font-size:20px"></asp:Label></td>
            </tr>
            <tr>
                <td>
                    <button type="button" Class="button2" onclick=divtolak() >Tolak</button>
                    <asp:Button ID="btnTerima" runat="server" Text="Terima" CssClass="button2"/>
                </td>
            </tr>
        </table>
    </div>
    <br />
    <div id="divTolak" style="display:none;width:80%;margin:auto">
        <table>
            <tr>
                <td><asp:Label ID="lblAlasan" runat="server" Text="Alasan Penolakan" style="font-size:20px"></asp:Label></td>
            </tr>
            <tr>
                <td><asp:TextBox ID="txtAlasan" runat="server" TextMode="MultiLine" Height="100" placeholder="Masukkan Alasan"></asp:TextBox></td>
            </tr>
            <tr>
                <td><button type="button" class="button2" onclick=divCancelTolak()>Cancel</button><asp:Button ID="btnTolak" runat="server" Text="Tolak" CssClass="button2"/></td>
            </tr>
        </table>
    </div>

    <script>
        //, sti, alamat, notelp, pemilik, hppemilik, picbayar, hppicbayar, picbeli, hppicbeli, picgudang, hppicgudang, picapj, hppicapj, pictax, hppictax, probayar, carabayar
        function showDiv(id) {
            document.getElementById("divDetail").style.display = "block";
            document.getElementById("<%=ID.ClientID %>").value = id;
            <%--document.getElementById("<%=shipToId.ClientID %>").textContent = sti;
            document.getElementById("<%=alamat.ClientID %>").textContent = alamat;
            document.getElementById("<%=noTlp.ClientID %>").textContent = notelp;
            document.getElementById("<%=nmPmlk.ClientID %>").textContent = pemilik;
            document.getElementById("<%=noHpPmlk.ClientID %>").textContent = hppemilik;
            document.getElementById("<%=picByr.ClientID %>").textContent = picbayar;
            document.getElementById("<%=hpPicByr.ClientID %>").textContent = hppicbayar;
            document.getElementById("<%=picBeli.ClientID %>").textContent = picbeli;
            document.getElementById("<%=hpPicBeli.ClientID %>").textContent = hppicbeli;
            document.getElementById("<%=picgdg.ClientID %>").textContent = picgudang;
            document.getElementById("<%=hpPicGdg.ClientID %>").textContent = hppicgudang;
            document.getElementById("<%=picApj.ClientID %>").textContent = picapj;
            document.getElementById("<%=hpPicApj.ClientID %>").textContent = hppicapj;
            document.getElementById("<%=picTax.ClientID %>").textContent = pictax;
            document.getElementById("<%=hpPicTax.ClientID %>").textContent = hppictax;
            document.getElementById("<%=prsdrByr.ClientID %>").textContent = probayar;
            document.getElementById("<%=crByr.ClientID %>").textContent = carabayar;--%>
            return false;
        }

        function showDiv2() {
            document.getElementById("divDetail").style.display = "block";
            document.getElementById("div2").style.display = "block";
            return false;
        }

        function alrtNoData() {
            alert("Data tidak ada!!!");
        }

        function alrtApproveDataSucceed() {

            window.location.href = 'Approval.aspx';
            alert("Data Berhasil Diupdate");
        }

        function alrtApproveDataFailed() {
            window.location.href = 'Approval.aspx';
            alert("Data Gagal Diupdate");
        }

        function divtolak() {
            document.getElementById("divTolak").style.display = "block";
            return false;
        }

        function divCancelTolak() {
            document.getElementById("divTolak").style.display = "none";
            return false;
        }

        function alertTolakValid() {
            alert("Alasan minimal 5 character!!!");
            return false;
        }

    </script>

</asp:Content>

