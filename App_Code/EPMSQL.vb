﻿Imports System.Data
Imports System.Data.SqlClient

Public Class GetDataWS
    Public strConn As String = ConfigurationManager.ConnectionStrings("SQLICE").ConnectionString
    Public oConn As New SqlConnection(strConn)
    Public oDa As SqlDataAdapter
    Public oDs As New DataSet
    Public oCmd As SqlCommand
    Public oDR As SqlDataReader
    Public trans As SqlTransaction

    Public Sub OpenConnection()

        If oConn.State = ConnectionState.Open Then
            oConn.Close()
        End If
        oConn.Open()

    End Sub

    Public Sub CloseConnection()
        If oConn.State = ConnectionState.Open Then
            oConn.Close()
        End If
    End Sub


    Public Function CreateDataSet(ByVal strSQL As String,
          Optional ByVal sqlParam As SqlParameter = Nothing) As DataSet

        OpenConnection()

        Dim scnnNW As New SqlConnection(strConn)

        Dim scmd As New SqlCommand(strSQL, scnnNW)
        scmd.CommandTimeout = 600
        If Not IsNothing(sqlParam) Then
            scmd.Parameters.Add(sqlParam)
        End If

        Dim sda As New SqlDataAdapter(scmd)

        Dim ds As New DataSet
        sda.Fill(ds)

        Return ds
        CloseConnection()
    End Function

    Function CreateDataReader(ByVal strSQL As String) As SqlDataReader
        OpenConnection()
        oCmd = New SqlCommand

        With oCmd
            .Connection = oConn
            .CommandType = CommandType.Text
            .CommandText = strSQL
            oDR = .ExecuteReader
        End With

        CreateDataReader = oDR
        oCmd.Dispose()

        Return CreateDataReader
    End Function

    Function GetDataReader(ByVal strSQL As String, ByVal iFieldIndex As Int16) As String
        GetDataReader = ""
        OpenConnection()
        oCmd = New SqlCommand

        With oCmd
            .Connection = oConn
            .CommandType = CommandType.Text
            .CommandText = strSQL
            oDR = .ExecuteReader
        End With


        If oDR.RecordsAffected = -1 Then
            While oDR.Read
                If IsDBNull(oDR.Item(iFieldIndex)) Then
                    GetDataReader = ""
                Else
                    GetDataReader = oDR.Item(iFieldIndex).ToString
                End If
            End While
        End If

        CloseConnection()

        Return GetDataReader
    End Function

    Function GetDataReaderNum(ByVal strSQL As String, ByVal iFieldIndex As Int16) As Integer
        GetDataReaderNum = 0
        OpenConnection()
        oCmd = New SqlCommand

        With oCmd
            .Connection = oConn
            .CommandType = CommandType.Text
            .CommandText = strSQL
            oDR = .ExecuteReader
        End With

        If oDR.RecordsAffected = -1 Then
            While oDR.Read
                If IsDBNull(oDR.Item(iFieldIndex)) Then
                    GetDataReaderNum = 0
                Else
                    GetDataReaderNum = oDR.Item(iFieldIndex)
                End If
            End While
        End If

        CloseConnection()

        Return GetDataReaderNum
    End Function

    Function ExecuteDataSQLReturnID(ByVal strSQL As String) As Integer
        Dim cmd As New SqlCommand
        Try
            Using con As New SqlConnection(strConn)
                con.Open()
                cmd = New SqlCommand(strSQL, con)
                Dim id As Integer = Convert.ToInt32(cmd.ExecuteScalar())
                con.Close()
                Return id
            End Using
        Catch ex As Exception
            Return 0
        End Try
    End Function

    Sub ExecuteDataReader(ByVal strSQL As String)

        OpenConnection()
        oCmd = New SqlCommand

        With oCmd
            .Connection = oConn
            .CommandType = CommandType.Text
            .CommandText = strSQL
            oDR = .ExecuteReader
        End With

        CloseConnection()

    End Sub

    Function ExecuteDataSQL(ByVal strSQL As String) As Integer

        OpenConnection()
        oCmd = New SqlCommand
        trans = oConn.BeginTransaction()

        Try
            With oCmd
                .Transaction = trans
                .Connection = oConn
                .CommandType = CommandType.Text
                .CommandText = strSQL
                .ExecuteNonQuery()
                trans.Commit()
            End With
            Return 1
        Catch ex As Exception
            trans.Rollback()
            Return 0
        End Try

        CloseConnection()
    End Function





    Function GetSplit(ByVal sParam As String) As String
        Dim i As Integer
        Dim j As Integer = 0
        Dim x As String

        GetSplit = ""

        For i = 0 To Len(sParam) - 1
            x = sParam.Substring(i, 1)
            If x = "," Then
                j = j + 1
            End If
        Next
        GetSplit = "('"
        If j > 0 Then
            For i = 0 To j
                GetSplit = GetSplit + sParam.Split(",")(i)
                If i <> j Then
                    GetSplit = GetSplit + "','"
                End If
            Next
        ElseIf j = 0 Then
            GetSplit = GetSplit + sParam
        End If
        GetSplit = GetSplit + "')"

        Return GetSplit
    End Function

End Class
