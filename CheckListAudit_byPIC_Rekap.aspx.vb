﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Web.UI

Partial Class CheckListAudit_byPIC_Rekap
    Inherits System.Web.UI.Page


    Dim myChef As New Chef
    Dim myhtml As New StringBuilder

    Dim myhtml1 As New StringBuilder
    Public MyHTMLString As String

    Dim PublicPengali As String
    Public Shared strConn As String = ConfigurationManager.ConnectionStrings("SQLICE").ConnectionString
    Dim cmd As New SqlCommand
    Public Shared dt_penilaian As DataTable

    Protected Sub cekConfirmAuditor(ByVal kode_tmp As String, ByVal kodecab As String, ByVal kode_prd As String)
        Try
            Using con As New SqlConnection(strConn)
                con.Open()
                Dim da As New SqlDataAdapter("select auditor_1 from checklist_entryhdr where kode_tmp = '" & kode_tmp & "' and kodecab = '" & kodecab & "' and kode_prd = '" & kode_prd & "'", con)
                Dim dt As New DataTable
                da.Fill(dt)
                If dt.Rows.Count > 0 Then
                    btnSaveAuditor.Enabled = False
                Else
                    btnSaveAuditor.Enabled = True
                End If
                con.Close()
            End Using
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnFind_Click(sender As Object, e As EventArgs) Handles btnFind.Click
        If doBranch.Text <> "" And doTemplate.Text <> "" And doPeriode.Text <> "" Then
            _cabang.Text = doBranch.Text
            _kode.Text = doTemplate.Text
            _periode.Text = doPeriode.Text

            _statusFind.Text = doStatusFind.Text
            _entryFind.Text = doEntryFind.Text

            'Call sub_Refresh1()

            loadICE_PIC_Rekap()

            ddlAuditor1.SelectedValue = " "
            ddlAuditor2.SelectedValue = " "
            ddlAuditor3.SelectedValue = " "

            cekConfirmAuditor(_kode.Text, _cabang.Text, _periode.Text)

            If btnSaveAuditor.Enabled = True Then
                Dim dt As DataTable = GetDataSql("select * from CHECKLIST_ENTRY_AUDITOR_TEMP where kode_tmp = '" & _kode.Text & "' and kodecab = '" & _cabang.Text & "' and Kode_Prd = '" & _periode.Text & "'")
                If dt.Rows.Count > 0 Then
                    Try
                        ddlAuditor1.SelectedValue = dt.Rows(0).Item("auditor_1")
                        ddlAuditor2.SelectedValue = dt.Rows(0).Item("auditor_2")
                        ddlAuditor3.SelectedValue = dt.Rows(0).Item("auditor_3")
                    Catch ex As Exception

                    End Try

                End If
            Else
                Dim dt As DataTable = GetDataSql("select * from checklist_entryhdr where kode_tmp = '" & _kode.Text & "' and kodecab = '" & _cabang.Text & "' and Kode_Prd = '" & _periode.Text & "'")
                If dt.Rows.Count > 0 Then
                    Try
                        ddlAuditor1.SelectedValue = dt.Rows(0).Item("auditor_1")
                        ddlAuditor2.SelectedValue = dt.Rows(0).Item("auditor_2")
                        ddlAuditor3.SelectedValue = dt.Rows(0).Item("auditor_3")
                    Catch ex As Exception

                    End Try

                End If
            End If





            infoerror1.Text = ""
        Else
            infoerror1.Text = "Kriteria tidak boleh ada yang kosong"
        End If
    End Sub


    Private Sub CheckListAudit_Init(sender As Object, e As EventArgs) Handles Me.Init
        If Not Page.IsPostBack Then
            Dim myData As Data.DataSet
            Dim i As Integer
            Dim j As Integer

            Dim MyDT As DateTime = Now
            Dim MyDTString As String

            If Request.QueryString("a") <> "" Then
                myData = myChef.ICE_LOG_LOGIN_SEL(Request.QueryString("a"))
                If myData.Tables(0).Rows.Count > 0 Then
                    sUsername_.Text = myData.Tables(0).Rows(0).Item(1)
                    levelakses_.Text = myData.Tables(0).Rows(0).Item(2)
                    jabatan_.Text = myData.Tables(0).Rows(0).Item(3)
                    sCabang_.Text = myData.Tables(0).Rows(0).Item(4)
                    kodedept_.Text = myData.Tables(0).Rows(0).Item(5)
                Else
                    Response.Redirect("Default.aspx")
                End If
            ElseIf Session("sUsername") Is Nothing Then
                Response.Redirect("Default.aspx")
            Else
                MyDTString = Session("sUsername") & MyDT.ToString("MMddyyyyHHmmss")
                myChef.ICE_LOG_LOGIN_INS(MyDTString, Session("sUsername"), Session("levelakses"), Session("jabatan"), Session("sCabang"), Session("kodedept"))
                Response.Redirect("CheckListAudit_byPIC_Rekap.aspx?a=" & MyDTString)
            End If

            'updatepilhannilai(_kode.Text)
            hdnPengali.Value = PublicPengali
            'If Not IsPostBack Then

            'If sUsername_.Text = "" Then
            '    If Session("sUsername") Is Nothing Then
            '        Response.Redirect("Default.aspx")
            '    Else
            '        sUsername_.Text = Session("sUsername")
            '        levelakses_.Text = Session("levelakses")
            '        jabatan_.Text = Session("jabatan")
            '        sCabang_.Text = Session("sCabang")
            '        kodedept_.Text = Session("kodedept")
            '    End If
            'End If

            'End If

            '        If Session("levelakses") <> "1" And Session("levelakses") <> "2" Then           
            myData = myChef.SQLLISTTEMPLATE
            j = 1
            doTemplate.Items.Add("")
            For i = 0 To myData.Tables(0).Rows.Count - 1

                If (myData.Tables(0).Rows(i).Item("flag_selasses") = "Y" And sCabang_.Text <> "PST") Or
                sCabang_.Text = "PST" Then

                    'myData.Tables(0).Rows(i).Item("kodedept") = "" Or
                    '    If UCase(sUsername_.Text) = "SUPERADMIN" Or
                    '    kodedept_.Text = "3501" Or
                    'myData.Tables(0).Rows(i).Item("kodedept") = kodedept_.Text Then

                    doTemplate.Items.Add(myData.Tables(0).Rows(i).Item("nama_tmp"))
                        doTemplate.Items(j).Value = myData.Tables(0).Rows(i).Item("kode")
                        j = j + 1

                    'End If
                End If
            Next
            myData = myChef.SQLMSTPERIODEAKTIF
            j = 1
            doPeriode.Items.Add("")
            For i = 0 To myData.Tables(0).Rows.Count - 1

                doPeriode.Items.Add(myData.Tables(0).Rows(i).Item("nama_prd"))
                doPeriode.Items(j).Value = myData.Tables(0).Rows(i).Item("kode_prd")
                j = j + 1

            Next
            myData = myChef.SQLLISTCAB
            j = 1
            doBranch.Items.Add("")
            For i = 0 To myData.Tables(0).Rows.Count - 1

                If sCabang_.Text = "PST" Or sCabang_.Text = myData.Tables(0).Rows(i).Item("kodecab") Then

                    doBranch.Items.Add(myData.Tables(0).Rows(i).Item("namacab"))
                    doBranch.Items(j).Value = myData.Tables(0).Rows(i).Item("kodecab")
                    j = j + 1

                End If
            Next

            loadAuditor()

            If Session("kodedept") <> "3501" And Session("sUsername") <> "SUPERADMIN" Then
                divAuditor.Visible = False
            End If

            'txtDone1.Items.Add("Belum Dijalankan")
            'txtDone1.Items(0).Value = "0"
            'txtDone1.Items.Add("Sudah Dijalankan")
            'txtDone1.Items(1).Value = "1"
            'txtDone1.Items.Add("Tidak Dinilai")
            'txtDone1.Items(2).Value = "N/A"
            'txtDone2.Items.Add("Belum Dijalankan")
            'txtDone2.Items(0).Value = "0"
            'txtDone2.Items.Add("Sudah Dijalankan")
            'txtDone2.Items(1).Value = "1"
            'txtDone2.Items.Add("Tidak Dinilai")
            'txtDone2.Items(2).Value = "N/A"


            'txtDone1__1.Items.Add("Belum Dijalankan")
            'txtDone1__1.Items(0).Value = "0"
            'txtDone1__1.Items.Add("Sudah Dijalankan")
            'txtDone1__1.Items(1).Value = "1"
            'txtDone1__1.Items.Add("Tidak Dinilai")
            'txtDone1__1.Items(2).Value = "N/A"
            'txtDone2__2.Items.Add("Belum Dijalankan")
            'txtDone2__2.Items(0).Value = "0"
            'txtDone2__2.Items.Add("Sudah Dijalankan")
            'txtDone2__2.Items(1).Value = "1"
            'txtDone2__2.Items.Add("Tidak Dinilai")
            'txtDone2__2.Items(2).Value = "N/A"


            'doStatus.Items.Add("")
            'doStatus.Items.Add("OK")
            'doStatus.Items.Add("Not OK")
            'doStatus.Items.Add("Not Applicable")
            'doStatus.Items.Add("Observasi")


            doStatusFind.Items.Add("")
            doStatusFind.Items.Add("OK")
            doStatusFind.Items.Add("Not Applicable")
            doStatusFind.Items.Add("Not OK")

            doEntryFind.Items.Add("")
            doEntryFind.Items.Add("Complete")
            doEntryFind.Items.Add("InComplete")

            'Call sub_Refresh1()
        End If

    End Sub

    Protected Sub loadAuditor()
        ddlAuditor1.Items.Add(" ")
        ddlAuditor2.Items.Add(" ")
        ddlAuditor3.Items.Add(" ")

        Try
            Using con As New SqlConnection(strConn)
                con.Open()
                Dim da As New SqlDataAdapter("select * from mstauditor order by nama", con)
                Dim dt As New DataTable
                da.Fill(dt)
                If dt.Rows.Count > 0 Then
                    For i = 0 To dt.Rows.Count - 1
                        ddlAuditor1.Items.Add(New ListItem(dt.Rows(i).Item("nama"), dt.Rows(i).Item("nik")))
                        ddlAuditor2.Items.Add(New ListItem(dt.Rows(i).Item("nama"), dt.Rows(i).Item("nik")))
                        ddlAuditor3.Items.Add(New ListItem(dt.Rows(i).Item("nama"), dt.Rows(i).Item("nik")))
                    Next
                End If
                con.Close()
            End Using
        Catch ex As Exception

        End Try
    End Sub

    Private Sub myaddstr(ByVal mystr As String)
        myhtml.Append(mystr)
        myhtml1.Append(mystr)

    End Sub

    Sub loadICE_PIC_Rekap()
        Dim strsql As String
        strsql = "exec sp_ICE_byPIC_Rekap '" & doTemplate.SelectedValue & "','" & doBranch.SelectedValue & "','" & doPeriode.SelectedValue & "'"


        dt_penilaian = GetDataSql("select * from ICE_PENILAIAN_DTL1 where kode = (select NNILAI from TMP_CHECKLIST_HDR where kode = '" & doTemplate.SelectedValue & "') ")

        Dim dt As DataTable = GetDataSql(strsql)

        rptTable1.DataSource = dt
        rptTable1.DataBind()

        'If GridView1.Rows.Count > 0 Then
        '    'Adds THEAD And TBODY Section.
        '    GridView1.HeaderRow.TableSection = TableRowSection.TableHeader

        '    'Adds TH element in Header Row.  
        '    GridView1.UseAccessibleHeader = True

        '    'Adds TFOOT section. 
        '    GridView1.FooterRow.TableSection = TableRowSection.TableFooter
        'End If


    End Sub

    Private Sub sub_Refresh1()
        Dim myData As Data.DataSet
        '    Dim myData1 As Data.DataSet

        Dim i As Integer
        Dim myrownum As Integer = 0

        Dim vEntryDoneControl As Integer = 0

        'updatepilhannilai(_kode.Text)
        hdnPengali.Value = PublicPengali
        Dim checkConf As Boolean = myChef.SQLCONFIRMCHCK(_kode.Text, _periode.Text, _cabang.Text)

        Dim vflag_selasses As String = myChef.SQLCHECKLIST_SELASSES(_kode.Text)

        'myData = myChef.SQLCHECKLIST

        myhtml.Clear()
        myhtml1.Clear()

        myhtml.Append(" <table id='mytable' class='cell-border table table-striped table-bordered' cellspacing='0' width='100%'> ")
        myhtml1.Append("<div  aria-hidden='true' style='display: none '>")
        myhtml1.Append(" <table id='mytable1' cellspacing='0' width='100%'> ")
        myaddstr("   <thead>")
        myaddstr("      <tr>")
        myhtml.Append("         <th rowspan='2'></th>")
        myaddstr("         <th rowspan='2'>No</th>")
        myaddstr("         <th colspan='3'>Standar</th>")

        'myaddstr("         <th colspan='4'>1st Line Defence</th>")

        myhtml1.Append("         <th colspan='3'>1st Line Defence</th>")
        myhtml.Append("         <th colspan='4'>1st Line Defence</th>")

        myaddstr("         <th colspan='3'>2nd Line Defence</th>")
        myhtml.Append("         <th rowspan='2'></th>")
        myaddstr("         <th rowspan='2'>Element Control</th>")
        If PublicPengali = 1 Then
            myaddstr("         <th rowspan='2'>Done Control</th>")
        Else
            myaddstr("         <th rowspan='2'>Total Poin</br>Done Control</th>")
        End If
        myaddstr("         <th rowspan='2'>% Done</th>")

        'myaddstr("         <th colspan='7'>Working Paper</th>")

        myhtml1.Append("         <th colspan='3'>Working Paper</th>")
        myhtml.Append("         <th colspan='3'>Working Paper</th>")

        myhtml.Append("         <th rowspan='2'></th>")

        If vflag_selasses = "Y" And checkConf Then
            myhtml.Append("         <th rowspan='2'></th>")
            myaddstr("         <th rowspan='2'></th>")
            myaddstr("         <th rowspan='2'></th>")

        End If

        myaddstr("      </tr>")
        myaddstr("      <tr>")
        myaddstr("         <th>ISO</th>")
        myaddstr("         <th>Policy</th>")
        myaddstr("         <th>Lainnya</th>")
        myaddstr("         <th>Activity</th>")
        myaddstr("         <th>PIC</th>")
        If PublicPengali = 1 Then
            myaddstr("         <th>Done Control</th>")
        Else
            myaddstr("         <th>Poin Done</br>Control</th>")
        End If

        myhtml.Append("         <th></th>")
        myaddstr("         <th>Activity</th>")
        myaddstr("         <th>PIC</th>")
        If PublicPengali = 1 Then
            myaddstr("         <th>Done Control</th>")
        Else
            myaddstr("         <th>Poin Done</br>Control</th>")
        End If
        myaddstr("         <th>Status</th>")
        myaddstr("         <th>Hasil Audit</th>")
        myaddstr("         <th>Bukti Objektif</th>")
        myaddstr("         <th class='large-width'>Guidance</th>")
        myaddstr("      </tr>")
        myaddstr("   </thead>")
        myaddstr("   <tbody>")

        Dim MyNo As String = ""
        Dim sql_jabatan_audit As Boolean = myChef.SQLJABATANAUDIT(jabatan_.Text)
        myData = myChef.SQLCHECKLISTDTL_NEW_NEW(_kode.Text, _cabang.Text, _periode.Text, _statusFind.Text, _entryFind.Text)
        For i = 0 To myData.Tables(0).Rows.Count - 1
            Dim sql_jabatan_induk_pica1entry As Boolean = myChef.SQLJABATANINDUK(myData.Tables(0).Rows(i).Item("PICA1ENTRY"), jabatan_.Text)
            Dim sql_jabatan_induk_pica2entry As Boolean = myChef.SQLJABATANINDUK(myData.Tables(0).Rows(i).Item("PICA2ENTRY"), jabatan_.Text)
            PublicPengali = getMaxNilai(myData.Tables(0).Rows(i).Item("Kode_Pilihan_Nilai1").ToString, myData.Tables(0).Rows(i).Item("Kode_Pilihan_Nilai2").ToString)

            MyNo = myData.Tables(0).Rows(i).Item("no")
            vEntryDoneControl = 0

            If myData.Tables(0).Rows(i).Item("typeHD") = "H" Then
                myrownum = 0
            Else
                'myData1 = myChef.SQLchecklist_entry(_kode.Text, myData.Tables(0).Rows(i).Item("no"), _cabang.Text, _periode.Text)
                If IsDBNull(myData.Tables(0).Rows(i).Item("kode_tmp")) Then
                    myrownum = 0
                Else
                    myrownum = 1
                End If
            End If

            myaddstr("      <tr>")
            myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("no") & "</td>")
            myaddstr("         <td>" & myData.Tables(0).Rows(i).Item("no_report") & "</td>")
            If myData.Tables(0).Rows(i).Item("std_iso") = "Y" Then

                If myData.Tables(0).Rows(i).Item("dir_iso") = "" Then
                    myaddstr("         <td>" & myData.Tables(0).Rows(i).Item("std_iso") & "</td>")
                Else
                    myaddstr("         <td><a href=" & Chr(34) & "#" & Chr(34) & " onclick=" & Chr(34) & "openw('" & myData.Tables(0).Rows(i).Item("dir_ISO") & "');return false;" & Chr(34) & ">" & myData.Tables(0).Rows(i).Item("std_ISO") & "</a></td>")
                End If

            Else
                myaddstr("         <td></td>")
            End If
            If myData.Tables(0).Rows(i).Item("std_policy") = "Y" Then

                If myData.Tables(0).Rows(i).Item("dir_policy") = "" Then
                    myaddstr("         <td>" & myData.Tables(0).Rows(i).Item("std_policy") & "</td>")
                Else
                    myaddstr("         <td><a href=" & Chr(34) & "#" & Chr(34) & " onclick=" & Chr(34) & "openw('" & myData.Tables(0).Rows(i).Item("dir_policy") & "');return false;" & Chr(34) & ">" & myData.Tables(0).Rows(i).Item("std_policy") & "</a></td>")
                End If

            Else
                myaddstr("         <td></td>")
            End If
            If myData.Tables(0).Rows(i).Item("std_lain") = "Y" Then

                'myaddstr("         <td>" & myData.Tables(0).Rows(i).Item("std_lain") & "</td>")

                If myData.Tables(0).Rows(i).Item("dir_lain") = "" Then
                    myaddstr("         <td>" & myData.Tables(0).Rows(i).Item("std_lain") & "</td>")
                Else
                    myaddstr("         <td><a href=" & Chr(34) & "#" & Chr(34) & " onclick=" & Chr(34) & "openw('" & myData.Tables(0).Rows(i).Item("dir_lain") & "');return false;" & Chr(34) & ">" & myData.Tables(0).Rows(i).Item("std_lain") & "</a></td>")
                End If

            Else
                myaddstr("         <td></td>")
            End If

            myaddstr("         <td>" & myData.Tables(0).Rows(i).Item("activity1") & "</td>")
            myaddstr("         <td>" & myData.Tables(0).Rows(i).Item("PICA1") & "</td>")
            If myrownum = 0 Then
                myaddstr("         <td></td>")
                vEntryDoneControl = vEntryDoneControl + 1
            Else
                myaddstr("         <td>" & myData.Tables(0).Rows(i).Item("Done_MainControl") & "</td>")
                If Not IsDBNull(myData.Tables(0).Rows(i).Item("Done_MainControl")) Then
                    vEntryDoneControl = vEntryDoneControl + 100
                End If
            End If

            Dim Temps As String
            If myrownum = 0 Then
                Temps = _kode.Text & "|" & myData.Tables(0).Rows(i).Item("no") & "|" &
                    _cabang.Text & "|" & _periode.Text & "|" &
                    "0|0|||||||" &
                myData.Tables(0).Rows(i).Item("Activity1") & "|" &
                myData.Tables(0).Rows(i).Item("PICA1") & "|" &
                myData.Tables(0).Rows(i).Item("Activity2") & "|" &
                myData.Tables(0).Rows(i).Item("PICA2") & "||||||" &
                myData.Tables(0).Rows(i).Item("Kode_Pilihan_Nilai1") & "|" &
                myData.Tables(0).Rows(i).Item("Kode_Pilihan_Nilai2") & "|" &
                PublicPengali

            Else
                Temps = _kode.Text & "|" & myData.Tables(0).Rows(i).Item("no") & "|" &
                    _cabang.Text & "|" & _periode.Text & "|" &
                    myData.Tables(0).Rows(i).Item("Done_MainControl") & "|" &
                    myData.Tables(0).Rows(i).Item("Done_SecondControl") & "|" &
                    myData.Tables(0).Rows(i).Item("chk_status") & "|" &
                    myData.Tables(0).Rows(i).Item("hasil_audit") & "|" &
                    myData.Tables(0).Rows(i).Item("bukti_obyektif") & "|" &
                    myData.Tables(0).Rows(i).Item("Recommendation") & "|" &
                    myData.Tables(0).Rows(i).Item("Branch_FU") & "|" &
                    myData.Tables(0).Rows(i).Item("Implemen_Recom") & "|" &
                myData.Tables(0).Rows(i).Item("Activity1") & "|" &
                myData.Tables(0).Rows(i).Item("PICA1") & "|" &
                myData.Tables(0).Rows(i).Item("Activity2") & "|" &
                myData.Tables(0).Rows(i).Item("PICA2") & "|" &
                myData.Tables(0).Rows(i).Item("verifikasi_audit_status") & "|" &
                myData.Tables(0).Rows(i).Item("verifikasi_audit_remark") & "|" &
                myData.Tables(0).Rows(i).Item("hasil_audit_2") & "|" &
                    myData.Tables(0).Rows(i).Item("bukti_obyektif_2") & "|" &
                    myData.Tables(0).Rows(i).Item("Branch_FU_2") & "|" &
                    myData.Tables(0).Rows(i).Item("Kode_Pilihan_Nilai1") & "|" &
                    myData.Tables(0).Rows(i).Item("Kode_Pilihan_Nilai2") & "|" &
                    PublicPengali

            End If

            Dim Temps_1 As String = Temps

            If myData.Tables(0).Rows(i).Item("typeHD") = "H" Or checkConf Then
                myhtml.Append("         <td></td>")
            ElseIf vflag_selasses = "Y" Then
                If myData.Tables(0).Rows(i).Item("PICA1ENTRY") = jabatan_.Text Or
                    sql_jabatan_induk_pica1entry Then

                    'If IsDBNull(myData.Tables(0).Rows(i).Item("verifikasi_audit_status")) Then
                    '    myhtml.Append("<td class='text-right'>")
                    '    myhtml.Append("<button type='button' class='button2xx' data-book-id='" & Temps_1 & "' data-target='#formSelfReg01' data-toggle='modal'>Update</button>")
                    '    myhtml.Append("</td>")
                    'ElseIf myData.Tables(0).Rows(i).Item("verifikasi_audit_status") <> "OK" Then
                    '    myhtml.Append("<td class='text-right'>")
                    '    myhtml.Append("<button type='button' class='button2xx' data-book-id='" & Temps_1 & "' data-target='#formSelfReg01' data-toggle='modal'>Update</button>")
                    '    myhtml.Append("</td>")
                    'Else
                    '    myhtml.Append("         <td></td>")
                    'End If

                    If IsDBNull(myData.Tables(0).Rows(i).Item("verifikasi_audit_status")) OrElse myData.Tables(0).Rows(i).Item("verifikasi_audit_status").ToString() <> "OK" Then
                        myhtml.Append("<td class='text-right'>")
                        myhtml.Append("<button type='button' class='button2xx' data-book-id='" & Temps_1 & "' data-target='#formSelfReg01' data-toggle='modal'>Update</button>")
                        myhtml.Append("</td>")
                    Else
                        myhtml.Append("         <td></td>")
                    End If
                Else
                    myhtml.Append("         <td></td>")
                End If
            Else
                myhtml.Append("         <td></td>")
            End If



            myaddstr("         <td>" & myData.Tables(0).Rows(i).Item("activity2") & "</td>")
            myaddstr("         <td>" & myData.Tables(0).Rows(i).Item("PICA2") & "</td>")

            If myrownum = 0 Then
                myaddstr("         <td></td>")
                vEntryDoneControl = vEntryDoneControl + 1
            Else
                myaddstr("         <td>" & myData.Tables(0).Rows(i).Item("Done_SecondControl") & "</td>")
                If Not IsDBNull(myData.Tables(0).Rows(i).Item("Done_SecondControl")) Then
                    vEntryDoneControl = vEntryDoneControl + 100
                End If
            End If


            If myData.Tables(0).Rows(i).Item("typeHD") = "H" Or checkConf Then
                myhtml.Append("         <td></td>")
            ElseIf vflag_selasses = "Y" Then
                If myrownum <> 0 Then
                    If myData.Tables(0).Rows(i).Item("PICA2ENTRY") = jabatan_.Text Or
                        sql_jabatan_induk_pica2entry Then

                        'If IsDBNull(myData.Tables(0).Rows(i).Item("verifikasi_audit_status")) Then
                        '    myhtml.Append("<td class='text-right'>")
                        '    myhtml.Append("<button type='button' class='button2xx' data-book-id='" & Temps & "' data-target='#formModalEdit_2' data-toggle='modal'>Update</button>")
                        '    myhtml.Append("</td>")
                        'ElseIf myData.Tables(0).Rows(i).Item("verifikasi_audit_status") <> "OK" Then
                        '    myhtml.Append("<td class='text-right'>")
                        '    myhtml.Append("<button type='button' class='button2xx' data-book-id='" & Temps & "' data-target='#formModalEdit_2' data-toggle='modal'>Update</button>")
                        '    myhtml.Append("</td>")
                        'Else
                        '    myhtml.Append("         <td></td>")
                        'End If

                        If IsDBNull(myData.Tables(0).Rows(i).Item("verifikasi_audit_status")) OrElse myData.Tables(0).Rows(i).Item("verifikasi_audit_status").ToString() <> "OK" Then
                            myhtml.Append("<td class='text-right'>")
                            myhtml.Append("<button type='button' class='button2xx' data-book-id='" & Temps & "' data-target='#formModalEdit_2' data-toggle='modal'>Update</button>")
                            myhtml.Append("</td>")
                        Else
                            myhtml.Append("         <td></td>")
                        End If

                    Else
                        myhtml.Append("         <td></td>")
                    End If
                Else
                    myhtml.Append("         <td></td>")
                End If
            Else
                myhtml.Append("<td class='text-right'>")
                myhtml.Append("<button type='button' class='button2xx' data-book-id='" & Temps & "' data-target='#formModalEdit' data-toggle='modal'>Update</button>")
                myhtml.Append("</td>")
            End If


            If myrownum = 0 Then
                ' myaddstr("         <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>")
                myhtml1.Append("         <td></td><td></td><td></td><td></td><td></td><td></td>")
                myhtml.Append("         <td></td><td></td><td></td><td></td><td></td><td></td>")
            Else
                Dim vEleControl As Integer = 0
                Dim vDoneControl As Integer = 0
                Dim vDonePresent As Double = 0
                Dim tmpEleControl As String
                Dim tmpDoneControl As String
                Dim tmpDonePersent As String
                If IsDBNull(myData.Tables(0).Rows(i).Item("Done_MainControl")) Then
                    vEleControl = vEleControl + 1
                Else
                    If myData.Tables(0).Rows(i).Item("Done_MainControl") <> "N/A" Then
                        vEleControl = vEleControl + 1
                    End If
                End If
                If IsDBNull(myData.Tables(0).Rows(i).Item("Done_SecondControl")) Then
                    vEleControl = vEleControl + 1
                Else
                    If myData.Tables(0).Rows(i).Item("Done_SecondControl") <> "N/A" Then
                        vEleControl = vEleControl + 1
                    End If
                End If
                If Not IsDBNull(myData.Tables(0).Rows(i).Item("Done_MainControl")) Then
                    'If myData.Tables(0).Rows(i).Item("Done_MainControl") = "1" Then
                    'If myData.Tables(0).Rows(i).Item("Done_MainControl") = PublicPengali Then
                    '    vDoneControl = vDoneControl + 1
                    'End If
                    vDoneControl = vDoneControl + Val(myData.Tables(0).Rows(i).Item("Done_MainControl"))
                End If
                If Not IsDBNull(myData.Tables(0).Rows(i).Item("Done_SecondControl")) Then
                    'If myData.Tables(0).Rows(i).Item("Done_SecondControl") = "1" Then
                    'If myData.Tables(0).Rows(i).Item("Done_SecondControl") = PublicPengali Then
                    '    vDoneControl = vDoneControl + 1
                    'End If
                    vDoneControl = vDoneControl + Val(myData.Tables(0).Rows(i).Item("Done_SecondControl"))
                End If

                If vEleControl = 0 Then
                    vDonePresent = 0
                Else
                    'vDonePresent = vDoneControl / vEleControl * 100
                    'vDonePresent = vDoneControl / (vEleControl * Val(PublicPengali)) * 100
                    vDonePresent = vDoneControl / Val(PublicPengali) * 100
                End If
                If vEleControl = 0 Then
                    tmpEleControl = "N/A"
                    tmpDoneControl = "N/A"
                    tmpDonePersent = ""
                Else
                    tmpEleControl = vEleControl
                    tmpDoneControl = vDoneControl
                    tmpDonePersent = vDonePresent & "%"
                End If

                myaddstr("         <td style='text-align:right'>" & tmpEleControl & "</td><td style='text-align:right'>" & tmpDoneControl & "</td><td style='text-align:right'>" & tmpDonePersent & "</td>")
                myaddstr("         <td>" & myData.Tables(0).Rows(i).Item("chk_status") & "</td>")

                myaddstr("         <td>")
                If myData.Tables(0).Rows(i).Item("hasil_audit") <> "" Then
                    myaddstr("<b>1st:</b> " & myData.Tables(0).Rows(i).Item("hasil_audit") & "<br /><br />")
                End If
                If Not IsDBNull(myData.Tables(0).Rows(i).Item("hasil_audit_2")) Then
                    myaddstr("<b>2nd:</b> " & myData.Tables(0).Rows(i).Item("hasil_audit_2"))
                End If
                myaddstr("</td>")

                myaddstr("         <td>")
                If myData.Tables(0).Rows(i).Item("bukti_obyektif") <> "" Then
                    myaddstr("<b>1st:</b> <a href='bo/" & myData.Tables(0).Rows(i).Item("bukti_obyektif") & "'>" & myData.Tables(0).Rows(i).Item("bukti_obyektif") & "</a><br /><br />")
                End If
                If Not IsDBNull(myData.Tables(0).Rows(i).Item("bukti_obyektif_2")) Then
                    myaddstr("<b>2nd:</b> <a href='bo/" & myData.Tables(0).Rows(i).Item("bukti_obyektif_2") & "'>" & myData.Tables(0).Rows(i).Item("bukti_obyektif_2") & "</a>")
                End If
                myaddstr("</td>")

            End If


            If myData.Tables(0).Rows(i).Item("typeHD") = "D" Then
                'If vEntryDoneControl = 200 Then
                If IsDBNull(myData.Tables(0).Rows(i).Item("chk_status")) Then
                    myhtml.Append("         <td>EntryInComplete</td>")
                ElseIf (myData.Tables(0).Rows(i).Item("chk_status")).ToString.Length = 0 Then
                    myhtml.Append("         <td>EntryInComplete</td>")
                Else
                    myhtml.Append("         <td>EntryComplete</td>")
                End If
            Else
                myhtml.Append("         <td></td>")
            End If






            If vflag_selasses = "Y" And checkConf Then

                If myrownum = 0 Or myData.Tables(0).Rows(i).Item("typeHD") = "H" Then
                    myhtml.Append("         <td></td>")
                    myaddstr("         <td></td>")
                    myaddstr("         <td></td>")
                ElseIf sql_jabatan_audit Then

                    myhtml.Append("<td class='text-right'>")
                    myhtml.Append("<button type='button' class='button2xx' data-book-id='" & Temps & "' data-target='#formModalEdit_5' data-toggle='modal'>Verifikasi<br/>Audit</button>")
                    myhtml.Append("</td>")

                    'myData.Tables(0).Rows(i).Item("no")

                    myhtml.Append("         <td><asp:label id='" & MyNo & "' runat='server'>" & myData.Tables(0).Rows(i).Item("verifikasi_audit_status") & "<asp:label></td>")
                    myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("verifikasi_audit_remark") & "</td>")
                    myhtml1.Append("         <td>" & myData.Tables(0).Rows(i).Item("verifikasi_audit_status") & "</td>")
                    myhtml1.Append("         <td>" & myData.Tables(0).Rows(i).Item("verifikasi_audit_remark") & "</td>")

                Else
                    myhtml.Append("         <td></td>")
                    myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("verifikasi_audit_status") & "</td>")
                    myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("verifikasi_audit_remark") & "</td>")
                    myhtml1.Append("         <td>" & myData.Tables(0).Rows(i).Item("verifikasi_audit_status") & "</td>")
                    myhtml1.Append("         <td>" & myData.Tables(0).Rows(i).Item("verifikasi_audit_remark") & "</td>")

                End If

            End If


            myhtml.Append("<td class='large-width'>" & Regex.Replace(myData.Tables(0).Rows(i).Item("detail_guidance").ToString(), "\r\n?|\n", "<br />") & "</td>")


            myaddstr("      </tr>")
        Next

        myaddstr("   </tbody>")
        myaddstr(" </table> ")
        myhtml1.Append("</div>")

        'myHTMLTable.Text = myhtml.ToString() & "<br/>" & myhtml1.ToString()
        'MyHTMLString = myhtml.ToString() & "<br/>" & myhtml1.ToString()

        myPlaceH.Controls.Add(New Literal() With {
          .Text = myhtml.ToString() & "<br/>" & myhtml1.ToString()
         })

    End Sub


    Function getMaxNilai(ByVal kode_kriteria1 As String, ByVal kode_kriteria2 As String) As Integer
        Dim dt As DataTable = GetDataSql("select cast((select max(nilai) nilai_max from MST_KRITERIA_PENILAIAN_DETAIL where kode_kriteria = '" & kode_kriteria1 & "') as int) + cast((select max(nilai) nilai_max from MST_KRITERIA_PENILAIAN_DETAIL where kode_kriteria = '" & kode_kriteria2 & "') as int)")
        If dt.Rows.Count > 0 Then
            If IsDBNull(dt.Rows(0)(0)) Then
                Return 0
            Else
                Return dt.Rows(0)(0)
            End If
        End If
        Return 0
    End Function

    Private Sub sub_refresh2()

        Call sub_Refresh1()

        'MyHTMLString = myHTMLTable.Text
        'myPlaceH.Controls.Add(New Literal() With {
        '  .Text = MyHTMLString
        ' })

    End Sub



    Protected Sub InsertUpdateCommandSql(ByVal query As String)
        Try
            Using con As New SqlConnection(strConn)
                con.Open()
                cmd = New SqlCommand(query, con)
                cmd.ExecuteNonQuery()
                con.Close()
            End Using
        Catch ex As Exception
            Dim a As String = ex.Message.ToString
        End Try
    End Sub

    Protected Function GetDataSql(ByVal query As String) As DataTable
        Try
            Using con As New SqlConnection(strConn)
                con.Open()
                Dim da As New SqlDataAdapter(query, con)
                Dim dt As New DataTable
                da.Fill(dt)
                Return dt
                con.Close()
            End Using
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function getKriteriaPenilaian(ByVal kode_kriteria As String) As String
        Dim return_string As String = "["
        Try
            Using con As New SqlConnection(strConn)
                con.Open()
                Dim da As New SqlDataAdapter("select * from MST_KRITERIA_PENILAIAN_DETAIL where kode_kriteria = '" & kode_kriteria & "' order by nilai desc", con)
                Dim dt As New DataTable
                da.Fill(dt)
                con.Close()
                If dt.Rows.Count > 0 Then
                    For i = 0 To dt.Rows.Count - 1
                        return_string = return_string + "{""nilai"": """ & dt.Rows(i)("nilai") & """, ""penjelasan"": """ & dt.Rows(i)("nilai") & " - " & dt.Rows(i)("penjelasan") & """},"
                    Next
                    return_string = return_string.Remove(return_string.Length - 1, 1)
                End If
            End Using
        Catch ex As Exception

        End Try
        return_string = return_string & "]"
        Return return_string
    End Function
    Protected Sub GridView1_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then

        End If
    End Sub
    Protected Sub btnSaveAuditor_Click(sender As Object, e As EventArgs)

    End Sub
    Protected Sub rptTable1_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim item As RepeaterItem = e.Item

            Dim total_score As Decimal = Decimal.Parse((TryCast(item.FindControl("lblTotal_Score"), Label)).Text)

            Dim result_penilaian = From dr As DataRow In dt_penilaian.AsEnumerable()
                                   Where total_score >= dr.Field(Of Decimal)("nilai1") And total_score <= dr.Field(Of Decimal)("nilai2")
                                   Select dr

            Dim plhCategory As PlaceHolder = TryCast(item.FindControl("plhCategory"), PlaceHolder)

            If result_penilaian.Count <> 0 Then
                plhCategory.Controls.Add(New Literal() With {
          .Text = "<td style='background-color:" & result_penilaian(0)("warna").ToString() & "'>" & result_penilaian(0)("keterangan").ToString() & "</td>"
         })
            Else
                plhCategory.Controls.Add(New Literal() With {
          .Text = "<td></td>"
         })
            End If
        End If
    End Sub
End Class
