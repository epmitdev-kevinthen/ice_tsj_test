﻿
Partial Class NHHasil
    Inherits System.Web.UI.Page
    Dim myChef As New Chef
    Dim j, i As Integer

    Private Sub hdrCheckList_Init(sender As Object, e As EventArgs) Handles Me.Init
        lblkodetmp.Text = Session("kodetmp")
        Dim myData As Data.DataSet
        Dim myhtml As New StringBuilder

        myData = myChef.NILAIHDR1Q(lblkodetmp.Text)
        If myData.Tables(0).Rows.Count > 0 Then
            lblnamatmp.Text = myData.Tables(0).Rows(0).Item("keterangan")
        End If

        myData = myChef.NILAIDTL1(lblkodetmp.Text)
        myhtml.Append(" <table id='mytable1' class='cell-border' cellspacing='0' width='100%' style=' margin: 0 auto; width: 100%; clear: both; border-collapse: collapse; table-layout: fixed; word-wrap:break-word; '> ")
        myhtml.Append("   <thead>")
        myhtml.Append("      <tr>")
        myhtml.Append("         <th>Kode</th>")
        myhtml.Append("         <th></th>")
        myhtml.Append("         <th>Keterangan</th>")
        myhtml.Append("         <th>Nilai 1</th>")
        myhtml.Append("         <th>Nilai 2</th>")
        myhtml.Append("         <th>Warna</th>")
        myhtml.Append("         <th></th>")
        myhtml.Append("         <th></th>")
        myhtml.Append("      </tr>")
        myhtml.Append("   </thead>")
        myhtml.Append("   <tbody>")
        For i = 0 To myData.Tables(0).Rows.Count - 1
            myhtml.Append("      <tr>")

            Dim Temps As String = myData.Tables(0).Rows(i).Item("nilai_code") & "|" & myData.Tables(0).Rows(i).Item("keterangan") & "|" &
             myData.Tables(0).Rows(i).Item("nilai1") & "|" & myData.Tables(0).Rows(i).Item("nilai2") & "|" & myData.Tables(0).Rows(i).Item("warna")

            myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("nilai_code") & "</td>")

            myhtml.Append("<td class='text-right'>")
            myhtml.Append("<button type='button' class='button2xx' data-book-id='" & Temps & "' data-target='#formModalEdit' data-toggle='modal'>Edit</button>")
            myhtml.Append("</td>")

            myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("keterangan") & "</td>")
            myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("nilai1") & "</td>")
            myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("nilai2") & "</td>")
            myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("warna") & "</td>")
            myhtml.Append("         <td><svg width='55' height='30'><rect width='50' height='25' style='fill:" & myData.Tables(0).Rows(i).Item("warna") & ";stroke-width:1;stroke:rgb(0,0,0)' />.</svg></td>")

            myhtml.Append("<td class='text-right'>")
            myhtml.Append("<button type='button' class='button2xx' data-book-id='" & Temps & "' data-target='#formModalDelete' data-toggle='modal'>Delete</button>")
            myhtml.Append("</td>")

            myhtml.Append("      </tr>")
        Next
        myhtml.Append("   </tbody>")
        myhtml.Append(" </table> ")

        myPlaceH.Controls.Add(New Literal() With {
          .Text = myhtml.ToString()
         })

    End Sub


    Private Sub btnInsert3_Click(sender As Object, e As EventArgs) Handles btnInsert3.Click

        Try

            infoerror.Text = ""

            Dim mpLabel As Label
            Dim myusername As String = ""
            mpLabel = CType(Master.FindControl("lblusername"), Label)
            If Not mpLabel Is Nothing Then
                myusername = mpLabel.Text
            End If
            infoerror.Text = myChef.INSNILAIDTL1(lblkodetmp.Text, TxtNilai_Code3.Text, TxtKet3.Text, Val(TxtNilai13.Text), Val(TxtNilai23.Text), TxtWarna3.Text)
            Response.Redirect(HttpContext.Current.Request.Url.ToString(), True)
        Catch ex As Exception
            infoerror.Text = ex.Message
        End Try

    End Sub

    Private Sub btnDelete2_Click(sender As Object, e As EventArgs) Handles btnDelete2.Click


        Try

            infoerror.Text = ""
            infoerror.Text = myChef.DELNILAIDTL1(lblkodetmp.Text, TxtNilai_Code2.Text)
            Response.Redirect(HttpContext.Current.Request.Url.ToString(), True)
        Catch ex As Exception
            infoerror.Text = ex.Message
        End Try


    End Sub

    Private Sub btnUpdate1_Click(sender As Object, e As EventArgs) Handles btnUpdate1.Click

        Try

            infoerror.Text = ""

            Dim mpLabel As Label
            Dim myusername As String = ""
            mpLabel = CType(Master.FindControl("lblusername"), Label)
            If Not mpLabel Is Nothing Then
                myusername = mpLabel.Text
            End If
            infoerror.Text = myChef.UPDNILAIDTL1(lblkodetmp.Text, TxtNilai_Code.Text, TxtKet.Text, Val(TxtNilai1.Text), Val(TxtNilai2.Text), TxtWarna.Text)
            Response.Redirect(HttpContext.Current.Request.Url.ToString(), True)
        Catch ex As Exception
            infoerror.Text = ex.Message
        End Try


    End Sub

End Class
