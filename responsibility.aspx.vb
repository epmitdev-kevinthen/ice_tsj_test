﻿
Imports System.Data
Imports System.Data.SqlClient

Partial Class responsibility
    Inherits System.Web.UI.Page
    Public Shared ConnStrAudit As String = ConfigurationManager.ConnectionStrings("SQLICE").ConnectionString
    Public Shared cmd As New SqlCommand

    Protected Sub clearData_Click(sender As Object, e As EventArgs)
        Response.Redirect("KelPemeriksa.aspx")
    End Sub
    Protected Sub insertData_Click(sender As Object, e As EventArgs)
        If cekInsertedUser(ddlResponsibility.SelectedValue, ddlUser.SelectedValue) = 0 Then
            Dim con As New SqlConnection(ConnStrAudit)
            Dim sqlstr As String
            sqlstr = "INSERT INTO [dbo].[MSTRESPONSIBILITY]" &
    "           ([responsibility]" &
    "           ,[user_responsibility]" &
    "           ,[created_by]" &
    "           ,[creation_date])" &
    "     VALUES" &
    "           ('" & ddlResponsibility.SelectedValue & "'" &
    "           ,'" & ddlUser.SelectedValue & "'" &
    "           ,'" & Session("sUsername") & "'" &
    "           ,current_timestamp)"

            Try
                con.Open()
                cmd = New SqlCommand(sqlstr, con)
                cmd.ExecuteNonQuery()
                con.Close()

                div_lbl.Visible = True
                lbl_msg.Text = "Insert Successful!"
                tampilTable()
            Catch ex As Exception
                con.Close()
                div_lbl.Visible = True
                lbl_msg.Text = ex.Message.ToString
            End Try
        Else
            div_lbl.Visible = True
            lbl_msg.Text = "Responsibility dan User yang dipilih sudah terdaftar"
        End If

    End Sub
    Function cekInsertedUser(ByVal responsibility As String, ByVal user_responsibility As String) As Integer
        Dim dt As DataTable = GetDataSql("select count(1) from [MSTRESPONSIBILITY] where responsibility = '" & responsibility & "' and user_responsibility = '" & user_responsibility & "'")
        If dt.Rows.Count > 0 Then
            Return dt.Rows(0).Item(0)
        Else
            Return 0
        End If
    End Function
    Protected Sub deleteData_Click(sender As Object, e As EventArgs)
        If cekInsertedUser(ddlResponsibility.SelectedValue, ddlUser.SelectedValue) = 1 Then
            Dim sqlstr As String = "delete from [MSTRESPONSIBILITY] where responsibility = '" & ddlResponsibility.SelectedValue & "' and user_responsibility = '" & ddlUser.SelectedValue & "'"
            Try
                Using con As New SqlConnection(ConnStrAudit)
                    con.Open()
                    cmd = New SqlCommand(sqlstr, con)
                    cmd.ExecuteNonQuery()
                    con.Close()
                End Using

                div_lbl.Visible = True
                lbl_msg.Text = "Delete Successful!"
                tampilTable()
            Catch ex As Exception
                div_lbl.Visible = True
                lbl_msg.Text = ex.Message.ToString
            End Try
        Else
            div_lbl.Visible = True
            lbl_msg.Text = "Responsibility dan User yang dipilih belum pernah terdaftar"
        End If

    End Sub

    Private Sub responsibility_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then

            tampilTable()
            loadResponsibilityUser()


        End If
    End Sub

    Private Sub loadResponsibilityUser()
        Dim dt As DataTable = GetDataSql("select kode, jabatan, flag_cabangpusat from MSTJABATAN order by flag_cabangpusat,jabatan")
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                ddlResponsibility.Items.Add(New ListItem(dt.Rows(i).Item(2) + " - " + dt.Rows(i).Item(1), dt.Rows(i).Item(0)))
                ddlUser.Items.Add(New ListItem(dt.Rows(i).Item(2) + " - " + dt.Rows(i).Item(1), dt.Rows(i).Item(0)))
            Next
        End If
    End Sub

    Protected Sub tampilTable()
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                Dim da As New SqlDataAdapter("select *, a.responsibility + '-' + (select jabatan from MSTJABATAN where kode = a.responsibility) as responsibility_jabatan, a.user_responsibility + '-' + (select jabatan from MSTJABATAN where kode = a.user_responsibility) as user_jabatan from [MSTRESPONSIBILITY] a order by id", con)
                Dim dt As New DataTable
                da.Fill(dt)
                GridView1.DataSource = dt
                GridView1.DataBind()
                GridView1.HeaderRow.TableSection = TableRowSection.TableHeader
                con.Close()
            End Using
        Catch ex As Exception

        End Try

    End Sub
    Protected Sub GridView1_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        tampilTable()
    End Sub
    Public Shared Function GetDataSql(ByVal query As String) As DataTable
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                Dim da As New SqlDataAdapter(query, con)
                Dim dt As New DataTable
                da.Fill(dt)
                con.Close()
                Return dt
            End Using
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Protected Sub updateData_Click(sender As Object, e As EventArgs)
        InsertUpdateCommandSql("update MSTRESPONSIBILITY set responsibility = '" & ddlResponsibility.SelectedValue & "', user_responsibility = '" & ddlUser.SelectedValue & "' where id = '" & hdnIdResponsibility.Value & "'")
        div_lbl.Visible = True
        lbl_msg.Text = "Update Successful!"
        tampilTable()
    End Sub
    Protected Sub InsertUpdateCommandSql(ByVal query As String)

        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                cmd = New SqlCommand(query, con)
                cmd.ExecuteNonQuery()
                con.Close()
            End Using
        Catch ex As Exception
            lbl_msg.Text = ex.Message.ToString
        End Try
    End Sub
End Class
