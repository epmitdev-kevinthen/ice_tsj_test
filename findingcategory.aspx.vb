﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class findingcategory
    Inherits System.Web.UI.Page

    Public Shared ConnStrAudit As String = ConfigurationManager.ConnectionStrings("SQLICE").ConnectionString
    Public Shared cmd As New SqlCommand

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        If Not Page.IsPostBack Then
            userEdit.Text = Request.QueryString("kode")
            If userEdit.Text = "" Then
                tampilTable()
            Else
                tampilTable()
                tampil()
                'insertData.Style("display") = "none"
            End If

        End If
    End Sub
    Protected Sub tampilTable()
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                Dim da As New SqlDataAdapter("select * from MSTFINDINGCATEGORY order by kodecategory", con)
                Dim dt As New DataTable
                da.Fill(dt)
                GridView1.DataSource = dt
                GridView1.DataBind()
                GridView1.HeaderRow.TableSection = TableRowSection.TableHeader
                con.Close()
            End Using
        Catch ex As Exception

        End Try

    End Sub
    Protected Sub GridView1_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        tampilTable()
    End Sub
    Protected Sub clearData_Click(sender As Object, e As EventArgs)
        Response.Redirect("findingcategory.aspx")
    End Sub
    Protected Sub insertData_Click(sender As Object, e As EventArgs)
        Dim sqlstr As String = "insert into MSTFINDINGCATEGORY(kodecategory, namacategory, kel_pemeriksa, created_by, creation_date, last_update_by, last_update_date) values('" & txtKode.Text & "', '" & txtCategory.Text & "', '" & txtKelPemeriksa.Text & "', '" & Session("sUsername") & "', current_timestamp, '" & Session("sUsername") & "', current_timestamp)"
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                cmd = New SqlCommand(sqlstr, con)
                cmd.ExecuteNonQuery()
                con.Close()
            End Using

            div_lbl.Visible = True
            lbl_msg.Text = "Insert Successful!"
            tampilTable()
        Catch ex As Exception
            div_lbl.Visible = True
            lbl_msg.Text = ex.Message.ToString
        End Try
    End Sub
    Protected Sub updateData_Click(sender As Object, e As EventArgs)
        Dim sqlstr As String = "update MSTFINDINGCATEGORY set kodecategory = '" & txtKode.Text & "', namacategory = '" & txtCategory.Text & "', kel_pemeriksa = '" & txtKelPemeriksa.Text & "', last_update_by = '" & Session("sUsername") & "', last_update_date = current_timestamp where kodecategory='" & txtKode.Text & "'"
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                cmd = New SqlCommand(sqlstr, con)
                cmd.ExecuteNonQuery()
                con.Close()
            End Using

            div_lbl.Visible = True
            lbl_msg.Text = "Update Successful!"
            tampilTable()
        Catch ex As Exception
            div_lbl.Visible = True
            lbl_msg.Text = ex.Message.ToString
        End Try
    End Sub
    Protected Sub deleteData_Click(sender As Object, e As EventArgs)
        Dim sqlstr As String = "delete from MSTFINDINGCATEGORY where kodecategory = '" & txtKode.Text & "'"
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                cmd = New SqlCommand(sqlstr, con)
                cmd.ExecuteNonQuery()
                con.Close()
            End Using

            div_lbl.Visible = True
            lbl_msg.Text = "Delete Successful!"
            tampilTable()
        Catch ex As Exception
            div_lbl.Visible = True
            lbl_msg.Text = ex.Message.ToString
        End Try
    End Sub
    Protected Sub GridView1_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GridView1.RowCommand
        If e.CommandName = "cmdsubmit" Then
            Dim kode As String = e.CommandArgument
            Response.Redirect("findingcategory.aspx?kode=" & kode & "")
        ElseIf e.CommandName = "cmddel" Then


        End If
    End Sub
    Protected Sub tampil()
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                Dim da As New SqlDataAdapter("select * from MSTFINDINGCATEGORY where kode = '" & userEdit.Text & "'", con)
                Dim dt As New DataTable
                da.Fill(dt)
                If dt.Rows.Count > 0 Then
                    txtKode.Text = dt.Rows(0).Item("kodecategory")
                    txtCategory.Text = dt.Rows(0).Item("namacategory")
                    createdBy.InnerText = dt.Rows(0).Item("created_by")
                    createdDate.InnerText = dt.Rows(0).Item("creation_date")
                    lastUpdateBy.InnerText = dt.Rows(0).Item("last_update_by")
                    lastUpdateDate.InnerText = dt.Rows(0).Item("last_update_date")
                End If
                con.Close()
            End Using
        Catch ex As Exception

        End Try

    End Sub

End Class
