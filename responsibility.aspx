﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="responsibility.aspx.vb" Inherits="responsibility" MasterPageFile="~/MasterPage.master"%>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="script/jquery-1.4.1.min.js"></script>
    <script>
        $(document).ready(function () {
            var table = $("#ContentPlaceHolder1_GridView1").DataTable({
                paging: true,
                searching: true,
                lengthChange: false,
            });
            $('#ContentPlaceHolder1_GridView1 tbody').on('click', 'tr', function () {
                var data = table.row(this).data();

                $("#<%=hdnIdResponsibility.ClientID%>").val(data[0]);                
                $("#<%=ddlResponsibility.ClientID%>").val(data[1]).change();
                $("#<%=ddlUser.ClientID%>").val(data[2]).change();
                $("#ContentPlaceHolder1_createdBy").text(data[5]);
                $("#ContentPlaceHolder1_createdDate").text(data[6]);
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <asp:Label ID="lblHead" runat="server" Text="Master Responsibility"
        Style="font-size: 20px;"></asp:Label>
    <div>
        <asp:Label ID="userEdit" runat="server"></asp:Label>
    </div>
    <div style="display: flex;">
        <asp:HiddenField id="hdnIdResponsibility" runat="server"/>
        <table>
            <tr>
                <td>Responsibility</td>
                <td>:</td>
                <td>
                    <asp:DropDownList ID="ddlResponsibility" runat="server"></asp:DropDownList></td>
            </tr>
            <tr>
                <td>User</td>
                <td>:</td>
                <td>
                    <asp:DropDownList ID="ddlUser" runat="server"></asp:DropDownList></td>
            </tr>
        </table>
        <div style="float: right; margin-left: 15px;">
            Created By: <span id="createdBy" runat="server"></span>
            <br />
            Creation Date: <span id="createdDate" runat="server"></span>
            <br />
        </div>
    </div>
    <br />
    <br />
    <div id="div_lbl" runat="server">
        <asp:Label ID="lbl_msg" runat="server"></asp:Label>
    </div>
    <asp:Button ID="clearData" runat="server" Text="Clear Data" OnClick="clearData_Click" CssClass="button2"/>
    <asp:Button ID="insertData" runat="server" Text="Insert Data" OnClick="insertData_Click" CssClass="button2"/>    
    <asp:Button ID="updateData" runat="server" Text="Update Data" OnClick="updateData_Click" CssClass="button2"/>
    <asp:Button ID="deleteData" runat="server" Text="Delete Data" OnClick="deleteData_Click" CssClass="button2"/>

    <div>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" AllowPaging="false" AllowSorting="false"
            PageSize="10" CssClass="dataTable">
            <Columns>
                <asp:BoundField DataField="id" HeaderText="ID" HeaderStyle-CssClass="hidden" ItemStyle-CssClass="hidden"/>
                <asp:BoundField DataField="responsibility" HeaderText="Responsibility" HeaderStyle-CssClass="hidden" ItemStyle-CssClass="hidden"/>
                <asp:BoundField DataField="user_responsibility" HeaderText="Responsibility" HeaderStyle-CssClass="hidden" ItemStyle-CssClass="hidden"/>
                <asp:BoundField DataField="responsibility_jabatan" HeaderText="Responsibility" />
                <asp:BoundField DataField="user_jabatan" HeaderText="User" />
                <asp:Boundfield DataField="created_by" HeaderText="RBM" HeaderStyle-CssClass="hidden" ItemStyle-CssClass="hidden"/>
                <asp:Boundfield DataField="creation_date" HeaderText="RBM" >
                    <HeaderStyle CssClass="hidden"></HeaderStyle>

                        <ItemStyle CssClass="hidden"></ItemStyle>
                </asp:Boundfield>
            </Columns>
            <PagerStyle CssClass="paginate_button" />
        </asp:GridView>
    </div>
</asp:Content>


