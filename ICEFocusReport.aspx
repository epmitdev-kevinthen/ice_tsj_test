﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ICEFocusReport.aspx.vb" Inherits="ICEFocusReport" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <title>Internal Control Effectiveness</title>

    <link rel="icon" href="image/Logoplikasi_Layer 2.png">


    <script>

        function fnExcelReport() {
            var tab_text = "<table border='2px'><tr bgcolor='#87AFC6'>";
            var textRange; var j = 0;
            tab = document.getElementById('TabReport'); // id of table

            for (j = 0 ; j < tab.rows.length ; j++) {
                tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
                //tab_text=tab_text+"</tr>";
            }

            tab_text = tab_text + "</table>";
            tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
            tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
            tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");

            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
            {
                txtArea1.document.open("txt/html", "replace");
                txtArea1.document.write(tab_text);
                txtArea1.document.close();
                txtArea1.focus();
                sa = txtArea1.document.execCommand("SaveAs", true, "Say Thanks to Sumit.xls");
            }
            else                 //other browser not tested on IE 11
                sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

            return (sa);
        }

    </script>



    <style>
table {
    border-collapse: collapse;
    border: 1px solid black;
    width: 100%;
}

th, td {
    text-align: left;
    border: 1px solid black;    
    padding: 8px;
}


th {
    background-color: white ;
    color: black;
}
</style>

    <!-- style
        tr:nth-child(even){background-color: #f2f2f2}
-->


</head>
<body>
    <form id="form1" runat="server">

        <iframe id="txtArea1" style="display:none"></iframe>

        <table style="width: 400px">
            <tr>
               <td>Nama Laporan</td>
               <td>
               <asp:Label ID="Label1" runat="server">ICE Focus (Focus Internal Control Effectiveness By Activity)</asp:Label> 
               </td>
                <td rowspan="4">

                <asp:Label ID="dohist" runat="server"></asp:Label><br />
                <asp:Button ID="btnback" Text="Back" runat="server" />
                </td>

                <td rowspan="4">
                <button id="btnExport" onclick="fnExcelReport();">Export to Excel</button>
                    </td>
            </tr>
            <tr>
               <td>Cabang</td>
               <td>
               <asp:Label ID="_CabangKode" runat="server"></asp:Label> - 
               <asp:Label ID="_Cabang" runat="server"></asp:Label>
               </td>
            </tr>
            <tr>
               <td>CheckList</td>
               <td>
               <asp:Label ID="_tmpkode" runat="server"></asp:Label> - 
               <asp:Label ID="_tmp" runat="server"></asp:Label>
               </td>
            </tr>
            <tr>
               <td>Periode</td>
               <td>
               <asp:Label ID="_periodekd" runat="server"></asp:Label> - 
               <asp:Label ID="_periode" runat="server"></asp:Label>
               </td>
            </tr>
        </table>
        <br />
    <div style="width: 1600px">
<asp:PlaceHolder ID="myPlaceH" runat="server"></asp:PlaceHolder>    
    </div>
    </form>
</body>
</html>
