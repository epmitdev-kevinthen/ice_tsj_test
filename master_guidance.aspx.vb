﻿
Imports System.Data
Imports System.Data.SqlClient

Partial Class master_guidance
    Inherits System.Web.UI.Page



    Public Shared ConnStrAudit As String = ConfigurationManager.ConnectionStrings("SQLICE").ConnectionString
    Public Shared cmd As New SqlCommand

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        If Not Page.IsPostBack Then
            tampilTable()

        End If
    End Sub

    Public Shared Function GetDataSql(ByVal query As String) As DataTable
        Dim con As New SqlConnection(ConnStrAudit)
        Try
            con.Open()
            Dim da As New SqlDataAdapter(query, con)
            Dim dt As New DataTable
            da.Fill(dt)
            con.Close()
            Return dt
        Catch ex As Exception
            con.Close()
            Return Nothing
        End Try
    End Function

    Protected Sub tampilTable()
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                Dim da As New SqlDataAdapter("select * from MST_GUIDANCE order by kode_guidance", con)
                Dim dt As New DataTable
                da.Fill(dt)
                GridView1.DataSource = dt
                GridView1.DataBind()
                GridView1.HeaderRow.TableSection = TableRowSection.TableHeader
                con.Close()
            End Using
        Catch ex As Exception

        End Try

    End Sub
    Protected Sub GridView1_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        tampilTable()
    End Sub
    Protected Sub clearData_Click(sender As Object, e As EventArgs)
        Response.Redirect("master_guidance.aspx")
    End Sub
    Protected Sub insertData_Click(sender As Object, e As EventArgs)
        Dim con As New SqlConnection(ConnStrAudit)
        Dim sqlstr As String = "insert into mst_guidance(kode_guidance, nama_guidance, detail_guidance, createdby, createdat, lasteditby, lasteditat) values('" & txtKode_Guidance.Text & "', '" & txtNama_Guidance.Text & "', '" & txtDetail_Guidance.Text & "', '" & Session("sUsername") & "', current_timestamp, '" & Session("sUsername") & "', current_timestamp)"
        Try
            con.Open()
            cmd = New SqlCommand(sqlstr, con)
            cmd.ExecuteNonQuery()
            con.Close()

            div_lbl.Visible = True
            lbl_msg.Text = "Insert Successful!"
            tampilTable()
        Catch ex As Exception
            con.Close()
            div_lbl.Visible = True
            lbl_msg.Text = ex.Message.ToString
        End Try
    End Sub
    Protected Sub updateData_Click(sender As Object, e As EventArgs)
        Dim sqlstr As String = "update mst_guidance set kode_guidance = '" & txtKode_Guidance.Text & "', nama_guidance = '" & txtNama_Guidance.Text & "', lasteditby = '" & Session("sUsername") & "', lasteditat = current_timestamp, detail_guidance = '" & txtDetail_Guidance.Text & "' where kode_guidance='" & txtKode_Guidance.Text & "'"
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                cmd = New SqlCommand(sqlstr, con)
                cmd.ExecuteNonQuery()
                con.Close()
            End Using

            div_lbl.Visible = True
            lbl_msg.Text = "Update Successful!"
            tampilTable()
        Catch ex As Exception
            div_lbl.Visible = True
            lbl_msg.Text = ex.Message.ToString
        End Try
    End Sub
    Protected Sub deleteData_Click(sender As Object, e As EventArgs)
        Dim sqlstr As String = "delete from mst_guidance where kode_guidance = '" & txtKode_Guidance.Text & "'"
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                cmd = New SqlCommand(sqlstr, con)
                cmd.ExecuteNonQuery()
                con.Close()
            End Using

            div_lbl.Visible = True
            lbl_msg.Text = "Delete Successful!"
            tampilTable()
        Catch ex As Exception
            div_lbl.Visible = True
            lbl_msg.Text = ex.Message.ToString
        End Try
    End Sub
End Class
