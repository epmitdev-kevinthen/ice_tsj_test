﻿Imports System.Data
Imports System.Data.SqlClient


Partial Class AksesUser
    Inherits System.Web.UI.Page

    Public Shared ConnDB As String = ConfigurationManager.ConnectionStrings("SQLICE").ConnectionString
    Public oconn As New SqlConnection(ConnDB)
    Public Shared cmd As New SqlCommand
    Public trans As SqlTransaction

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        If Not Page.IsPostBack Then

            tampilJabatan()
            tampilCabang()
            tampilLevelAkses()
            tampilKodeDept()

        End If
    End Sub

    Protected Sub showDiv(sender As Object, e As EventArgs) Handles btnCkUsrnm.Click

        Dim i As DataSet

        i = cekUsername()

        If i IsNot Nothing Then

            If i.Tables(0).Rows.Count > 0 Then

                If i.Tables(0).Rows(0).Item("flag_akses_ice").ToString() = "Y" Then

                    cbICE.Checked = True

                End If

                If i.Tables(0).Rows(0).Item("flag_akses_sensus").ToString() = "Y" Then

                    cbSensus.Checked = True

                End If

                Dim script As String = "window.onload = function() { showDiv(); };"

                ClientScript.RegisterStartupScript(Me.GetType(), "showDiv", script, True)

            End If

        Else

            Dim script As String = "window.onload = function() { noUsrnm(); };"

            ClientScript.RegisterStartupScript(Me.GetType(), "noUsrnm", script, True)

        End If


    End Sub

    Protected Function cekUsername() As DataSet

        Try

            Using con As New SqlConnection(ConnDB)

                con.Open()
                Dim da As New SqlDataAdapter("SELECT * FROM MSTUSER WHERE username = '" + username.Text + "'", con)
                Dim ds As New DataSet
                da.Fill(ds)

                If ds.Tables(0).Rows.Count > 0 Then

                    Return ds

                End If

            End Using

        Catch ex As Exception

        End Try

    End Function

    Protected Sub updateFlag(sender As Object, e As EventArgs) Handles btnAdd.Click

        If cbICE.Checked = False And cbSensus.Checked = False Then

            Dim script As String = "window.onload = function() { noSbmt(); };"

            ClientScript.RegisterStartupScript(Me.GetType(), "noSbmt", script, True)

        Else

            Dim sqlstr As String = "update MSTUSER set flag_akses_ice = "

            If cbICE.Checked = True Then
                sqlstr = sqlstr + "'Y'"
            Else
                sqlstr = sqlstr + "'N'"
            End If

            sqlstr = sqlstr + ", flag_akses_sensus = "

            If cbSensus.Checked = True Then
                sqlstr = sqlstr + "'Y'"
            Else
                sqlstr = sqlstr + "'N'"
            End If

            sqlstr = sqlstr + " where username = '" + username.Text + "'"

            Try
                Using con As New SqlConnection(ConnDB)
                    con.Open()
                    cmd = New SqlCommand(sqlstr, con)
                    cmd.ExecuteNonQuery()
                    con.Close()
                End Using

                cbICE.Checked = False
                cbSensus.Checked = False

                Dim script As String = "window.onload = function() { alrtSuccess(); };"

                ClientScript.RegisterStartupScript(Me.GetType(), "alrtSuccess", script, True)

            Catch ex As Exception

            End Try

        End If

    End Sub

    'Protected Sub addUser(sender As Object, e As EventArgs) Handles btnAddUser.Click

    '    If usernameAdd.Text = "" Then

    '        Dim script As String = "window.onload = function() { alrtDataKosong(); };"

    '        ClientScript.RegisterStartupScript(Me.GetType(), "alrtDataKosong", script, True)

    '    ElseIf userpass.Text = "" Then

    '        Dim script As String = "window.onload = function() { alrtDataKosong(); };"

    '        ClientScript.RegisterStartupScript(Me.GetType(), "alrtDataKosong", script, True)

    '    ElseIf fullName.Text = "" Then

    '        Dim script As String = "window.onload = function() { alrtDataKosong(); };"

    '        ClientScript.RegisterStartupScript(Me.GetType(), "alrtDataKosong", script, True)

    '    ElseIf nik.Text = "" Then

    '        Dim script As String = "window.onload = function() { alrtDataKosong(); };"

    '        ClientScript.RegisterStartupScript(Me.GetType(), "alrtDataKosong", script, True)

    '    ElseIf jabatan.Text = "" Then

    '        Dim script As String = "window.onload = function() { alrtDataKosong(); };"

    '        ClientScript.RegisterStartupScript(Me.GetType(), "alrtDataKosong", script, True)

    '    ElseIf email.Text = "" Then

    '        Dim script As String = "window.onload = function() { alrtDataKosong(); };"

    '        ClientScript.RegisterStartupScript(Me.GetType(), "alrtDataKosong", script, True)

    '    ElseIf kodeDept.Text = "" Then

    '        Dim script As String = "window.onload = function() { alrtDataKosong(); };"

    '        ClientScript.RegisterStartupScript(Me.GetType(), "alrtDataKosong", script, True)

    '    ElseIf nohp.Text = "" Then

    '        Dim script As String = "window.onload = function() { alrtDataKosong(); };"

    '        ClientScript.RegisterStartupScript(Me.GetType(), "alrtDataKosong", script, True)

    '    Else

    '        Dim sqlstr = "insert into MSTUSER values ('" + usernameAdd.Text + "', '" + userpass.Text + "', '" + fullName.Text + "', '" + nik.Text + "',"
    '        If jenisKelamin.Text = "Laki-Laki" Then
    '            sqlstr = sqlstr + " 'L', "
    '        Else
    '            sqlstr = sqlstr + " 'P', "
    '        End If
    '        sqlstr = sqlstr + "'" + kodeJabatan.Text + "', '" + jabatan.Text + "', '" + kodeCabang.Text + "', '" + email.Text + "', " + levelAkses.Text + ", '" + Session("sUsername") + "', GETDATE(), '" + Session("sUsername")
    '        sqlstr = sqlstr + "', GETDATE(), '" + kodeDept.Text + "', '" + department.Text + "', '" + nohp.Text + "', 'Y', 'N')"

    '        Dim stat = ExecuteDataSQL(sqlstr)

    '        If stat = 1 Then

    '            Dim script As String = "window.onload = function() { alrtAddSuceed(); };"

    '            ClientScript.RegisterStartupScript(Me.GetType(), "alrtAddSuceed", script, True)

    '        Else

    '            Dim script As String = "window.onload = function() { alrtAddFailed(); };"

    '            ClientScript.RegisterStartupScript(Me.GetType(), "alrtAddFailed", script, True)

    '        End If


    '        'insert into MSTUSER values ('hakeemsaputra', 'hakeemsaputra', 'hakeemsaputra', 'hakeemsaputra', 'hakeemsaputra', 'hakeemsaputra', 'hakeemsaputra', 'hakeemsaputra', 'hakeemsaputra', 1, 'hakeemsaputra', GETDATE(), 
    '        ''hakeemsaputra', GETDATE(), 'hakeemsaputra', 'hakeemsaputra', 'hakeemsaputra', 'Y', 'N')

    '    End If


    'End Sub

    Function ExecuteDataSQL(ByVal strSQL As String) As Integer

        If oconn.State = ConnectionState.Open Then
            oconn.Close()
        End If
        oconn.Open()

        cmd = New SqlCommand
        trans = oConn.BeginTransaction()

        Try
            With cmd
                .Transaction = trans
                .Connection = oconn
                .CommandType = CommandType.Text
                .CommandText = strSQL
                .ExecuteNonQuery()
                trans.Commit()
            End With
            Return 1
        Catch ex As Exception
            trans.Rollback()
            Return 0
        End Try

        If oconn.State = ConnectionState.Open Then
            oconn.Close()
        End If
    End Function

    Protected Sub tampilJabatan()
        Try
            Using con As New SqlConnection(ConnDB)
                con.Open()
                Dim da As New SqlDataAdapter("select * from MSTJABATAN order by jabatan", con)
                Dim dt As New DataTable
                da.Fill(dt)
                If dt.Rows.Count > 0 Then
                    For i = 0 To dt.Rows.Count - 1
                        ddlJabatan.Items.Add(New ListItem(dt.Rows(i).Item("jabatan"), dt.Rows(i).Item("kode") & "-" & dt.Rows(i).Item("jabatan")))
                    Next
                End If
                con.Close()
            End Using
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub tampilCabang()
        Try
            Using con As New SqlConnection(ConnDB)
                con.Open()
                Dim da As New SqlDataAdapter("select * from MSTCAB", con)
                Dim dt As New DataTable
                da.Fill(dt)
                If dt.Rows.Count > 0 Then
                    For i = 0 To dt.Rows.Count - 1
                        ddlCabang.Items.Add(New ListItem(dt.Rows(i).Item("kodecab") & "-" & dt.Rows(i).Item("namacab"), dt.Rows(i).Item("kodecab")))
                    Next
                End If
                con.Close()
            End Using
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub tampilLevelAkses()
        If Session("levelakses") = 1 Then
            ddlLvlAkses.Items.Add(New ListItem("1 - IT ADMINISTRATOR", "1"))
            ddlLvlAkses.Items.Add(New ListItem("2 - AUDIT ADMINISTRATOR", "2"))
            ddlLvlAkses.Items.Add(New ListItem("3 - AUDITOR", "3"))
            ddlLvlAkses.Items.Add(New ListItem("4 - AUDITEE", "4"))
            ddlLvlAkses.Items.Add(New ListItem("5 - USER", "5"))
        Else
            ddlLvlAkses.Items.Add(New ListItem("2 - AUDIT ADMINISTRATOR", "2"))
            ddlLvlAkses.Items.Add(New ListItem("3 - AUDITOR", "3"))
            ddlLvlAkses.Items.Add(New ListItem("4 - AUDITEE", "4"))
            ddlLvlAkses.Items.Add(New ListItem("5 - USER", "5"))
        End If

    End Sub

    Protected Sub tampilKodeDept()
        Try
            Using con As New SqlConnection(ConnDB)
                con.Open()
                Dim da As New SqlDataAdapter("select * from MSTDIREKTORAT", con)
                Dim dt As New DataTable
                da.Fill(dt)
                If dt.Rows.Count > 0 Then
                    For i = 0 To dt.Rows.Count - 1
                        ddlKodeDept.Items.Add(New ListItem(dt.Rows(i).Item("kode") & "-" & dt.Rows(i).Item("direktorat"), dt.Rows(i).Item("kode") & "-" & dt.Rows(i).Item("direktorat")))
                    Next
                End If
                con.Close()
            End Using
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub insertData_Click(sender As Object, e As EventArgs)

        If txtUserName.Text = "" Or txtPass.Text = "" Or txtFullUserName.Text = "" Or txtNIK.Text = "" Or ddlJabatan.SelectedValue = "" Or ddlCabang.SelectedValue = "" Or txtEmail.Text = "" Or ddlKodeDept.SelectedValue = "" Or txtNoWA.Text = "" Then

            Dim script As String = "window.onload = function() { alrtDataKosong(); };"

            ClientScript.RegisterStartupScript(Me.GetType(), "alrtDataKosong", script, True)

        Else

            If Regex.IsMatch(txtNoWA.Text, "^[0-9 ]+$") And Regex.IsMatch(txtNIK.Text, "^[0-9 ]+$") Then

                Dim selectedjabatan As String = ddlJabatan.SelectedValue
                Dim selectedDept As String = ddlKodeDept.SelectedValue
                Dim kodejabatan As String = selectedjabatan.Substring(0, selectedjabatan.IndexOf("-"))
                Dim jabatan As String = selectedjabatan.Substring(selectedjabatan.IndexOf("-") + 1, selectedjabatan.Count - 1 - selectedjabatan.IndexOf("-"))
                Dim kodedept As String = Nothing
                Dim dept As String = Nothing
                If selectedDept <> "" Then
                    kodedept = selectedDept.Substring(0, selectedDept.IndexOf("-"))
                    dept = selectedDept.Substring(selectedDept.IndexOf("-") + 1, selectedDept.Count - 1 - selectedDept.IndexOf("-"))
                End If
                Dim sqlstr As String = "insert into mstuser(username,userpass,fullusername,jk,kodejabatan,jabatan,kodecab,email,levelakses,created_by,creation_date,last_update_by,last_update_date,kodedept,dept,nik,no_wa, flag_akses_ice, flag_akses_sensus) values ('" & txtUserName.Text & "', '" & txtPass.Text & "', '" & txtFullUserName.Text & "', '" & ddlJK.SelectedValue & "', '" & kodejabatan & "', '" & jabatan & "', '" & ddlCabang.SelectedValue & "', '" & txtEmail.Text & "', '" & ddlLvlAkses.SelectedValue & "', '" & Session("sUsername") & "', current_timestamp, '" & Session("sUsername") & "', current_timestamp, '" & kodedept & "', '" & dept & "','" & txtNIK.Text & "','" & txtNoWA.Text & "', 'N', 'Y')"
                Try
                    Using con As New SqlConnection(ConnDB)
                        con.Open()
                        cmd = New SqlCommand(sqlstr, con)
                        cmd.ExecuteNonQuery()
                        con.Close()
                    End Using

                    Dim script As String = "window.onload = function() { alrtAddSuceed(); };"

                    ClientScript.RegisterStartupScript(Me.GetType(), "alrtAddFailed", script, True)

                Catch ex As Exception

                    Dim script As String = "window.onload = function() { alrtAddFailed(); };"

                    ClientScript.RegisterStartupScript(Me.GetType(), "alrtAddFailed", script, True)

                End Try

            Else

                Dim script As String = "window.onload = function() { alrtDataNumOnly(); };"

                ClientScript.RegisterStartupScript(Me.GetType(), "alrtDataNumOnly", script, True)

            End If

        End If
    End Sub
    Protected Sub clearData_Click(sender As Object, e As EventArgs)
        txtUserName.Text = ""
        txtPass.Text = ""
        txtFullUserName.Text = ""
        ddlJabatan.SelectedValue = ""
        ddlCabang.SelectedValue = ""
        txtEmail.Text = ""
        ddlKodeDept.SelectedValue = ""
    End Sub

    Private Sub initTable(sender As Object, e As EventArgs) Handles Me.Init

        Dim myData As DataSet
        Dim myHtml As New StringBuilder

        myData = getAllUserData()

        myHtml.Append(" <table id='mytable' class='stripe row-border order-column' cellspacing='0' width='100%'> ")
        myHtml.Append("   <thead>")
        myHtml.Append("      <tr>")
        myHtml.Append("         <th>Username</th>")
        myHtml.Append("         <th>Password</th>")
        myHtml.Append("         <th>Full Username</th>")
        myHtml.Append("         <th>JK</th>")
        myHtml.Append("         <th>Jabatan</th>")
        myHtml.Append("         <th>Cabang</th>")
        myHtml.Append("         <th>Email</th>")
        myHtml.Append("         <th>Level Akses</th>")
        myHtml.Append("         <th>Dept</th>")
        myHtml.Append("         <th>NIK</th>")
        myHtml.Append("         <th>No. WA</th>")
        myHtml.Append("         <th>Details</th>")
        myHtml.Append("      </tr>")
        myHtml.Append("   </thead>")
        myHtml.Append("   <tbody>")

        For i = 0 To myData.Tables(0).Rows.Count - 1

            myHtml.Append("      <tr>")
            myHtml.Append("         <td style='text-align:center'>" & myData.Tables(0).Rows(i).Item("username").ToString() & "</td>")
            myHtml.Append("         <td style='text-align:center'>" & myData.Tables(0).Rows(i).Item("userpass").ToString() & "</td>")
            myHtml.Append("         <td style='text-align:left'>" & myData.Tables(0).Rows(i).Item("fullusername").ToString() & "</td>")
            myHtml.Append("         <td style='text-align:left'>" & myData.Tables(0).Rows(i).Item("jk").ToString() & "</td>")
            myHtml.Append("         <td style='text-align:left'>" & myData.Tables(0).Rows(i).Item("jabatan").ToString() & "</td>")
            myHtml.Append("         <td style='text-align:left'>" & myData.Tables(0).Rows(i).Item("kodecab").ToString() & "</td>")
            myHtml.Append("         <td style='text-align:left'>" & myData.Tables(0).Rows(i).Item("email").ToString() & "</td>")
            myHtml.Append("         <td style='text-align:left'>" & myData.Tables(0).Rows(i).Item("levelakses").ToString() & "</td>")
            myHtml.Append("         <td style='text-align:left'>" & myData.Tables(0).Rows(i).Item("dept").ToString() & "</td>")
            myHtml.Append("         <td style='text-align:left'>" & myData.Tables(0).Rows(i).Item("nik").ToString() & "</td>")
            myHtml.Append("         <td style='text-align:left'>" & myData.Tables(0).Rows(i).Item("no_wa").ToString() & "</td>")
            myHtml.Append("         <td><button type='button' class='button2'>Details</button></td>")
            myHtml.Append("      </tr>")

        Next

        myHtml.Append("   </tbody>")
        myHtml.Append(" </table> ")

        tblUser.Controls.Add(New Literal() With {
            .Text = myHtml.ToString()
        })

    End Sub

    Public Function getAllUserData() As DataSet

        Dim strSQL As String
        strSQL = "SELECT * FROM MSTUSER"

        Dim ds As DataSet = createDS(strSQL)
        Return ds

    End Function

    Public Function createDS(ByVal strSQL As String, Optional ByVal sqlParam As SqlParameter = Nothing) As DataSet

        If oconn.State = ConnectionState.Open Then
            oconn.Close()
        End If
        oconn.Open()

        Dim scmd As New SqlCommand(strSQL, oconn)
        scmd.CommandTimeout = 600

        If Not IsNothing(sqlParam) Then
            scmd.Parameters.Add(sqlParam)
        End If

        Dim sda As New SqlDataAdapter(scmd)
        Dim ds As New DataSet
        sda.Fill(ds)

        Return ds
        If oconn.State = ConnectionState.Open Then
            oconn.Close()
        End If

    End Function

End Class
