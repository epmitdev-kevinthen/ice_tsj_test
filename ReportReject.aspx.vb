﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.OracleClient
Imports Oracle.DataAccess.Client
Imports ClosedXML.Excel
Imports System.IO
Partial Class ReportReject
    Inherits System.Web.UI.Page

    Public Shared ConnDB As String = ConfigurationManager.ConnectionStrings("SQLICE").ConnectionString
    Public Shared oConnDB As New SqlConnection(ConnDB)
    Public Shared cmd As New SqlCommand

    Dim wsICE As New SPKTrxData.SPKTrxData

    'Public oraDB As String = ConfigurationManager.ConnectionStrings("oracon").ConnectionString
    'Public oraConn As New OracleConnection(oraDB) ' VB.NET
    'Public oraCmd As New OracleCommand
    'Public oraDR As OracleDataReader

    'Sub OpenOracleConnection()
    '    If oraConn.State = Data.ConnectionState.Closed Then
    '        oraConn.Open()
    '    End If
    'End Sub

    'Sub CloseOracleConnection()
    '    If oraConn.State = Data.ConnectionState.Open Then
    '        oraConn.Close()
    '    End If
    'End Sub

    Private Sub initTable(sender As Object, e As EventArgs) Handles Me.Init

        Dim myData As DataSet
        Dim myHtml As New StringBuilder
        Dim dtime, atime As DateTime
        Dim deto, aeto As String

        If Session("sUsername") Is Nothing Then
            Response.Redirect("Default.aspx")
        End If

        myData = getAllData()

        myHtml.Append(" <table id='mytable' class='stripe row-border order-column' cellspacing='0' width='100%'> ")
        myHtml.Append("   <thead>")
        myHtml.Append("      <tr>")
        myHtml.Append("         <th>ID</th>")
        myHtml.Append("         <th>Kode Cabang</th>")
        myHtml.Append("         <th>Customer Number</th>")
        myHtml.Append("         <th>Customer Name</th>")
        myHtml.Append("         <th>Submit Date</th>")
        myHtml.Append("         <th>Submit By</th>")
        myHtml.Append("         <th>Approve Date</th>")
        myHtml.Append("         <th>Approve By</th>")
        myHtml.Append("         <th></th>")
        myHtml.Append("      </tr>")
        myHtml.Append("   </thead>")
        myHtml.Append("   <tbody>")

        For i = 0 To myData.Tables(0).Rows.Count - 1

            myHtml.Append("      <tr>")
            myHtml.Append("         <td style='text-align:right'>" & myData.Tables(0).Rows(i).Item("ID").ToString() & "</td>")
            myHtml.Append("         <td style='text-align:left;width:30px'>" & myData.Tables(0).Rows(i).Item("ORG_CODE").ToString() & "</td>")
            myHtml.Append("         <td style='text-align:right;width:30px'>" & myData.Tables(0).Rows(i).Item("CUSTOMER_NUMBER").ToString() & "</td>")
            myHtml.Append("         <td style='text-align:left'>" & myData.Tables(0).Rows(i).Item("CUSTOMER_NAME").ToString() & "</td>")
            dtime = Convert.ToDateTime(myData.Tables(0).Rows(i).Item("SUBMIT_DATE").ToString())
            deto = dtime.ToString("dd-MMM-yyyy  hh:mm:ss")
            myHtml.Append("         <td style='text-align:right'>" & deto & "</td>")
            myHtml.Append("         <td style='text-align:left'>" & myData.Tables(0).Rows(i).Item("SUBMITTED_BY").ToString() & "</td>")
            atime = Convert.ToDateTime(myData.Tables(0).Rows(i).Item("VALIDATION_DATE").ToString())
            aeto = atime.ToString("dd-MMM-yyyy hh:mm:ss")
            myHtml.Append("         <td style='text-align:right;width:100px'>" & aeto & "</td>")
            myHtml.Append("         <td style='text-align:left'>" & myData.Tables(0).Rows(i).Item("APPROVED_BY").ToString() & "</td>")
            'myHtml.Append("         <td><button type='button'>Details</button></td>")
            'myHtml.Append("         <td><button type='button' onclick='showDiv(" & myData.Tables(0).Rows(i).Item("ID") & ", " & myData.Tables(0).Rows(i).Item("CUSTOMER_NUMBER") & ", " & myData.Tables(0).Rows(i).Item("CUSTOMER_NAME") &
            '              ", " & myData.Tables(0).Rows(i).Item("SHIP_TO_ID") & ", " & myData.Tables(0).Rows(i).Item("ALAMAT") & ", " & myData.Tables(0).Rows(i).Item("NO_TELEPON") & ", " & myData.Tables(0).Rows(i).Item("NAMA_PEMILIK") &
            '              ", " & myData.Tables(0).Rows(i).Item("NO_HP_PEMILIK") & ", " & myData.Tables(0).Rows(i).Item("NAMA_PIC_PEMBAYARAN") & ", " & myData.Tables(0).Rows(i).Item("NO_HP_PIC_PEMBAYARAN") &
            '              ", " & myData.Tables(0).Rows(i).Item("NAMA_PIC_PEMBELIAN") & ", " & myData.Tables(0).Rows(i).Item("NO_HP_PIC_PEMBELIAN") & ", " & myData.Tables(0).Rows(i).Item("NAMA_PIC_GUDANG") &
            '              ", " & myData.Tables(0).Rows(i).Item("NO_HP_PIC_GUDANG") & ", " & myData.Tables(0).Rows(i).Item("NAMA_PIC_APJ") & ", " & myData.Tables(0).Rows(i).Item("NO_HP_PIC_APJ") &
            '              ", " & myData.Tables(0).Rows(i).Item("NAMA_PIC_TAX") & ", " & myData.Tables(0).Rows(i).Item("NO_HP_PIC_TAX") & ", " & myData.Tables(0).Rows(i).Item("PROSEDUR_PEMBAYARAN") &
            '              ", " & myData.Tables(0).Rows(i).Item("CARA_PEMBAYARAN") & ")'>Details</button></td>")
            'myHtml.Append("         <td><button type='button' onclick=showDiv('" & myData.Tables(0).Rows(i).Item("ID").ToString() & "','" & myData.Tables(0).Rows(i).Item("ID").ToString() & "')>Details</button></td>")
            'myHtml.Append("         <td><button type='button' onclick=showDiv(1,1)>Details</button></td>")
            myHtml.Append("         <td><button type='button' class='button2' onclick=showDiv(" & myData.Tables(0).Rows(i).Item("ID") & ")>Pilih</button></td>")

            myHtml.Append("      </tr>")

        Next

        myHtml.Append("   </tbody>")
        myHtml.Append(" </table> ")

        tblApproval.Controls.Add(New Literal() With {
            .Text = myHtml.ToString()
        })

    End Sub

    Public Function getAllData() As DataSet

        Dim strSQL As String
        If Session("sCabang") = "PST" Then
            strSQL = "SELECT * FROM APPROVAL_DATA_CUSTOMER where STATUS = 'DITOLAK'"
        Else
            strSQL = "SELECT * FROM APPROVAL_DATA_CUSTOMER where STATUS = 'DITOLAK' AND ORG_CODE = '" + Session("sCabang") + "'"
        End If


        Dim ds As DataSet = createDS(strSQL)
        Return ds

    End Function

    Public Function createDS(ByVal strSQL As String, Optional ByVal sqlParam As SqlParameter = Nothing) As DataSet

        If oConnDB.State = ConnectionState.Open Then
            oConnDB.Close()
        End If
        oConnDB.Open()

        Dim scmd As New SqlCommand(strSQL, oConnDB)
        scmd.CommandTimeout = 600

        If Not IsNothing(sqlParam) Then
            scmd.Parameters.Add(sqlParam)
        End If

        Dim sda As New SqlDataAdapter(scmd)
        Dim ds As New DataSet
        sda.Fill(ds)

        Return ds
        If oConnDB.State = ConnectionState.Open Then
            oConnDB.Close()
        End If

    End Function

    'Show Detail///////////////////////////////////////////////////////////////////////
    Protected Sub showDetail(sender As Object, e As EventArgs) Handles btnDetail.Click

        Dim ds As DataSet
        ds = getDetails(ID.Text)
        Dim index As Int16
        Dim value As String
        Dim dtime As Date
        Dim deto As String


        If ds.Tables(0).Rows.Count > 0 Then

            'If ds.Tables(0).Rows(0).Item("CUSTOMER_NUMBER").ToString().Contains("'") Then

            '    index = ds.Tables(0).Rows(0).Item("CUSTOMER_NUMBER").ToString().IndexOf("'")

            '    cusNum.Text = ds.Tables(0).Rows(0).Item("CUSTOMER_NUMBER").ToString().Remove(index, 1)

            'Else

            '    cusNum.Text = ds.Tables(0).Rows(0).Item("CUSTOMER_NUMBER").ToString()

            'End If

            value = ds.Tables(0).Rows(0).Item("CUSTOMER_NUMBER").ToString()
            While value.Contains("'")

                index = value.IndexOf("'")
                value = value.Remove(index, 1)

            End While
            While value.Contains("#")

                index = value.IndexOf("#")
                value = value.Remove(index, 1)

            End While
            cusNum.Text = value

            'If ds.Tables(0).Rows(0).Item("CUSTOMER_NAME").ToString().Contains("'") Then

            '    index = ds.Tables(0).Rows(0).Item("CUSTOMER_NAME").ToString().IndexOf("'")

            '    cusName.Text = ds.Tables(0).Rows(0).Item("CUSTOMER_NAME").ToString().Remove(index, 1)

            'Else

            '    cusName.Text = ds.Tables(0).Rows(0).Item("CUSTOMER_NAME").ToString()

            'End If

            value = ds.Tables(0).Rows(0).Item("CUSTOMER_NAME").ToString()
            While value.Contains("'")

                index = value.IndexOf("'")
                value = value.Remove(index, 1)

            End While
            While value.Contains("#")

                index = value.IndexOf("#")
                value = value.Remove(index, 1)

            End While
            cusName.Text = value

            'If ds.Tables(0).Rows(0).Item("SHIP_TO_ID").ToString().Contains("'") Then

            '    index = ds.Tables(0).Rows(0).Item("SHIP_TO_ID").ToString().IndexOf("'")

            '    shipToId.Text = ds.Tables(0).Rows(0).Item("SHIP_TO_ID").ToString().Remove(index, 1)

            'Else

            '    shipToId.Text = ds.Tables(0).Rows(0).Item("SHIP_TO_ID").ToString()

            'End If

            value = ds.Tables(0).Rows(0).Item("SHIP_TO_ID").ToString()
            While value.Contains("'")

                index = value.IndexOf("'")
                value = value.Remove(index, 1)

            End While
            While value.Contains("#")

                index = value.IndexOf("#")
                value = value.Remove(index, 1)

            End While
            shipToId.Text = value

            'If ds.Tables(0).Rows(0).Item("ALAMAT").ToString().Contains("'") Then

            '    index = ds.Tables(0).Rows(0).Item("ALAMAT").ToString().IndexOf("'")

            '    alamat.Text = ds.Tables(0).Rows(0).Item("ALAMAT").ToString().Remove(index, 1)

            'Else

            '    alamat.Text = ds.Tables(0).Rows(0).Item("ALAMAT").ToString()

            'End If

            value = ds.Tables(0).Rows(0).Item("ALAMAT").ToString()
            While value.Contains("'")

                index = value.IndexOf("'")
                value = value.Remove(index, 1)

            End While
            While value.Contains("#")

                index = value.IndexOf("#")
                value = value.Remove(index, 1)

            End While
            alamat.Text = value

            'If ds.Tables(0).Rows(0).Item("NO_TELEPON").ToString().Contains("'") Then

            '    index = ds.Tables(0).Rows(0).Item("NO_TELEPON").ToString().IndexOf("'")

            '    noTlp.Text = ds.Tables(0).Rows(0).Item("NO_TELEPON").ToString().Remove(index, 1)

            'Else

            '    noTlp.Text = ds.Tables(0).Rows(0).Item("NO_TELEPON").ToString()

            'End If

            value = ds.Tables(0).Rows(0).Item("NO_TELEPON").ToString()
            While value.Contains("'")

                index = value.IndexOf("'")
                value = value.Remove(index, 1)

            End While
            While value.Contains("#")

                index = value.IndexOf("#")
                value = value.Remove(index, 1)

            End While
            noTlp.Text = value

            'If ds.Tables(0).Rows(0).Item("NAMA_PEMILIK").ToString().Contains("'") Then

            '    index = ds.Tables(0).Rows(0).Item("NAMA_PEMILIK").ToString().IndexOf("'")

            '    nmPmlk.Text = ds.Tables(0).Rows(0).Item("NAMA_PEMILIK").ToString().Remove(index, 1)

            'Else

            '    nmPmlk.Text = ds.Tables(0).Rows(0).Item("NAMA_PEMILIK").ToString()

            'End If

            value = ds.Tables(0).Rows(0).Item("NAMA_PEMILIK").ToString()
            While value.Contains("'")

                index = value.IndexOf("'")
                value = value.Remove(index, 1)

            End While
            While value.Contains("#")

                index = value.IndexOf("#")
                value = value.Remove(index, 1)

            End While
            nmPmlk.Text = value

            'If ds.Tables(0).Rows(0).Item("NO_HP_PEMILIK").ToString().Contains("'") Then

            '    index = ds.Tables(0).Rows(0).Item("NO_HP_PEMILIK").ToString().IndexOf("'")

            '    noHpPmlk.Text = ds.Tables(0).Rows(0).Item("NO_HP_PEMILIK").ToString().Remove(index, 1)

            'Else

            '    noHpPmlk.Text = ds.Tables(0).Rows(0).Item("NO_HP_PEMILIK").ToString()

            'End If

            value = ds.Tables(0).Rows(0).Item("NO_HP_PEMILIK").ToString()
            While value.Contains("'")

                index = value.IndexOf("'")
                value = value.Remove(index, 1)

            End While
            While value.Contains("#")

                index = value.IndexOf("#")
                value = value.Remove(index, 1)

            End While
            noHpPmlk.Text = value

            'If ds.Tables(0).Rows(0).Item("NAMA_PIC_PEMBAYARAN").ToString().Contains("'") Then

            '    index = ds.Tables(0).Rows(0).Item("NAMA_PIC_PEMBAYARAN").ToString().IndexOf("'")

            '    picByr.Text = ds.Tables(0).Rows(0).Item("NAMA_PIC_PEMBAYARAN").ToString().Remove(index, 1)

            'Else

            '    picByr.Text = ds.Tables(0).Rows(0).Item("NAMA_PIC_PEMBAYARAN").ToString()

            'End If

            value = ds.Tables(0).Rows(0).Item("NAMA_PIC_PEMBAYARAN").ToString()
            While value.Contains("'")

                index = value.IndexOf("'")
                value = value.Remove(index, 1)

            End While
            While value.Contains("#")

                index = value.IndexOf("#")
                value = value.Remove(index, 1)

            End While
            picByr.Text = value

            'If ds.Tables(0).Rows(0).Item("NO_HP_PIC_PEMBAYARAN").ToString().Contains("'") Then

            '    index = ds.Tables(0).Rows(0).Item("NO_HP_PIC_PEMBAYARAN").ToString().IndexOf("'")

            '    hpPicByr.Text = ds.Tables(0).Rows(0).Item("NO_HP_PIC_PEMBAYARAN").ToString().Remove(index, 1)

            'Else

            '    hpPicByr.Text = ds.Tables(0).Rows(0).Item("NO_HP_PIC_PEMBAYARAN").ToString()

            'End If

            value = ds.Tables(0).Rows(0).Item("NO_HP_PIC_PEMBAYARAN").ToString()
            While value.Contains("'")

                index = value.IndexOf("'")
                value = value.Remove(index, 1)

            End While
            While value.Contains("#")

                index = value.IndexOf("#")
                value = value.Remove(index, 1)

            End While
            hpPicByr.Text = value

            'If ds.Tables(0).Rows(0).Item("NAMA_PIC_PEMBELIAN").ToString().Contains("'") Then

            '    index = ds.Tables(0).Rows(0).Item("NAMA_PIC_PEMBELIAN").ToString().IndexOf("'")

            '    picBeli.Text = ds.Tables(0).Rows(0).Item("NAMA_PIC_PEMBELIAN").ToString().Remove(index, 1)

            'Else

            '    picBeli.Text = ds.Tables(0).Rows(0).Item("NAMA_PIC_PEMBELIAN").ToString()

            'End If

            value = ds.Tables(0).Rows(0).Item("NAMA_PIC_PEMBELIAN").ToString()
            While value.Contains("'")

                index = value.IndexOf("'")
                value = value.Remove(index, 1)

            End While
            While value.Contains("#")

                index = value.IndexOf("#")
                value = value.Remove(index, 1)

            End While
            picBeli.Text = value

            'If ds.Tables(0).Rows(0).Item("NO_HP_PIC_PEMBELIAN").ToString().Contains("'") Then

            '    index = ds.Tables(0).Rows(0).Item("NO_HP_PIC_PEMBELIAN").ToString().IndexOf("'")

            '    hpPicBeli.Text = ds.Tables(0).Rows(0).Item("NO_HP_PIC_PEMBELIAN").ToString().Remove(index, 1)

            'Else

            '    hpPicBeli.Text = ds.Tables(0).Rows(0).Item("NO_HP_PIC_PEMBELIAN").ToString()

            'End If

            value = ds.Tables(0).Rows(0).Item("NO_HP_PIC_PEMBELIAN").ToString()
            While value.Contains("'")

                index = value.IndexOf("'")
                value = value.Remove(index, 1)

            End While
            While value.Contains("#")

                index = value.IndexOf("#")
                value = value.Remove(index, 1)

            End While
            hpPicBeli.Text = value

            'If ds.Tables(0).Rows(0).Item("NAMA_PIC_GUDANG").ToString().Contains("'") Then

            '    index = ds.Tables(0).Rows(0).Item("NAMA_PIC_GUDANG").ToString().IndexOf("'")

            '    picgdg.Text = ds.Tables(0).Rows(0).Item("NAMA_PIC_GUDANG").ToString().Remove(index, 1)

            'Else

            '    picgdg.Text = ds.Tables(0).Rows(0).Item("NAMA_PIC_GUDANG").ToString()

            'End If

            value = ds.Tables(0).Rows(0).Item("NAMA_PIC_GUDANG").ToString()
            While value.Contains("'")

                index = value.IndexOf("'")
                value = value.Remove(index, 1)

            End While
            While value.Contains("#")

                index = value.IndexOf("#")
                value = value.Remove(index, 1)

            End While
            picgdg.Text = value

            'If ds.Tables(0).Rows(0).Item("NO_HP_PIC_GUDANG").ToString().Contains("'") Then

            '    index = ds.Tables(0).Rows(0).Item("NO_HP_PIC_GUDANG").ToString().IndexOf("'")

            '    hpPicGdg.Text = ds.Tables(0).Rows(0).Item("NO_HP_PIC_GUDANG").ToString().Remove(index, 1)

            'Else

            '    hpPicGdg.Text = ds.Tables(0).Rows(0).Item("NO_HP_PIC_GUDANG").ToString()

            'End If

            value = ds.Tables(0).Rows(0).Item("NO_HP_PIC_GUDANG").ToString()
            While value.Contains("'")

                index = value.IndexOf("'")
                value = value.Remove(index, 1)

            End While
            While value.Contains("#")

                index = value.IndexOf("#")
                value = value.Remove(index, 1)

            End While
            hpPicGdg.Text = value

            'If ds.Tables(0).Rows(0).Item("NAMA_PIC_APJ").ToString().Contains("'") Then

            '    index = ds.Tables(0).Rows(0).Item("NAMA_PIC_APJ").ToString().IndexOf("'")

            '    picApj.Text = ds.Tables(0).Rows(0).Item("NAMA_PIC_APJ").ToString().Remove(index, 1)

            'Else

            '    picApj.Text = ds.Tables(0).Rows(0).Item("NAMA_PIC_APJ").ToString()

            'End If

            value = ds.Tables(0).Rows(0).Item("NAMA_PIC_APJ").ToString()
            While value.Contains("'")

                index = value.IndexOf("'")
                value = value.Remove(index, 1)

            End While
            While value.Contains("#")

                index = value.IndexOf("#")
                value = value.Remove(index, 1)

            End While
            picApj.Text = value

            'If ds.Tables(0).Rows(0).Item("NO_HP_PIC_APJ").ToString().Contains("'") Then

            '    index = ds.Tables(0).Rows(0).Item("NO_HP_PIC_APJ").ToString().IndexOf("'")

            '    hpPicApj.Text = ds.Tables(0).Rows(0).Item("NO_HP_PIC_APJ").ToString().Remove(index, 1)

            'Else

            '    hpPicApj.Text = ds.Tables(0).Rows(0).Item("NO_HP_PIC_APJ").ToString()

            'End If

            value = ds.Tables(0).Rows(0).Item("NO_HP_PIC_APJ").ToString()
            While value.Contains("'")

                index = value.IndexOf("'")
                value = value.Remove(index, 1)

            End While
            While value.Contains("#")

                index = value.IndexOf("#")
                value = value.Remove(index, 1)

            End While
            hpPicApj.Text = value

            'If ds.Tables(0).Rows(0).Item("NAMA_PIC_TAX").ToString().Contains("'") Then

            '    index = ds.Tables(0).Rows(0).Item("NAMA_PIC_TAX").ToString().IndexOf("'")

            '    picTax.Text = ds.Tables(0).Rows(0).Item("NAMA_PIC_TAX").ToString().Remove(index, 1)

            'Else

            '    picTax.Text = ds.Tables(0).Rows(0).Item("NAMA_PIC_TAX").ToString()

            'End If

            value = ds.Tables(0).Rows(0).Item("NAMA_PIC_TAX").ToString()
            While value.Contains("'")

                index = value.IndexOf("'")
                value = value.Remove(index, 1)

            End While
            While value.Contains("#")

                index = value.IndexOf("#")
                value = value.Remove(index, 1)

            End While
            picTax.Text = value

            'If ds.Tables(0).Rows(0).Item("NO_HP_PIC_TAX").ToString().Contains("'") Then

            '    index = ds.Tables(0).Rows(0).Item("NO_HP_PIC_TAX").ToString().IndexOf("'")

            '    hpPicTax.Text = ds.Tables(0).Rows(0).Item("NO_HP_PIC_TAX").ToString().Remove(index, 1)

            'Else

            '    hpPicTax.Text = ds.Tables(0).Rows(0).Item("NO_HP_PIC_TAX").ToString()

            'End If

            value = ds.Tables(0).Rows(0).Item("NO_HP_PIC_TAX").ToString()
            While value.Contains("'")

                index = value.IndexOf("'")
                value = value.Remove(index, 1)

            End While
            While value.Contains("#")

                index = value.IndexOf("#")
                value = value.Remove(index, 1)

            End While
            hpPicTax.Text = value

            'If ds.Tables(0).Rows(0).Item("PROSEDUR_PEMBAYARAN").ToString().Contains("'") Then

            '    index = ds.Tables(0).Rows(0).Item("PROSEDUR_PEMBAYARAN").ToString().IndexOf("'")

            '    prsdrByr.Text = ds.Tables(0).Rows(0).Item("PROSEDUR_PEMBAYARAN").ToString().Remove(index, 1)

            'Else

            '    prsdrByr.Text = ds.Tables(0).Rows(0).Item("PROSEDUR_PEMBAYARAN").ToString()

            'End If

            value = ds.Tables(0).Rows(0).Item("PROSEDUR_PEMBAYARAN").ToString()
            While value.Contains("'")

                index = value.IndexOf("'")
                value = value.Remove(index, 1)

            End While
            While value.Contains("#")

                index = value.IndexOf("#")
                value = value.Remove(index, 1)

            End While
            prsdrByr.Text = value

            'If ds.Tables(0).Rows(0).Item("CARA_PEMBAYARAN").ToString().Contains("'") Then

            '    index = ds.Tables(0).Rows(0).Item("CARA_PEMBAYARAN").ToString().IndexOf("'")

            '    crByr.Text = ds.Tables(0).Rows(0).Item("CARA_PEMBAYARAN").ToString().Remove(index, 1)

            'Else

            '    crByr.Text = ds.Tables(0).Rows(0).Item("CARA_PEMBAYARAN").ToString()

            'End If

            value = ds.Tables(0).Rows(0).Item("CARA_PEMBAYARAN").ToString()
            While value.Contains("'")

                index = value.IndexOf("'")
                value = value.Remove(index, 1)

            End While
            While value.Contains("#")

                index = value.IndexOf("#")
                value = value.Remove(index, 1)

            End While
            crByr.Text = value

            orgCode.Text = ds.Tables(0).Rows(0).Item("ORG_CODE").ToString()
            dtime = ds.Tables(0).Rows(0).Item("SUBMIT_DATE").ToString()
            subDate.Text = dtime.Date.ToString("dd-MMM-yyyy")
            inFrom.Text = ds.Tables(0).Rows(0).Item("INPUT_FROM").ToString()
            ReasonReject.Text = ds.Tables(0).Rows(0).Item("NOTES").ToString()

            Dim script As String = "window.onload = function() { showDiv2(); };"

            ClientScript.RegisterStartupScript(Me.GetType(), "showDiv2", script, True)
        Else
            Dim script As String = "window.onload = function() { alrtNoData(); };"

            ClientScript.RegisterStartupScript(Me.GetType(), "alrtNoData", script, True)
        End If

    End Sub

    Public Function getDetails(ByVal id As String) As DataSet

        Dim strSQL As String
        strSQL = "SELECT * FROM APPROVAL_DATA_CUSTOMER where STATUS = 'DITOLAK' and ID =" + id

        Dim ds As DataSet = createDS(strSQL)
        Return ds

    End Function

    'Approve User////////////////////////////////////////////////////////////////////////////////


    Function ExecuteDataSQL(ByVal strSQL As String) As Integer

        If oConnDB.State = ConnectionState.Open Then
            oConnDB.Close()
        End If
        oConnDB.Open()

        Dim oCmd = New SqlCommand
        Dim trans = oConnDB.BeginTransaction()

        Try
            With oCmd
                .Transaction = trans
                .Connection = oConnDB
                .CommandType = CommandType.Text
                .CommandText = strSQL
                .ExecuteNonQuery()
                trans.Commit()
            End With
            Return 1
        Catch ex As Exception
            trans.Rollback()
            Return 0
        End Try

        If oConnDB.State = ConnectionState.Open Then
            oConnDB.Close()
        End If
    End Function

    'Tolak User///////////////////////////////////////////////////////////////////////////


    'Export Excel////////////////////////////////////////////////////////////////////////
    Protected Sub BtnExportExcel_Click(sender As Object, e As EventArgs) Handles BtnExportExcel2.Click

        Dim strSQL As String
        If Session("sCabang") = "PST" Then
            strSQL = "SELECT ID,ORG_CODE,CUSTOMER_NUMBER,CUSTOMER_NAME,SUBMIT_DATE,SUBMITTED_BY,VALIDATION_DATE,APPROVED_BY FROM APPROVAL_DATA_CUSTOMER where STATUS = 'DITOLAK' ORDER BY ID DESC"
        Else
            strSQL = "SELECT ID,ORG_CODE,CUSTOMER_NUMBER,CUSTOMER_NAME,SUBMIT_DATE,SUBMITTED_BY,VALIDATION_DATE,APPROVED_BY FROM APPROVAL_DATA_CUSTOMER where STATUS = 'DITOLAK' AND ORG_CODE = '" + Session("sCabang") + "' ORDER BY ID DESC"
        End If

        Dim temp_date As DateTime
        temp_date = DateTime.Now
        Dim temp_date2, temp_cabang As String
        temp_cabang = Session("sCabang")
        temp_date2 = temp_date.ToString("ddMMyyyyHHmmss") & temp_cabang

        Dim ds As DataSet = createDS(strSQL)

        Dim dt As DataTable = ds.Tables(0)


        Dim dt_excel As New DataTable()
        dt_excel.Columns.AddRange(New DataColumn(7) {New DataColumn("ID"), New DataColumn("KodeCabang"), New DataColumn("Customer Number"), New DataColumn("Customer Name"), New DataColumn("Submit Date"), New DataColumn("Submit By"), New DataColumn("Approve Date"), New DataColumn("Approve By")})

        Dim temp_Id, temp_KodeCabang, temp_CustNum, temp_CustName, temp_SubDate, temp_SubBy, temp_AppDate, temp_AppBy As String
        Dim dtime, atime As DateTime
        Dim count As Integer
        count = 0

        For Each row As DataRow In dt.Rows
            temp_Id = row.Item(0)
            'validasi kodecabang kosong 
            If row.IsNull(1) Then

                temp_KodeCabang = ""
            Else
                temp_KodeCabang = row.Item(1)
            End If

            temp_CustNum = row.Item(2)
            temp_CustName = row.Item(3)

            'Validasi Submit Date
            temp_SubDate = row.Item(4)
            dtime = Convert.ToDateTime(temp_SubDate)
            temp_SubDate = dtime.ToString("dd-MMM-yyyy  hh:mm:ss")


            temp_SubBy = row.Item(5)

            'Validasi Approve Date
            temp_AppDate = row.Item(6)
            atime = Convert.ToDateTime(temp_AppDate)
            temp_AppDate = atime.ToString("dd-MMM-yyyy hh:mm:ss")

            temp_AppBy = row.Item(7)

            count = count + 1

            dt_excel.Rows.Add(temp_Id, temp_KodeCabang, temp_CustNum, temp_CustName, temp_SubDate, temp_SubBy, temp_AppDate, temp_AppBy)


        Next

        Dim temp_biasa As String = "temp"

        Using wb As New XLWorkbook()

            Dim ReportRejectExcel = wb.AddWorksheet("ReportReject")
            ReportRejectExcel.Cell(1, 1).InsertTable(dt_excel)
            ReportRejectExcel.Columns().AdjustToContents()
            ReportRejectExcel.Rows().AdjustToContents()



            For kolum_header As Integer = 1 To 8
                'BeExcel.Cell(1, kolum_header).Style.Fill.BackgroundColor = XLColor.White
                ReportRejectExcel.Cell(1, kolum_header).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                ReportRejectExcel.Cell(1, kolum_header).Style.Fill.BackgroundColor = XLColor.White
                'BeExcel.Cell(1, kolum_header).Style.Font.Bold = True

                ReportRejectExcel.Cell(1, kolum_header).Style.Font.FontColor = XLColor.FromTheme(XLThemeColor.Text1)
            Next


            'For kolum_warna As Integer = 1 To 8
            '    For border_warna As Integer = 1 To count + 1
            '        ReportApprovalExcel.Cell(border_warna, kolum_warna).Style.Border.BottomBorder = XLBorderStyleValues.Thick
            '        ReportApprovalExcel.Cell(border_warna, kolum_warna).Style.Border.BottomBorderColor = XLColor.White
            '        ReportApprovalExcel.Cell(border_warna, kolum_warna).Style.Border.TopBorder = XLBorderStyleValues.Thick
            '        ReportApprovalExcel.Cell(border_warna, kolum_warna).Style.Border.TopBorderColor = XLColor.White
            '        ReportApprovalExcel.Cell(border_warna, kolum_warna).Style.Border.RightBorder = XLBorderStyleValues.Thick
            '        ReportApprovalExcel.Cell(border_warna, kolum_warna).Style.Border.RightBorderColor = XLColor.White
            '        ReportApprovalExcel.Cell(border_warna, kolum_warna).Style.Border.LeftBorder = XLBorderStyleValues.Thick
            '        ReportApprovalExcel.Cell(border_warna, kolum_warna).Style.Border.LeftBorderColor = XLColor.White
            '    Next
            'Next

            For kolum_warna As Integer = 1 To 8
                For border_warna As Integer = 1 To count + 1
                    ReportRejectExcel.Cell(border_warna, kolum_warna).Style.Fill.BackgroundColor = XLColor.White
                    ReportRejectExcel.Cell(border_warna, kolum_warna).Style.Border.BottomBorder = XLBorderStyleValues.Thick
                    ReportRejectExcel.Cell(border_warna, kolum_warna).Style.Border.BottomBorderColor = XLColor.White
                    ReportRejectExcel.Cell(border_warna, kolum_warna).Style.Border.TopBorder = XLBorderStyleValues.Thick
                    ReportRejectExcel.Cell(border_warna, kolum_warna).Style.Border.TopBorderColor = XLColor.White
                    ReportRejectExcel.Cell(border_warna, kolum_warna).Style.Border.RightBorder = XLBorderStyleValues.Thick
                    ReportRejectExcel.Cell(border_warna, kolum_warna).Style.Border.RightBorderColor = XLColor.White
                    ReportRejectExcel.Cell(border_warna, kolum_warna).Style.Border.LeftBorder = XLBorderStyleValues.Thick
                    ReportRejectExcel.Cell(border_warna, kolum_warna).Style.Border.LeftBorderColor = XLColor.White
                Next
            Next


            For rataKanan As Integer = 2 To count + 1

                'Rata Kanan
                ReportRejectExcel.Cell(rataKanan, 1).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right)
                ReportRejectExcel.Cell(rataKanan, 3).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right)

            Next

            'Response.AddHeader("content-disposition", "attachment; filename=GiroOpname" & FileDate & "_" & orgId & ".pdf")


            Response.Clear()
            Response.Buffer = True
            'Response.Charset = ""
            'Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            'Response.AddHeader("content-disposition", "attachment;filename=CapexHeaderGiro.xlsx")
            Response.AddHeader("content-disposition", "attachment;filename=RPTRJCT" + temp_date2 + ".xls")
            Response.Charset = ""
            Response.ContentType = "application/vnd.ms-excel"

            Using MyMemoryStream As New MemoryStream()
                wb.SaveAs(MyMemoryStream)
                MyMemoryStream.WriteTo(Response.OutputStream)
                Response.Flush()
                Response.End()
            End Using
        End Using

    End Sub

End Class
