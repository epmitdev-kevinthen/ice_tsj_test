﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="cabang.aspx.vb" Inherits="cabang" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%--    <link rel="stylesheet" type="text/css" href="style/datatables.min.css" />

<script type="text/javascript" src="script/datatables.min.js"></script>--%>
    <script type="text/javascript" src="script/jquery-1.4.1.min.js"></script>
    <script>
        $(document).ready(function () {
            var table = $("#ContentPlaceHolder1_GridView1").DataTable({
                paging: true,
                searching: true,
                lengthChange: false,
            });
            $('#ContentPlaceHolder1_GridView1 tbody').on('click', 'tr', function () {
                var data = table.row(this).data();

                $("#ContentPlaceHolder1_txtKodeCab").val(data[0]);
                $("#ContentPlaceHolder1_txtNamaCab").val(data[1]);
                $("#ContentPlaceHolder1_ddlRegion").val(data[2]).change();
                $("#ContentPlaceHolder1_ddlClass").val(data[3]).change();
                $("#ContentPlaceHolder1_ddlRBM").val(data[4]).change();
                $("#ContentPlaceHolder1_ddlABM").val(data[6]).change();
                $("#ContentPlaceHolder1_txtAlamat1").val(data[8]);
                $("#ContentPlaceHolder1_txtAlamat2").val(data[9]);
                $("#ContentPlaceHolder1_createdBy").text(data[10]);
                $("#ContentPlaceHolder1_createdDate").text(data[11]);
                $("#ContentPlaceHolder1_lastUpdateBy").text(data[12]);
                $("#ContentPlaceHolder1_lastUpdateDate").text(data[13]);
            });
        });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <asp:Label ID="lblHead" runat="server" Text="Master Cabang"
        Style="font-size: 20px;"></asp:Label>
    <div>
        <asp:Label ID="cabangEdit" runat="server"></asp:Label>
    </div>
    <div style="display: flex;">
        <table>
            <tr>
                <td>Kode Cabang</td>
                <td>:</td>
                <td>
                    <asp:TextBox ID="txtKodeCab" runat="server" CssClass=""></asp:TextBox></td>
            </tr>
            <tr>
                <td>Nama Cabang</td>
                <td>:</td>
                <td>
                    <asp:TextBox ID="txtNamaCab" runat="server" CssClass=""></asp:TextBox></td>
            </tr>
            <tr>
                <td>Region</td>
                <td>:</td>
                <td>
                    <asp:DropDownList ID="ddlRegion" runat="server">
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td>Class</td>
                <td>:</td>
                <td>
                    <asp:DropDownList ID="ddlClass" runat="server">
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td>RBM</td>
                <td>:</td>
                <td>
                    <asp:DropDownList ID="ddlRBM" runat="server">
                        <asp:ListItem >Select</asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td>ABM</td>
                <td>:</td>
                <td>
                    <asp:DropDownList ID="ddlABM" runat="server">
                        <asp:ListItem >Select</asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td>Alamat Line 1</td>
                <td>:</td>
                <td>
                    <asp:TextBox ID="txtAlamat1" runat="server" TextMode="MultiLine"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Alamat Line 2</td>
                <td>:</td>
                <td>
                    <asp:TextBox ID="txtAlamat2" runat="server" TextMode="MultiLine"></asp:TextBox></td>
            </tr>

        </table>
        <div style="float: right; margin-left: 15px;">
            Created By: <span id="createdBy" runat="server"></span>
            <br />
            Creation Date: <span id="createdDate" runat="server"></span>
            <br />
            Last Update By: <span id="lastUpdateBy" runat="server"></span>
            <br />
            Last Update Date:<span id="lastUpdateDate" runat="server"></span><br />
        </div>
    </div>
    <div id="div_lbl" runat="server">
        <asp:Label ID="lbl_msg" runat="server"></asp:Label>
    </div>
    <asp:Button ID="clearData" runat="server" Text="Clear Data" OnClick="clearData_Click" CssClass="button2"/>
    <asp:Button ID="insertData" runat="server" Text="Insert Data" OnClick="insertData_Click" CssClass="button2"/>
    <asp:Button ID="updateData" runat="server" Text="Update Data" OnClick="updateData_Click" CssClass="button2"/>
    <asp:Button ID="deleteData" runat="server" Text="Delete Data" OnClick="deleteData_Click" CssClass="button2"/>

    <div>

        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" AllowPaging="false" AllowSorting="false"
            PageSize="10" CssClass="dataTable">
            <Columns>
                <asp:BoundField DataField="kodecab" HeaderText="Kode" />
                <asp:BoundField DataField="namacab" HeaderText="Nama Cabang" />
                <asp:BoundField DataField="region" HeaderText="Region" />
                <asp:BoundField DataField="class" HeaderText="Class" />
                <asp:Boundfield DataField="rbm" HeaderText="RBM" >
                    <HeaderStyle CssClass="hidden"></HeaderStyle>

                        <ItemStyle CssClass="hidden"></ItemStyle>
                </asp:Boundfield>
                <asp:BoundField DataField="rbmnm" HeaderText="RBM" />
                <asp:Boundfield DataField="abm" HeaderText="RBM" >
                    <HeaderStyle CssClass="hidden"></HeaderStyle>

                        <ItemStyle CssClass="hidden"></ItemStyle>
                </asp:Boundfield>
                <asp:BoundField DataField="abmnm" HeaderText="ABM" />
                <asp:BoundField DataField="alamat" HeaderText="Alamat" />
                <asp:Boundfield DataField="alamat2" HeaderText="Alamat 2" />
                <asp:Boundfield DataField="created_by" HeaderText="RBM" >
                    <HeaderStyle CssClass="hidden"></HeaderStyle>

                        <ItemStyle CssClass="hidden"></ItemStyle>
                </asp:Boundfield>
                <asp:Boundfield DataField="creation_date" HeaderText="RBM" >
                    <HeaderStyle CssClass="hidden"></HeaderStyle>

                        <ItemStyle CssClass="hidden"></ItemStyle>
                </asp:Boundfield>
                <asp:Boundfield DataField="last_update_by" HeaderText="RBM" >
                    <HeaderStyle CssClass="hidden"></HeaderStyle>

                        <ItemStyle CssClass="hidden"></ItemStyle>
                </asp:Boundfield>
                <asp:Boundfield DataField="last_update_date" HeaderText="RBM" >
                    <HeaderStyle CssClass="hidden"></HeaderStyle>

                        <ItemStyle CssClass="hidden"></ItemStyle>
                </asp:Boundfield>
                <%--<asp:TemplateField ShowHeader="True" HeaderText="Submit Option">
                    <ItemTemplate>
                        <asp:Button runat="server" CommandName="cmdsubmit" ID="LinkButton1" CommandArgument='<%#Eval("kodecab")%>' Text="Update/Delete" CssClass="button2"></asp:Button>

                    </ItemTemplate>
                </asp:TemplateField>--%>
            </Columns>
            <PagerStyle CssClass="paginate_button" />
        </asp:GridView>
    </div>
</asp:Content>


