﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ListUser.aspx.vb" Inherits="ListUser" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <br />



    <asp:Label ID="lblHead" runat="server" Text="List User"
        style="font-size: 20px;"
        ></asp:Label>

    
<script  type="text/javascript">
    $(document).ready(function () {
        var table = $('#mytable').DataTable({
            scrollY: "300px",
            scrollX: "800px",
            scrollCollapse: true,
            paging: true,
            sScrollX: "100%", 
            bProcessing: true, // shows 'processing' label
            bStateSave: true, // presumably saves state for reloads
            fixedColumns: {
                leftColumns: 1
            }
        });
    });  

    function btnClick(args) {
        window.location.href = "r.aspx?a=1&b=" + args;
    }

</script>

<div style="margin:30px;">

    <asp:PlaceHolder ID="myPlaceH" runat="server"></asp:PlaceHolder>

</div>


    <asp:Label ID="infoerror" runat="server" style="color: red" ></asp:Label>

</asp:Content>


