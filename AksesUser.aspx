﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="AksesUser.aspx.vb" Inherits="AksesUser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <%--<br />
    <button type="button" onclick="showAksesUser()">Akses User</button>
    <br />--%>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css"/>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>

    <script>

        $(document).ready(function () {
            $('#mytable').DataTable({
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
            });
        });

    </script>

    <div style="display:flex;">
        <table>
            <tr>
                <td>
                    <asp:Label ID="lblHead" runat="server" Text="Akses ICE dan Sensus"
                        Style="font-size: 20px;"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblUsrnm" runat="server" Text="Username"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="username" runat="server" placeholder="masukkan username"></asp:TextBox>
                </td>
                <td>
                    <asp:Button ID="btnCkUsrnm" runat="server" Text="Check" CssClass="button2"/>
                </td>
            </tr>
                
        </table>
        
    </div>
    <div id="div1" style="display:none">
        <table>
            <tr>
                <td>
                    <asp:Label ID="lblAkses" runat="server" Text="Akses"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:CheckBox ID="cbICE" runat="server" /><asp:Label ID="lblICE" runat="server" Text="ICE"></asp:Label>
                </td>
                <td>
                    <asp:CheckBox ID="cbSensus" runat="server" /><asp:Label ID="lblSensus" runat="server" Text="Sensus"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <button type="button" onclick="btnCancelSensus()" Class="button2">Cancel</button>
                </td>
                <td>
                    <asp:Button ID="btnAdd" runat="server" Text="Submit" CssClass="button2"/>
                </td>
            </tr>
        </table>
    </div>

    <br />

    <asp:Label ID="lblMaster" runat="server" Text="Tambah User"
        Style="font-size: 20px;"></asp:Label>
    <div style="display: flex;">
        <table>
            <tr>
                <td>Username</td>
                <td>:</td>
                <td>
                    <asp:TextBox ID="txtUserName" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Password</td>
                <td>:</td>
                <td>
                    <asp:TextBox ID="txtPass" runat="server" Text="" TextMode="Password"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Full Username</td>
                <td>:</td>
                <td>
                    <asp:TextBox ID="txtFullUserName" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>NIK</td>
                <td>:</td>
                <td>
                    <asp:TextBox ID="txtNIK" runat="server" maxlength="9"></asp:TextBox></td>
            </tr>
            <tr>
                <td>J K</td>
                <td>:</td>
                <td>
                    <asp:DropDownList ID="ddlJK" runat="server">
                        <asp:ListItem Text="Laki - laki" Value="L"></asp:ListItem>
                        <asp:ListItem Text="Perempuan" Value="P"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>Jabatan</td>
                <td>:</td>
                <td>
                    <asp:DropDownList ID="ddlJabatan" runat="server">
                        <asp:ListItem Text="-" Value=""></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>Cabang</td>
                <td>:</td>
                <td>
                    <asp:DropDownList ID="ddlCabang" runat="server">
                        <asp:ListItem Text="-" Value=""></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>Email</td>
                <td>:</td>
                <td>
                    <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Level Akses</td>
                <td>:</td>
                <td>
                    <asp:DropDownList ID="ddlLvlAkses" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>Kode Dept</td>
                <td>:</td>
                <td>
                    <asp:DropDownList ID="ddlKodeDept" runat="server">
                        <asp:ListItem Text="-" Value=""></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>No WA</td>
                <td>:</td>
                <td>
                    <asp:TextBox ID="txtNoWA" runat="server" maxlength="15"></asp:TextBox>
                </td>
            </tr>
        </table>
        <%--<div style="float: right; margin-left: 15px;">
            Created By: <span id="createdBy" runat="server"></span>
            <br />
            Creation Date: <span id="createdDate" runat="server"></span>
            <br />
            Last Update By: <span id="lastUpdateBy" runat="server"></span>
            <br />
            Last Update Date: <span id="lastUpdateDate" runat="server"></span><br />
        </div>--%>
    </div>

    <asp:Button ID="clearData" runat="server" Text="Clear Data" OnClick="clearData_Click" CssClass="button2"/>
    <asp:Button ID="insertData" runat="server" Text="Insert Data" OnClick="insertData_Click" CssClass="button2"/>
    <br />
    <br />
    <div style="width:100%;margin:auto">
        <asp:Label ID="myTable" runat="server" Text="Data User" style="font-size:20px"></asp:Label>
        <asp:PlaceHolder ID="tblUser" runat="server"></asp:PlaceHolder>
    </div>


    <%--<button type="button" onclick="showAddUser()">Tambah User</button>

    <div id="addUser" style="display:none">
        <label>Username</label>
        <br />
        <asp:TextBox ID="usernameAdd" runat="server" placeholder="Username" MaxLength="50"></asp:TextBox>
        <br />
        <label>Password</label>
        <br />
        <asp:TextBox ID="userpass" runat="server" placeholder="Password" MaxLength="50"></asp:TextBox>
        <br />
        <label>Nama lengkap</label>
        <br />
        <asp:TextBox ID="fullName" runat="server" placeholder="Nama Lengkap" MaxLength="100"></asp:TextBox>
        <br />
        <label>NIK</label>
        <br />
        <asp:TextBox ID="nik" runat="server" placeholder="NIK" MaxLength="50"></asp:TextBox>
        <br />
        <label>Jenis Kelamin</label>
        <br />
        <asp:DropDownList ID="jenisKelamin" runat="server">
            <asp:ListItem ID="laki" runat="server" >Laki-Laki</asp:ListItem>
            <asp:ListItem ID="perempuan" runat="server" >Perempuan</asp:ListItem>
        </asp:DropDownList>
        <br />
        <label>Kode Jabatan</label>
        <br />
        <asp:TextBox ID="kodeJabatan" runat="server" placeholder="Kode Jabatan" MaxLength="10"></asp:TextBox>
        <br />
        <label>Jabatan</label>
        <br />
        <asp:TextBox ID="jabatan" runat="server" placeholder="Jabatan" MaxLength="50"></asp:TextBox>
        <br />
        <label>Kode Cabang</label>
        <br />
        <asp:TextBox ID="kodeCabang" runat="server" placeholder="Kode Cabang" MaxLength="3"></asp:TextBox>
        <br />
        <label>Email</label>
        <br />
        <asp:TextBox ID="email" runat="server" placeholder="Email" MaxLength="50"></asp:TextBox>
        <br />
        <label>Level Akses</label>
        <br />
        <asp:DropDownList ID="levelAkses" runat="server">
            <asp:ListItem ID="la1" runat="server" >1</asp:ListItem>
            <asp:ListItem ID="la2" runat="server" >2</asp:ListItem>
            <asp:ListItem ID="la3" runat="server" >3</asp:ListItem>
            <asp:ListItem ID="la4" runat="server" >4</asp:ListItem>
        </asp:DropDownList>
        <br />--%>
        <%--<label>Level Akses</label>
        <br />
        <asp:TextBox ID="levelAkses" runat="server" placeholder="Level Akses"></asp:TextBox>
        <br />--%>
        <%--<label>Kode Department</label>
        <br />
        <asp:TextBox ID="kodeDept" runat="server" placeholder="Kode Department" MaxLength="10"></asp:TextBox>
        <br />
        <label>Department</label>
        <br />
        <asp:TextBox ID="department" runat="server" placeholder="Department" MaxLength="200"></asp:TextBox>
        <br />
        <label>No. HP</label>
        <br />
        <asp:TextBox ID="nohp" runat="server" placeholder="No. Hp" MaxLength="20"></asp:TextBox>
        <br />
        <br />

        <asp:Button ID="btnAddUser" runat="server" Text="Add User" />
        <button type="button" onclick="btnClearAll()">Clear All</button>
        <button type="button" onclick="btnCancel()">Cancel</button>

     </div>  --%>  

    <script>

        function showDiv() {

            document.getElementById("div1").style.display = "block";

        }

        function noUsrnm() {

            alert("Username tidak ada!");

        }

        function noSbmt() {

            alert("Checkbox tidak boleh kosong!");
            document.getElementById("div1").style.display = "block";
            return false;

        }

        function alrtSuccess() {

            document.getElementById("<%= username.ClientID %>").value = "";
            alert("Data berhasil diupdate!");
            return true;

        }

        function showAddUser() {

            document.getElementById("addUser").style.display = "block";

        }

        function btnCancel(){

            btnClearAll();
            document.getElementById("addUser").style.display = "none";

        }

        function btnCancelSensus() {

            document.getElementById("div1").style.display = "none";
            document.getElementById("<%= cbICE.ClientID %>").checked = false;
            document.getElementById("<%= cbSensus.ClientID %>").checked = false;
            document.getElementById("<%= username.ClientID %>").value = "";

        }

        <%--function btnClearAll() {

            document.getElementById("<%= usernameAdd.ClientID %>").value = "";
            document.getElementById("<%= userpass.ClientID %>").value = "";
            document.getElementById("<%= fullName.ClientID %>").value = "";
            document.getElementById("<%= nik.ClientID %>").value = "";
            document.getElementById("<%= kodeJabatan.ClientID %>").value = "";
            document.getElementById("<%= kodeCabang.ClientID %>").value = "";
            document.getElementById("<%= email.ClientID %>").value = "";
            document.getElementById("<%= levelAkses.ClientID %>").value = "";
            document.getElementById("<%= kodeDept.ClientID %>").value = "";
            document.getElementById("<%= department.ClientID %>").value = "";
            document.getElementById("<%= nohp.ClientID %>").value = "";

        }--%>

        function alrtAddSuceed(){

            alert("Data user berhasil ditambahkan");
            window.location.replace("http://localhost:60700/AksesUser.aspx");

        }

        function alrtAddFailed() {

            alert("Data user gagal ditambahkan");
            window.location.replace("http://localhost:60700/AksesUser.aspx");

        }

        function alrtDataKosong() {

            alert("Data tidak boleh ada yang kosong!");

        }

        function alrtDataNumOnly() {

            alert("Data no wa hanya boleh berupa nomor!");

        }

    </script>

</asp:Content>

