﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="strCheckList.aspx.vb" Inherits="strCheckList" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <br />



    <asp:Label ID="lblHead" runat="server" Text="Template - Struktur CheckList"
        style="font-size: 20px;"
        ></asp:Label>

    
<script  type="text/javascript">
    $(document).ready(function () {
        var table = $('#mytable').DataTable({
            scrollY: "300px",
            scrollX: "800px",
            scrollCollapse: true,
            paging: true,
            sScrollX: "100%", 
            aoColumns : [
             { "sWidth": "40px"},
             { "sWidth": "40px"},
             { "sWidth": "40px"},
             { "sWidth": "250px"},
             { "sWidth": "80px"},
             { "sWidth": "80px"},
             { "sWidth": "90px"},
             { "sWidth": "40px"},
             { "sWidth": "40px" },
             { "sWidth": "40px" },
             { "sWidth": "40px" },
             { "sWidth": "40px" },
             { "sWidth": "40px" }],
            bProcessing: true, // shows 'processing' label
            bStateSave: true, // presumably saves state for reloads
            fixedColumns: {
                leftColumns: 1
            }
        });
    });  

    function btnClick(args) {
        window.location.href = "r.aspx?a=1&b=" + args;
    }

</script>

<div style="margin:30px;">

    <button type='button' class='button2xx' data-book-id='||||A|A|||' data-target='#formModalNew' data-toggle='modal'>Add New</button>
    <asp:PlaceHolder ID="myPlaceH" runat="server"></asp:PlaceHolder>

</div>

    <div id="formModalEdit" tabindex="-1" role="dialog" aria-hidden="true" style="display: none ">
        <table style="position: fixed; top:200px ; left:200px; background-color: ButtonFace; border: 5px; border-radius: 15px; padding: 20px; box-shadow: 5px 5px 5px gray;">
            <tr><td>
           <label for="TxtKode">Kode &nbsp;&nbsp;</label></td><td>
           <asp:TextBox ID="TxtKode" runat="server" MaxLength="10" Width="100px" ClientIDMode="Static" style="background-color: lightgray"></asp:TextBox></td>
            </tr><tr><td>
           <label for="TxtNama_Tmp">Nama Template &nbsp;&nbsp;</label></td><td>
           <asp:TextBox ID="TxtNama_Tmp" runat="server" MaxLength="100" Width="200px" ClientIDMode="Static"></asp:TextBox></td>
           </tr><tr><td>
           <label for="txtStartDate">Start Date &nbsp;&nbsp;</label></td><td>
           <asp:TextBox ID="txtStartDate" runat="server" MaxLength ="12" Width="100px" ClientIDMode="Static"></asp:TextBox></td>
           </tr><tr><td>
           <label for="TxtEndDate">End Date &nbsp;&nbsp;</label></td><td>
           <asp:TextBox ID="TxtEndDate" runat="server" MaxLength="12" Width="100px"  ClientIDMode="Static"></asp:TextBox></td>
           </tr><tr><td>

               
           <label for="TxtFlagEntry">Flag Entry? &nbsp;&nbsp;</label></td><td>
           <asp:DropDownList ID="TxtFlagEntry" runat="server" Width="50px"  ClientIDMode="Static"></asp:DropDownList></td>
           </tr><tr><td>
           <label for="TxtFlagReport">Flag Report? &nbsp;&nbsp;</label></td><td>
           <asp:DropDownList ID="TxtFlagReport" runat="server" Width="50px"  ClientIDMode="Static"></asp:DropDownList></td>
           </tr>
<tr><td>
           <label for="TxtFlagAsses">Self Assesment &nbsp;&nbsp;</label></td><td>
           <asp:DropDownList ID="txtSelfAssesment" runat="server" Width="50px"  ClientIDMode="Static"></asp:DropDownList></td>
           </tr>
<tr><td>
           <label for="Txtdept">Division &nbsp;&nbsp;</label></td><td>
           <asp:DropDownList ID="txtDept" runat="server" Width="200px"  ClientIDMode="Static"></asp:DropDownList></td>
           </tr>
            <tr>
                <td><label>Flag Notification</label></td>
                <td><asp:DropDownList ID="ddlNotif_Edit" runat="server" Width="200px"  ClientIDMode="Static">
                    <asp:ListItem Text="N" Value="N"></asp:ListItem>
                    <asp:ListItem Text="Y" Value="Y"></asp:ListItem>                    
                    </asp:DropDownList></td>
            </tr>
           <tr><td>
           <label for="txtNNILAI">Hasil Penilaian &nbsp;&nbsp;</label></td><td>
           <asp:TextBox ID="txtNNILAI" runat="server" MaxLength="3" Width="30px" ClientIDMode="Static"></asp:TextBox></td>
            </tr>
           <tr><td>
           <label for="txtPNILAI">Pilihan Penilaian &nbsp;&nbsp;</label></td><td>
           <asp:TextBox ID="txtPNILAI" runat="server" MaxLength="3" Width="30px" ClientIDMode="Static"></asp:TextBox></td>
            </tr>

                       <tr><td colspan="2" style="text-align: center;">&nbsp;
               </td>
           </tr>
           <tr><td colspan="2" style="text-align: center;">
               <asp:Button ID="btnUpdate" CssClass="btn1" runat="server" text="Update"/> &nbsp;&nbsp;
               <asp:Button ID="btnCancel1" CssClass="btn1" runat="server" text="Cancel"/>
               </td>
           </tr>
       </table>
    </div>

    <div id="formModalDelete" tabindex="-1" role="dialog" aria-hidden="true"style="display: none ">
        <table style="position: fixed; top:200px ; left:200px; background-color: ButtonFace; border: 5px; border-radius: 15px; padding: 20px; box-shadow: 5px 5px 5px gray;">
            <tr><td>
           <label for="TxtKode2">Kode &nbsp;&nbsp;</label></td><td>
           <asp:TextBox ID="TxtKode2"  runat="server" MaxLength="10" Width="100px" ClientIDMode="Static" style="background-color: lightgray"></asp:TextBox></td>
            </tr><tr><td>
           <label for="TxtNama_Tmp2">Nama Template &nbsp;&nbsp;</label></td><td>
           <asp:TextBox ID="TxtNama_Tmp2" runat="server" MaxLength="100" Width="200px" ClientIDMode="Static" style="background-color: lightgray"></asp:TextBox></td>
           </tr>
            <!--
            <tr><td>
           <label for="txtStartDate2">Start Date &nbsp;&nbsp;</label></td><td>
           <asp:TextBox ID="txtStartDate2" runat="server" MaxLength ="12" Width="100px" ClientIDMode="Static" style="background-color: lightgray"></asp:TextBox></td>
           </tr><tr><td>
           <label for="TxtEndDate2">End Date &nbsp;&nbsp;</label></td><td>
           <asp:TextBox ID="TxtEndDate2" runat="server" MaxLength="12" Width="100px"  ClientIDMode="Static" style="background-color: lightgray"></asp:TextBox></td>
           </tr><tr><td>
           <label for="TxtFlagEntry2">Flag Entry? &nbsp;&nbsp;</label></td><td>
           <asp:TextBox ID="TxtFlagEntry2" runat="server" MaxLength="1" Width="50px"  ClientIDMode="Static" style="background-color: lightgray"></asp:TextBox></td>
           </tr><tr><td>
           <label for="TxtFlagReport2">Flag Report? &nbsp;&nbsp;</label></td><td>
           <asp:TextBox ID="TxtFlagReport2" runat="server" MaxLength="1" Width="50px"  ClientIDMode="Static" style="background-color: lightgray"></asp:TextBox></td>
           </tr>
            -->
           <tr><td colspan="2" style="text-align: center;">&nbsp;
               </td>
           </tr>
           <tr><td colspan="2" style="text-align: center;">
               <asp:Button ID="btnDelete" CssClass="btn1" runat="server" text="Delete"/> &nbsp;&nbsp;
               <asp:Button ID="btnCancel2" CssClass="btn1" runat="server" text="Cancel"/>
               </td>
           </tr>
       </table>
    </div>

    <div id="formModalNew" tabindex="-1" role="dialog" aria-hidden="true"style="display: none ">
        <table style="position: fixed; top:200px ; left:200px; background-color: ButtonFace; border: 5px; border-radius: 15px; padding: 20px; box-shadow: 5px 5px 5px gray;">
            <tr><td>
           <label for="TxtKode3">Kode &nbsp;&nbsp;</label></td><td>
           <asp:TextBox ID="TxtKode3" runat="server" MaxLength="10" Width="100px" ClientIDMode="Static"></asp:TextBox></td>
            </tr><tr><td>
           <label for="TxtNama_Tmp3">Nama Template &nbsp;&nbsp;</label></td><td>
           <asp:TextBox ID="TxtNama_Tmp3"  runat="server" MaxLength="100" Width="200px" ClientIDMode="Static"></asp:TextBox></td>
           </tr><tr><td>
           <label for="txtStartDate3">Start Date &nbsp;&nbsp;</label></td><td>
           <asp:TextBox ID="txtStartDate3"  runat="server" MaxLength ="12" Width="100px" ClientIDMode="Static"></asp:TextBox></td>
           </tr><tr><td>
           <label for="TxtEndDate3">End Date &nbsp;&nbsp;</label></td><td>
           <asp:TextBox ID="TxtEndDate3"  runat="server" MaxLength="12" Width="100px"  ClientIDMode="Static"></asp:TextBox></td>
           </tr>
            
            <!--
            <tr><td>
           <label for="TxtFlagEntry3">Flag Entry? &nbsp;&nbsp;</label></td><td>
           <asp:DropDownList ID="TxtFlagEntry3" runat="server" Width="50px"  ClientIDMode="Static"></asp:DropDownList></td>
           </tr><tr><td>
           <label for="TxtFlagReport3">Flag Report? &nbsp;&nbsp;</label></td><td>
           <asp:DropDownList ID="TxtFlagReport3" runat="server" Width="50px"  ClientIDMode="Static"></asp:DropDownList></td>
           </tr>
-->

            <tr><td>
           <label for="TxtFlagAsses">Self Assesment &nbsp;&nbsp;</label></td><td>
           <asp:DropDownList ID="txtSelfAssesment3" runat="server" Width="50px"  ClientIDMode="Static"></asp:DropDownList></td>
           </tr>

            <tr><td>
           <label for="Txtdept">Division &nbsp;&nbsp;</label></td><td>
           <asp:DropDownList ID="txtdept3" runat="server" Width="200px"  ClientIDMode="Static"></asp:DropDownList></td>
           </tr>
            <tr>
                <td><label>Flag Notification</label></td>
                <td><asp:DropDownList ID="ddlNotif_New" runat="server" Width="200px"  ClientIDMode="Static">
                    <asp:ListItem Text="N" Value="N"></asp:ListItem>
                    <asp:ListItem Text="Y" Value="Y"></asp:ListItem>                    
                    </asp:DropDownList></td>
            </tr>
           <tr><td>
           <label for="txtNNILAI3">Hasil Penilaian &nbsp;&nbsp;</label></td><td>
           <asp:TextBox ID="txtNNILAI3" runat="server" MaxLength="3" Width="30px" ClientIDMode="Static"></asp:TextBox></td>
            </tr>
           <tr><td>
           <label for="txtPNILAI3">Pilihan Penilaian &nbsp;&nbsp;</label></td><td>
           <asp:TextBox ID="txtPNILAI3" runat="server" MaxLength="3" Width="30px" ClientIDMode="Static"></asp:TextBox></td>
            </tr>


           <tr><td colspan="2" style="text-align: center;">&nbsp;
               </td>
           </tr>
           <tr><td colspan="2" style="text-align: center;">
               <asp:Button ID="btnInsert" CssClass="btn1" runat="server" text="Insert"/> &nbsp;&nbsp;
               <asp:Button ID="btnCancel3" CssClass="btn1" runat="server" text="Cancel"/>
               </td>
           </tr>
       </table>
    </div>

    <script type="text/javascript">

        $(function () {
            var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "personal";
            $('#Tabs a[href="#' + tabName + '"]').tab('show');
            $("#Tabs a").click(function () {
                $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
            });
        });

        $('#formModalEdit').on('show.bs.modal', function (e) {
            var bookId = $(e.relatedTarget).data('book-id');
            var arrString = []

            arrString = bookId.split("|")

            //var type = JSON.parse("[" + arrString[2] + "]");
            //console.log(arrString[1])
            $(e.currentTarget).find("#<%=TxtKode.ClientID%>").val(arrString[0]);
            $(e.currentTarget).find("#<%=TxtNama_Tmp.ClientID%>").val(arrString[1]);
            $(e.currentTarget).find("#<%=txtStartDate.ClientID%>").val(arrString[2]);
            $(e.currentTarget).find("#<%=TxtEndDate.ClientID%>").val(arrString[3]);

            $(e.currentTarget).find("#<%=TxtFlagEntry.ClientID%>").val(arrString[4]);
            $(e.currentTarget).find("#<%=TxtFlagReport.ClientID%>").val(arrString[5]);
            $(e.currentTarget).find("#<%=txtSelfAssesment.ClientID%>").val(arrString[6]);
            $(e.currentTarget).find("#<%=txtDept.ClientID%>").val(arrString[7]);

            $(e.currentTarget).find("#<%=txtNNILAI.ClientID%>").val(arrString[8]);
            $(e.currentTarget).find("#<%=txtPNILAI.ClientID%>").val(arrString[9]);
            $(e.currentTarget).find("#<%=ddlNotif_Edit.ClientID%>").val(arrString[10]).change();
            
            document.getElementById("TxtKode").onkeydown = function () { return false; }

        });

        $('#formModalDelete').on('show.bs.modal', function (e) {
            var bookId = $(e.relatedTarget).data('book-id');
            var arrString = []

            arrString = bookId.split("|")

            //var type = JSON.parse("[" + arrString[2] + "]");
            //console.log(arrString[1])
            $(e.currentTarget).find("#<%=TxtKode2.ClientID%>").val(arrString[0]);
            $(e.currentTarget).find("#<%=TxtNama_Tmp2.ClientID%>").val(arrString[1]);

            
            //$(e.currentTarget).find("#<%=txtStartDate2.ClientID%>").val(arrString[2]);
            //$(e.currentTarget).find("#<%=TxtEndDate2.ClientID%>").val(arrString[3]);

            //$(e.currentTarget).find("#<%=TxtFlagEntry2.ClientID%>").val(arrString[4]);
            //$(e.currentTarget).find("#<%=TxtFlagReport2.ClientID%>").val(arrString[5]);

            document.getElementById("TxtKode2").onkeydown = function () { return false; }
            document.getElementById("TxtNama_Tmp2").onkeydown = function () { return false; }
            //document.getElementById("txtStartDate2").onkeydown = function () { return false; }
            //document.getElementById("TxtEndDate2").onkeydown = function () { return false; }
            //document.getElementById("TxtFlagEntry2").onkeydown = function () { return false; }
            //document.getElementById("TxtFlagReport2").onkeydown = function () { return false; }

        });

        $('#formModalNew').on('show.bs.modal', function (e) {
            var bookId = $(e.relatedTarget).data('book-id');
            var arrString = []

            arrString = bookId.split("|")

            //var type = JSON.parse("[" + arrString[2] + "]");
            //console.log(arrString[1])
            $(e.currentTarget).find("#<%=TxtKode3.ClientID%>").val(arrString[0]);
            $(e.currentTarget).find("#<%=TxtNama_Tmp3.ClientID%>").val(arrString[1]);
            $(e.currentTarget).find("#<%=txtStartDate3.ClientID%>").val(arrString[2]);
            $(e.currentTarget).find("#<%=TxtEndDate3.ClientID%>").val(arrString[3]);

                        $(e.currentTarget).find("#<%=txtSelfAssesment3.ClientID%>").val(arrString[6]);
                        $(e.currentTarget).find("#<%=txtdept3.ClientID%>").val(arrString[7]);

            $(e.currentTarget).find("#<%=txtNNILAI3.ClientID%>").val(arrString[8]);
            $(e.currentTarget).find("#<%=txtPNILAI3.ClientID%>").val(arrString[9]);


            //$(e.currentTarget).find("#<%=TxtFlagEntry3.ClientID%>").val(arrString[4]);
            //$(e.currentTarget).find("#<%=TxtFlagReport3.ClientID%>").val(arrString[5]);

        });

    </script>

    <asp:Label ID="infoerror" runat="server" style="color: red" ></asp:Label>

</asp:Content>


