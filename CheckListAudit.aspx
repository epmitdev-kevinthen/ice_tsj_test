﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="CheckListAudit.aspx.vb" Inherits="CheckListAudit" MaintainScrollPositionOnPostback="true"%>




<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>
        .large-width {
            min-width: 500px !important;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <br />


    <asp:HiddenField id="hdnMaxNilai" runat="server"/>
    <asp:Label ID="lblHead" runat="server" Text="CheckList ICE"
        style="font-size: 20px;"
        ></asp:Label>
    <br />
    <div style="font-size: 20px;">
        <b>
            <Table>
                <tr>
                    <td><asp:Label ID="Lblbranch" runat="server">Cabang </asp:Label>&nbsp;&nbsp;</td>
                    <td><asp:Dropdownlist id="doBranch" runat="server" width="250px"></asp:Dropdownlist></td>
                    <td rowspan="3"> &nbsp;&nbsp;&nbsp; <asp:Button ID="btnFind" CssClass="button2" runat="server" Text="Find" /></td>
                    <td rowspan="3"> &nbsp;&nbsp;&nbsp; <asp:Button ID="btnConfirm" CssClass="button2" runat="server" Text="Approve" /></td>

                    <td rowspan="3"> &nbsp;&nbsp;&nbsp; <asp:Button ID="btnUnconfirm" CssClass="button2" runat="server" Text="UnApprove" /></td>

                    <td rowspan="3"> &nbsp;&nbsp;&nbsp; <asp:Button ID="btnValidate" CssClass="button2" runat="server" Text="Validate for Reporting" /></td>

                    <td rowspan="3"> &nbsp;&nbsp;&nbsp; <button id="btnExport" type="button" class="button2" onclick="fnExcelReport();">Export to Excel</button></td>
                    

                    <td rowspan="3"> &nbsp;&nbsp;&nbsp; <asp:label ID="infoerror1" runat="server" Font-Bold="true"  ></asp:label>   </td>
                </tr>
                <tr>
                    <td><asp:Label ID="lblTemplate" runat="server">CheckList </asp:Label>&nbsp;&nbsp;</td>
                    <td><asp:Dropdownlist id="doTemplate" runat="server" width="400px"></asp:Dropdownlist></td>
                </tr>
                <tr>
                    <td><asp:Label ID="lblPeriode" runat="server">Periode </asp:Label>&nbsp;&nbsp;</td>
                    <td><asp:Dropdownlist id="doPeriode" runat="server" width="400px"></asp:Dropdownlist></td>
                </tr>
                <tr>
                    <td><asp:Label ID="Label1" runat="server">Status </asp:Label>&nbsp;&nbsp;</td>
                    <td><asp:Dropdownlist id="doStatusFind" runat="server" width="200px"></asp:Dropdownlist></td>
                </tr>
                <tr>
                    <td><asp:Label ID="Label2" runat="server">Entry </asp:Label>&nbsp;&nbsp;</td>
                    <td><asp:Dropdownlist id="doEntryFind" runat="server" width="200px"></asp:Dropdownlist></td>
                </tr>
                
    
                </Table>
            </b>
        <div id="divAuditor" runat="server" style="border: double; display: inline-block;">
            <b>AUDITOR</b> <br />
                    <asp:Label ID="Label3" runat="server">Auditor - 1 </asp:Label>&nbsp;&nbsp;
                    <asp:Dropdownlist id="ddlAuditor1" runat="server" width="200px"></asp:Dropdownlist> <br />
                
                
                    <asp:Label ID="Label4" runat="server">Auditor - 2 </asp:Label>&nbsp;&nbsp;
                    <asp:Dropdownlist id="ddlAuditor2" runat="server" width="200px"></asp:Dropdownlist> <br />
                
                
                    <asp:Label ID="Label5" runat="server">Auditor - 3 </asp:Label>&nbsp;&nbsp;
                    <asp:Dropdownlist id="ddlAuditor3" runat="server" width="200px"></asp:Dropdownlist> <br />
            <asp:Button id="btnSaveAuditor" runat="server" OnClick="btnSaveAuditor_Click" Text="Save Auditor" CssClass="button2"/>
                
        </div>
    </div>

    <asp:HiddenField id="scroll_position" runat="server"/>
    
<script  type="text/javascript">
    $(document).ready(function () {
        var table = $('#mytable').DataTable({
            scrollX: "800px",
            scrollCollapse: true,
            bProcessing: true, // shows 'processing' label
            bStateSave: true, // presumably saves state for reloads
            paging: true,
            pagelangth: 5,
            lengthMenu: [
                [ 5, 10, 25, 50, -1 ],
                [ '5 rows', '10 rows', '25 rows', '50 rows', 'Show all' ]
            ],
            fixedColumns: {
                leftColumns: 2
            }
        });
    });  

    function btnClick(args) {
        window.location.href = "r.aspx?a=1&b=" + args;
    }

    function openw(p) {
        window.open('brwfolder.aspx?p=' + p, 'mywindow', 'left=100,top=100,width=1300,height=1000,scrollbars=yes,resizable=no');
    };

    function fncBrowseF() {
        window.open('frmBrowseFile.aspx', 'mywindow', 'left=100,top=100,width=450,height=300,scrollbars=yes,resizable=no');
    }

    function fncBrowseClear() {
        document.getElementById("txtBuktiObyektif").value = "";
    }


    function fncBrowseF__3() {
        window.open('frmBrowseFile__3.aspx', 'mywindow', 'left=100,top=100,width=450,height=300,scrollbars=yes,resizable=no');
    }

    function fncBrowseClear__3() {
        document.getElementById("txtBuktiObyektif__3").value = "";
    }

    function fncBrowseF__4() {
        window.open('frmBrowseFile__4.aspx', 'mywindow', 'left=100,top=100,width=450,height=300,scrollbars=yes,resizable=no');
    }

    function fncBrowseClear__4() {
        document.getElementById("txtBuktiObyektif__4").value = "";
    }


    function fncBrowseF__6() {
        window.open('frmBrowseFile__6.aspx', 'mywindow', 'left=100,top=100,width=450,height=300,scrollbars=yes,resizable=no');
    }

    function fncBrowseClear__6() {
        document.getElementById("txtBuktiObyektif__6").value = "";
    }




    function DropOnChange1() {
        if (document.getElementById("txtDone1").value == "N/A") { document.getElementById("txtDone2").value = "N/A"; $("#<%= hdnDone2.ClientID %>").val("N/A");};
        }

    function DropOnChange2() {
        if (document.getElementById("txtDone2").value == "N/A") { document.getElementById("txtDone1").value = "N/A"; $("#<%= hdnDone1.ClientID %>").val("N/A"); };
    }

    function fnExcelReport() {
        var tab_text = "<table border='2px'><tr bgcolor='#87AFC6'>";
        var textRange; var j = 0;
        tab = document.getElementById('mytable1'); // id of table

        for (j = 0 ; j < tab.rows.length ; j++) {
            tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
            //tab_text=tab_text+"</tr>";
        }

        tab_text = tab_text + "</table>";
        tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
        tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
        tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
        {
            txtArea1.document.open("txt/html", "replace");
            txtArea1.document.write(tab_text);
            txtArea1.document.close();
            txtArea1.focus();
            sa = txtArea1.document.execCommand("SaveAs", true, "Say Thanks to Sumit.xls");
        }
        else                 //other browser not tested on IE 11
            sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

        return (sa);
    }

</script>

<div style="margin:30px;">

    <!--<button type='button' class='button2xx' data-book-id='||||A|A' data-target='#formModalNew' data-toggle='modal'>Add New</button>
    -->
    
    <asp:Label ID="_periode" style="display: none " runat="server"></asp:Label>
    <asp:Label ID="_cabang" style="display: none " runat="server"></asp:Label>
    <asp:Label ID="_kode" style="display: none " runat="server"></asp:Label>

    <asp:Label ID="_statusFind" style="display: none " runat="server"></asp:Label>
    <asp:Label ID="_entryFind" style="display: none " runat="server"></asp:Label>

    <asp:PlaceHolder ID="myPlaceH" runat="server"></asp:PlaceHolder>

</div>

    <div id="formModalEdit" tabindex="-1" role="dialog" aria-hidden="true" style="display: none ">
        <table style="position: fixed; top:50px ; left:50px; background-color: ButtonFace; border: 5px; border-radius: 15px; padding: 20px; box-shadow: 5px 5px 5px gray;">
            <tr>
                <td><label>PIC</label>

                               <asp:TextBox ID="TxtNo" runat="server" width="0"
               ClientIDMode="Static" style="background-color: lightgray"></asp:TextBox>

                </td>
                <td><label>1st Line Defence Activity</label></td>
                <td><label>Done Control</label></td>
            </tr><tr>
                <td>
           <asp:TextBox ID="TxtPIC1" runat="server" MaxLength="100" Width="100px" ClientIDMode="Static" style="background-color: lightgray"></asp:TextBox></td><td>
           <asp:TextBox ID="txtActivity1" runat="server" TextMode="MultiLine"  MaxLength="1000" Width="350px" ClientIDMode="Static" style="background-color: lightgray"></asp:TextBox></td><td>
           <asp:DropDownList ID="txtDone1" runat="server" Width="120px" ClientIDMode="Static" Visible="false"></asp:DropDownList><div id="divTxtDone1" runat="server" visible="false"><select id="txtDone1_New"></select></div><asp:HiddenField id="hdnDone1" runat="server"/></td>
            </tr><tr>
                <td><label>PIC</label></td>
                <td><label>2nd Line Defence Activity</label></td>
                <td><label>Done Control</label></td>
            </tr><tr><td>
           <asp:TextBox ID="TxtPIC2" runat="server" MaxLength="100" Width="100px" ClientIDMode="Static" style="background-color: lightgray"></asp:TextBox></td><td>
           <asp:TextBox ID="txtActivity2" runat="server" TextMode="MultiLine"  MaxLength="1000" Width="350px" ClientIDMode="Static" style="background-color: lightgray"></asp:TextBox></td><td>
           <asp:DropDownList ID="txtDone2" runat="server" Width="120px" ClientIDMode="Static" Visible="false"></asp:DropDownList><div id="divTxtDone2" runat="server" visible="false"><select id="txtDone2_New"></select></div><asp:HiddenField id="hdnDone2" runat="server"/></td>
           </tr>

            <!--
          <tr><td>
           <asp:Label runat="server">Status</asp:Label></td>
              <td>
           <asp:DropDownList ID="doStatus" runat="server" Width="120px" ClientIDMode="Static"></asp:DropDownList></td>
              <td></td>
           </tr>
            -->

          <tr><td>
           <asp:Label runat="server">Hasil Audit</asp:Label></td>
              <td>
           <asp:TextBox ID="txtHasilAudit" runat="server" textmode="MultiLine" MaxLength="1000" Width="350px" ClientIDMode="Static"></asp:TextBox></td>
              <td></td>
           </tr>
          <tr><td>
           <asp:Label runat="server">Bukti Obyektif</asp:Label></td>
              <td>
                  <button id="brwA1" type="button" onclick="fncBrowseF()">Upload</button> &nbsp;
                  <button id="brwclear" type="button" onclick="fncBrowseClear()">Clear Upload</button> &nbsp;
           <asp:TextBox ID="txtBuktiObyektif" runat="server"  style="background-color: lightgray" MaxLength="1000" Width="350px" ClientIDMode="Static"></asp:TextBox></td>
              <td></td>
           </tr>
          <tr style="display:none;"><td>
           <asp:Label runat="server">Recommendation</asp:Label></td>
              <td>
           <asp:TextBox ID="txtRecomendation" runat="server" textmode="MultiLine" MaxLength="1000" Width="350px" ClientIDMode="Static"></asp:TextBox></td>
              <td></td>
           </tr>
          <tr style="display:none;"><td>
           <asp:Label runat="server">Branch Follow-Up</asp:Label></td>
              <td>
           <asp:TextBox ID="txtBranchFU" runat="server" textmode="MultiLine" MaxLength="1000" Width="350px" ClientIDMode="Static"></asp:TextBox></td>
              <td></td>
           </tr>
          <tr style="display:none;"><td>
           <asp:Label runat="server">Implementasi Recommendation</asp:Label></td>
              <td>
           <asp:DropDownList ID="doImplemenRecom" runat="server" Width="120px" ClientIDMode="Static"></asp:DropDownList></td>
              <td></td>
           </tr>

           <tr><td colspan="3" style="text-align: center;">&nbsp;
               </td>
           </tr>
           <tr><td colspan="3" style="text-align: center;">
               <asp:Button ID="btnUpdate" CssClass="btn1" runat="server" text="Update"/> &nbsp;&nbsp;
               <asp:Button ID="btnCancel1" CssClass="btn1" runat="server" text="Cancel"/>
               </td>
           </tr>
       </table>
    </div>

    <asp:Label ID="infoerror" runat="server" style="color: red" ></asp:Label>


    <div id="formSelfReg01" style="display: none ">
        <table style="position: fixed; top:50px ; left:50px; background-color: ButtonFace; border: 5px; border-radius: 15px; padding: 20px; box-shadow: 5px 5px 5px gray;">
            <tr>
                <td><label>PIC</label>

                               <asp:TextBox ID="TxtNo__1" runat="server" width="0"
               ClientIDMode="Static" style="background-color: lightgray"></asp:TextBox>

                </td>
                <td><label>1st Line Defence Activity</label></td>
                <td><label>Done Control</label></td>
            </tr><tr>
                <td>
           <asp:TextBox ID="TxtPIC1__1" runat="server" MaxLength="100" Width="100px" ClientIDMode="Static" style="background-color: lightgray"></asp:TextBox></td><td>
           <asp:TextBox ID="txtActivity1__1" runat="server" TextMode="MultiLine"  MaxLength="1000" Width="350px" ClientIDMode="Static" style="background-color: lightgray"></asp:TextBox></td><td>
           <asp:DropDownList ID="txtDone1__1" runat="server" Width="120px" ClientIDMode="Static" Visible="false"></asp:DropDownList><div id="divTxtDone1__1" runat="server" visible="false"><select id="txtDone1__1_New"></select></div><asp:HiddenField id="hdnDone1__1" runat="server"/></td>
            </tr>
           <tr><td colspan="3" style="text-align: center;">&nbsp;
               </td>
           </tr>
           <tr><td colspan="3" style="text-align: center;">
               <asp:Button ID="btnUpdate__1" CssClass="btn1" runat="server" text="Update"/> &nbsp;&nbsp;
               <asp:Button ID="btnCancel1__1" CssClass="btn1" runat="server" text="Cancel"/>
               </td>
           </tr>
       </table>
    </div>



    <div id="formModalEdit_2" style="display: none ">
        
        <table style="position: fixed; top:50px ; left:50px; background-color: ButtonFace; border: 5px; border-radius: 15px; padding: 20px; box-shadow: 5px 5px 5px gray;">
            <tr>
                <td><label>PIC</label>

                               <asp:TextBox ID="TxtNo__2" runat="server" width="0"
               ClientIDMode="Static" style="background-color: lightgray"></asp:TextBox>

                </td>
                <td><label>2nd Line Defence Activity</label></td>
                <td><label>Done Control</label></td>
            </tr><tr><td>
           <asp:TextBox ID="TxtPIC2__2" runat="server" MaxLength="100" Width="100px" ClientIDMode="Static" style="background-color: lightgray"></asp:TextBox></td><td>
           <asp:TextBox ID="txtActivity2__2" runat="server" TextMode="MultiLine"  MaxLength="1000" Width="350px" ClientIDMode="Static" style="background-color: lightgray"></asp:TextBox></td><td>
           <asp:DropDownList ID="txtDone2__2" runat="server" Width="120px" ClientIDMode="Static" Visible="false"></asp:DropDownList><div id="divTxtDone2__2" runat="server" visible="false"><select id="txtDone2__2_New"></select></div><asp:HiddenField id="hdnDone2__2" runat="server"/></td>
           </tr>
           <tr><td colspan="3" style="text-align: center;">&nbsp;
               </td>
           </tr>
           <tr><td colspan="3" style="text-align: center;">
               <asp:Button ID="btnUpdate__2" CssClass="btn1" runat="server" text="Update"/> &nbsp;&nbsp;
               <asp:Button ID="btnCancel1__2" CssClass="btn1" runat="server" text="Cancel"/>
               </td>
           </tr>
       </table>
    </div>



        <div id="formModalEdit_3" style="display: none ">
<asp:TextBox ID="TxtNo__3" runat="server" width="0"
               ClientIDMode="Static" style="background-color: lightgray"></asp:TextBox>
        <table style="position: fixed; top:50px ; left:50px; background-color: ButtonFace; border: 5px; border-radius: 15px; padding: 20px; box-shadow: 5px 5px 5px gray;">
          <tr><td>
           <asp:Label runat="server">Hasil Audit</asp:Label></td>
              <td>
           <asp:TextBox ID="txtHasilAudit__3" runat="server" textmode="MultiLine" MaxLength="1000" Width="350px" ClientIDMode="Static"></asp:TextBox></td>
              <td></td>
           </tr>
          <tr><td>
           <asp:Label runat="server">Bukti Obyektif</asp:Label></td>
              <td>
                  <button id="brwA1__3" type="button" onclick="fncBrowseF__3()">Upload</button> &nbsp;
                  <button id="brwclear__3" type="button" onclick="fncBrowseClear__3()">Clear Upload</button> &nbsp;
           <asp:TextBox ID="txtBuktiObyektif__3" runat="server"  style="background-color: lightgray" MaxLength="1000" Width="350px" ClientIDMode="Static"></asp:TextBox></td>
              <td></td>
           </tr>
          <tr style="display:none;"><td>
           <asp:Label runat="server">Recommendation</asp:Label></td>
              <td>
           <asp:TextBox ID="txtRecomendation__3" runat="server" textmode="MultiLine" MaxLength="1000" Width="350px" ClientIDMode="Static"></asp:TextBox></td>
              <td></td>
           </tr>
          <tr style="display:none;"><td>
           <asp:Label runat="server">Implementasi Recommendation</asp:Label></td>
              <td>
           <asp:DropDownList ID="doImplemenRecom__3" runat="server" Width="120px" ClientIDMode="Static"></asp:DropDownList></td>
              <td></td>
           </tr>

           <tr><td colspan="3" style="text-align: center;">&nbsp;
               </td>
           </tr>
           <tr><td colspan="3" style="text-align: center;">
               <asp:Button ID="btnUpdate__3" CssClass="btn1" runat="server" text="Update"/> &nbsp;&nbsp;
               <asp:Button ID="btnCancel1__3" CssClass="btn1" runat="server" text="Cancel"/>
               </td>
           </tr>
       </table>
    </div>


        <div id="formModalEdit_4" style="display: none ">
<asp:TextBox ID="TxtNo__4" runat="server" width="0"
               ClientIDMode="Static" style="background-color: lightgray"></asp:TextBox>
        <table style="position: fixed; top:50px ; left:50px; background-color: ButtonFace; border: 5px; border-radius: 15px; padding: 20px; box-shadow: 5px 5px 5px gray;">

          <tr><td>
           <asp:Label runat="server">Hasil Audit</asp:Label></td>
              <td>
           <asp:TextBox ID="txtHasilAudit__4" runat="server" textmode="MultiLine" MaxLength="1000" Width="350px" ClientIDMode="Static"></asp:TextBox></td>
              <td></td>
           </tr>

          <tr><td>
           <asp:Label runat="server">Bukti Obyektif</asp:Label></td>
              <td>
                  <button id="brwA1__4" type="button" onclick="fncBrowseF__4()">Upload</button> &nbsp;
                  <button id="brwclear__4" type="button" onclick="fncBrowseClear__4()">Clear Upload</button> &nbsp;
           <asp:TextBox ID="txtBuktiObyektif__4" runat="server"  style="background-color: lightgray" MaxLength="1000" Width="350px" ClientIDMode="Static"></asp:TextBox></td>
              <td></td>
           </tr>


          <tr style="display:none;"><td>
           <asp:Label runat="server">Branch Follow-Up</asp:Label></td>
              <td>
           <asp:TextBox ID="txtBranchFU__4" runat="server" textmode="MultiLine" MaxLength="1000" Width="350px" ClientIDMode="Static"></asp:TextBox></td>
              <td></td>
           </tr>

           <tr><td colspan="3" style="text-align: center;">&nbsp;
               </td>
           </tr>
           <tr><td colspan="3" style="text-align: center;">
               <asp:Button ID="btnUpdate__4" CssClass="btn1" runat="server" text="Update"/> &nbsp;&nbsp;
               <asp:Button ID="btnCancel1__4" CssClass="btn1" runat="server" text="Cancel"/>
               </td>
           </tr>
       </table>
    </div>


            <div id="formModalEdit_6" style="display: none ">
<asp:TextBox ID="TxtNo__6" runat="server" width="0"
               ClientIDMode="Static" style="background-color: lightgray"></asp:TextBox>
        <table style="position: fixed; top:50px ; left:50px; background-color: ButtonFace; border: 5px; border-radius: 15px; padding: 20px; box-shadow: 5px 5px 5px gray;">

          <tr><td>
           <asp:Label runat="server">Hasil Audit</asp:Label></td>
              <td>
           <asp:TextBox ID="txtHasilAudit__6" runat="server" textmode="MultiLine" MaxLength="1000" Width="350px" ClientIDMode="Static"></asp:TextBox></td>
              <td></td>
           </tr>

          <tr><td>
           <asp:Label runat="server">Bukti Obyektif</asp:Label></td>
              <td>
                  <button id="brwA1__6" type="button" onclick="fncBrowseF__6()">Upload</button> &nbsp;
                  <button id="brwclear__6" type="button" onclick="fncBrowseClear__6()">Clear Upload</button> &nbsp;
           <asp:TextBox ID="txtBuktiObyektif__6" runat="server"  style="background-color: lightgray" MaxLength="1000" Width="350px" ClientIDMode="Static"></asp:TextBox></td>
              <td></td>
           </tr>


          <tr style="display:none;"><td>
           <asp:Label runat="server">Branch Follow-Up</asp:Label></td>
              <td>
           <asp:TextBox ID="txtBranchFU__6" runat="server" textmode="MultiLine" MaxLength="1000" Width="350px" ClientIDMode="Static"></asp:TextBox></td>
              <td></td>
           </tr>

           <tr><td colspan="3" style="text-align: center;">&nbsp;
               </td>
           </tr>
           <tr><td colspan="3" style="text-align: center;">
               <asp:Button ID="btnUpdate__6" CssClass="btn1" runat="server" text="Update"/> &nbsp;&nbsp;
               <asp:Button ID="btnCancel1__6" CssClass="btn1" runat="server" text="Cancel"/>
               </td>
           </tr>
       </table>
    </div>

    <!-- tabindex="-1" role="dialog" aria-hidden="true" -->
        <div id="formModalEdit_5" style="display: none ">
<asp:TextBox ID="TxtNo__5" runat="server" width="0"
               ClientIDMode="Static" style="background-color: lightgray"></asp:TextBox>
        <table style="position: fixed; top:50px ; left:50px; background-color: ButtonFace; border: 5px; border-radius: 15px; padding: 20px; box-shadow: 5px 5px 5px gray;">

          <tr><td>
           <asp:Label runat="server">Verifikasi Audit</asp:Label></td>
              <td>

<asp:DropDownList ID="doVerifikasi_Audit_Status__5" runat="server" Width="120px" ClientIDMode="Static"></asp:DropDownList>
           <br/>
           <asp:TextBox ID="txtVerifikasi_Audit_Remark__5" runat="server" textmode="MultiLine" MaxLength="1000" Width="350px" ClientIDMode="Static"></asp:TextBox></td>
              <td></td>
           </tr>

           <tr><td colspan="3" style="text-align: center;">&nbsp;
               </td>
           </tr>
           <tr><td colspan="3" style="text-align: center;">
               <asp:Button ID="btnUpdate__5" CssClass="btn1" runat="server" text="Update"/> &nbsp;&nbsp;
               <asp:Button ID="btnCancel1__5" CssClass="btn1" runat="server" text="Cancel"/>
               </td>
           </tr>
       </table>
    </div>



    <script type="text/javascript">

        window.onload = function () {
            var div = $(".dataTables_scrollBody");
            var div_position = $("#<%= scroll_position.ClientID %>");
            var position = parseInt($("#<%= scroll_position.ClientID %>").val());
            if (isNaN(position)) {
                position = 0;
            }
            div.scrollLeft(position);
            div.scroll(function () {
                div_position.val(div.scrollLeft());
            });
        };

        $(function () {
            var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "personal";
            $('#Tabs a[href="#' + tabName + '"]').tab('show');
            $("#Tabs a").click(function () {
                $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
            });
        });


        $('#formSelfReg01').on('show.bs.modal', function (e) {

            //alert("1");

            var bookId = $(e.relatedTarget).data('book-id');
            var arrString = []
            arrString = bookId.split("|")
            //console.log(arrString[20]);
            if($("#txtDone1__1_New").length){
                $("#txtDone1__1_New option").each(function () {
                    $(this).remove();
                });
                drawOption("txtDone1__1_New", { "nilai": "N/A", "penjelasan": "N/A" });
                $.ajax({
                    type: "POST",
                    url: "CheckListAudit.aspx/getKriteriaPenilaian",
                    data: '{kode_kriteria: "' + arrString[21] + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var json_result = JSON.parse(response.d);                    
                        for (var i = 0; i < json_result.length; i++) {
                            drawOption("txtDone1__1_New", json_result[i]);
                        }
                        $("#<%= hdnDone1__1.ClientID %>").val($('#txtDone1__1_New').val());

                    if (arrString[4] != "") {
                        $(e.currentTarget).find("#txtDone1__1_New").val(arrString[4]);
                        $("#<%= hdnDone1__1.ClientID %>").val($('#txtDone1__1_New').val());
                    } else {
                        $(e.currentTarget).find("#txtDone1__1_New :nth-child(2)").prop('selected', true);
                        $("#<%= hdnDone1__1.ClientID %>").val($('#txtDone1__1_New').val());
                    }
                },
                error: function (response) { console.log(response);}
            });
            }
            
            $(e.currentTarget).find("#<%=TxtNo__1.ClientID%>").val(arrString[1]);
            $(e.currentTarget).find("#<%=txtDone1__1.ClientID%>").val(arrString[4]);
            $(e.currentTarget).find("#<%=hdnDone1__1.ClientID%>").val(arrString[4]);
            $(e.currentTarget).find("#<%=TxtPIC1__1.ClientID%>").val(arrString[13]);
            $(e.currentTarget).find("#<%=txtActivity1__1.ClientID%>").val(arrString[12]);
            document.getElementById("TxtNo__1").onkeydown = function () { return false; }
            document.getElementById("TxtPIC1__1").onkeydown = function () { return false; }
            document.getElementById("txtActivity1__1").onkeydown = function () { return false; }
        });

        function drawOption(id_select, rowData) {
            $("#" + id_select).append(`<option value="${rowData.nilai}"> 
                                       ${rowData.penjelasan} 
                                  </option>`);
        }

        $('#txtDone1__1_New').on('change', function (e) {
            $("#<%= hdnDone1__1.ClientID %>").val($(this).val());
        });

        $('#<%= txtDone1__1.ClientID %>').on('change', function (e) {
            $("#<%= hdnDone1__1.ClientID %>").val($(this).val());
        });

        $('#<%= txtDone2__2.ClientID %>').on('change', function (e) {
            //console.log("asd");
            $("#<%= hdnDone2__2.ClientID %>").val($(this).val());
        });

        $('#<%= txtDone1.ClientID %>').on('change', function (e) {
            $("#<%= hdnDone1.ClientID %>").val($(this).val());
        });

        $('#<%= txtDone2.ClientID %>').on('change', function (e) {
            $("#<%= hdnDone2.ClientID %>").val($(this).val());
        });

        $('#txtDone2__2_New').on('change', function (e) {
            $("#<%= hdnDone2__2.ClientID %>").val($(this).val());
        });

        $('#txtDone1_New').on('change', function (e) {
            $("#<%= hdnDone1.ClientID %>").val($(this).val());
        });

        $('#txtDone2_New').on('change', function (e) {
            $("#<%= hdnDone2.ClientID %>").val($(this).val());
        });

        $('#formModalEdit').on('show.bs.modal', function (e) {
            var bookId = $(e.relatedTarget).data('book-id');
            var arrString = []

            //alert("1");

            arrString = bookId.split("|")
            if($("#txtDone1_New").length){
                $("#txtDone1_New option").each(function () {
                    $(this).remove();
                });
                drawOption("txtDone1_New", {"nilai" : "N/A", "penjelasan": "N/A"});
                $.ajax({
                    type: "POST",
                    url: "CheckListAudit.aspx/getKriteriaPenilaian",
                    data: '{kode_kriteria: "' + arrString[21] + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var json_result = JSON.parse(response.d);
                        for (var i = 0; i < json_result.length; i++) {
                            drawOption("txtDone1_New", json_result[i]);
                        }
                        $("#<%= hdnDone1.ClientID %>").val($('#txtDone1_New').val());

                        if (arrString[4] != "") {
                            $(e.currentTarget).find("#txtDone1_New").val(arrString[4]);
                            $("#<%= hdnDone1.ClientID %>").val($('#txtDone1_New').val());
                        } else {
                            $(e.currentTarget).find("#txtDone1_New :nth-child(2)").prop('selected', true);
                            $("#<%= hdnDone1.ClientID %>").val($('#txtDone1_New').val());
                    }
                    },
                    error: function (response) { console.log(response); }
                });
        }
            
            if($("#txtDone2_New").length){
                $("#txtDone2_New option").each(function () {
                    $(this).remove();
                });
                drawOption("txtDone2_New", { "nilai": "N/A", "penjelasan": "N/A" });
                $.ajax({
                    type: "POST",
                    url: "CheckListAudit.aspx/getKriteriaPenilaian",
                    data: '{kode_kriteria: "' + arrString[22] + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var json_result = JSON.parse(response.d);
                        for (var i = 0; i < json_result.length; i++) {
                            drawOption("txtDone2_New", json_result[i]);
                        }
                        $("#<%= hdnDone2.ClientID %>").val($('#txtDone2_New').val());

                        if (arrString[5] != "") {
                            $(e.currentTarget).find("#txtDone2_New").val(arrString[5]);
                            $("#<%= hdnDone2.ClientID %>").val($('#txtDone2_New').val());
                    } else {
                        $(e.currentTarget).find("#txtDone2_New :nth-child(2)").prop('selected', true);
                        $("#<%= hdnDone2.ClientID %>").val($('#txtDone2_New').val());
                    }
                    },
                    error: function (response) { console.log(response); }
                });
        }
            

            //alert(arrString[1]);
            //alert(arrString[4]);
            //var type = JSON.parse("[" + arrString[2] + "]");
            //console.log(arrString[1])
            //$(e.currentTarget).find("#<%=TxtNo.ClientID%>").val(arrString[1]);
            //$(e.currentTarget).find("#<%=txtDone1.ClientID%>").val(arrString[4]);
            //$(e.currentTarget).find("#<%=txtDone2.ClientID%>").val(arrString[5]);
            //$(e.currentTarget).find("#<%=doStatus.ClientID%>").val(arrString[6]);
            //$(e.currentTarget).find("#<%=txtHasilAudit.ClientID%>").val(arrString[7);
            //$(e.currentTarget).find("#<%=txtBuktiObyektif.ClientID%>").val(arrString[8);
            //$(e.currentTarget).find("#<%=txtRecomendation.ClientID%>").val(arrString[9);
            //$(e.currentTarget).find("#<%=txtBranchFU.ClientID%>").val(arrString[10);
            //$(e.currentTarget).find("#<%=doImplemenRecom.ClientID%>").val(arrString[11);
            $(e.currentTarget).find("#<%=TxtNo.ClientID%>").val(arrString[1]);
            $(e.currentTarget).find("#<%=txtHasilAudit.ClientID%>").val(arrString[7]);
            $(e.currentTarget).find("#<%=txtBuktiObyektif.ClientID%>").val(arrString[8]);
            $(e.currentTarget).find("#<%=txtRecomendation.ClientID%>").val(arrString[9]);
            $(e.currentTarget).find("#<%=txtBranchFU.ClientID%>").val(arrString[10]);
            $(e.currentTarget).find("#<%=doImplemenRecom.ClientID%>").val(arrString[11]);
            $(e.currentTarget).find("#<%=txtDone1.ClientID%>").val(arrString[4]);
            $(e.currentTarget).find("#<%=txtDone2.ClientID%>").val(arrString[5]);
            $(e.currentTarget).find("#<%=hdnDone1.ClientID%>").val(arrString[4]);
            $(e.currentTarget).find("#<%=hdnDone2.ClientID%>").val(arrString[5]);
            //$(e.currentTarget).find("#<%=doStatus.ClientID%>").val(arrString[6]);
            $(e.currentTarget).find("#<%=TxtPIC1.ClientID%>").val(arrString[13]);
            $(e.currentTarget).find("#<%=txtActivity1.ClientID%>").val(arrString[12]);
            $(e.currentTarget).find("#<%=TxtPIC2.ClientID%>").val(arrString[15]);
            $(e.currentTarget).find("#<%=txtActivity2.ClientID%>").val(arrString[14]);
            $("#<%=hdnMaxNilai.ClientID%>").val(arrString[23]);
            document.getElementById("TxtNo").onkeydown = function () { return false; }
            document.getElementById("TxtPIC1").onkeydown = function () { return false; }
            document.getElementById("txtActivity1").onkeydown = function () { return false; }
            document.getElementById("TxtPIC2").onkeydown = function () { return false; }
            document.getElementById("txtActivity2").onkeydown = function () { return false; }
            document.getElementById("txtBuktiObyektif").onkeydown = function () { return false; }
        });



        $('#formModalEdit_2').on('show.bs.modal', function (e) {
            var bookId = $(e.relatedTarget).data('book-id');
            var arrString = []

            arrString = bookId.split("|")
            if($("#txtDone2__2_New").length){
                $("#txtDone2__2_New option").each(function () {
                    $(this).remove();
                });
                drawOption("txtDone2__2_New", { "nilai": "N/A", "penjelasan": "N/A" });
                $.ajax({
                    type: "POST",
                    url: "CheckListAudit.aspx/getKriteriaPenilaian",
                    data: '{kode_kriteria: "' + arrString[22] + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var json_result = JSON.parse(response.d);
                        for (var i = 0; i < json_result.length; i++) {
                            drawOption("txtDone2__2_New", json_result[i]);
                        }
                        $("#<%= hdnDone2__2.ClientID %>").val($('#txtDone2__2_New').val());
                    if (arrString[5] != "") {
                        $(e.currentTarget).find("#txtDone2__2_New").val(arrString[5]);
                        $("#<%= hdnDone2__2.ClientID %>").val($('#txtDone2__2_New').val());
                    } else {
                        $(e.currentTarget).find("#txtDone2__2_New :nth-child(2)").prop('selected', true);
                        $("#<%= hdnDone2__2.ClientID %>").val($('#txtDone2__2_New').val());
                    }
                    console.log(json_result);
                },
                error: function (response) { console.log(response); }
            });
            
            }
            
            
            $(e.currentTarget).find("#<%=TxtNo__2.ClientID%>").val(arrString[1]);

            $(e.currentTarget).find("#<%=txtDone2__2.ClientID%>").val(arrString[5]);
            $(e.currentTarget).find("#<%=hdnDone2__2.ClientID%>").val(arrString[5]);


            $(e.currentTarget).find("#<%=TxtPIC2__2.ClientID%>").val(arrString[15]);
            $(e.currentTarget).find("#<%=txtActivity2__2.ClientID%>").val(arrString[14]);
            $("#<%=hdnMaxNilai.ClientID%>").val(arrString[23]);

            document.getElementById("TxtNo__2").onkeydown = function () { return false; }
            document.getElementById("TxtPIC2__2").onkeydown = function () { return false; }
            document.getElementById("txtActivity2__2").onkeydown = function () { return false; }

        });


        $('#formModalEdit_3').on('show.bs.modal', function (e) {
            var bookId = $(e.relatedTarget).data('book-id');
            var arrString = []

            arrString = bookId.split("|")

            
            $(e.currentTarget).find("#<%=TxtNo__3.ClientID%>").val(arrString[1]);
            $(e.currentTarget).find("#<%=txtHasilAudit__3.ClientID%>").val(arrString[7]);
            $(e.currentTarget).find("#<%=txtBuktiObyektif__3.ClientID%>").val(arrString[8]);
            $(e.currentTarget).find("#<%=txtRecomendation__3.ClientID%>").val(arrString[9]);
            $(e.currentTarget).find("#<%=doImplemenRecom__3.ClientID%>").val(arrString[11]);

            document.getElementById("txtBuktiObyektif__3").onkeydown = function () { return false; }

        });


                $('#formModalEdit_6').on('show.bs.modal', function (e) {
            var bookId = $(e.relatedTarget).data('book-id');
            var arrString = []

            arrString = bookId.split("|")

            
            $(e.currentTarget).find("#<%=TxtNo__6.ClientID%>").val(arrString[1]);
            $(e.currentTarget).find("#<%=txtHasilAudit__6.ClientID%>").val(arrString[18]);
            $(e.currentTarget).find("#<%=txtBuktiObyektif__6.ClientID%>").val(arrString[19]);
            $(e.currentTarget).find("#<%=txtBranchFU__6.ClientID%>").val(arrString[20]);

            document.getElementById("txtBuktiObyektif__6").onkeydown = function () { return false; }

        });


        $('#formModalEdit_4').on('show.bs.modal', function (e) {
            var bookId = $(e.relatedTarget).data('book-id');
            var arrString = []

            arrString = bookId.split("|")

            
            $(e.currentTarget).find("#<%=TxtNo__4.ClientID%>").val(arrString[1]);
            $(e.currentTarget).find("#<%=txtHasilAudit__4.ClientID%>").val(arrString[7]);
            $(e.currentTarget).find("#<%=txtBuktiObyektif__4.ClientID%>").val(arrString[8]);
            $(e.currentTarget).find("#<%=txtBranchFU__4.ClientID%>").val(arrString[10]);

            document.getElementById("txtBuktiObyektif__4").onkeydown = function () { return false; }

        });





                $('#formModalEdit_5').on('show.bs.modal', function (e) {
            var bookId = $(e.relatedTarget).data('book-id');
            var arrString = []

            arrString = bookId.split("|")

            
            $(e.currentTarget).find("#<%=TxtNo__5.ClientID%>").val(arrString[1]);
            $(e.currentTarget).find("#<%=doVerifikasi_Audit_Status__5.ClientID%>").val(arrString[16]);
            $(e.currentTarget).find("#<%=txtVerifikasi_Audit_Remark__5.ClientID%>").val(arrString[17]);


        });



    </script>


    <div style="display:none;">
        <asp:Label ID="myHTMLTable" runat="server"></asp:Label>

            <asp:textbox id="sUsername_" runat="server"></asp:textbox>
        <asp:textbox id="levelakses_" runat="server"></asp:textbox>
        <asp:textbox id="jabatan_" runat="server"></asp:textbox>
        <asp:textbox id="sCabang_" runat="server"></asp:textbox>
        <asp:textbox id="kodedept_" runat="server"></asp:textbox>
        
    </div>


    <asp:HiddenField id="hdnPengali" runat="server"/>

</asp:Content>


