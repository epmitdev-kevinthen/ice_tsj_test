﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="division.aspx.vb" Inherits="division" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="script/jquery-1.4.1.min.js"></script>
    <script>
        $(document).ready(function () {
            var table = $("#ContentPlaceHolder1_GridView1").DataTable({
                paging: true,
                searching: true,
                lengthChange: false,
            });
            $('#ContentPlaceHolder1_GridView1 tbody').on('click', 'tr', function () {
                var data = table.row(this).data();

                $("#ContentPlaceHolder1_txtKode").val(data[0]);
                $("#ContentPlaceHolder1_txtDivision").val(data[1]);
                $("#ContentPlaceHolder1_createdBy").text(data[2]);
                $("#ContentPlaceHolder1_createdDate").text(data[3]);
                $("#ContentPlaceHolder1_lastUpdateBy").text(data[4]);
                $("#ContentPlaceHolder1_lastUpdateDate").text(data[5]);
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <asp:Label ID="lblHead" runat="server" Text="Master Division"
        Style="font-size: 20px;"></asp:Label>
    <div>
        <asp:Label ID="userEdit" runat="server"></asp:Label>
    </div>
    <div style="display: flex;">
        <table>
            <tr>
                <td>Kode</td>
                <td>:</td>
                <td>
                    <asp:TextBox ID="txtKode" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Division</td>
                <td>:</td>
                <td>
                    <asp:TextBox ID="txtDivision" runat="server"></asp:TextBox></td>
            </tr>
        </table>
        <div style="float: right; margin-left: 15px;">
            Created By: <span id="createdBy" runat="server"></span>
            <br />
            Creation Date: <span id="createdDate" runat="server"></span>
            <br />
            Last Update By: <span id="lastUpdateBy" runat="server"></span>
            <br />
            Last Update Date:<span id="lastUpdateDate" runat="server"></span><br />
        </div>
    </div>
    <br />
    <br />
    <div id="div_lbl" runat="server">
        <asp:Label ID="lbl_msg" runat="server"></asp:Label>
    </div>
    <asp:Button ID="clearData" runat="server" Text="Clear Data" OnClick="clearData_Click" CssClass="button2"/>
    <asp:Button ID="insertData" runat="server" Text="Insert Data" OnClick="insertData_Click" CssClass="button2"/>
    <asp:Button ID="updateData" runat="server" Text="Update Data" OnClick="updateData_Click" CssClass="button2"/>
    <asp:Button ID="deleteData" runat="server" Text="Delete Data" OnClick="deleteData_Click" CssClass="button2"/>

    <div>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" AllowPaging="false" AllowSorting="false"
            PageSize="10" CssClass="dataTable">
            <Columns>
                <asp:BoundField DataField="kode" HeaderText="Kode" />
                <asp:BoundField DataField="direktorat" HeaderText="Division" />
                <asp:Boundfield DataField="created_by" HeaderText="RBM" >
                    <HeaderStyle CssClass="hidden"></HeaderStyle>

                        <ItemStyle CssClass="hidden"></ItemStyle>
                </asp:Boundfield>
                <asp:Boundfield DataField="creation_date" HeaderText="RBM" >
                    <HeaderStyle CssClass="hidden"></HeaderStyle>

                        <ItemStyle CssClass="hidden"></ItemStyle>
                </asp:Boundfield>
                <asp:Boundfield DataField="last_update_by" HeaderText="RBM" >
                    <HeaderStyle CssClass="hidden"></HeaderStyle>

                        <ItemStyle CssClass="hidden"></ItemStyle>
                </asp:Boundfield>
                <asp:Boundfield DataField="last_update_date" HeaderText="RBM" >
                    <HeaderStyle CssClass="hidden"></HeaderStyle>

                        <ItemStyle CssClass="hidden"></ItemStyle>
                </asp:Boundfield>
            </Columns>
            <PagerStyle CssClass="paginate_button" />
        </asp:GridView>
    </div>
</asp:Content>


