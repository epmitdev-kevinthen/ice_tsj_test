﻿Imports System.Data
Imports System.Data.SqlClient


Partial Class KelPemeriksaan
    Inherits System.Web.UI.Page

    Public Shared ConnStrAudit As String = ConfigurationManager.ConnectionStrings("SQLICE").ConnectionString
    Public Shared cmd As New SqlCommand

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        If Not Page.IsPostBack Then
            If Not Page.IsPostBack Then
                userEdit.Text = Request.QueryString("kode")
                If userEdit.Text = "" Then
                    tampilTable()
                Else
                    tampilTable()
                    tampil()
                    'insertData.Style("display") = "none"
                End If

            End If

        End If
    End Sub
    Protected Sub tampilTable()
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                Dim da As New SqlDataAdapter("select * from MSTKELPERIKSA", con)
                Dim dt As New DataTable
                da.Fill(dt)
                GridView1.DataSource = dt
                GridView1.DataBind()
                GridView1.HeaderRow.TableSection = TableRowSection.TableHeader
                con.Close()
            End Using
        Catch ex As Exception

        End Try

    End Sub
    Protected Sub tampil()
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                Dim da As New SqlDataAdapter("select * from MSTKELPERIKSA where kode = '" & userEdit.Text & "'", con)
                Dim dt As New DataTable
                da.Fill(dt)
                If dt.Rows.Count > 0 Then
                    txtKodeCab.Text = dt.Rows(0).Item("kode")
                    txtKelPer.Text = dt.Rows(0).Item("kelperiksa")
                    createdBy.InnerText = dt.Rows(0).Item("created_by")
                    createdDate.InnerText = dt.Rows(0).Item("creation_date")
                    lastUpdateBy.InnerText = dt.Rows(0).Item("last_update_by")
                    lastUpdateDate.InnerText = dt.Rows(0).Item("last_update_date")
                End If
                con.Close()
            End Using
        Catch ex As Exception

        End Try

    End Sub
    Protected Sub GridView1_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        tampilTable()
    End Sub
    Protected Sub btnSubKel_Click(sender As Object, e As EventArgs)
        If txtKodeCab.Text = "" Or txtKelPer.Text = "" Then
            div_lbl.Visible = True
            lbl_msg.Text = "Isi Kode Cabang dan Kelompok pemeriksaan terlebih dahulu!"
        Else
            Response.Redirect("SubKelPemeriksaan.aspx?kode=" & txtKodeCab.Text & "")
        End If
    End Sub
    Protected Sub GridView1_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GridView1.RowCommand
        If e.CommandName = "cmdsubmit" Then
            Dim kode As String = e.CommandArgument
            Response.Redirect("KelPemeriksaan.aspx?kode=" & kode & "")
        ElseIf e.CommandName = "cmddel" Then


        End If
    End Sub
    Protected Sub clearData_Click(sender As Object, e As EventArgs)
        Response.Redirect("KelPemeriksaan.aspx")
    End Sub
    Protected Sub insertData_Click(sender As Object, e As EventArgs)
        Dim sqlstr As String = "insert into MSTKELPERIKSA(kode, kelperiksa, created_by, creation_date, last_update_by, last_update_date) values('" & txtKodeCab.Text & "', '" & txtKelPer.Text & "', '" & Session("sUsername") & "', current_timestamp, '" & Session("sUsername") & "', current_timestamp)"
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                cmd = New SqlCommand(sqlstr, con)
                cmd.ExecuteNonQuery()
                con.Close()
            End Using

            div_lbl.Visible = True
            lbl_msg.Text = "Insert Successful!"
            tampilTable()
        Catch ex As Exception
            div_lbl.Visible = True
            lbl_msg.Text = ex.Message.ToString
        End Try
    End Sub
    Protected Sub updateData_Click(sender As Object, e As EventArgs)
        Dim sqlstr As String = "update MSTKELPERIKSA set kode = '" & txtKodeCab.Text & "', kelperiksa = '" & txtKelPer.Text & "', last_update_by = '" & Session("sUsername") & "', last_update_date = current_timestamp where kode='" & txtKodeCab.Text & "'"
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                cmd = New SqlCommand(sqlstr, con)
                cmd.ExecuteNonQuery()
                con.Close()
            End Using

            div_lbl.Visible = True
            lbl_msg.Text = "Update Successful!"
            tampilTable()
        Catch ex As Exception
            div_lbl.Visible = True
            lbl_msg.Text = ex.Message.ToString
        End Try
    End Sub
    Protected Sub deleteData_Click(sender As Object, e As EventArgs)
        Dim sqlstr As String = "delete from MSTKELPERIKSA where kode = '" & txtKodeCab.Text & "'"
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                cmd = New SqlCommand(sqlstr, con)
                cmd.ExecuteNonQuery()
                con.Close()
            End Using

            div_lbl.Visible = True
            lbl_msg.Text = "Delete Successful!"
            tampilTable()
        Catch ex As Exception
            div_lbl.Visible = True
            lbl_msg.Text = ex.Message.ToString
        End Try
    End Sub
End Class
