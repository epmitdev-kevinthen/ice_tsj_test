﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="hdrCheckList.aspx.vb" Inherits="hdrCheckList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <br />



    <asp:Label ID="lblHead" runat="server" Text="Template Detail - Struktur CheckList"
        style="font-size: 20px;"
        ></asp:Label>

<style>

    .exportExcel{
  padding: 5px;
  border: 1px solid grey;
  margin: 5px;
  cursor: pointer;
}

</style>

<script  type="text/javascript">
    $(document).ready(function () {
        var table = $('#mytable1').DataTable({
            scrollY: "300px",
            scrollX: "800px",
            scrollCollapse: true,
            paging: true,
            "columnDefs": [
              { "width": "400px", "targets": 12 },
              { "width": "400px", "targets": 15 }
            ],
            bProcessing: true, // shows 'processing' label
            bStateSave: true, // presumably saves state for reloads
            fixedColumns: {
                leftColumns: 3
            }
        });
    });  




    function pclear1() {
        document.getElementById("txtkode_activity11").value = "";
    };

    function pclear3() {
        document.getElementById("txtkode_activity13").value = "";
    };

    function fncBrowse(p) {
        var pn = "";
        pn = document.getElementById('txtkode_activity1' + p).value;
        window.open('brwMaster.aspx?pn=' + pn + '&p=' + p, 'mywindow', 'left=10,top=10,width=450,height=450,scrollbars=yes,resizable=no');
    }

    function openw(p) {
        window.open('brwfolder.aspx?p=' + p  , 'mywindow', 'left=100,top=100,width=450,height=200,scrollbars=yes,resizable=no');
    };

    function fnExcelReport() {
        var tab_text = "<table border='2px'><tr bgcolor='#87AFC6'>";
        var textRange; var j = 0;
        tab = document.getElementById('mytable1'); // id of table

        for (j = 0 ; j < tab.rows.length ; j++) {
            tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
            //tab_text=tab_text+"</tr>";
        }

        tab_text = tab_text + "</table>";
        tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
        tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
        tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
        {
            txtArea1.document.open("txt/html", "replace");
            txtArea1.document.write(tab_text);
            txtArea1.document.close();
            txtArea1.focus();
            sa = txtArea1.document.execCommand("SaveAs", true, "Say Thanks to Sumit.xls");
        }
        else                 //other browser not tested on IE 11
            sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

        return (sa);
    }

</script>

    <br />
    <div style="font-size: 20px;">
        <b>
    <asp:Label ID="lblkodetmp" runat="server"></asp:Label> - <asp:Label ID="lblnamatmp" runat="server"></asp:Label>
            </b>

        &nbsp;&nbsp;&nbsp;
        Clear dan Copy Template dari Template No : <asp:TextBox runat="server" ID="txtcopytemplate" Width="100" MaxLength="10"></asp:TextBox>
        &nbsp;&nbsp; <asp:Button id="btncopy" Text="Copy" runat="server" />

        <!-- &nbsp;&nbsp; <button id="btnExport" onclick="fnExcelReport();">Export to Excel</button> -->


    </div>

    <div style="margin:30px;">
    <button type='button' class='button2xx' data-book-id='0000|0000||H|N|N|N|2|||||||||' data-target='#formModalNew' data-toggle='modal'>Add New</button>
        <asp:PlaceHolder ID="myPlaceH" runat="server"></asp:PlaceHolder>
            </div>
    <asp:Label ID="lblerror" runat="server" ForeColor="Red"></asp:Label>



    <div id="formModalEdit" tabindex="-1" role="dialog" aria-hidden="true" style="display: none ">
        <table style="position: fixed; top:100px ; left:100px; background-color: ButtonFace; border: 5px; border-radius: 15px; padding: 20px; box-shadow: 5px 5px 5px gray;">
            <tr><td colspan="2">

           <label for="txtNo1">No &nbsp;&nbsp;</label>
           <asp:TextBox ID="txtNo1" runat="server" MaxLength="10" Width="100px" ClientIDMode="Static"  style="background-color: lightgray"></asp:TextBox> &nbsp;&nbsp;
           <label for="txtNo_Parent1">No Parent&nbsp;&nbsp;</label>
           <asp:TextBox ID="txtNo_Parent1" runat="server" MaxLength="10" Width="100px" ClientIDMode="Static"></asp:TextBox>

            &nbsp;&nbsp; <label for="dolevel1">Level &nbsp;&nbsp;</label>
           <asp:DropDownList ID="dolevel1"  runat="server" Width="100px" ClientIDMode="Static"></asp:DropDownList>

           &nbsp;&nbsp; <label for="txtBobot1">Bobot &nbsp;&nbsp;</label>
           <asp:TextBox ID="txtBobot1"  runat="server" MaxLength="3" Width="100px" ClientIDMode="Static"></asp:TextBox>

                </td>
            </tr><tr><td>
           <label for="txtNo_Report1">No Report &nbsp;&nbsp;</label></td><td>
           <asp:TextBox ID="txtNo_Report1"  runat="server" MaxLength="15" Width="100px" ClientIDMode="Static"></asp:TextBox></td>
           </tr><tr><td>


           <label for="txtTyoeHD1">Type &nbsp;&nbsp;</label></td><td>
           <asp:DropDownList ID="txtTyoeHD1"  runat="server" Width="50px" ClientIDMode="Static"></asp:DropDownList></td>
           </tr><tr><td>

           <label for="txtstd_ISO1">Standard &nbsp;&nbsp;</label></td><td>
           
           <label for="txtstd_ISO1">ISO &nbsp;</label>
           <asp:DropDownList ID="txtstd_ISO1"  runat="server" Width="50px"  ClientIDMode="Static"></asp:DropDownList> &nbsp;
               <label>Directory &nbsp;</label>
             <asp:TextBox ID="txtdir_ISO1" runat="server" MaxLength="250" Width="200px"  ClientIDMode="Static"></asp:TextBox>
           <br />
               <label for="txtstd_Policy1">Policy &nbsp;</label>
           <asp:DropDownList ID="txtstd_Policy1"  runat="server" Width="50px"  ClientIDMode="Static"></asp:DropDownList> &nbsp;
               <label>Directory &nbsp;</label>
             <asp:TextBox ID="txtdir_Policy1" runat="server" MaxLength="250" Width="200px"  ClientIDMode="Static"></asp:TextBox>
           <br />
               <label for="txtstd_Lain1">Lainnya &nbsp;</label>
           <asp:DropDownList ID="txtstd_Lain1"  runat="server" Width="50px"  ClientIDMode="Static"></asp:DropDownList> &nbsp;
               <label>Directory &nbsp;</label>
             <asp:TextBox ID="txtdir_Lain1" runat="server" MaxLength="250" Width="200px"  ClientIDMode="Static"></asp:TextBox>
           

               </td>
           </tr><tr><td>
           <label for="Txtjml_control1">Jumlah Control &nbsp;&nbsp;</label></td><td>
           <asp:TextBox ID="Txtjml_control1" runat="server" MaxLength="1" Width="20px"  ClientIDMode="Static"></asp:TextBox></td>
           </tr><tr><td>
               <label for="txtkode_activity11">Main Activity &nbsp;&nbsp;</label>
           <asp:TextBox ID="txtkode_activity11" runat="server" MaxLength="25" Width="100px"  ClientIDMode="Static"></asp:TextBox>&nbsp;&nbsp;

               
               <button id="brwA1" type="button" onclick="fncBrowse(1)">..</button>
               <button id="brwB1" type="button" onclick="pclear1()">X</button>

           </td><td>
           <asp:TextBox ID="txtactivity11" runat="server" TextMode="MultiLine" MaxLength="300" Width="400px"  ClientIDMode="Static"></asp:TextBox>
               </td>
           </tr>

             <tr><td>
           <label for="txtPIC11">Main PIC &nbsp;&nbsp;</label></td><td>
           <asp:DropDownList ID="txtPIC11"  runat="server" Width="200px" ClientIDMode="Static"></asp:DropDownList></td>
           </tr>

                         <tr><td>
           <label for="txtPIC11Entry">Main PIC Update &nbsp;&nbsp;</label></td><td>
           <asp:DropDownList ID="txtPIC11Entry"  runat="server" Width="200px" ClientIDMode="Static"></asp:DropDownList></td>
           </tr>
            <tr>
                <td>Kriteria Penilaian 1</td>
                <td><asp:DropDownList ID="ddlKriteriaPenilaian_1_Edit" runat="server" Width="200px"></asp:DropDownList></td>
            </tr>

           <tr><td>
           <label for="txtActivity21">Second Activity &nbsp;&nbsp;</label></td><td>
           <asp:TextBox ID="txtActivity21"  runat="server" TextMode="MultiLine" MaxLength="300" Width="400px" ClientIDMode="Static"></asp:TextBox></td>
           </tr>

             <tr><td>
           <label for="txtPIC21">Second PIC &nbsp;&nbsp;</label></td><td>
           <asp:DropDownList ID="txtPIC21"  runat="server" Width="200px" ClientIDMode="Static"></asp:DropDownList></td>
           </tr>
             <tr><td>
           <label for="txtPIC21Entry">Second PIC Update &nbsp;&nbsp;</label></td><td>
           <asp:DropDownList ID="txtPIC21Entry"  runat="server" Width="200px" ClientIDMode="Static"></asp:DropDownList></td>
           </tr>
            <tr>
                <td>Kriteria Penilaian 2</td>
                <td><asp:DropDownList ID="ddlKriteriaPenilaian_2_Edit" runat="server" Width="200px"></asp:DropDownList></td>
            </tr>
            <tr>
                <td>Guidance</td>
                <td><asp:DropDownList ID="ddlGuidance_Edit" runat="server" Width="200px"></asp:DropDownList></td>
            </tr>
           <tr><td colspan="2" style="text-align: center;">&nbsp;
               </td>
           </tr>
           <tr><td colspan="2" style="text-align: center;">
               <asp:Button ID="btnUpdate1" CssClass="btn1" runat="server" text="Update"/> &nbsp;&nbsp;
               <asp:Button ID="btnCancel1" CssClass="btn1" runat="server" text="Cancel"/>
               </td>
           </tr>
       </table>
    </div>

    <div id="formModalDelete" tabindex="-1" role="dialog" aria-hidden="true"style="display: none ">
        <table style="position: fixed; top:100px ; left:100px; background-color: ButtonFace; border: 5px; border-radius: 15px; padding: 20px; box-shadow: 5px 5px 5px gray;">
            <tr><td>

           <label for="txtNo2">No &nbsp;&nbsp;</label>
           <asp:TextBox ID="txtNo2" runat="server" MaxLength="10" Width="100px" ClientIDMode="Static"  style="background-color: lightgray"></asp:TextBox> &nbsp;&nbsp;
               </td>
           </tr>

<tr><td>
           <label for="txtNo_Report2">No Report &nbsp;&nbsp;</label></td><td>
           <asp:TextBox ID="txtNo_Report2"  runat="server" MaxLength="15" Width="100px" ClientIDMode="Static"  style="background-color: lightgray"></asp:TextBox></td>
           </tr><tr>

           <tr><td>
               <label for="txtkode_activity12">Main Activity &nbsp;&nbsp;</label>
           <asp:TextBox ID="txtkode_activity12" runat="server" MaxLength="10" Width="100px"  ClientIDMode="Static"  style="background-color: lightgray"></asp:TextBox>&nbsp;&nbsp;
           </td><td>
           <asp:TextBox ID="txtactivity12" runat="server" TextMode="MultiLine" MaxLength="300" Width="400px"  ClientIDMode="Static"  style="background-color: lightgray"></asp:TextBox>
               </td>
           </tr>

           <tr><td colspan="2" style="text-align: center;">&nbsp;
               </td>
           </tr>
           <tr><td colspan="2" style="text-align: center;">
               <asp:Button ID="btnDelete2" CssClass="btn1" runat="server" text="Delete"/> &nbsp;&nbsp;
               <asp:Button ID="btnCancel2" CssClass="btn1" runat="server" text="Cancel"/>
               </td>
           </tr>
       </table>
    </div>

    <div id="formModalNew" tabindex="-1" role="dialog" aria-hidden="true"style="display: none ">
        <table style="position: fixed; top:100px ; left:100px; background-color: ButtonFace; border: 5px; border-radius: 15px; padding: 20px; box-shadow: 5px 5px 5px gray;">
            <tr><td colspan="2">
           <label for="txtNo3">No &nbsp;&nbsp;</label>
           <asp:TextBox ID="txtNo3" runat="server" MaxLength="10" Width="100px" ClientIDMode="Static"></asp:TextBox> &nbsp;&nbsp;
           <label for="txtNo_Parent3">No Parent&nbsp;&nbsp;</label>
           <asp:TextBox ID="txtNo_Parent3" runat="server" MaxLength="10" Width="100px" ClientIDMode="Static"></asp:TextBox>

            &nbsp;&nbsp; <label for="dolevel3">Level &nbsp;&nbsp;</label>
           <asp:DropDownList ID="dolevel3"  runat="server" Width="100px" ClientIDMode="Static"></asp:DropDownList>

                           &nbsp;&nbsp; <label for="txtBobot2">Bobot &nbsp;&nbsp;</label>
           <asp:TextBox ID="txtBobot3"  runat="server" MaxLength="3" Width="100px" ClientIDMode="Static"></asp:TextBox>

                </td>
            </tr><tr><td>
           <label for="txtNo_Report3">No Report &nbsp;&nbsp;</label></td><td>
           <asp:TextBox ID="txtNo_Report3"  runat="server" MaxLength="15" Width="100px" ClientIDMode="Static"></asp:TextBox></td>
           </tr><tr><td>

           <label for="txtTyoeHD3">Type &nbsp;&nbsp;</label></td><td>
           <asp:DropDownList ID="txtTyoeHD3"  runat="server" Width="50px" ClientIDMode="Static"></asp:DropDownList></td>
           </tr><tr><td>

           <label for="txtstd_ISO3">Standard &nbsp;&nbsp;</label></td><td>
           
               <label for="txtstd_ISO3">ISO &nbsp;</label>
           <asp:DropDownList ID="txtstd_ISO3"  runat="server" Width="50px"  ClientIDMode="Static"></asp:DropDownList> &nbsp;
                          <label>Directory &nbsp;</label>
           <asp:TextBox ID="txtdir_ISO3" runat="server" MaxLength="250" Width="200px"  ClientIDMode="Static"></asp:TextBox>
               <br />
           <label for="txtstd_Policy3">Policy &nbsp;</label>
           <asp:DropDownList ID="txtstd_Policy3"  runat="server" Width="50px"  ClientIDMode="Static"></asp:DropDownList> &nbsp;
               <label>Directory &nbsp;</label>
           <asp:TextBox ID="txtdir_Policy3" runat="server" MaxLength="250" Width="200px"  ClientIDMode="Static"></asp:TextBox>
               <br />
           
               <label for="txtstd_Lain3">Lainnya &nbsp;</label>
           <asp:DropDownList ID="txtstd_Lain3"  runat="server" Width="50px"  ClientIDMode="Static"></asp:DropDownList> &nbsp;
               <label>Directory &nbsp;</label>
           <asp:TextBox ID="txtdir_Lain3" runat="server" MaxLength="250" Width="200px"  ClientIDMode="Static"></asp:TextBox>
           

               </td>
           </tr><tr><td>
           <label for="Txtjml_control3">Jumlah Control &nbsp;&nbsp;</label></td><td>
           <asp:TextBox ID="Txtjml_control3" runat="server" MaxLength="1" Width="20px"  ClientIDMode="Static"></asp:TextBox></td>
           </tr><tr><td>
               <label for="txtkode_activity13">Main Activity &nbsp;&nbsp;</label>
           <asp:TextBox ID="txtkode_activity13" runat="server" MaxLength="25" Width="100px"  ClientIDMode="Static"></asp:TextBox>&nbsp;&nbsp;
               <button id="brwA3" type="button" onclick="fncBrowse(3)">..</button>
               <button id="brwB3" type="button" onclick="pclear3()">X</button>
           </td><td>
           <asp:TextBox ID="txtactivity13" runat="server" TextMode="MultiLine" MaxLength="300" Width="400px"  ClientIDMode="Static"></asp:TextBox>
               </td>
           </tr>

             <tr><td>
           <label for="txtPIC13">Main PIC &nbsp;&nbsp;</label></td><td>
           <asp:DropDownList ID="txtPIC13"  runat="server" Width="200px" ClientIDMode="Static"></asp:DropDownList></td>
           </tr>
             <tr><td>
           <label for="txtPIC13Entry">Main PIC Update &nbsp;&nbsp;</label></td><td>
           <asp:DropDownList ID="txtPIC13Entry"  runat="server" Width="200px" ClientIDMode="Static"></asp:DropDownList></td>
           </tr>
            <tr>
                <td>Kriteria Penilaian 1</td>
                <td><asp:DropDownList ID="ddlKriteriaPenilaian_1_New" runat="server" Width="200px"></asp:DropDownList></td>
            </tr>
           <tr><td>
           <label for="txtActivity23">Second Activity &nbsp;&nbsp;</label></td><td>
           <asp:TextBox ID="txtActivity23"  runat="server" TextMode="MultiLine" MaxLength="300" Width="400px" ClientIDMode="Static"></asp:TextBox></td>
           </tr>

             <tr><td>
           <label for="txtPIC23">Second PIC &nbsp;&nbsp;</label></td><td>
           <asp:DropDownList ID="txtPIC23"  runat="server" Width="200px" ClientIDMode="Static"></asp:DropDownList></td>
           </tr>

                         <tr><td>
           <label for="txtPIC23Entry">Second PIC Entry &nbsp;&nbsp;</label></td><td>
           <asp:DropDownList ID="txtPIC23Entry"  runat="server" Width="200px" ClientIDMode="Static"></asp:DropDownList></td>
           </tr>
            <tr>
                <td>Kriteria Penilaian</td>
                <td><asp:DropDownList ID="ddlKriteriaPenilaian_2_New" runat="server" Width="200px"></asp:DropDownList></td>
            </tr>
            <tr>
                <td>Guidance</td>
                <td><asp:DropDownList ID="ddlGuidance_New" runat="server" Width="200px"></asp:DropDownList></td>
            </tr>
           <tr><td colspan="2" style="text-align: center;">&nbsp;
               </td>
           </tr>
           <tr><td colspan="2" style="text-align: center;">
               <asp:Button ID="btnInsert3" CssClass="btn1" runat="server" text="Insert"/> &nbsp;&nbsp;
               <asp:Button ID="btnCancel3" CssClass="btn1" runat="server" text="Cancel"/>
               </td>
           </tr>
       </table>
    </div>






    <script type="text/javascript">

        $(function () {
            var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "personal";
            $('#Tabs a[href="#' + tabName + '"]').tab('show');
            $("#Tabs a").click(function () {
                $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
            });
        });



        $('#formModalEdit').on('show.bs.modal', function (e) {
            var bookId = $(e.relatedTarget).data('book-id');
            var arrString = []

            arrString = bookId.split("|")

            $(e.currentTarget).find("#<%=txtNo1.ClientID%>").val(arrString[0]);
            $(e.currentTarget).find("#<%=txtNo_Parent1.ClientID%>").val(arrString[1]);
            $(e.currentTarget).find("#<%=txtNo_Report1.ClientID%>").val(arrString[2]);
            $(e.currentTarget).find("#<%=txtTyoeHD1.ClientID%>").val(arrString[3]);

            $(e.currentTarget).find("#<%=txtstd_ISO1.ClientID%>").val(arrString[4]);
            $(e.currentTarget).find("#<%=txtstd_Policy1.ClientID%>").val(arrString[5]);
            $(e.currentTarget).find("#<%=txtstd_Lain1.ClientID%>").val(arrString[6]);
            $(e.currentTarget).find("#<%=Txtjml_control1.ClientID%>").val(arrString[7]);

            $(e.currentTarget).find("#<%=txtkode_activity11.ClientID%>").val(arrString[8]);
            $(e.currentTarget).find("#<%=txtactivity11.ClientID%>").val(arrString[9]);
            $(e.currentTarget).find("#<%=txtPIC11.ClientID%>").val(arrString[10]);
            $(e.currentTarget).find("#<%=txtActivity21.ClientID%>").val(arrString[11]);
            $(e.currentTarget).find("#<%=txtPIC21.ClientID%>").val(arrString[12]);

            $(e.currentTarget).find("#<%=dolevel1.ClientID%>").val(arrString[13]);
            $(e.currentTarget).find("#<%=txtBobot1.ClientID%>").val(arrString[14]);

            $(e.currentTarget).find("#<%=txtdir_ISO1.ClientID%>").val(arrString[15]);
            $(e.currentTarget).find("#<%=txtdir_Policy1.ClientID%>").val(arrString[16]);
            $(e.currentTarget).find("#<%=txtdir_Lain1.ClientID%>").val(arrString[17]);

            $(e.currentTarget).find("#<%=txtPIC11Entry.ClientID%>").val(arrString[18]);
            $(e.currentTarget).find("#<%=txtPIC21Entry.ClientID%>").val(arrString[19]);
            $(e.currentTarget).find("#<%=ddlKriteriaPenilaian_1_Edit.ClientID%>").val(arrString[20]);
            $(e.currentTarget).find("#<%=ddlKriteriaPenilaian_2_Edit.ClientID%>").val(arrString[21]);
            $(e.currentTarget).find("#<%=ddlGuidance_Edit.ClientID%>").val(arrString[22]);
            
            document.getElementById("txtNo1").onkeydown = function () { return false; }

        });

        $('#formModalDelete').on('show.bs.modal', function (e) {
            var bookId = $(e.relatedTarget).data('book-id');
            var arrString = []

            arrString = bookId.split("|")

            $(e.currentTarget).find("#<%=txtNo2.ClientID%>").val(arrString[0]);
            $(e.currentTarget).find("#<%=txtNo_Report2.ClientID%>").val(arrString[2]);
            $(e.currentTarget).find("#<%=txtkode_activity12.ClientID%>").val(arrString[8]);
            $(e.currentTarget).find("#<%=txtactivity12.ClientID%>").val(arrString[9]);

            document.getElementById("txtNo2").onkeydown = function () { return false; }
            document.getElementById("txtNo_Report2").onkeydown = function () { return false; }
            document.getElementById("txtkode_activity12").onkeydown = function () { return false; }
            document.getElementById("txtactivity12").onkeydown = function () { return false; }

        });

        $('#formModalNew').on('show.bs.modal', function (e) {
            var bookId = $(e.relatedTarget).data('book-id');
            var arrString = []

            arrString = bookId.split("|")

            //var type = JSON.parse("[" + arrString[2] + "]");
            //console.log(arrString[1])
            $(e.currentTarget).find("#<%=txtNo3.ClientID%>").val(arrString[0]);
            $(e.currentTarget).find("#<%=txtNo_Parent3.ClientID%>").val(arrString[1]);
            $(e.currentTarget).find("#<%=txtNo_Report3.ClientID%>").val(arrString[2]);
            $(e.currentTarget).find("#<%=txtTyoeHD3.ClientID%>").val(arrString[3]);

            $(e.currentTarget).find("#<%=txtstd_ISO3.ClientID%>").val(arrString[4]);
            $(e.currentTarget).find("#<%=txtstd_Policy3.ClientID%>").val(arrString[5]);
            $(e.currentTarget).find("#<%=txtstd_Lain3.ClientID%>").val(arrString[6]);
            $(e.currentTarget).find("#<%=Txtjml_control3.ClientID%>").val(arrString[7]);

            $(e.currentTarget).find("#<%=txtkode_activity13.ClientID%>").val(arrString[8]);
            $(e.currentTarget).find("#<%=txtactivity13.ClientID%>").val(arrString[9]);
            $(e.currentTarget).find("#<%=txtPIC13.ClientID%>").val(arrString[10]);
            $(e.currentTarget).find("#<%=txtActivity23.ClientID%>").val(arrString[11]);
            $(e.currentTarget).find("#<%=txtPIC23.ClientID%>").val(arrString[12]);

            $(e.currentTarget).find("#<%=dolevel3.ClientID%>").val(arrString[13]);
            $(e.currentTarget).find("#<%=txtBobot3.ClientID%>").val(arrString[14]);

            $(e.currentTarget).find("#<%=txtdir_ISO3.ClientID%>").val(arrString[15]);
            $(e.currentTarget).find("#<%=txtdir_Policy3.ClientID%>").val(arrString[16]);
            $(e.currentTarget).find("#<%=txtdir_Lain3.ClientID%>").val(arrString[17]);

            $(e.currentTarget).find("#<%=txtPIC13Entry.ClientID%>").val(arrString[18]);
            $(e.currentTarget).find("#<%=txtPIC23Entry.ClientID%>").val(arrString[19]);

        });

    </script>

    <asp:Label ID="infoerror" runat="server" style="color: red" ></asp:Label>


</asp:Content>

