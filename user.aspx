﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="user.aspx.vb" Inherits="user" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="script/jquery-1.4.1.min.js"></script>
    <script>
        $(document).ready(function () {
            var table = $("#ContentPlaceHolder1_GridView1").DataTable({
                paging: true,
                searching: true,
                lengthChange: false,
            });
            $('#ContentPlaceHolder1_GridView1 tbody').on('click', 'tr', function () {
                var data = table.row(this).data();

                $("#ContentPlaceHolder1_txtUserName").val(data[0]);
                $("#ContentPlaceHolder1_txtPass").val(data[1]);
                $("#ContentPlaceHolder1_txtFullUserName").val(data[2]);
                $("#ContentPlaceHolder1_ddlJK").val(data[3]).change();
                $("#ContentPlaceHolder1_ddlJabatan").val(data[4] + "-" + data[5]).change();
                $("#ContentPlaceHolder1_ddlCabang").val(data[6]).change();
                $("#ContentPlaceHolder1_txtEmail").val(data[7]);
                $("#ContentPlaceHolder1_ddlLvlAkses").val(data[8]).change();
                $("#ContentPlaceHolder1_ddlKodeDept").val(data[9] + "-" + data[10]).change();
                $("#ContentPlaceHolder1_createdBy").text(data[11]);
                $("#ContentPlaceHolder1_createdDate").text(data[12]);
                $("#ContentPlaceHolder1_lastUpdateBy").text(data[13]);
                $("#ContentPlaceHolder1_lastUpdateDate").text(data[14]);
                $("#ContentPlaceHolder1_txtNIK").val(data[15]);
                $("#ContentPlaceHolder1_txtNoWA").val(data[16]);
                $("#ContentPlaceHolder1_aksesIce").val(data[17]);
                $("#ContentPlaceHolder1_aksesSensus").val(data[18]);
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <asp:Label ID="lblHead" runat="server" Text="Master User"
        Style="font-size: 20px;"></asp:Label>
    <div>
        <asp:Label ID="userEdit" runat="server"></asp:Label>
    </div>
    <div style="display: flex;">
        <table>
            <tr>
                <td>Username</td>
                <td>:</td>
                <td>
                    <asp:TextBox ID="txtUserName" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Password</td>
                <td>:</td>
                <td>
                    <asp:TextBox ID="txtPass" runat="server" Text="password"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Full Username</td>
                <td>:</td>
                <td>
                    <asp:TextBox ID="txtFullUserName" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>NIK</td>
                <td>:</td>
                <td>
                    <asp:TextBox ID="txtNIK" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>J K</td>
                <td>:</td>
                <td>
                    <asp:DropDownList ID="ddlJK" runat="server">
                        <asp:ListItem Text="Laki - laki" Value="L"></asp:ListItem>
                        <asp:ListItem Text="Perempuan" Value="P"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>Jabatan</td>
                <td>:</td>
                <td>
                    <asp:DropDownList ID="ddlJabatan" runat="server">
                        <asp:ListItem Text="" Value=""></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>Cabang</td>
                <td>:</td>
                <td>
                    <asp:DropDownList ID="ddlCabang" runat="server">
                        <asp:ListItem Text="" Value=""></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>Email</td>
                <td>:</td>
                <td>
                    <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Level Akses</td>
                <td>:</td>
                <td>
                    <asp:DropDownList ID="ddlLvlAkses" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>Kode Dept</td>
                <td>:</td>
                <td>
                    <asp:DropDownList ID="ddlKodeDept" runat="server">
                        <asp:ListItem Text="" Value=""></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>No WA</td>
                <td>:</td>
                <td>
                    <asp:TextBox ID="txtNoWA" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>Akses ICE</td>
                <td>:</td>
                <td>
                    <asp:DropDownList ID="aksesIce" runat="server">
                        <asp:ListItem Text="Y" Value="Y"></asp:ListItem>
                        <asp:ListItem Text="N" Value="N"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>Akses Sensus</td>
                <td>:</td>
                <td>
                    <asp:DropDownList ID="aksesSensus" runat="server">
                        <asp:ListItem Text="Y" Value="Y"></asp:ListItem>
                        <asp:ListItem Text="N" Value="N"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
        <div style="float: right; margin-left: 15px;">
            Created By: <span id="createdBy" runat="server"></span>
            <br />
            Creation Date: <span id="createdDate" runat="server"></span>
            <br />
            Last Update By: <span id="lastUpdateBy" runat="server"></span>
            <br />
            Last Update Date: <span id="lastUpdateDate" runat="server"></span><br />
        </div>
    </div>
    <br />
    <br />
    <div id="div_lbl" runat="server">
        <asp:Label ID="lbl_msg" runat="server"></asp:Label>
    </div>
    <%--<asp:Button ID="clearData" runat="server" Text="Clear Data" OnClick="clearData_Click" CssClass="button2"/>--%>
    <%--<button class="button2" onclick="btnClearData()">Clear Data</button>--%>
    <asp:Button ID="insertData" runat="server" Text="Insert Data" OnClick="insertData_Click" CssClass="button2"/>
    <asp:Button ID="updateData" runat="server" Text="Update Data" onclick="updateData_Click" CssClass="button2"/>
    <asp:Button ID="deleteData" runat="server" Text="Delete Data" OnClick="deleteData_Click" CssClass="button2"/>

    <script>
        function btnClearData() {
            
            document.getElementById("<%= txtUserName.ClientID %>").value = "";
            //txtPass.Text = ""
            document.getElementById("<%= txtPass.ClientID %>").value = "";
            //txtFullUserName.Text = ""
            document.getElementById("<%= txtFullUserName.ClientID %>").value = "";
            //txtNIK.Text = ""
            document.getElementById("<%= txtNIK.ClientID %>").value = "";
            //ddlJabatan.SelectedValue = ""
            document.getElementById("<%= ddlJabatan.ClientID %>").value = "";
            //ddlCabang.SelectedValue = ""
            document.getElementById("<%= ddlCabang.ClientID %>").value = "";
            //txtEmail.Text = ""
            document.getElementById("<%= txtEmail.ClientID %>").value = "";
            //ddlKodeDept.SelectedValue = ""
            document.getElementById("<%= ddlKodeDept.ClientID %>").value = "";
            //txtNoWA.Text = ""
            document.getElementById("<%= txtNoWA.ClientID %>").value = "";
            //createdBy.InnerText = ""
            document.getElementById("createdBy").value = "";
            //createdDate.InnerText = ""
            document.getElementById("createdDate").value = "";
            //lastUpdateBy.InnerText = ""
            document.getElementById("lastUpdateBy").value = "";
            //lastUpdateDate.InnerText = ""
            document.getElementById("lastUpdateDate").value = "";
        }
    </script>

    <div>

        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" AllowPaging="false" AllowSorting="false"
            PageSize="20" CssClass="dataTable" >
            <Columns>
                <asp:BoundField DataField="username" HeaderText="Username" />
                <asp:Boundfield DataField="userpass" HeaderText="RBM" >
                    <HeaderStyle CssClass="hidden"></HeaderStyle>

                        <ItemStyle CssClass="hidden"></ItemStyle>
                </asp:Boundfield>
                <asp:BoundField DataField="fullusername" HeaderText="Full Username" />
                <asp:BoundField DataField="jk" HeaderText="JK" />
                <asp:Boundfield DataField="kodejabatan" HeaderText="RBM" >
                    <HeaderStyle CssClass="hidden"></HeaderStyle>

                        <ItemStyle CssClass="hidden"></ItemStyle>
                </asp:Boundfield>
                <asp:BoundField DataField="jabatan" HeaderText="Jabatan" />
                <asp:BoundField DataField="kodecab" HeaderText="Cabang" />
                <asp:BoundField DataField="email" HeaderText="Email" />
                <asp:BoundField DataField="levelakses" HeaderText="Level Akses" />
                <asp:Boundfield DataField="kodedept" HeaderText="RBM" >
                    <HeaderStyle CssClass="hidden"></HeaderStyle>

                        <ItemStyle CssClass="hidden"></ItemStyle>
                </asp:Boundfield>
                <asp:BoundField DataField="dept" HeaderText="Dept" />
                <asp:Boundfield DataField="created_by" HeaderText="RBM" >
                    <HeaderStyle CssClass="hidden"></HeaderStyle>

                        <ItemStyle CssClass="hidden"></ItemStyle>
                </asp:Boundfield>
                <asp:Boundfield DataField="creation_date" HeaderText="RBM" >
                    <HeaderStyle CssClass="hidden"></HeaderStyle>

                        <ItemStyle CssClass="hidden"></ItemStyle>
                </asp:Boundfield>
                <asp:Boundfield DataField="last_update_by" HeaderText="RBM" >
                    <HeaderStyle CssClass="hidden"></HeaderStyle>

                        <ItemStyle CssClass="hidden"></ItemStyle>
                </asp:Boundfield>
                <asp:Boundfield DataField="last_update_date" HeaderText="RBM" >
                    <HeaderStyle CssClass="hidden"></HeaderStyle>

                        <ItemStyle CssClass="hidden"></ItemStyle>
                </asp:Boundfield>
                <asp:BoundField DataField="nik" HeaderText="NIK" />
                <asp:BoundField DataField="no_wa" HeaderText="No WA" />
                <asp:BoundField DataField="flag_akses_ice" HeaderText="Akses ICE" />
                <asp:BoundField DataField="flag_akses_sensus" HeaderText="Akses Sensus" />
            </Columns>
            <PagerStyle CssClass="paginate_button" />
        </asp:GridView>
    </div>
</asp:Content>


