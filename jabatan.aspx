﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="jabatan.aspx.vb" Inherits="jabatan" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="script/jquery-1.4.1.min.js"></script>
    <script>
        $(document).ready(function () {
            var table = $("#ContentPlaceHolder1_GridView1").DataTable({
                paging: true,
                searching: true,
                lengthChange: false,
            });
            $('#ContentPlaceHolder1_GridView1 tbody').on('click', 'tr', function () {
                var data = table.row(this).data();

                $("#ContentPlaceHolder1_txtKode").val(data[0]);
                $("#ContentPlaceHolder1_txtJabatan").val(data[1]);
                $("#ContentPlaceHolder1_createdBy").text(data[2]);
                $("#ContentPlaceHolder1_createdDate").text(data[3]);
                $("#ContentPlaceHolder1_lastUpdateBy").text(data[4]);
                $("#ContentPlaceHolder1_lastUpdateDate").text(data[5]);
                $("#ContentPlaceHolder1_lastUpdateDate").val(data[7]).change();
                if (data[6] == "1") {
                    $("#ContentPlaceHolder1_chckBoxApp_0").prop("checked", true);
                } else {
                    $("#ContentPlaceHolder1_chckBoxApp_0").prop("checked", false);
                }
                if (data[7] == "1") {
                    $("#ContentPlaceHolder1_chckBoxApp_1").prop("checked", true);
                } else {
                    $("#ContentPlaceHolder1_chckBoxApp_1").prop("checked", false);
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <asp:Label ID="lblHead" runat="server" Text="Master Jabatan"
        Style="font-size: 20px;"></asp:Label>
    <div>
        <asp:Label ID="userEdit" runat="server"></asp:Label>
    </div>
    <div style="display: flex;">
        <table>
            <tr>
                <td>Kode</td>
                <td>:</td>
                <td>
                    <asp:TextBox ID="txtKode" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Jabatan</td>
                <td>:</td>
                <td>
                    <asp:TextBox ID="txtJabatan" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Approve/UnApprove</td>
                <td>:</td>
                <td>
                    <asp:CheckBoxList ID="chckBoxApp" runat="server">
                        <asp:ListItem Value="1" Text="Approve"></asp:ListItem>
                        <asp:ListItem Value="1" Text="UnApprove"></asp:ListItem>
                    </asp:CheckBoxList> </td>
            </tr>
            <tr>
                <td>Cabang/Pusat</td>
                <td>:</td>
                <td>
                    <asp:DropDownList ID="ddlCabangPusat" runat="server">
                        <asp:ListItem Value="CABANG" Text="CABANG"></asp:ListItem>
                        <asp:ListItem Value="PUSAT" Text="PUSAT"></asp:ListItem>
                        <asp:ListItem Value="SUBSIDIARIES" Text="SUBSIDIARIES"></asp:ListItem>
                    </asp:DropDownList> </td>
            </tr>
        </table>
        <div style="float: right; margin-left: 15px;">
            Created By: <span id="createdBy" runat="server"></span>
            <br />
            Creation Date: <span id="createdDate" runat="server"></span>
            <br />
            Last Update By: <span id="lastUpdateBy" runat="server"></span>
            <br />
            Last Update Date:<span id="lastUpdateDate" runat="server"></span><br />
        </div>
    </div>
    <br />
    <br />
    <div id="div_lbl" runat="server">
        <asp:Label ID="lbl_msg" runat="server"></asp:Label>
    </div>
    <asp:Button ID="clearData" runat="server" Text="Clear Data" OnClick="clearData_Click" CssClass="button2"/>
    <asp:Button ID="insertData" runat="server" Text="Insert Data" OnClick="insertData_Click" CssClass="button2"/>
    <asp:Button ID="updateData" runat="server" Text="Update Data" OnClick="updateData_Click" CssClass="button2"/>
    <asp:Button ID="deleteData" runat="server" Text="Delete Data" OnClick="deleteData_Click" CssClass="button2"/>

    <div>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" AllowPaging="false" AllowSorting="false"
            PageSize="10" CssClass="dataTable">
            <Columns>
                <asp:BoundField DataField="kode" HeaderText="Kode" />
                <asp:BoundField DataField="jabatan" HeaderText="Jabatan" />
                <asp:Boundfield DataField="created_by" HeaderText="RBM" >
                    <HeaderStyle CssClass="hidden"></HeaderStyle>

                        <ItemStyle CssClass="hidden"></ItemStyle>
                </asp:Boundfield>
                <asp:Boundfield DataField="creation_date" HeaderText="RBM" >
                    <HeaderStyle CssClass="hidden"></HeaderStyle>

                        <ItemStyle CssClass="hidden"></ItemStyle>
                </asp:Boundfield>
                <asp:Boundfield DataField="last_update_by" HeaderText="RBM" >
                    <HeaderStyle CssClass="hidden"></HeaderStyle>

                        <ItemStyle CssClass="hidden"></ItemStyle>
                </asp:Boundfield>
                <asp:Boundfield DataField="last_update_date" HeaderText="RBM" >
                    <HeaderStyle CssClass="hidden"></HeaderStyle>

                        <ItemStyle CssClass="hidden"></ItemStyle>
                </asp:Boundfield>
                <asp:BoundField DataField="flag_approve" HeaderText="Approve" />
                <asp:BoundField DataField="flag_unapprove" HeaderText="UnApprove" />
                <asp:BoundField DataField="flag_cabangpusat" HeaderText="CABANG/PUSAT" />
            </Columns>
            <PagerStyle CssClass="paginate_button" />
        </asp:GridView>
    </div>
</asp:Content>


