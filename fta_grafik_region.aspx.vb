﻿
Imports System.Data
Imports System.Data.SqlClient

Partial Class fta_grafik_region
    Inherits System.Web.UI.Page
    Public Shared dt_grafik As DataTable
    Private Sub fta_grafik_region_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            loadDDLFilter()
            'loadKelompokPemeriksa()
            'loadTable()
        End If
    End Sub
    Sub loadDDLFilter()
        Dim dt As New DataTable

        dt = GetDataSql("select * from mstcab order by namacab")
        If dt.Rows.Count > 0 Then
            ddlBranch.Items.Add(New ListItem("All", "%"))
            For i = 0 To dt.Rows.Count - 1
                ddlBranch.Items.Add(New ListItem(dt.Rows(i).Item("namacab"), dt.Rows(i).Item("kodecab")))
            Next
        End If

        dt = GetDataSql("select * from MSTCASECATEGORY order by keterangan")
        If dt.Rows.Count > 0 Then
            ddlCaseCategory.Items.Add(New ListItem("All", "%"))
            For i = 0 To dt.Rows.Count - 1
                ddlCaseCategory.Items.Add(New ListItem(dt.Rows(i).Item("keterangan"), dt.Rows(i).Item("kode")))
            Next
        End If

        dt = GetDataSql("select * from MSTFINDINGCATEGORY order by namacategory")
        If dt.Rows.Count > 0 Then
            ddlFindingCategory.Items.Add(New ListItem("All", "%"))
            For i = 0 To dt.Rows.Count - 1
                ddlFindingCategory.Items.Add(New ListItem(dt.Rows(i).Item("namacategory"), dt.Rows(i).Item("kodecategory")))
            Next
        End If

        dt = GetDataSql("SELECT * FROM mst_periode where flag_aktif='A' order by start_date,nama_prd,kode_prd desc")
        If dt.Rows.Count > 0 Then
            ddlPeriode.Items.Add(New ListItem("All", "%"))
            For i = 0 To dt.Rows.Count - 1
                ddlPeriode.Items.Add(New ListItem(dt.Rows(i).Item("nama_prd"), dt.Rows(i).Item("kode_prd")))
            Next
        End If

        dt = GetDataSql("select * from MST_FTA_STATUS")
        If dt.Rows.Count > 0 Then
            ddlStatus.Items.Add(New ListItem("All", "%"))
            For i = 0 To dt.Rows.Count - 1
                ddlStatus.Items.Add(New ListItem(dt.Rows(i).Item("namastatus") + " (" & dt.Rows(i).Item("kodestatus") & ")", dt.Rows(i).Item("id")))
            Next
        End If

        dt = GetDataSql("select * from MSTKELOMPOKPEMERIKSA order by kodekel")
        If dt.Rows.Count > 0 Then
            ddlKelompokPemeriksa.Items.Add(New ListItem("All", "%"))
            For i = 0 To dt.Rows.Count - 1
                ddlKelompokPemeriksa.Items.Add(New ListItem(dt.Rows(i).Item(1), dt.Rows(i).Item(0)))
            Next
        End If

    End Sub
    Sub loadKelompokPemeriksa()
        Dim dt As DataTable = GetDataSql("select * from MSTKELOMPOKPEMERIKSA order by kodekel")
        'ddlKelompokPemeriksa.Items.Add(New ListItem("Pilih Kelompok Pemeriksa", ""))
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                ddlKelompokPemeriksa.Items.Add(New ListItem(dt.Rows(i).Item(1), dt.Rows(i).Item(0)))
            Next
        End If
    End Sub

    Private Sub loadTable(ByVal param_branch As String, ByVal param_caseCat As String, ByVal param_finCat As String, ByVal param_periode As String, ByVal param_status As String, ByVal param_kelompokpemeriksa As String)
        Dim sb As String
        sb = "SELECT a.*, CASE WHEN a.due_for_implement <> '0' THEN Cast( ( Cast(a.implemented AS DECIMAL) * 100 / Cast(a.due_for_implement AS DECIMAL) ) AS DECIMAL ) ELSE 0 END AS ISR FROM ( SELECT region, Sum(follow_up_recommendation) Follow_Up_Recommendation, Sum(not_implemented) Not_Implemented, Sum(not_applicable) Not_Applicable, Sum(in_progress) In_Progress, Sum(not_due) Not_Due, Sum(implemented) Implemented, Sum(due_for_implement) Due_For_Implement FROM ( SELECT a.*, CASE WHEN isr = 100 THEN 'Done' ELSE 'Not Done' END AS status_follow_up FROM ( SELECT a.*, CASE WHEN a.due_for_implement <> '0' THEN Cast( ( Cast(a.implemented AS DECIMAL) * 100 / Cast(a.due_for_implement AS DECIMAL) ) AS DECIMAL ) ELSE 0 END AS ISR FROM ( SELECT a.fta_no, a.tahun, a.kel_pemeriksa, a.kodecab, a.class, a.region, a.nama_prd, b.follow_up_recommendation, b.not_implemented, b.not_applicable, b.in_progress, b.not_due, b.implemented, b.due_for_implement, CASE WHEN auditor IS NULL THEN ( SELECT auditor FROM ( SELECT fta_no, auditor FROM fta_checklist_hdr a INNER JOIN ( SELECT a.kode_prd, a.kode_tmp, a.kodecab, ( CASE WHEN b3.nama IS NULL THEN b1.nama + ',' + b2.nama ELSE b1.nama + ',' + b2.nama + ',' + b3.nama END ) AS auditor FROM checklist_entryhdr a LEFT JOIN mstauditor b3 ON a.auditor_3 = b3.nik, mstauditor b1, mstauditor b2 WHERE a.auditor_1 = b1.nik AND auditor_2 = b2.nik ) b ON a.kodecab = b.kodecab AND a.kode_tmp = b.kode_tmp AND a.kode_prd = b.kode_prd ) e WHERE e.fta_no = a.fta_no ) ELSE auditor END AS auditor FROM ( SELECT a.fta_no, a.tahun, a.kel_pemeriksa, a.kode_tmp, a.kodecab, b.class, b.region, a.kode_prd, c.nama_prd FROM ( SELECT a.kodecab, a.kode_prd, a.kode_tmp, a.fta_no, a.tahun, a.kel_pemeriksa FROM fta_checklist_hdr a INNER JOIN ( SELECT a.kodecab, status, max(fta_no) as fta_no_last, tahun FROM fta_checklist_hdr a inner join ( select max(tahun) last_tahun, kodecab, kel_pemeriksa from fta_checklist_hdr group by kodecab, kel_pemeriksa ) b on a.Kodecab = b.Kodecab and a.tahun = b.last_tahun and a.kel_pemeriksa = b.kel_pemeriksa WHERE status = '2' AND " & param_kelompokpemeriksa & " GROUP BY a.kodecab, status, tahun ) b ON a.kodecab = b.kodecab AND a.fta_no = b.fta_no_last AND a.status = b.status ) a LEFT JOIN mstcab b ON a.kodecab = b.kodecab LEFT JOIN mst_periode c ON a.kode_prd = c.kode_prd ) a INNER JOIN ( SELECT fta_no, kode_prd, Count(1) AS [Follow_Up_Recommendation], Sum(CASE WHEN status = 1 THEN 1 ELSE 0 END) AS [Not_Implemented], Sum(CASE WHEN status = 2 THEN 1 ELSE 0 END) AS [Not_Applicable], Sum(CASE WHEN status = 3 THEN 1 ELSE 0 END) AS [In_Progress], Sum(CASE WHEN status = 4 THEN 1 ELSE 0 END) AS [Not_Due], Sum(CASE WHEN status = 5 THEN 1 ELSE 0 END) AS [Implemented], Sum( CASE WHEN status = 1 THEN 1 WHEN status = 3 THEN 1 WHEN status = 5 THEN 1 ELSE 0 END ) AS [Due_For_Implement] FROM fta_checklist_dtl where " & param_branch & " and " & param_periode & " and " & param_caseCat & " and " & param_finCat & " and " & param_status & " GROUP BY fta_no, Kode_Prd ) b ON a.fta_no = b.fta_no and a.Kode_Prd = b.Kode_Prd LEFT JOIN ( SELECT fta_no, tahun, ( CASE WHEN b3.nama IS NULL THEN b1.nama + ',' + b2.nama ELSE b1.nama + ',' + b2.nama + ',' + b3.nama END ) AS auditor FROM fta_checklist_auditor a LEFT JOIN mstauditor b3 ON a.auditor_3 = b3.nik, mstauditor b1, mstauditor b2 WHERE a.auditor_1 = b1.nik AND auditor_2 = b2.nik ) c ON a.fta_no = c.fta_no and a.tahun = c.tahun ) a ) a ) a GROUP BY region ) a UNION SELECT 'Total' AS region, Sum(follow_up_recommendation) AS Follow_Up_Recommendation, Sum(not_implemented) AS Not_Implemented, Sum(not_applicable) AS Not_Applicable, Sum(in_progress) AS In_Progress, Sum(not_due) AS Not_Due, Sum(implemented) AS Implemented, Sum(due_for_implement) AS Due_For_Implement, CASE WHEN Sum(a.due_for_implement) <> '0' THEN Cast( ( Cast( Sum(a.implemented) AS DECIMAL ) * 100 / Cast( Sum(a.due_for_implement) AS DECIMAL ) ) AS DECIMAL ) ELSE 0 END AS ISR FROM ( SELECT a.*, CASE WHEN a.due_for_implement <> '0' THEN Cast( ( Cast(a.implemented AS DECIMAL) * 100 / Cast(a.due_for_implement AS DECIMAL) ) AS DECIMAL ) ELSE 0 END AS ISR FROM ( SELECT region, Sum(follow_up_recommendation) Follow_Up_Recommendation, Sum(not_implemented) Not_Implemented, Sum(not_applicable) Not_Applicable, Sum(in_progress) In_Progress, Sum(not_due) Not_Due, Sum(implemented) Implemented, Sum(due_for_implement) Due_For_Implement FROM ( SELECT a.*, CASE WHEN isr = 100 THEN 'Done' ELSE 'Not Done' END AS status_follow_up FROM ( SELECT a.*, CASE WHEN a.due_for_implement <> '0' THEN Cast( ( Cast(a.implemented AS DECIMAL) * 100 / Cast(a.due_for_implement AS DECIMAL) ) AS DECIMAL ) ELSE 0 END AS ISR FROM ( SELECT a.fta_no, a.tahun, a.kel_pemeriksa, a.kodecab, a.class, a.region, a.nama_prd, b.follow_up_recommendation, b.not_implemented, b.not_applicable, b.in_progress, b.not_due, b.implemented, b.due_for_implement, CASE WHEN auditor IS NULL THEN ( SELECT auditor FROM ( SELECT fta_no, auditor FROM fta_checklist_hdr a INNER JOIN ( SELECT a.kode_prd, a.kode_tmp, a.kodecab, ( CASE WHEN b3.nama IS NULL THEN b1.nama + ',' + b2.nama ELSE b1.nama + ',' + b2.nama + ',' + b3.nama END ) AS auditor FROM checklist_entryhdr a LEFT JOIN mstauditor b3 ON a.auditor_3 = b3.nik, mstauditor b1, mstauditor b2 WHERE a.auditor_1 = b1.nik AND auditor_2 = b2.nik ) b ON a.kodecab = b.kodecab AND a.kode_tmp = b.kode_tmp AND a.kode_prd = b.kode_prd ) e WHERE e.fta_no = a.fta_no ) ELSE auditor END AS auditor FROM ( SELECT a.fta_no, a.tahun, a.kel_pemeriksa, a.kode_tmp, a.kodecab, b.class, b.region, a.kode_prd, c.nama_prd FROM ( SELECT a.kodecab, a.kode_prd, a.kode_tmp, a.fta_no, a.tahun, a.kel_pemeriksa FROM fta_checklist_hdr a INNER JOIN ( SELECT a.kodecab, status, max(fta_no) as fta_no_last, tahun FROM fta_checklist_hdr a inner join ( select max(tahun) last_tahun, kodecab, kel_pemeriksa from fta_checklist_hdr group by kodecab, kel_pemeriksa ) b on a.Kodecab = b.Kodecab and a.tahun = b.last_tahun and a.kel_pemeriksa = b.kel_pemeriksa WHERE status = '2' AND " & param_kelompokpemeriksa & " GROUP BY a.kodecab, status, tahun ) b ON a.kodecab = b.kodecab AND a.fta_no = b.fta_no_last AND a.status = b.status ) a LEFT JOIN mstcab b ON a.kodecab = b.kodecab LEFT JOIN mst_periode c ON a.kode_prd = c.kode_prd ) a INNER JOIN ( SELECT fta_no, kode_prd, Count(1) AS [Follow_Up_Recommendation], Sum(CASE WHEN status = 1 THEN 1 ELSE 0 END) AS [Not_Implemented], Sum(CASE WHEN status = 2 THEN 1 ELSE 0 END) AS [Not_Applicable], Sum(CASE WHEN status = 3 THEN 1 ELSE 0 END) AS [In_Progress], Sum(CASE WHEN status = 4 THEN 1 ELSE 0 END) AS [Not_Due], Sum(CASE WHEN status = 5 THEN 1 ELSE 0 END) AS [Implemented], Sum( CASE WHEN status = 1 THEN 1 WHEN status = 3 THEN 1 WHEN status = 5 THEN 1 ELSE 0 END ) AS [Due_For_Implement] FROM fta_checklist_dtl where " & param_branch & " and " & param_periode & " and " & param_caseCat & " and " & param_finCat & " and " & param_status & " GROUP BY fta_no, kode_prd ) b ON a.fta_no = b.fta_no and a.Kode_Prd = b.kode_prd LEFT JOIN ( SELECT fta_no, tahun, ( CASE WHEN b3.nama IS NULL THEN b1.nama + ',' + b2.nama ELSE b1.nama + ',' + b2.nama + ',' + b3.nama END ) AS auditor FROM fta_checklist_auditor a LEFT JOIN mstauditor b3 ON a.auditor_3 = b3.nik, mstauditor b1, mstauditor b2 WHERE a.auditor_1 = b1.nik AND auditor_2 = b2.nik ) c ON a.fta_no = c.fta_no and a.tahun = c.tahun ) a ) a ) a GROUP BY region ) a ) a "
        Dim dt As DataTable = GetDataSql(sb)
        dt_grafik = dt
        GridView1.DataSource = dt
        GridView1.DataBind()
        If dt.Rows.Count > 0 Then
            btnExportExcel.Visible = True
        End If
        'lblMsg.Text = sb
    End Sub

    Protected Sub GridView1_RowDataBound(sender As Object, e As GridViewRowEventArgs)

    End Sub

    <System.Web.Services.WebMethod()>
    Public Shared Function TableLoad() As String
        Dim strConn As String = ConfigurationManager.ConnectionStrings("SQLICE").ConnectionString
        Dim ds As New Data.DataSet

        Dim i As Double
        Dim Chrs As String = Chr(34)

        Dim sb As New StringBuilder()
        sb.Append("[")
        For i = 0 To dt_grafik.Rows.Count - 1
            If dt_grafik.Rows(i).Item("region") <> "Total" Then
                sb.Append("{")
                System.Threading.Thread.Sleep(50)
                sb.Append(String.Format("" & Chrs & "region" & Chrs & " : " & Chrs & "{0}" & Chrs & " , " & Chrs & "isr" & Chrs & " : " & "{1}" & ", ""color"" : ""{2}""",
                                        dt_grafik.Rows(i).Item("region"), dt_grafik.Rows(i).Item("ISR"), "rgba(75, 192, 192, 1)"))
                sb.Append("},")
            End If
        Next
        sb = sb.Remove(sb.Length - 1, 1)
        sb.Append("]")
        Return sb.ToString()
    End Function
    Protected Function GetDataSql(ByVal query As String) As DataTable
        Dim strConn As String = ConfigurationManager.ConnectionStrings("SQLICE").ConnectionString
        Try
            Using con As New SqlConnection(strConn)
                con.Open()
                Dim da As New SqlDataAdapter(query, con)
                Dim dt As New DataTable
                da.Fill(dt)
                con.Close()
                Return dt
            End Using
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Protected Sub btnFind_Click(sender As Object, e As EventArgs)
        Dim selected_branch As String = hdnSelectedBranch.Value
        Dim selected_caseCat As String = hdnSelectedCaseCategory.Value
        Dim selected_finCat As String = hdnSelectedFindingCategory.Value
        Dim selected_periode As String = hdnSelectedPeriode.Value
        Dim selected_status As String = hdnSelectedStatus.Value
        Dim selected_kelompokpemeriksa As String = hdnSelectedKelompokPemeriksa.Value

        Dim list_selected_branch As String() = selected_branch.Split(",")
        Dim list_selected_caseCat As String() = selected_caseCat.Split(",")
        Dim list_selected_finCat As String() = selected_finCat.Split(",")
        Dim list_selected_periode As String() = selected_periode.Split(",")
        Dim list_selected_status As String() = selected_status.Split(",")
        Dim list_selected_kelompokpemeriksa As String() = selected_kelompokpemeriksa.Split(",")

        Dim param_branch As String = ""
        Dim param_caseCat As String = ""
        Dim param_finCat As String = ""
        Dim param_periode As String = ""
        Dim param_status As String = ""
        Dim param_kelompokpemeriksa As String = ""

        If selected_branch = "%" Or selected_branch = "" Then
            param_branch = "kodecab like '%%'"
        Else
            param_branch = "kodecab in ('" & selected_branch.Replace(",", "','") & "')"
        End If

        If selected_caseCat = "%" Or selected_caseCat = "" Then
            param_caseCat = "case_category like '%%'"
        Else
            param_caseCat = "case_category in ('" & selected_caseCat.Replace(",", "','") & "')"
        End If

        If selected_finCat = "%" Or selected_finCat = "" Then
            param_finCat = "fin_category like '%%'"
        Else
            param_finCat = "fin_category in ('" & selected_finCat.Replace(",", "','") & "')"
        End If

        If selected_periode = "%" Or selected_periode = "" Then
            param_periode = "Kode_Prd like '%%'"
        Else
            param_periode = "Kode_Prd in ('" & selected_periode.Replace(",", "','") & "')"
        End If

        If selected_status = "%" Or selected_status = "" Then
            param_status = "status like '%%'"
        Else
            param_status = "status in ('" & selected_status.Replace(",", "','") & "')"
        End If

        If selected_kelompokpemeriksa = "%" Or selected_kelompokpemeriksa = "" Then
            param_kelompokpemeriksa = "a.kel_pemeriksa like '%%'"
        Else
            param_kelompokpemeriksa = "a.kel_pemeriksa in ('" & selected_kelompokpemeriksa.Replace(",", "','") & "')"
        End If

        loadTable(param_branch, param_caseCat, param_finCat, param_periode, param_status, param_kelompokpemeriksa)
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "CallMyFunction", "loadGrafik()", True)
    End Sub
End Class
