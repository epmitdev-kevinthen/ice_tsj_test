﻿
Imports System.Data
Imports System.Data.SqlClient

Partial Class hdrCheckList
    Inherits System.Web.UI.Page
    Dim myChef As New Chef
    Dim j, i As Integer
    Public Shared strConn As String = ConfigurationManager.ConnectionStrings("SQLICE").ConnectionString

    Private Sub hdrCheckList_Init(sender As Object, e As EventArgs) Handles Me.Init
        lblkodetmp.Text = Session("kodetmp")
        Dim myData As Data.DataSet
        Dim myhtml As New StringBuilder

        txtTyoeHD3.Items.Add("H")
        txtTyoeHD3.Items.Add("D")
        txtstd_ISO3.Items.Add("N")
        txtstd_ISO3.Items.Add("Y")
        txtstd_Policy3.Items.Add("N")
        txtstd_Policy3.Items.Add("Y")
        txtstd_Lain3.Items.Add("N")
        txtstd_Lain3.Items.Add("Y")


        txtTyoeHD1.Items.Add("H")
        txtTyoeHD1.Items.Add("D")
        txtstd_ISO1.Items.Add("N")
        txtstd_ISO1.Items.Add("Y")
        txtstd_Policy1.Items.Add("N")
        txtstd_Policy1.Items.Add("Y")
        txtstd_Lain1.Items.Add("N")
        txtstd_Lain1.Items.Add("Y")




        myData = myChef.SQLJABATAN
        txtPIC13.Items.Add("")
        txtPIC13.Items(0).Value = ""
        txtPIC23.Items.Add("")
        txtPIC23.Items(0).Value = ""

        txtPIC11.Items.Add("")
        txtPIC11.Items(0).Value = ""
        txtPIC21.Items.Add("")
        txtPIC21.Items(0).Value = ""

        txtPIC13Entry.Items.Add("")
        txtPIC13Entry.Items(0).Value = ""
        txtPIC23Entry.Items.Add("")
        txtPIC23Entry.Items(0).Value = ""

        txtPIC11Entry.Items.Add("")
        txtPIC11Entry.Items(0).Value = ""
        txtPIC21Entry.Items.Add("")
        txtPIC21Entry.Items(0).Value = ""

        Dim i As Integer

        For i = 1 To 10
            dolevel1.Items.Add(i)
            dolevel3.Items.Add(i)
        Next


        For i = 0 To myData.Tables(0).Rows.Count - 1
            j = i + 1
            txtPIC13.Items.Add(myData.Tables(0).Rows(i).Item("jabatan"))
            txtPIC13.Items(j).Value = myData.Tables(0).Rows(i).Item("kode")
            txtPIC23.Items.Add(myData.Tables(0).Rows(i).Item("jabatan"))
            txtPIC23.Items(j).Value = myData.Tables(0).Rows(i).Item("kode")

            txtPIC11.Items.Add(myData.Tables(0).Rows(i).Item("jabatan"))
            txtPIC11.Items(j).Value = myData.Tables(0).Rows(i).Item("kode")
            txtPIC21.Items.Add(myData.Tables(0).Rows(i).Item("jabatan"))
            txtPIC21.Items(j).Value = myData.Tables(0).Rows(i).Item("kode")

            txtPIC13Entry.Items.Add(myData.Tables(0).Rows(i).Item("jabatan"))
            txtPIC13Entry.Items(j).Value = myData.Tables(0).Rows(i).Item("kode")
            txtPIC23Entry.Items.Add(myData.Tables(0).Rows(i).Item("jabatan"))
            txtPIC23Entry.Items(j).Value = myData.Tables(0).Rows(i).Item("kode")

            txtPIC11Entry.Items.Add(myData.Tables(0).Rows(i).Item("jabatan"))
            txtPIC11Entry.Items(j).Value = myData.Tables(0).Rows(i).Item("kode")
            txtPIC21Entry.Items.Add(myData.Tables(0).Rows(i).Item("jabatan"))
            txtPIC21Entry.Items(j).Value = myData.Tables(0).Rows(i).Item("kode")


        Next

        myData = myChef.SQLCHECKLIST_KODETMP(lblkodetmp.Text)
        If myData.Tables(0).Rows.Count > 0 Then
            lblnamatmp.Text = myData.Tables(0).Rows(0).Item("nama_tmp")
        End If

        myData = myChef.SQLCHECKLISTDTL(lblkodetmp.Text)
        myhtml.Append(" <table id='mytable1' class='cell-border' cellspacing='0' width='100%' style=' margin: 0 auto; width: 100%; clear: both; border-collapse: collapse; table-layout: fixed; word-wrap:break-word; '> ")
        myhtml.Append("   <thead>")
        myhtml.Append("      <tr>")
        myhtml.Append("         <th>No</th>")
        myhtml.Append("         <th>No Parent</th>")
        myhtml.Append("         <th>Level</th>")
        myhtml.Append("         <th>Bobot</th>")
        'myhtml.Append("         <th></th>")
        myhtml.Append("         <th></th>")
        myhtml.Append("         <th>No Report</th>")
        myhtml.Append("         <th>Type</th>")
        myhtml.Append("         <th>Standard<br/>ISO</th>")
        myhtml.Append("         <th>Standard<br/>Policy</th>")
        myhtml.Append("         <th>Standard<br/>Lainnya</th>")
        myhtml.Append("         <th>Jumlah<br/>Control</th>")
        myhtml.Append("         <th>Kode<br/>Activity</th>")
        myhtml.Append("         <th>Main<br/>Activity</th>")
        'myhtml.Append("         <th></th>")
        myhtml.Append("         <th>Main<br/>PIC</th>")
        myhtml.Append("         <th>Main PIC<br/>Update</th>")
        myhtml.Append("         <th>Main PIC<br/>Penilaian</th>")
        myhtml.Append("         <th>Second<br/>Activity</th>")
        'myhtml.Append("         <th></th>")
        myhtml.Append("         <th>Second<br/>PIC</th>")
        myhtml.Append("         <th>Second PIC<br/>Update</th>")
        myhtml.Append("         <th>Second PIC<br/>Penilaian</th>")
        myhtml.Append("         <th>Guidance</th>")
        myhtml.Append("         <th></th>")
        myhtml.Append("      </tr>")
        myhtml.Append("   </thead>")
        myhtml.Append("   <tbody>")
        For i = 0 To myData.Tables(0).Rows.Count - 1
            myhtml.Append("      <tr>")

            Dim Temps As String = myData.Tables(0).Rows(i).Item("no") & "|" & myData.Tables(0).Rows(i).Item("no_parent") & "|" &
             myData.Tables(0).Rows(i).Item("no_report") & "|" & myData.Tables(0).Rows(i).Item("typeHD") & "|" &
             myData.Tables(0).Rows(i).Item("std_ISO") & "|" & myData.Tables(0).Rows(i).Item("std_Policy") & "|" &
             myData.Tables(0).Rows(i).Item("std_Lain") & "|" & myData.Tables(0).Rows(i).Item("jml_control") & "|" &
             myData.Tables(0).Rows(i).Item("kode_activity1") & "|" & myData.Tables(0).Rows(i).Item("activity1") & "|" &
             myData.Tables(0).Rows(i).Item("pic1") & "|" &
             myData.Tables(0).Rows(i).Item("activity2") & "|" & myData.Tables(0).Rows(i).Item("pic2") & "|" & myData.Tables(0).Rows(i).Item("level") & "|" & myData.Tables(0).Rows(i).Item("bobot") & "|" &
             myData.Tables(0).Rows(i).Item("dir_ISO") & "|" & myData.Tables(0).Rows(i).Item("dir_Policy") & "|" &
             myData.Tables(0).Rows(i).Item("dir_Lain") & "|" &
             myData.Tables(0).Rows(i).Item("pic1entry") & "|" & myData.Tables(0).Rows(i).Item("pic2entry") & "|" & myData.Tables(0).Rows(i).Item("Kode_Pilihan_Nilai1") & "|" & myData.Tables(0).Rows(i).Item("Kode_Pilihan_Nilai2") & "|" & myData.Tables(0).Rows(i).Item("Guidance")

            'myhtml.Append("<td class='text-right'>")
            'myhtml.Append("<button type='button' class='button2xx' onClick='btnClick(" & Chr(34) & myData.Tables(0).Rows(i).Item("kode") & Chr(34) & ")'>Detail</button>")
            'myhtml.Append("</td>")
            myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("no") & "</td>")
            myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("no_parent") & "</td>")
            myhtml.Append("         <td style='text-align:right'>" & myData.Tables(0).Rows(i).Item("level") & "</td>")
            myhtml.Append("         <td style='text-align:right'>" & myData.Tables(0).Rows(i).Item("bobot") & "</td>")

            myhtml.Append("<td class='text-right'>")
            myhtml.Append("<button type='button' class='button2xx' data-book-id='" & Temps & "' data-target='#formModalEdit' data-toggle='modal'>Edit</button>")
            myhtml.Append("</td>")

            myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("no_report") & "</td>")
            myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("typeHD") & "</td>")

            If myData.Tables(0).Rows(i).Item("dir_ISO") = "" Then
                myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("std_ISO") & "</td>")
            Else
                myhtml.Append("         <td><a href=" & Chr(34) & "#" & Chr(34) & " onclick=" & Chr(34) & "openw('" & myData.Tables(0).Rows(i).Item("dir_ISO") & "');return false;" & Chr(34) & ">" & myData.Tables(0).Rows(i).Item("std_ISO") & "</a></td>")
            End If
            If myData.Tables(0).Rows(i).Item("dir_Policy") = "" Then
                myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("std_Policy") & "</td>")
            Else
                myhtml.Append("         <td><a href=" & Chr(34) & "#" & Chr(34) & " onclick=" & Chr(34) & "openw('" & myData.Tables(0).Rows(i).Item("dir_Policy") & "');return false;" & Chr(34) & ">" & myData.Tables(0).Rows(i).Item("std_Policy") & "</a></td>")
            End If
            If myData.Tables(0).Rows(i).Item("dir_Lain") = "" Then
                myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("std_Lain") & "</td>")
            Else
                myhtml.Append("         <td><a href=" & Chr(34) & "#" & Chr(34) & " onclick=" & Chr(34) & "openw('" & myData.Tables(0).Rows(i).Item("dir_Lain") & "');return false;" & Chr(34) & ">" & myData.Tables(0).Rows(i).Item("std_Lain") & "</a></td>")
            End If

            myhtml.Append("         <td style='text-align:right'>" & myData.Tables(0).Rows(i).Item("jml_control") & "</td>")
            myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("kode_activity1") & "</td>")
            myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("activity1") & "</td>")
            'myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("pic1") & "</td>")
            myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("pica1") & "</td>")
            myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("pica1entry") & "</td>")
            myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("Kode_Pilihan_Nilai1") & "</td>")
            myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("activity2") & "</td>")
            'myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("pic2") & "</td>")
            myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("pica2") & "</td>")
            myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("pica2entry") & "</td>")
            myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("Kode_Pilihan_Nilai2") & "</td>")
            myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("guidance") & "</td>")

            myhtml.Append("<td class='text-right'>")
            myhtml.Append("<button type='button' class='button2xx' data-book-id='" & Temps & "' data-target='#formModalDelete' data-toggle='modal'>Delete</button>")
            myhtml.Append("</td>")

            myhtml.Append("      </tr>")
        Next
        myhtml.Append("   </tbody>")
        myhtml.Append(" </table> ")

        myPlaceH.Controls.Add(New Literal() With {
          .Text = myhtml.ToString()
         })

        loadKriteriaPenilaian()
        loadGuidance()
    End Sub

    Sub loadGuidance()
        Dim dt As DataTable = GetDataSql("select * from MST_GUIDANCE order by kode_guidance")
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                ddlGuidance_Edit.Items.Add(New ListItem(dt.Rows(i)("kode_guidance") + "-" + dt.Rows(i)("nama_guidance"), dt.Rows(i)("kode_guidance")))
                ddlGuidance_New.Items.Add(New ListItem(dt.Rows(i)("kode_guidance") + "-" + dt.Rows(i)("nama_guidance"), dt.Rows(i)("kode_guidance")))
            Next
        End If
    End Sub

    Sub loadKriteriaPenilaian()
        Dim dt As DataTable = GetDataSql("select * from MST_KRITERIA_PENILAIAN order by kode_kriteria")
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                ddlKriteriaPenilaian_1_Edit.Items.Add(New ListItem(dt.Rows(i)("kode_kriteria") + "-" + dt.Rows(i)("nama_kriteria"), dt.Rows(i)("kode_kriteria")))
                ddlKriteriaPenilaian_2_Edit.Items.Add(New ListItem(dt.Rows(i)("kode_kriteria") + "-" + dt.Rows(i)("nama_kriteria"), dt.Rows(i)("kode_kriteria")))
                ddlKriteriaPenilaian_1_New.Items.Add(New ListItem(dt.Rows(i)("kode_kriteria") + "-" + dt.Rows(i)("nama_kriteria"), dt.Rows(i)("kode_kriteria")))
                ddlKriteriaPenilaian_2_New.Items.Add(New ListItem(dt.Rows(i)("kode_kriteria") + "-" + dt.Rows(i)("nama_kriteria"), dt.Rows(i)("kode_kriteria")))
            Next
        End If

    End Sub

    Private Sub btnInsert3_Click(sender As Object, e As EventArgs) Handles btnInsert3.Click

        Try

            infoerror.Text = ""

            Dim mpLabel As Label
            Dim myusername As String = ""
            mpLabel = CType(Master.FindControl("lblusername"), Label)
            If Not mpLabel Is Nothing Then
                myusername = mpLabel.Text
            End If
            infoerror.Text = myChef.SQLNewCHECKLISTDTL(lblkodetmp.Text, txtNo3.Text, txtNo_Parent3.Text, txtNo_Report3.Text,
                               txtTyoeHD3.Text, txtstd_ISO3.Text, txtstd_Policy3.Text, txtstd_Lain3.Text, Val(Txtjml_control3.Text),
                               txtkode_activity13.Text, txtactivity13.Text, txtPIC13.Text, txtActivity23.Text, txtPIC23.Text,
                                 myusername, dolevel3.Text, Val(txtBobot3.Text),
                              txtdir_ISO3.Text, txtdir_Policy3.Text, txtdir_Lain3.Text, txtPIC13Entry.Text, txtPIC23Entry.Text, ddlKriteriaPenilaian_1_New.SelectedValue, ddlKriteriaPenilaian_2_New.SelectedValue, ddlGuidance_New.SelectedValue)
            Response.Redirect(HttpContext.Current.Request.Url.ToString(), True)
        Catch ex As Exception
            infoerror.Text = ex.Message
        End Try

    End Sub

    Private Sub btnDelete2_Click(sender As Object, e As EventArgs) Handles btnDelete2.Click


        Try

            infoerror.Text = ""
            infoerror.Text = myChef.SQLDelCHECKLISTDet(lblkodetmp.Text, txtNo2.Text)
            Response.Redirect(HttpContext.Current.Request.Url.ToString(), True)
        Catch ex As Exception
            infoerror.Text = ex.Message
        End Try


    End Sub

    Private Sub btnUpdate1_Click(sender As Object, e As EventArgs) Handles btnUpdate1.Click

        Try

            infoerror.Text = ""

            Dim mpLabel As Label
            Dim myusername As String = ""
            mpLabel = CType(Master.FindControl("lblusername"), Label)
            If Not mpLabel Is Nothing Then
                myusername = mpLabel.Text
            End If
            infoerror.Text = myChef.SQLEditCHECKLISTDTL(lblkodetmp.Text, txtNo1.Text, txtNo_Parent1.Text, txtNo_Report1.Text,
                               txtTyoeHD1.Text, txtstd_ISO1.Text, txtstd_Policy1.Text, txtstd_Lain1.Text, Val(Txtjml_control1.Text),
                               txtkode_activity11.Text, txtactivity11.Text, txtPIC11.Text, txtActivity21.Text, txtPIC21.Text,
                                 myusername, dolevel1.Text, Val(txtBobot1.Text),
                                                        txtdir_ISO1.Text, txtdir_Policy1.Text, txtdir_Lain1.Text,
                                                        txtPIC11Entry.Text, txtPIC21Entry.Text, ddlKriteriaPenilaian_1_Edit.SelectedValue, ddlKriteriaPenilaian_2_Edit.SelectedValue, ddlGuidance_Edit.SelectedValue)
            Response.Redirect(HttpContext.Current.Request.Url.ToString(), True)
        Catch ex As Exception
            infoerror.Text = ex.Message
        End Try


    End Sub

    Private Sub btncopy_Click(sender As Object, e As EventArgs) Handles btncopy.Click

        If txtcopytemplate.Text <> lblkodetmp.Text And txtcopytemplate.Text <> "" And lblkodetmp.Text <> "" Then
            Dim mystringbtncopy As String = myChef.SQLCOPYTMP(txtcopytemplate.Text, lblkodetmp.Text)
            If mystringbtncopy = "" Then
                Response.Redirect(HttpContext.Current.Request.Url.ToString(), True)
            Else
                infoerror.Text = mystringbtncopy
            End If
        End If

    End Sub

    Public Shared Function GetDataSql(ByVal query As String) As DataTable
        Try
            Using con As New SqlConnection(strConn)
                con.Open()
                Dim da As New SqlDataAdapter(query, con)
                Dim dt As New DataTable
                da.Fill(dt)
                con.Close()
                Return dt
            End Using
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Protected Sub InsertUpdateCommandSql(ByVal query As String)
        Dim cmd As New SqlCommand
        Try
            Using con As New SqlConnection(strConn)
                con.Open()
                cmd = New SqlCommand(query, con)
                cmd.ExecuteNonQuery()
                con.Close()
            End Using
        Catch ex As Exception
            lblerror.Text = ex.Message.ToString
        End Try
    End Sub
End Class
