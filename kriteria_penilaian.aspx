﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="kriteria_penilaian.aspx.vb" Inherits="kriteria_penilaian" MasterPageFile="~/MasterPage.master"%>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        /* The Modal (background) */
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        /* Modal Content */
        .modal-content {
            position: relative;
            background-color: #fefefe;
            margin: auto;
            padding: 0;
            border: 1px solid #888;
            width: 40%;
            box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
            -webkit-animation-name: animatetop;
            -webkit-animation-duration: 0.4s;
            animation-name: animatetop;
            animation-duration: 0.4s
        }

        /* Add Animation */
        @-webkit-keyframes animatetop {
            from {
                top: -300px;
                opacity: 0
            }

            to {
                top: 0;
                opacity: 1
            }
        }

        @keyframes animatetop {
            from {
                top: -300px;
                opacity: 0
            }

            to {
                top: 0;
                opacity: 1
            }
        }

        /* The Close Button */
        .close {
            color: white;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

            .close:hover,
            .close:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }

        .modal-header {
            padding: 2px 16px;
            background-color: #5cb85c;
            color: white;
        }

        .modal-body {
            padding: 10px 40px;
        }

        .modal-footer {
            padding: 2px 16px;
            background-color: #5cb85c;
            color: white;
        }
    </style>
    <script type="text/javascript" src="script/jquery-1.4.1.min.js"></script>
    <script>
        $(document).ready(function () {
            var table = $("#ContentPlaceHolder1_GridView1").DataTable({
                paging: true,
                searching: true,
                lengthChange: false,
            });
            $('#ContentPlaceHolder1_GridView1 tbody').on('click', 'tr', function () {
                var data = table.row(this).data();

                $("#ContentPlaceHolder1_txtKodeKriteria").val(data[0]);
                $("#ContentPlaceHolder1_txtNamaKriteria").val(data[1]);
                $("#ContentPlaceHolder1_createdBy").text(data[3]);
                $("#ContentPlaceHolder1_createdDate").text(data[4]);
                $("#ContentPlaceHolder1_lastUpdateBy").text(data[5]);
                $("#ContentPlaceHolder1_lastUpdateDate").text(data[6]);
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <asp:Label ID="lblHead" runat="server" Text="Master Kriteria Penilaian"
        Style="font-size: 20px;"></asp:Label>
    <div>
        <asp:Label ID="userEdit" runat="server"></asp:Label>
    </div>
    <div style="display: flex;">
        <table>
            <tr>
                <td>Kode Kriteria</td>
                <td>:</td>
                <td>
                    <asp:TextBox ID="txtKodeKriteria" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Nama Kriteria</td>
                <td>:</td>
                <td>
                    <asp:TextBox ID="txtNamaKriteria" runat="server"></asp:TextBox></td>
            </tr>
        </table>
        <div style="float: right; margin-left: 15px;">
            Created By: <span id="createdBy" runat="server"></span>
            <br />
            Creation Date: <span id="createdDate" runat="server"></span>
            <br />
            Last Update By: <span id="lastUpdateBy" runat="server"></span>
            <br />
            Last Update Date:<span id="lastUpdateDate" runat="server"></span><br />
        </div>
    </div>
    <br />
    <br />
    <div id="div_lbl" runat="server">
        <asp:Label ID="lbl_msg" runat="server"></asp:Label>
    </div>
    <asp:Button ID="clearData" runat="server" Text="Clear Data" OnClick="clearData_Click" CssClass="button2"/>
    <asp:Button ID="insertData" runat="server" Text="Insert Data" OnClick="insertData_Click" CssClass="button2"/>
    <asp:Button ID="updateData" runat="server" Text="Update Data" OnClick="updateData_Click" CssClass="button2"/>
    <asp:Button ID="deleteData" runat="server" Text="Delete Data" OnClick="deleteData_Click" CssClass="button2"/>

    <div>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" AllowPaging="false" AllowSorting="false"
            PageSize="10" CssClass="dataTable">
            <Columns>
                <asp:BoundField DataField="kode_kriteria" HeaderText="Kode Kriteria" />
                <asp:BoundField DataField="nama_kriteria" HeaderText="Nama Kriteria" />
                <asp:TemplateField HeaderText="Detail">
                    <ItemTemplate>
                        <button class="button2" data-toggle="modal" data-target="#modalDetail" onclick="clearModal(); loadDetail_Kriteria('<%# Eval("kode_kriteria") %>'); return false;">Detail Kriteria</button>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:Boundfield DataField="createdby" HeaderText="RBM" >
                    <HeaderStyle CssClass="hidden"></HeaderStyle>

                        <ItemStyle CssClass="hidden"></ItemStyle>
                </asp:Boundfield>
                <asp:Boundfield DataField="createdat" HeaderText="RBM" >
                    <HeaderStyle CssClass="hidden"></HeaderStyle>

                        <ItemStyle CssClass="hidden"></ItemStyle>
                </asp:Boundfield>
                <asp:Boundfield DataField="lasteditby" HeaderText="RBM" >
                    <HeaderStyle CssClass="hidden"></HeaderStyle>

                        <ItemStyle CssClass="hidden"></ItemStyle>
                </asp:Boundfield>
                <asp:Boundfield DataField="lasteditat" HeaderText="RBM" >
                    <HeaderStyle CssClass="hidden"></HeaderStyle>

                        <ItemStyle CssClass="hidden"></ItemStyle>
                </asp:Boundfield>
            </Columns>
            <PagerStyle CssClass="paginate_button" />
        </asp:GridView>

        <%--Modal Detail--%>
        <div id="modalDetail" class="modal">
            <!-- Modal content -->
            <div class="modal-content">
                <div class="modal-header">
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                    <h2>Detail Kriteria Penilaian <span id="lblKode_Penilaian"></span></h2>
                </div>
                <div class="modal-body">
                    <div id="divLoading" class="text-center" style="display:none;">
                        Loading....
                    </div>
                    <div id="divMsg" style="display:none;"><span id="lblMsg" style="color:red;"></span></div>
                    <table>
                        <tr>
                            <td>Nilai
                                <br />
                                <input type="text" id="txtDetail_Nilai" /></td>
                            <td>Penjelasan
                                <br />
                                <input type="text" id="txtDetail_Penjelasan" /></td>
                        </tr>
                    </table>
                    <input type="button" id="btnDetail_Add" onclick="addDetail_Kriteria(); return false;" value="Add" class="button2" />
                    <input type="button" id="btnDetail_Edit" onclick="editDetail_Kriteria(); return false;" value="Update" class="button2" />
                    <input type="button" id="btnDetail_Delete" onclick="deleteDetail_Kriteria(); return false;" value="Delete" class="button2" />
                    <table id="tblDetail" border="1">
                        <thead>
                            <tr>
                                <th>Nilai</th>
                                <th>Penjelasan</th>
                            </tr>
                        </thead>
                    </table>
                </div>

                <div class="modal-footer">
                    <button data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>

        <script>
            function clearModal() {
                $("#lblMsg").text('');
                $("#txtDetail_Nilai").val('');
                $("#txtDetail_Penjelasan").val('');
            }
            function putData(nilai,penjelasan) {
                $("#txtDetail_Nilai").val(nilai);
                $("#txtDetail_Penjelasan").val(penjelasan);
            }
            function loadDetail_Kriteria(kode_kriteria) {
                $("#divLoading").show();
                $("#divMsg").hide();
                deleteAllRow();
                $("#tblDetail").hide();
                $("#lblKode_Penilaian").text(kode_kriteria);
                $.ajax({
                    type: "POST",
                    url: "kriteria_penilaian.aspx/loadDetail",
                    data: '{kode_kriteria: "' + kode_kriteria + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        $("#divLoading").hide();
                        $("#divMsg").show();
                        $("#tblDetail").show();
                        var json_result = JSON.parse(response.d);
                        //let data = { "iocode": "BPP", "kodeprod": "HC001", "tanggal": "25-03-2020", "unitbaik": "8568", "lot_number": "19W1225U", "exp_date": "16-12-2020" };
                        drawTable(json_result);
                        //console.log(json_result);
                    },
                    error: function (response) { console.log(response); $("#divLoading").hide(); $("#divMsg").show(); $("#lblMsg").text("Error: Can't get data.")}
                });
            }

            function drawTable(data) {
                //console.log(data);
                for (var i = 0; i < data.length; i++) {
                    drawRow(data[i]);
                }
            }

            function drawRow(rowData) {
                var row = $("<tr onclick='putData(\"" + rowData.nilai + "\",\"" + rowData.penjelasan +"\")'/>");
                $("#tblDetail").append(row);
                row.append($("<td>" + rowData.nilai + "</td>"));
                row.append($("<td>" + rowData.penjelasan + "</td>"));
            }

            function deleteAllRow() {
                var table = document.getElementById("tblDetail");

                while (table.rows.length > 1) {
                    table.deleteRow(1);
                }
            }

            function addDetail_Kriteria() {
                let kode_kriteria = $("#lblKode_Penilaian").text();
                let nilai = $("#txtDetail_Nilai").val();
                let penjelasan = $("#txtDetail_Penjelasan").val();
                $("#divLoading").show();
                $("#divMsg").hide();
                deleteAllRow();
                $("#tblDetail").hide();
                $.ajax({
                    type: "POST",
                    url: "kriteria_penilaian.aspx/insertDetail",
                    data: '{kode_kriteria: "' + kode_kriteria + '", nilai: "' + nilai +'", penjelasan: "'+ penjelasan +'"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        $("#divLoading").hide();
                        $("#divMsg").show();
                        $("#tblDetail").show();
                        var json_result = JSON.parse(response.d);
                        //let data = { "iocode": "BPP", "kodeprod": "HC001", "tanggal": "25-03-2020", "unitbaik": "8568", "lot_number": "19W1225U", "exp_date": "16-12-2020" };
                        //drawTable(json_result);
                        $("#lblMsg").text(json_result.msg)
                        loadDetail_Kriteria(kode_kriteria)
                    },
                    error: function (response) { console.log(response); $("#divLoading").hide(); $("#divError").show(); }
                });
            }

            function editDetail_Kriteria() {
                let kode_kriteria = $("#lblKode_Penilaian").text();
                let nilai = $("#txtDetail_Nilai").val();
                let penjelasan = $("#txtDetail_Penjelasan").val();
                $("#divLoading").show();
                $("#divMsg").hide();
                deleteAllRow();
                $("#tblDetail").hide();
                $.ajax({
                    type: "POST",
                    url: "kriteria_penilaian.aspx/updateDetail",
                    data: '{kode_kriteria: "' + kode_kriteria + '", nilai: "' + nilai + '", penjelasan: "' + penjelasan + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        $("#divLoading").hide();
                        $("#divMsg").show();
                        $("#tblDetail").show();
                        var json_result = JSON.parse(response.d);
                        //let data = { "iocode": "BPP", "kodeprod": "HC001", "tanggal": "25-03-2020", "unitbaik": "8568", "lot_number": "19W1225U", "exp_date": "16-12-2020" };
                        //drawTable(json_result);
                        $("#lblMsg").text(json_result.msg)
                        loadDetail_Kriteria(kode_kriteria)
                    },
                    error: function (response) { console.log(response); $("#divLoading").hide(); $("#divError").show(); }
                });
            }

            function deleteDetail_Kriteria() {
                let kode_kriteria = $("#lblKode_Penilaian").text();
                let nilai = $("#txtDetail_Nilai").val();
                $("#divLoading").show();
                $("#divMsg").hide();
                deleteAllRow();
                $("#tblDetail").hide();
                $.ajax({
                    type: "POST",
                    url: "kriteria_penilaian.aspx/deleteDetail",
                    data: '{kode_kriteria: "' + kode_kriteria + '", nilai: "' + nilai + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        $("#divLoading").hide();
                        $("#divMsg").show();
                        $("#tblDetail").show();
                        var json_result = JSON.parse(response.d);
                        //let data = { "iocode": "BPP", "kodeprod": "HC001", "tanggal": "25-03-2020", "unitbaik": "8568", "lot_number": "19W1225U", "exp_date": "16-12-2020" };
                        //drawTable(json_result);
                        $("#lblMsg").text(json_result.msg)
                        loadDetail_Kriteria(kode_kriteria)
                    },
                    error: function (response) { console.log(response); $("#divLoading").hide(); $("#divError").show(); }
                });
            }
        </script>
    </div>
</asp:Content>


