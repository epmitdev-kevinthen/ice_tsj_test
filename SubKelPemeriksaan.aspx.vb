﻿Imports System.Data
Imports System.Data.SqlClient


Partial Class SubKelPemeriksaan
    Inherits System.Web.UI.Page

    Public Shared ConnStrAudit As String = ConfigurationManager.ConnectionStrings("SQLICE").ConnectionString
    Public Shared cmd As New SqlCommand

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        If Not Page.IsPostBack Then
            If Not Page.IsPostBack Then
                lblKodeCab.Text = Request.QueryString("kode")
                tampilPemeriksaan()
                tampilTable()
                If Request.QueryString("subkode") <> "" Then
                    tampilSubPemeriksaan(Request.QueryString("subkode").ToString)
                End If
            End If

        End If
    End Sub
    Protected Sub tampilSubPemeriksaan(ByVal subkode As String)
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                Dim da As New SqlDataAdapter("select * from MSTSUBKELPERIKSA where kode = '" & lblKodeCab.Text & "' and subkode='" & subkode & "'", con)
                Dim dt As New DataTable
                da.Fill(dt)
                If dt.Rows.Count > 0 Then
                    txtSubKode.Text = dt.Rows(0).Item("subkode")
                    txtSubKelPer.Text = dt.Rows(0).Item("subkelperiksa")
                    subcreatedBy.InnerText = dt.Rows(0).Item("created_by")
                    subcreatedDate.InnerText = dt.Rows(0).Item("creation_date")
                    sublastUpdateBy.InnerText = dt.Rows(0).Item("last_update_by")
                    sublastUpdateDate.InnerText = dt.Rows(0).Item("last_update_date")
                End If
                con.Close()
            End Using
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub tampilTable()
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                Dim da As New SqlDataAdapter("select * from MSTSUBKELPERIKSA where kode = '" & lblKodeCab.Text & "'", con)
                Dim dt As New DataTable
                da.Fill(dt)
                GridView1.DataSource = dt
                GridView1.DataBind()
                GridView1.HeaderRow.TableSection = TableRowSection.TableHeader
                con.Close()
            End Using
        Catch ex As Exception

        End Try

    End Sub
    Protected Sub tampilPemeriksaan()
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                Dim da As New SqlDataAdapter("select * from MSTKELPERIKSA where kode = '" & lblKodeCab.Text & "'", con)
                Dim dt As New DataTable
                da.Fill(dt)
                If dt.Rows.Count > 0 Then
                    lblKelPer.Text = dt.Rows(0).Item("kelperiksa")
                    createdBy.InnerText = dt.Rows(0).Item("created_by")
                    createdDate.InnerText = dt.Rows(0).Item("creation_date")
                    lastUpdateBy.InnerText = dt.Rows(0).Item("last_update_by")
                    lastUpdateDate.InnerText = dt.Rows(0).Item("last_update_date")
                End If
                con.Close()
            End Using
        Catch ex As Exception

        End Try

    End Sub
    Protected Sub GridView1_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        tampilTable()
    End Sub

    Protected Sub GridView1_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GridView1.RowCommand
        If e.CommandName = "cmdsubmit" Then
            Dim kode As String = lblKodeCab.Text
            Dim subkode As String = e.CommandArgument
            Response.Redirect("SubKelPemeriksaan.aspx?kode=" & kode & "&subkode=" & subkode & "")
        ElseIf e.CommandName = "cmddel" Then


        End If
    End Sub
    Protected Sub btnBackKel_Click(sender As Object, e As EventArgs)
        Response.Redirect("KelPemeriksaan.aspx?kode=" & lblKodeCab.Text & "")
    End Sub
    Protected Sub clearData_Click(sender As Object, e As EventArgs)
        txtSubKelPer.Text = ""
        txtSubKode.Text = ""
    End Sub
    Protected Sub insertData_Click(sender As Object, e As EventArgs)
        Dim sqlstr As String = "insert into MSTSUBKELPERIKSA(kode, kelperiksa, subkode, subkelperiksa, created_by, creation_date,  last_update_by, last_update_date) values('" & lblKodeCab.Text & "', '" & lblKelPer.Text & "', '" & txtSubKode.Text & "', '" & txtSubKelPer.Text & "', '" & Session("sUsername") & "', current_timestamp, '" & Session("sUsername") & "', current_timestamp)"
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                cmd = New SqlCommand(sqlstr, con)
                cmd.ExecuteNonQuery()
                con.Close()
            End Using

            div_lbl.Visible = True
            lbl_msg.Text = "Insert Successful!"
            tampilTable()
        Catch ex As Exception
            div_lbl.Visible = True
            lbl_msg.Text = ex.Message.ToString
        End Try
    End Sub
    Protected Sub updateData_Click(sender As Object, e As EventArgs)
        Dim sqlstr As String = "update MSTSUBKELPERIKSA set subkode = '" & txtSubKode.Text & "', subkelperiksa = '" & txtSubKelPer.Text & "', last_update_by = '" & Session("sUsername") & "', last_update_date = current_timestamp where kode='" & lblKodeCab.Text & "' and subkode='" & txtSubKode.Text & "'"
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                cmd = New SqlCommand(sqlstr, con)
                cmd.ExecuteNonQuery()
                con.Close()
            End Using

            div_lbl.Visible = True
            lbl_msg.Text = "Update Successful!"
            tampilTable()
        Catch ex As Exception
            div_lbl.Visible = True
            lbl_msg.Text = ex.Message.ToString
        End Try
    End Sub
    Protected Sub deleteData_Click(sender As Object, e As EventArgs)
        Dim sqlstr As String = "delete from MSTSUBKELPERIKSA where kode = '" & lblKodeCab.Text & "' and subkode='" & txtSubKode.Text & "'"
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                cmd = New SqlCommand(sqlstr, con)
                cmd.ExecuteNonQuery()
                con.Close()
            End Using

            div_lbl.Visible = True
            lbl_msg.Text = "Delete Successful!"
            tampilTable()
        Catch ex As Exception
            div_lbl.Visible = True
            lbl_msg.Text = ex.Message.ToString
        End Try
    End Sub
End Class
