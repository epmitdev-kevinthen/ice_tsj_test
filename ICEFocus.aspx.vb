﻿
Partial Class ICEFocus
    Inherits System.Web.UI.Page
    Dim myChef As New Chef

    Private Sub btnCheckHist_Click(sender As Object, e As EventArgs) Handles btnCheckHist.Click
        doHist.Items.Clear()
        Dim i As Integer
        Dim myData As Data.DataSet

        myData = myChef.SQLSUMUNC(doTemplate.SelectedItem.Value, doPeriode.SelectedItem.Value, doBranch.SelectedItem.Value)
        doHist.Items.Add("")
        For i = 0 To myData.Tables(0).Rows.Count - 1
            doHist.Items.Add(myData.Tables(0).Rows(i).Item("tgl_unconfirm"))
        Next

    End Sub

    Private Sub btnFind_Click(sender As Object, e As EventArgs) Handles btnFind.Click
        'Call sub_refresh()

        Session("cabangRpt") = doBranch.SelectedItem.Value
        Session("periodeRpt") = doPeriode.SelectedItem.Value
        Session("templateRpt") = doTemplate.SelectedItem.Value
        Session("cabangR") = doBranch.SelectedItem.Text
        Session("periodeR") = doPeriode.SelectedItem.Text
        Session("templateR") = doTemplate.SelectedItem.Text

        Session("unconfirm") = doHist.Text

        If myChef.SQLICECHCK(Session("templateRpt"), Session("periodeRpt"), Session("cabangRpt")) Then
            infoerror1.Text = ""
            Response.Redirect("ICEFocusReport.aspx")
        Else
            infoerror1.Text = "Tidak ada data"
        End If


    End Sub


    Private Sub CheckListAudit_Init(sender As Object, e As EventArgs) Handles Me.Init
        Dim myData As Data.DataSet
        Dim i As Integer
        Dim j As Integer

        If Session("sUsername") Is Nothing Then
            Response.Redirect("Default.aspx")
        End If


        myData = myChef.SQLLISTTEMPLATE1
        j = 0

        For i = 0 To myData.Tables(0).Rows.Count - 1
            'If (myData.Tables(0).Rows(i).Item("flag_selasses") = "Y" And Session("sCabang") <> "PST") Or
            '    Session("sCabang") = "PST" Then

            'If myData.Tables(0).Rows(i).Item("kodedept") = "" Or
            '        If UCase(Session("sUsername")) = "ADMIN" Or
            'Session("kodedept") = "3501" Or
            'myData.Tables(0).Rows(i).Item("kodedept") = Session("kodedept") Then

            doTemplate.Items.Add(myData.Tables(0).Rows(i).Item("nama_tmp"))
                doTemplate.Items(j).Value = myData.Tables(0).Rows(i).Item("kode")


                j = j + 1

            'End If

            'End If
        Next
        myData = myChef.SQLMSTPERIODEAKTIF
        j = 0

        For i = 0 To myData.Tables(0).Rows.Count - 1

            doPeriode.Items.Add(myData.Tables(0).Rows(i).Item("nama_prd"))
            doPeriode.Items(j).Value = myData.Tables(0).Rows(i).Item("kode_prd")
            j = j + 1

        Next
        myData = myChef.SQLLISTCAB

        'If Session("sCabang") = "PST" Then
        '    doBranch.Items.Add("")
        '    doBranch.Items(0).Value = ""
        '    j = 1
        'Else
        '    j = 0
        'End If

        j = 0

        For i = 0 To myData.Tables(0).Rows.Count - 1
            If Session("sCabang") = "PST" Or Session("sCabang") = myData.Tables(0).Rows(i).Item("kodecab") Then

                doBranch.Items.Add(myData.Tables(0).Rows(i).Item("namacab"))

                doBranch.Items(j).Value = myData.Tables(0).Rows(i).Item("kodecab")
                j = j + 1
            End If
        Next


        'Call sub_Refresh()
    End Sub


End Class
