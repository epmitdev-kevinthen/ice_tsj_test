﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.OracleClient
Imports Oracle.DataAccess.Client
Partial Class Approval
    Inherits System.Web.UI.Page

    Public Shared ConnDB As String = ConfigurationManager.ConnectionStrings("SQLICE").ConnectionString
    Public Shared oConnDB As New SqlConnection(ConnDB)
    Public Shared cmd As New SqlCommand

    Dim wsICE As New SPKTrxData.SPKTrxData

    'Public oraDB As String = ConfigurationManager.ConnectionStrings("oracon").ConnectionString
    'Public oraConn As New OracleConnection(oraDB) ' VB.NET
    'Public oraCmd As New OracleCommand
    'Public oraDR As OracleDataReader

    'Sub OpenOracleConnection()
    '    If oraConn.State = Data.ConnectionState.Closed Then
    '        oraConn.Open()
    '    End If
    'End Sub

    'Sub CloseOracleConnection()
    '    If oraConn.State = Data.ConnectionState.Open Then
    '        oraConn.Close()
    '    End If
    'End Sub

    Private Sub initTable(sender As Object, e As EventArgs) Handles Me.Init

        Dim myData As DataSet
        Dim myHtml As New StringBuilder
        Dim dtime As Date
        Dim deto As String

        If Session("sUsername") Is Nothing Then
            Response.Redirect("Default.aspx")
        End If

        myData = getAllData()

        myHtml.Append(" <table id='mytable' class='stripe row-border order-column' cellspacing='0' width='100%'> ")
        myHtml.Append("   <thead>")
        myHtml.Append("      <tr>")
        myHtml.Append("         <th>ID</th>")
        myHtml.Append("         <th>Kode Cabang</th>")
        myHtml.Append("         <th>Customer Number</th>")
        myHtml.Append("         <th>Customer Name</th>")
        myHtml.Append("         <th>Submit Date</th>")
        myHtml.Append("         <th>Submit By</th>")
        myHtml.Append("         <th></th>")
        myHtml.Append("      </tr>")
        myHtml.Append("   </thead>")
        myHtml.Append("   <tbody>")

        For i = 0 To myData.Tables(0).Rows.Count - 1

            myHtml.Append("      <tr>")
            myHtml.Append("         <td style='text-align:right'>" & myData.Tables(0).Rows(i).Item("ID").ToString() & "</td>")
            myHtml.Append("         <td style='text-align:left'>" & myData.Tables(0).Rows(i).Item("ORG_CODE").ToString() & "</td>")
            myHtml.Append("         <td style='text-align:right'>" & myData.Tables(0).Rows(i).Item("CUSTOMER_NUMBER").ToString() & "</td>")
            myHtml.Append("         <td style='text-align:left'>" & myData.Tables(0).Rows(i).Item("CUSTOMER_NAME").ToString() & "</td>")
            dtime = Convert.ToDateTime(myData.Tables(0).Rows(i).Item("SUBMIT_DATE").ToString())
            deto = dtime.ToString("dd-MMM-yyyy")
            myHtml.Append("         <td style='text-align:right'>" & deto & "</td>")
            myHtml.Append("         <td style='text-align:left'>" & myData.Tables(0).Rows(i).Item("SUBMITTED_BY").ToString() & "</td>")
            'myHtml.Append("         <td><button type='button'>Details</button></td>")
            'myHtml.Append("         <td><button type='button' onclick='showDiv(" & myData.Tables(0).Rows(i).Item("ID") & ", " & myData.Tables(0).Rows(i).Item("CUSTOMER_NUMBER") & ", " & myData.Tables(0).Rows(i).Item("CUSTOMER_NAME") &
            '              ", " & myData.Tables(0).Rows(i).Item("SHIP_TO_ID") & ", " & myData.Tables(0).Rows(i).Item("ALAMAT") & ", " & myData.Tables(0).Rows(i).Item("NO_TELEPON") & ", " & myData.Tables(0).Rows(i).Item("NAMA_PEMILIK") &
            '              ", " & myData.Tables(0).Rows(i).Item("NO_HP_PEMILIK") & ", " & myData.Tables(0).Rows(i).Item("NAMA_PIC_PEMBAYARAN") & ", " & myData.Tables(0).Rows(i).Item("NO_HP_PIC_PEMBAYARAN") &
            '              ", " & myData.Tables(0).Rows(i).Item("NAMA_PIC_PEMBELIAN") & ", " & myData.Tables(0).Rows(i).Item("NO_HP_PIC_PEMBELIAN") & ", " & myData.Tables(0).Rows(i).Item("NAMA_PIC_GUDANG") &
            '              ", " & myData.Tables(0).Rows(i).Item("NO_HP_PIC_GUDANG") & ", " & myData.Tables(0).Rows(i).Item("NAMA_PIC_APJ") & ", " & myData.Tables(0).Rows(i).Item("NO_HP_PIC_APJ") &
            '              ", " & myData.Tables(0).Rows(i).Item("NAMA_PIC_TAX") & ", " & myData.Tables(0).Rows(i).Item("NO_HP_PIC_TAX") & ", " & myData.Tables(0).Rows(i).Item("PROSEDUR_PEMBAYARAN") &
            '              ", " & myData.Tables(0).Rows(i).Item("CARA_PEMBAYARAN") & ")'>Details</button></td>")
            'myHtml.Append("         <td><button type='button' onclick=showDiv('" & myData.Tables(0).Rows(i).Item("ID").ToString() & "','" & myData.Tables(0).Rows(i).Item("ID").ToString() & "')>Details</button></td>")
            'myHtml.Append("         <td><button type='button' onclick=showDiv(1,1)>Details</button></td>")
            myHtml.Append("         <td><button type='button' class='button2' onclick=showDiv(" & myData.Tables(0).Rows(i).Item("ID") & ")>Pilih</button></td>")

            myHtml.Append("      </tr>")

        Next

        myHtml.Append("   </tbody>")
        myHtml.Append(" </table> ")

        tblApproval.Controls.Add(New Literal() With {
            .Text = myHtml.ToString()
        })

    End Sub

    Public Function getAllData() As DataSet

        Dim strSQL As String
        If Session("sCabang") = "PST" Then
            strSQL = "SELECT * FROM APPROVAL_DATA_CUSTOMER where STATUS = 'DIPROSES'"
        Else
            strSQL = "SELECT * FROM APPROVAL_DATA_CUSTOMER where STATUS = 'DIPROSES' AND ORG_CODE = '" + Session("sCabang") + "'"
        End If


        Dim ds As DataSet = createDS(strSQL)
        Return ds

    End Function

    Public Function createDS(ByVal strSQL As String, Optional ByVal sqlParam As SqlParameter = Nothing) As DataSet

        If oConnDB.State = ConnectionState.Open Then
            oConnDB.Close()
        End If
        oConnDB.Open()

        Dim scmd As New SqlCommand(strSQL, oConnDB)
        scmd.CommandTimeout = 600

        If Not IsNothing(sqlParam) Then
            scmd.Parameters.Add(sqlParam)
        End If

        Dim sda As New SqlDataAdapter(scmd)
        Dim ds As New DataSet
        sda.Fill(ds)

        Return ds
        If oConnDB.State = ConnectionState.Open Then
            oConnDB.Close()
        End If

    End Function

    'Show Detail///////////////////////////////////////////////////////////////////////
    Protected Sub showDetail(sender As Object, e As EventArgs) Handles btnDetail.Click

        Dim ds As DataSet
        Dim ds2 As DataSet
        ds = getDetails(ID.Text)

        Dim index As Int16
        Dim value As String
        Dim dtime As Date
        Dim dtime2 As DateTime
        Dim deto As String


        If ds.Tables(0).Rows.Count > 0 Then

            'If ds.Tables(0).Rows(0).Item("CUSTOMER_NUMBER").ToString().Contains("'") Then

            '    index = ds.Tables(0).Rows(0).Item("CUSTOMER_NUMBER").ToString().IndexOf("'")

            '    cusNum.Text = ds.Tables(0).Rows(0).Item("CUSTOMER_NUMBER").ToString().Remove(index, 1)

            'Else

            '    cusNum.Text = ds.Tables(0).Rows(0).Item("CUSTOMER_NUMBER").ToString()

            'End If

            ds2 = getDetails2(ds.Tables(0).Rows(0).Item("CUSTOMER_NUMBER").ToString())

            value = ds.Tables(0).Rows(0).Item("CUSTOMER_NUMBER").ToString()
            While value.Contains("'")

                index = value.IndexOf("'")
                value = value.Remove(index, 1)

            End While
            While value.Contains("#")

                index = value.IndexOf("#")
                value = value.Remove(index, 1)

            End While
            cusNum.Text = value

            'If ds.Tables(0).Rows(0).Item("CUSTOMER_NAME").ToString().Contains("'") Then

            '    index = ds.Tables(0).Rows(0).Item("CUSTOMER_NAME").ToString().IndexOf("'")

            '    cusName.Text = ds.Tables(0).Rows(0).Item("CUSTOMER_NAME").ToString().Remove(index, 1)

            'Else

            '    cusName.Text = ds.Tables(0).Rows(0).Item("CUSTOMER_NAME").ToString()

            'End If

            value = ds.Tables(0).Rows(0).Item("CUSTOMER_NAME").ToString()
            While value.Contains("'")

                index = value.IndexOf("'")
                value = value.Remove(index, 1)

            End While
            While value.Contains("#")

                index = value.IndexOf("#")
                value = value.Remove(index, 1)

            End While
            cusName.Text = value

            'If ds.Tables(0).Rows(0).Item("SHIP_TO_ID").ToString().Contains("'") Then

            '    index = ds.Tables(0).Rows(0).Item("SHIP_TO_ID").ToString().IndexOf("'")

            '    shipToId.Text = ds.Tables(0).Rows(0).Item("SHIP_TO_ID").ToString().Remove(index, 1)

            'Else

            '    shipToId.Text = ds.Tables(0).Rows(0).Item("SHIP_TO_ID").ToString()

            'End If

            value = ds.Tables(0).Rows(0).Item("SHIP_TO_ID").ToString()
            While value.Contains("'")

                index = value.IndexOf("'")
                value = value.Remove(index, 1)

            End While
            While value.Contains("#")

                index = value.IndexOf("#")
                value = value.Remove(index, 1)

            End While
            shipToId.Text = value

            'If ds.Tables(0).Rows(0).Item("ALAMAT").ToString().Contains("'") Then

            '    index = ds.Tables(0).Rows(0).Item("ALAMAT").ToString().IndexOf("'")

            '    alamat.Text = ds.Tables(0).Rows(0).Item("ALAMAT").ToString().Remove(index, 1)

            'Else

            '    alamat.Text = ds.Tables(0).Rows(0).Item("ALAMAT").ToString()

            'End If

            value = ds.Tables(0).Rows(0).Item("ALAMAT").ToString()
            While value.Contains("'")

                index = value.IndexOf("'")
                value = value.Remove(index, 1)

            End While
            While value.Contains("#")

                index = value.IndexOf("#")
                value = value.Remove(index, 1)

            End While
            alamat.Text = value

            'If ds.Tables(0).Rows(0).Item("NO_TELEPON").ToString().Contains("'") Then

            '    index = ds.Tables(0).Rows(0).Item("NO_TELEPON").ToString().IndexOf("'")

            '    noTlp.Text = ds.Tables(0).Rows(0).Item("NO_TELEPON").ToString().Remove(index, 1)

            'Else

            '    noTlp.Text = ds.Tables(0).Rows(0).Item("NO_TELEPON").ToString()

            'End If

            value = ds.Tables(0).Rows(0).Item("NO_TELEPON").ToString()
            While value.Contains("'")

                index = value.IndexOf("'")
                value = value.Remove(index, 1)

            End While
            While value.Contains("#")

                index = value.IndexOf("#")
                value = value.Remove(index, 1)

            End While
            noTlp.Text = value

            'If ds.Tables(0).Rows(0).Item("NAMA_PEMILIK").ToString().Contains("'") Then

            '    index = ds.Tables(0).Rows(0).Item("NAMA_PEMILIK").ToString().IndexOf("'")

            '    nmPmlk.Text = ds.Tables(0).Rows(0).Item("NAMA_PEMILIK").ToString().Remove(index, 1)

            'Else

            '    nmPmlk.Text = ds.Tables(0).Rows(0).Item("NAMA_PEMILIK").ToString()

            'End If

            value = ds.Tables(0).Rows(0).Item("NAMA_PEMILIK").ToString()
            While value.Contains("'")

                index = value.IndexOf("'")
                value = value.Remove(index, 1)

            End While
            While value.Contains("#")

                index = value.IndexOf("#")
                value = value.Remove(index, 1)

            End While
            nmPmlk.Text = value

            'If ds.Tables(0).Rows(0).Item("NO_HP_PEMILIK").ToString().Contains("'") Then

            '    index = ds.Tables(0).Rows(0).Item("NO_HP_PEMILIK").ToString().IndexOf("'")

            '    noHpPmlk.Text = ds.Tables(0).Rows(0).Item("NO_HP_PEMILIK").ToString().Remove(index, 1)

            'Else

            '    noHpPmlk.Text = ds.Tables(0).Rows(0).Item("NO_HP_PEMILIK").ToString()

            'End If

            value = ds.Tables(0).Rows(0).Item("NO_HP_PEMILIK").ToString()
            While value.Contains("'")

                index = value.IndexOf("'")
                value = value.Remove(index, 1)

            End While
            While value.Contains("#")

                index = value.IndexOf("#")
                value = value.Remove(index, 1)

            End While
            noHpPmlk.Text = value

            'If ds.Tables(0).Rows(0).Item("NAMA_PIC_PEMBAYARAN").ToString().Contains("'") Then

            '    index = ds.Tables(0).Rows(0).Item("NAMA_PIC_PEMBAYARAN").ToString().IndexOf("'")

            '    picByr.Text = ds.Tables(0).Rows(0).Item("NAMA_PIC_PEMBAYARAN").ToString().Remove(index, 1)

            'Else

            '    picByr.Text = ds.Tables(0).Rows(0).Item("NAMA_PIC_PEMBAYARAN").ToString()

            'End If

            value = ds.Tables(0).Rows(0).Item("NAMA_PIC_PEMBAYARAN").ToString()
            While value.Contains("'")

                index = value.IndexOf("'")
                value = value.Remove(index, 1)

            End While
            While value.Contains("#")

                index = value.IndexOf("#")
                value = value.Remove(index, 1)

            End While
            picByr.Text = value

            'If ds.Tables(0).Rows(0).Item("NO_HP_PIC_PEMBAYARAN").ToString().Contains("'") Then

            '    index = ds.Tables(0).Rows(0).Item("NO_HP_PIC_PEMBAYARAN").ToString().IndexOf("'")

            '    hpPicByr.Text = ds.Tables(0).Rows(0).Item("NO_HP_PIC_PEMBAYARAN").ToString().Remove(index, 1)

            'Else

            '    hpPicByr.Text = ds.Tables(0).Rows(0).Item("NO_HP_PIC_PEMBAYARAN").ToString()

            'End If

            value = ds.Tables(0).Rows(0).Item("NO_HP_PIC_PEMBAYARAN").ToString()
            While value.Contains("'")

                index = value.IndexOf("'")
                value = value.Remove(index, 1)

            End While
            While value.Contains("#")

                index = value.IndexOf("#")
                value = value.Remove(index, 1)

            End While
            hpPicByr.Text = value

            'If ds.Tables(0).Rows(0).Item("NAMA_PIC_PEMBELIAN").ToString().Contains("'") Then

            '    index = ds.Tables(0).Rows(0).Item("NAMA_PIC_PEMBELIAN").ToString().IndexOf("'")

            '    picBeli.Text = ds.Tables(0).Rows(0).Item("NAMA_PIC_PEMBELIAN").ToString().Remove(index, 1)

            'Else

            '    picBeli.Text = ds.Tables(0).Rows(0).Item("NAMA_PIC_PEMBELIAN").ToString()

            'End If

            value = ds.Tables(0).Rows(0).Item("NAMA_PIC_PEMBELIAN").ToString()
            While value.Contains("'")

                index = value.IndexOf("'")
                value = value.Remove(index, 1)

            End While
            While value.Contains("#")

                index = value.IndexOf("#")
                value = value.Remove(index, 1)

            End While
            picBeli.Text = value

            'If ds.Tables(0).Rows(0).Item("NO_HP_PIC_PEMBELIAN").ToString().Contains("'") Then

            '    index = ds.Tables(0).Rows(0).Item("NO_HP_PIC_PEMBELIAN").ToString().IndexOf("'")

            '    hpPicBeli.Text = ds.Tables(0).Rows(0).Item("NO_HP_PIC_PEMBELIAN").ToString().Remove(index, 1)

            'Else

            '    hpPicBeli.Text = ds.Tables(0).Rows(0).Item("NO_HP_PIC_PEMBELIAN").ToString()

            'End If

            value = ds.Tables(0).Rows(0).Item("NO_HP_PIC_PEMBELIAN").ToString()
            While value.Contains("'")

                index = value.IndexOf("'")
                value = value.Remove(index, 1)

            End While
            While value.Contains("#")

                index = value.IndexOf("#")
                value = value.Remove(index, 1)

            End While
            hpPicBeli.Text = value

            'If ds.Tables(0).Rows(0).Item("NAMA_PIC_GUDANG").ToString().Contains("'") Then

            '    index = ds.Tables(0).Rows(0).Item("NAMA_PIC_GUDANG").ToString().IndexOf("'")

            '    picgdg.Text = ds.Tables(0).Rows(0).Item("NAMA_PIC_GUDANG").ToString().Remove(index, 1)

            'Else

            '    picgdg.Text = ds.Tables(0).Rows(0).Item("NAMA_PIC_GUDANG").ToString()

            'End If

            value = ds.Tables(0).Rows(0).Item("NAMA_PIC_GUDANG").ToString()
            While value.Contains("'")

                index = value.IndexOf("'")
                value = value.Remove(index, 1)

            End While
            While value.Contains("#")

                index = value.IndexOf("#")
                value = value.Remove(index, 1)

            End While
            picgdg.Text = value

            'If ds.Tables(0).Rows(0).Item("NO_HP_PIC_GUDANG").ToString().Contains("'") Then

            '    index = ds.Tables(0).Rows(0).Item("NO_HP_PIC_GUDANG").ToString().IndexOf("'")

            '    hpPicGdg.Text = ds.Tables(0).Rows(0).Item("NO_HP_PIC_GUDANG").ToString().Remove(index, 1)

            'Else

            '    hpPicGdg.Text = ds.Tables(0).Rows(0).Item("NO_HP_PIC_GUDANG").ToString()

            'End If

            value = ds.Tables(0).Rows(0).Item("NO_HP_PIC_GUDANG").ToString()
            While value.Contains("'")

                index = value.IndexOf("'")
                value = value.Remove(index, 1)

            End While
            While value.Contains("#")

                index = value.IndexOf("#")
                value = value.Remove(index, 1)

            End While
            hpPicGdg.Text = value

            'If ds.Tables(0).Rows(0).Item("NAMA_PIC_APJ").ToString().Contains("'") Then

            '    index = ds.Tables(0).Rows(0).Item("NAMA_PIC_APJ").ToString().IndexOf("'")

            '    picApj.Text = ds.Tables(0).Rows(0).Item("NAMA_PIC_APJ").ToString().Remove(index, 1)

            'Else

            '    picApj.Text = ds.Tables(0).Rows(0).Item("NAMA_PIC_APJ").ToString()

            'End If

            value = ds.Tables(0).Rows(0).Item("NAMA_PIC_APJ").ToString()
            While value.Contains("'")

                index = value.IndexOf("'")
                value = value.Remove(index, 1)

            End While
            While value.Contains("#")

                index = value.IndexOf("#")
                value = value.Remove(index, 1)

            End While
            picApj.Text = value

            'If ds.Tables(0).Rows(0).Item("NO_HP_PIC_APJ").ToString().Contains("'") Then

            '    index = ds.Tables(0).Rows(0).Item("NO_HP_PIC_APJ").ToString().IndexOf("'")

            '    hpPicApj.Text = ds.Tables(0).Rows(0).Item("NO_HP_PIC_APJ").ToString().Remove(index, 1)

            'Else

            '    hpPicApj.Text = ds.Tables(0).Rows(0).Item("NO_HP_PIC_APJ").ToString()

            'End If

            value = ds.Tables(0).Rows(0).Item("NO_HP_PIC_APJ").ToString()
            While value.Contains("'")

                index = value.IndexOf("'")
                value = value.Remove(index, 1)

            End While
            While value.Contains("#")

                index = value.IndexOf("#")
                value = value.Remove(index, 1)

            End While
            hpPicApj.Text = value

            'If ds.Tables(0).Rows(0).Item("NAMA_PIC_TAX").ToString().Contains("'") Then

            '    index = ds.Tables(0).Rows(0).Item("NAMA_PIC_TAX").ToString().IndexOf("'")

            '    picTax.Text = ds.Tables(0).Rows(0).Item("NAMA_PIC_TAX").ToString().Remove(index, 1)

            'Else

            '    picTax.Text = ds.Tables(0).Rows(0).Item("NAMA_PIC_TAX").ToString()

            'End If

            value = ds.Tables(0).Rows(0).Item("NAMA_PIC_TAX").ToString()
            While value.Contains("'")

                index = value.IndexOf("'")
                value = value.Remove(index, 1)

            End While
            While value.Contains("#")

                index = value.IndexOf("#")
                value = value.Remove(index, 1)

            End While
            picTax.Text = value

            'If ds.Tables(0).Rows(0).Item("NO_HP_PIC_TAX").ToString().Contains("'") Then

            '    index = ds.Tables(0).Rows(0).Item("NO_HP_PIC_TAX").ToString().IndexOf("'")

            '    hpPicTax.Text = ds.Tables(0).Rows(0).Item("NO_HP_PIC_TAX").ToString().Remove(index, 1)

            'Else

            '    hpPicTax.Text = ds.Tables(0).Rows(0).Item("NO_HP_PIC_TAX").ToString()

            'End If

            value = ds.Tables(0).Rows(0).Item("NO_HP_PIC_TAX").ToString()
            While value.Contains("'")

                index = value.IndexOf("'")
                value = value.Remove(index, 1)

            End While
            While value.Contains("#")

                index = value.IndexOf("#")
                value = value.Remove(index, 1)

            End While
            hpPicTax.Text = value

            'If ds.Tables(0).Rows(0).Item("PROSEDUR_PEMBAYARAN").ToString().Contains("'") Then

            '    index = ds.Tables(0).Rows(0).Item("PROSEDUR_PEMBAYARAN").ToString().IndexOf("'")

            '    prsdrByr.Text = ds.Tables(0).Rows(0).Item("PROSEDUR_PEMBAYARAN").ToString().Remove(index, 1)

            'Else

            '    prsdrByr.Text = ds.Tables(0).Rows(0).Item("PROSEDUR_PEMBAYARAN").ToString()

            'End If

            value = ds.Tables(0).Rows(0).Item("PROSEDUR_PEMBAYARAN").ToString()
            While value.Contains("'")

                index = value.IndexOf("'")
                value = value.Remove(index, 1)

            End While
            While value.Contains("#")

                index = value.IndexOf("#")
                value = value.Remove(index, 1)

            End While
            prsdrByr.Text = value

            'If ds.Tables(0).Rows(0).Item("CARA_PEMBAYARAN").ToString().Contains("'") Then

            '    index = ds.Tables(0).Rows(0).Item("CARA_PEMBAYARAN").ToString().IndexOf("'")

            '    crByr.Text = ds.Tables(0).Rows(0).Item("CARA_PEMBAYARAN").ToString().Remove(index, 1)

            'Else

            '    crByr.Text = ds.Tables(0).Rows(0).Item("CARA_PEMBAYARAN").ToString()

            'End If

            value = ds.Tables(0).Rows(0).Item("CARA_PEMBAYARAN").ToString()
            While value.Contains("'")

                index = value.IndexOf("'")
                value = value.Remove(index, 1)

            End While
            While value.Contains("#")

                index = value.IndexOf("#")
                value = value.Remove(index, 1)

            End While
            crByr.Text = value

            orgCode.Text = ds.Tables(0).Rows(0).Item("ORG_CODE").ToString()
            dtime = ds.Tables(0).Rows(0).Item("SUBMIT_DATE").ToString()
            subDate.Text = dtime.Date.ToString("dd-MMM-yyyy")
            inFrom.Text = ds.Tables(0).Rows(0).Item("INPUT_FROM").ToString()

            If ds2.Tables(0).Rows.Count > 0 Then
                dtime2 = ds2.Tables(0).Rows(0).Item("SUBMIT_DATE").ToString()
                inFromHis.Text = ds2.Tables(0).Rows(0).Item("INPUT_FROM_HISTORY").ToString()
                HisDate.Text = dtime2.Date.ToString("dd-MMM-yyyy")
            End If





            Dim script As String = "window.onload = function() { showDiv2(); };"

            ClientScript.RegisterStartupScript(Me.GetType(), "showDiv2", script, True)
        Else
            Dim script As String = "window.onload = function() { alrtNoData(); };"

            ClientScript.RegisterStartupScript(Me.GetType(), "alrtNoData", script, True)
        End If

    End Sub

    Public Function getDetails(ByVal id As String) As DataSet

        Dim strSQL As String
        strSQL = "SELECT * FROM APPROVAL_DATA_CUSTOMER where STATUS = 'DIPROSES' and ID =" + id

        Dim ds As DataSet = createDS(strSQL)
        Return ds

    End Function

    Public Function getDetails2(ByVal id As String) As DataSet

        Dim strSQL As String
        strSQL = "SELECT top 1 * FROM APPROVAL_DATA_CUSTOMER where STATUS = 'DITERIMA' and customer_number =" + id + " order by submit_date desc"

        Dim ds As DataSet = createDS(strSQL)
        Return ds

    End Function

    'Approve User////////////////////////////////////////////////////////////////////////////////



    Protected Sub approveUser(sender As Object, e As EventArgs) Handles btnTerima.Click

        Dim strSQL As String
        Dim res As String
        Dim resOra As String
        Dim ds As DataSet

        ds = wsICE.getDataIceApproval(shipToId.Text)

        If ds.Tables(0).Rows.Count > 0 Then

            resOra = wsICE.IceApprovalUpdate(nmPmlk.Text, noHpPmlk.Text, picByr.Text, hpPicByr.Text, picBeli.Text, hpPicBeli.Text, picgdg.Text, hpPicGdg.Text, picApj.Text, hpPicApj.Text, picTax.Text, hpPicTax.Text, shipToId.Text, inFrom.Text, prsdrByr.Text, crByr.Text)

        Else

            'Dim org As String = cusName.Text
            'Dim len As Int16 = org.Length
            'Dim index As Int16 = org.IndexOf("-")

            'org = org.Remove(index, len - 3)

            resOra = wsICE.IceApprovalInsert(orgCode.Text, cusNum.Text, nmPmlk.Text, noHpPmlk.Text, picByr.Text, hpPicByr.Text, picBeli.Text, hpPicBeli.Text, picgdg.Text, hpPicGdg.Text, picApj.Text, hpPicApj.Text, picTax.Text, hpPicTax.Text, shipToId.Text, subDate.Text, inFrom.Text, prsdrByr.Text, crByr.Text)

        End If

        'strSQL = "UPDATE APPROVAL_DATA_CUSTOMER SET APPROVED_BY = '" + Session("sUsername") + "', VALIDATION_DATE = GETDATE(), STATUS = 'DITERIMA' where ID = " + ID.Text

        'res = ExecuteDataSQL(strSQL)

        'strSQL = "UPDATE xxepm.epm_master_cust_wa set NAMA_PEMILIK = '" + nmPmlk.Text + "', NO_HP_1 = '" + noHpPmlk.Text + "', NAMA_PIC_PEMBAYARAN = '" + picByr.Text + "', NO_HP_2 = '" + hpPicByr.Text + "',"
        'strSQL = strSQL + " NAMA_PIC_PEMBELIAN = '" + picBeli.Text + "', NO_HP_3 = '" + hpPicBeli.Text + "', NAMA_PIC_GUDANG = '" + picgdg.Text + "', NO_HP_4 = '" + hpPicGdg.Text + "', NAMA_PIC_APJ = '" + picApj.Text + "',"
        'strSQL = strSQL + " NO_HP_5 = '" + hpPicApj.Text + "', NAMA_PIC_TAX = '" + picTax.Text + "', NO_HP_6 = '" + hpPicTax.Text + "' where SHIP_TO_ID = " + shipToId.Text



        'resOra = wsICE.IceApprovalUpdate(nmPmlk.Text, noHpPmlk.Text, picByr.Text, hpPicByr.Text, picBeli.Text, hpPicBeli.Text, picgdg.Text, hpPicGdg.Text, picApj.Text, hpPicApj.Text, picTax.Text, hpPicTax.Text, shipToId.Text)

        If resOra = "1" Then

            strSQL = "UPDATE APPROVAL_DATA_CUSTOMER SET INPUT_FROM_HISTORY = '" + inFrom.Text + "' , APPROVED_BY = '" + Session("sUsername") + "', VALIDATION_DATE = GETDATE(), STATUS = 'DITERIMA' where ID = " + ID.Text

            res = ExecuteDataSQL(strSQL)

            strSQL = "UPDATE APPROVAL_DATA_CUSTOMER SET INPUT_FROM_HISTORY = '" + inFrom.Text + "'  where customer_number = " + cusNum.Text

            res = ExecuteDataSQL(strSQL)

            Dim script As String = "window.onload = function() { alrtApproveDataSucceed(); };"

            ClientScript.RegisterStartupScript(Me.GetType(), "alrtApproveDataSucceed", script, True)

        Else

            Dim script As String = "window.onload = function() { alrtApproveDataFailed(); };"

            ClientScript.RegisterStartupScript(Me.GetType(), "alrtApproveDataFailed", script, True)

        End If

    End Sub

    Function ExecuteDataSQL(ByVal strSQL As String) As Integer

        If oConnDB.State = ConnectionState.Open Then
            oConnDB.Close()
        End If
        oConnDB.Open()

        Dim oCmd = New SqlCommand
        Dim trans = oConnDB.BeginTransaction()

        Try
            With oCmd
                .Transaction = trans
                .Connection = oConnDB
                .CommandType = CommandType.Text
                .CommandText = strSQL
                .ExecuteNonQuery()
                trans.Commit()
            End With
            Return 1
        Catch ex As Exception
            trans.Rollback()
            Return 0
        End Try

        If oConnDB.State = ConnectionState.Open Then
            oConnDB.Close()
        End If
    End Function

    'Tolak User///////////////////////////////////////////////////////////////////////////
    Protected Sub tolakUser(sender As Object, e As EventArgs) Handles btnTolak.Click

        If txtAlasan.Text.Length() > 4 Then
            Dim strSQL As String
            Dim res As Int32
            strSQL = "UPDATE APPROVAL_DATA_CUSTOMER SET APPROVED_BY = '" + Session("sUsername") + "', VALIDATION_DATE = GETDATE(), STATUS = 'DITOLAK', NOTES = '" + txtAlasan.Text + "' where ID = " + ID.Text

            res = ExecuteDataSQL(strSQL)
            'Dim result As String

            'result = updateDataApproval()

            If res = "1" Then

                Dim script As String = "window.onload = function() { alrtApproveDataSucceed(); };"

                ClientScript.RegisterStartupScript(Me.GetType(), "alrtApproveDataSucceed", script, True)

            Else

                Dim script As String = "window.onload = function() { alrtApproveDataFailed(); };"

                ClientScript.RegisterStartupScript(Me.GetType(), "alrtApproveDataFailed", script, True)

            End If

        Else

            Dim script As String = "window.onload = function() { alertTolakValid(); };"

            ClientScript.RegisterStartupScript(Me.GetType(), "alertTolakValid", script, True)

        End If


    End Sub

    Public Function updateDataApproval() As String

        Dim strsql As String

        strsql = "UPDATE xxepm.epm_master_cust_wa set NAMA_PEMILIK = '" + nmPmlk.Text + "', NO_HP_1 = '" + noHpPmlk.Text + "', NAMA_PIC_PEMBAYARAN = '" + picByr.Text + "', NO_HP_2 = '" + hpPicByr.Text + "',"
        strsql = strsql + " NAMA_PIC_PEMBELIAN = '" + picBeli.Text + "', NO_HP_3 = '" + hpPicBeli.Text + "', NAMA_PIC_GUDANG = '" + picgdg.Text + "', NO_HP_4 = '" + hpPicGdg.Text + "', NAMA_PIC_APJ = '" + picApj.Text + "',"
        strsql = strsql + " NO_HP_5 = '" + hpPicApj.Text + "', NAMA_PIC_TAX = '" + picTax.Text + "', NO_HP_6 = '" + hpPicTax.Text + "' where SHIP_TO_ID = " + shipToId.Text

        'Try

        '    ExecuteDataReader(strsql)

        '    Return "Success"

        'Catch ex As Exception

        '    Return ex.Message.ToString()

        'End Try

        Dim ocmd As New OracleCommand
        ocmd.CommandType = CommandType.Text
        Dim oconn As New OracleConnection
        oconn.ConnectionString = "DATA SOURCE=TEST1;USER ID=APPS;Password=appskoq"

        oconn.Open()
        ocmd.Connection = oconn

        ocmd.CommandText = strsql

        Try
            ocmd.ExecuteNonQuery()
        Catch ex As Exception

        End Try

        'OpenOracleConnection()

        'Dim scnnNW As New OracleConnection(oraDB)

        '' A SqlCommand object is used to execute the SQL commands.
        'Dim oraCmd As New OracleCommand(strSQL, oraConn)
        'oraCmd.CommandTimeout = 600000

        '' A SqlDataAdapter uses the SqlCommand object to fill a DataSet.
        'Dim oraDA As New OracleDataAdapter(oraCmd)

        '' Create and Fill a new DataSet.
        'Dim oraDS As New Data.DataTable
        'oraDA.Fill(oraDS)
        'CloseOracleConnection()



        'Return oraDS
    End Function

    'Sub ExecuteDataReader(ByVal strSQL As String)

    '    OpenOracleConnection()

    '    Dim oraCmd As New OracleCommand
    '    With oraCmd
    '        .Connection = oraConn
    '        .CommandType = CommandType.Text
    '        .CommandText = strSQL
    '        oraDR = .ExecuteReader
    '    End With

    '    CloseOracleConnection()

    'End Sub

    'Sub nonQueryExecute(ByVal strsql As String)

    '    Dim ocmd As New OracleCommand
    '    ocmd.CommandType = CommandType.Text
    '    Dim oconn As New OracleConnection
    '    oconn.ConnectionString = "DATA SOURCE=TEST1;USER ID=APPS;Password=appskoq"

    '    oconn.Open()
    '    ocmd.Connection = oconn

    '    ocmd.CommandText = strsql

    '    Try
    '        ocmd.ExecuteNonQuery()
    '    Catch ex As Exception

    '    End Try

    'End Sub
End Class
