﻿Imports System.Data
Imports System.Data.SqlClient


Partial Class region
    Inherits System.Web.UI.Page

    Public Shared ConnStrAudit As String = ConfigurationManager.ConnectionStrings("SQLICE").ConnectionString
    Public Shared cmd As New SqlCommand

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        If Not Page.IsPostBack Then
            tampilTable()
        End If
    End Sub
    Protected Sub tampilTable()
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                Dim da As New SqlDataAdapter("select * from MSTREGION order by kode", con)
                Dim dt As New DataTable
                da.Fill(dt)
                GridView1.DataSource = dt
                GridView1.DataBind()
                GridView1.HeaderRow.TableSection = TableRowSection.TableHeader
                con.Close()
            End Using
        Catch ex As Exception

        End Try

    End Sub
    Protected Sub GridView1_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        tampilTable()
    End Sub
    Protected Sub clearData_Click(sender As Object, e As EventArgs)
        Response.Redirect("region.aspx")
    End Sub
    Protected Sub insertData_Click(sender As Object, e As EventArgs)
        Dim sqlstr As String = "insert into MSTREGION(kode, region, created_by, creation_date, last_update_by, last_update_date) values('" & txtKode.Text & "', '" & txtRegion.Text & "', '" & Session("sUsername") & "', current_timestamp, '" & Session("sUsername") & "', current_timestamp)"
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                cmd = New SqlCommand(sqlstr, con)
                cmd.ExecuteNonQuery()
                con.Close()
            End Using

            div_lbl.Visible = True
            lbl_msg.Text = "Insert Successful!"
            tampilTable()
        Catch ex As Exception
            div_lbl.Visible = True
            lbl_msg.Text = ex.Message.ToString
        End Try
    End Sub
    Protected Sub updateData_Click(sender As Object, e As EventArgs)
        Dim sqlstr As String = "update MSTREGION set kode = '" & txtKode.Text & "', region = '" & txtRegion.Text & "', last_update_by = '" & Session("sUsername") & "', last_update_date = current_timestamp where kode='" & txtKode.Text & "'"
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                cmd = New SqlCommand(sqlstr, con)
                cmd.ExecuteNonQuery()
                con.Close()
            End Using

            div_lbl.Visible = True
            lbl_msg.Text = "Update Successful!"
            tampilTable()
        Catch ex As Exception
            div_lbl.Visible = True
            lbl_msg.Text = ex.Message.ToString
        End Try
    End Sub
    Protected Sub deleteData_Click(sender As Object, e As EventArgs)
        Dim sqlstr As String = "delete from MSTREGION where kode = '" & txtKode.Text & "'"
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                cmd = New SqlCommand(sqlstr, con)
                cmd.ExecuteNonQuery()
                con.Close()
            End Using

            div_lbl.Visible = True
            lbl_msg.Text = "Delete Successful!"
            tampilTable()
        Catch ex As Exception
            div_lbl.Visible = True
            lbl_msg.Text = ex.Message.ToString
        End Try
    End Sub
    Protected Sub GridView1_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GridView1.RowCommand

    End Sub
End Class
