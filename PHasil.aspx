﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="PHasil.aspx.vb" Inherits="PHasil" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <br />



    <asp:Label ID="lblHead" runat="server" Text="Master Pilihan Penilaian"
        style="font-size: 20px;"
        ></asp:Label>

    
<script  type="text/javascript">
    $(document).ready(function () {
        var table = $('#mytable').DataTable({
            scrollY: "300px",
            scrollX: "800px",
            scrollCollapse: true,
            paging: true,
            sScrollX: "100%", 
            bProcessing: true, // shows 'processing' label
            bStateSave: true, // presumably saves state for reloads
            fixedColumns: {
                leftColumns: 1
            }
        });
    });  

    function btnClick(args) {
        window.location.href = "r.aspx?a=3&b=" + args;
    }

</script>

<div style="margin:30px;">

    <button type='button' class='button2xx' data-book-id='|' data-target='#formModalNew' data-toggle='modal'>Add New</button>
    <asp:PlaceHolder ID="myPlaceH" runat="server"></asp:PlaceHolder>

</div>

    <div id="formModalEdit" tabindex="-1" role="dialog" aria-hidden="true" style="display: none ">
        <table style="position: fixed; top:200px ; left:200px; background-color: ButtonFace; border: 5px; border-radius: 15px; padding: 20px; box-shadow: 5px 5px 5px gray;">
            <tr><td>
           <label for="TxtKode">Kode &nbsp;&nbsp;</label></td><td>
           <asp:TextBox ID="TxtKode" runat="server" MaxLength="10" Width="100px" ClientIDMode="Static" style="background-color: lightgray"></asp:TextBox></td>
            </tr><tr><td>
           <label for="TxtKeterangan">Keterangan &nbsp;&nbsp;</label></td><td>
           <asp:TextBox ID="TxtKet" runat="server" MaxLength="100" Width="200px" ClientIDMode="Static"></asp:TextBox></td>
           </tr><tr><td>
           <label for="TxtPengali">Pengali &nbsp;&nbsp;</label></td><td>
           <asp:TextBox ID="TxtPengali" runat="server" MaxLength="10" Width="50px" ClientIDMode="Static"></asp:TextBox></td>
           </tr>
           <tr><td colspan="2" style="text-align: center;">&nbsp;
               </td>
           </tr>
           <tr><td colspan="2" style="text-align: center;">
               <asp:Button ID="btnUpdate" CssClass="btn1" runat="server" text="Update"/> &nbsp;&nbsp;
               <asp:Button ID="btnCancel1" CssClass="btn1" runat="server" text="Cancel"/>
               </td>
           </tr>
       </table>
    </div>

    <div id="formModalDelete" tabindex="-1" role="dialog" aria-hidden="true"style="display: none ">
        <table style="position: fixed; top:200px ; left:200px; background-color: ButtonFace; border: 5px; border-radius: 15px; padding: 20px; box-shadow: 5px 5px 5px gray;">
            <tr><td>
           <label for="TxtKode2">Kode &nbsp;&nbsp;</label></td><td>
           <asp:TextBox ID="TxtKode2"  runat="server" MaxLength="10" Width="100px" ClientIDMode="Static" style="background-color: lightgray"></asp:TextBox></td>
            </tr><tr><td>
           <label for="TxtKet2">Keterangan &nbsp;&nbsp;</label></td><td>
           <asp:TextBox ID="TxtKet2" runat="server" MaxLength="100" Width="200px" ClientIDMode="Static" style="background-color: lightgray"></asp:TextBox></td>
            </tr><tr><td>
           <label for="TxtPengali2">Keterangan &nbsp;&nbsp;</label></td><td>
           <asp:TextBox ID="TxtPengali2" runat="server" MaxLength="10" Width="50px" ClientIDMode="Static" style="background-color: lightgray"></asp:TextBox></td>
           </tr>

           <tr><td colspan="2" style="text-align: center;">&nbsp;
               </td>
           </tr>
           <tr><td colspan="2" style="text-align: center;">
               <asp:Button ID="btnDelete" CssClass="btn1" runat="server" text="Delete"/> &nbsp;&nbsp;
               <asp:Button ID="btnCancel2" CssClass="btn1" runat="server" text="Cancel"/>
               </td>
           </tr>
       </table>
    </div>

    <div id="formModalNew" tabindex="-1" role="dialog" aria-hidden="true"style="display: none ">
        <table style="position: fixed; top:200px ; left:200px; background-color: ButtonFace; border: 5px; border-radius: 15px; padding: 20px; box-shadow: 5px 5px 5px gray;">
            <tr><td>
           <label for="TxtKode3">Kode &nbsp;&nbsp;</label></td><td>
           <asp:TextBox ID="TxtKode3" runat="server" MaxLength="10" Width="100px" ClientIDMode="Static"></asp:TextBox></td>
            </tr><tr><td>
           <label for="TxtKet3">Keterangan &nbsp;&nbsp;</label></td><td>
           <asp:TextBox ID="TxtKet3"  runat="server" MaxLength="100" Width="200px" ClientIDMode="Static"></asp:TextBox></td>
            </tr><tr><td>
           <label for="TxtPengali3">Keterangan &nbsp;&nbsp;</label></td><td>
           <asp:TextBox ID="TxtPengali3"  runat="server" MaxLength="10" Width="50px" ClientIDMode="Static"></asp:TextBox></td>
           </tr>

           <tr><td colspan="2" style="text-align: center;">&nbsp;
               </td>
           </tr>
           <tr><td colspan="2" style="text-align: center;">
               <asp:Button ID="btnInsert" CssClass="btn1" runat="server" text="Insert"/> &nbsp;&nbsp;
               <asp:Button ID="btnCancel3" CssClass="btn1" runat="server" text="Cancel"/>
               </td>
           </tr>
       </table>
    </div>

    <script type="text/javascript">

        $(function () {
            var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "personal";
            $('#Tabs a[href="#' + tabName + '"]').tab('show');
            $("#Tabs a").click(function () {
                $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
            });
        });

        $('#formModalEdit').on('show.bs.modal', function (e) {
            var bookId = $(e.relatedTarget).data('book-id');
            var arrString = []

            arrString = bookId.split("|")

            $(e.currentTarget).find("#<%=TxtKode.ClientID%>").val(arrString[0]);
            $(e.currentTarget).find("#<%=TxtKet.ClientID%>").val(arrString[1]);
            $(e.currentTarget).find("#<%=TxtPengali.ClientID%>").val(arrString[2]);
            
            document.getElementById("TxtKode").onkeydown = function () { return false; }

        });

        $('#formModalDelete').on('show.bs.modal', function (e) {
            var bookId = $(e.relatedTarget).data('book-id');
            var arrString = []

            arrString = bookId.split("|")

            $(e.currentTarget).find("#<%=TxtKode2.ClientID%>").val(arrString[0]);
            $(e.currentTarget).find("#<%=TxtKet2.ClientID%>").val(arrString[1]);
            $(e.currentTarget).find("#<%=TxtPengali2.ClientID%>").val(arrString[2]);


            document.getElementById("TxtKode2").onkeydown = function () { return false; }
            document.getElementById("TxtKet2").onkeydown = function () { return false; }
            document.getElementById("TxtPengali2").onkeydown = function () { return false; }

        });

        $('#formModalNew').on('show.bs.modal', function (e) {
            var bookId = $(e.relatedTarget).data('book-id');
            var arrString = []

            arrString = bookId.split("|")

            $(e.currentTarget).find("#<%=TxtKode3.ClientID%>").val(arrString[0]);
            $(e.currentTarget).find("#<%=TxtKet3.ClientID%>").val(arrString[1]);
            $(e.currentTarget).find("#<%=TxtPengali3.ClientID%>").val(arrString[1]);

        });

    </script>

    <asp:Label ID="infoerror" runat="server" style="color: red" ></asp:Label>

</asp:Content>


