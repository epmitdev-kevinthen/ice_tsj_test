﻿
Partial Class brwMaster
    Inherits System.Web.UI.Page
    Public vPN As String
    Public vP As String

    Private Sub brwMaster_Init(sender As Object, e As EventArgs) Handles Me.Init
        vPN = Request.QueryString("pn")
        vP = Request.QueryString("p")

        If vPN = "" Then
            SQLData1.SelectCommand = "select kode,kelperiksa from MSTKELPERIKSA order by kode"
        Else
            Dim vSp = Split(vPN, "-")
            SQLData1.SelectCommand = "select subkode as kode,subkelperiksa as kelperiksa from MSTSUBKELPERIKSA where kode='" & vSp(0) & "' order by subkode"
        End If
            txtCommand.Attributes.Add("onfocus", "fncSelect(" & vP & ");")
    End Sub

    Private Sub myGrid_SelectedIndexChanged(sender As Object, e As EventArgs) Handles myGrid.SelectedIndexChanged
        Dim vSp = Split(vPN, "-")

        txtCommand1.Text = myGrid.SelectedDataKey.Item(1).ToString()
        If vPN = "" Then
            txtCommand.Text = myGrid.SelectedDataKey.Item(0).ToString()
        Else
            txtCommand.Text = vSp(0) & "-" & myGrid.SelectedDataKey.Item(0).ToString()
        End If
        txtCommand.Focus()
    End Sub
End Class
