﻿Imports System.IO

Partial Class frmBrowseFile
    Inherits System.Web.UI.Page



    Private Sub cmdUploadFile_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdUploadFile.ServerClick


        If readFile.PostedFile Is Nothing Then
            lblShowMessage.Text = "You must specify file to upload!"
        Else


            Try


                Dim strExt As String = Path.GetExtension(Me.readFile.PostedFile.FileName)

                If strExt.ToLower() = ".doc" Or strExt.ToLower() = ".docx" Or strExt.ToLower() = ".xls" Or strExt.ToLower() = ".xlsx" Or
                    strExt.ToLower() = ".ppt" Or strExt.ToLower() = ".pptx" Or
                    strExt.ToLower() = ".jpg" Or strExt.ToLower() = ".jpeg" Or
                    strExt.ToLower() = ".pdf" Or strExt.ToLower() = ".png" Or
                    strExt.ToLower() = ".bmp" Or strExt.ToLower() = ".txt" Or
                    strExt.ToLower() = ".giff" Or strExt.ToLower() = ".rar" Or strExt.ToLower() = ".zip" Then

                    Dim savedFile As String
                    savedFile = Path.GetFileName(Me.readFile.PostedFile.FileName)

                    Dim mydt As String = Format(Now.Date, "ddmmyy") & Now.Hour & Now.Minute & Now.Second & "_"

                    Dim MyPath As String = "C:\My Upload Data\ICE_TSJ\"  'Server.MapPath("~/") & "imageprofile\"



                    readFile.PostedFile.SaveAs(MyPath & mydt & savedFile)
                    lblShowMessage.Text = "File Uploaded Successfully"

                    TextPassing.Text = Me.readFile.PostedFile.FileName
                    TextPassing.Text = mydt & Mid(TextPassing.Text, TextPassing.Text.LastIndexOf("\") + 2)

                    'ButtonPassing.Attributes.Add("OnClick", "")
                    TextAction.Attributes.Add("OnFocus", "fncGetFile()")
                    TextAction.Focus()

                Else

                    lblShowMessage.Text = "Format file tidak sesuai"

                End If

            Catch exp As Exception
                lblShowMessage.Text = exp.Message

            End Try

        End If


    End Sub


End Class
