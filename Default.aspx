﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <title>Internal Control Effectiveness</title>

    <link rel="icon" href="image/Logoplikasi_Layer 2.png">

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <!--        <link rel="stylesheet" href="assets/css/bootstrap-theme.min.css">-->


        <!--For Plugins external css-->

        <link rel="stylesheet" href="assets/css/plugins.css" />
        <link rel="stylesheet" href="assets/css/lato-webfont.css" />
        <link rel="stylesheet" href="assets/css/magnific-popup.css">

        <!--Theme custom css -->
        <link rel="stylesheet" href="assets/css/style.css">

        <!--Theme Responsive css-->
        <link rel="stylesheet" href="assets/css/responsive.css" />

        <script src="assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>



    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
		
		<div class='preloader'><div class='loaded'>&nbsp;</div></div>
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <%--<a class="navbar-brand" href="#"><img src="assets/images/logo1.png" alt="" /></a>--%>
                    <!--
                    <a class="navbar-brand" href="#"><img src="image/together.png" alt="" style="width: 110px ;height: 60px" /></a>
                    -->
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                    <ul class="nav navbar-nav navbar-right">
                        <li class="active"><a href="#home">Beranda</a></li>
                        <li><a href="#features">Fitur</a></li>

                        <li><a href="#gallery">Gallery</a></li>
<!--
                            <li><a href="#video">Video</a></li>
                        <li><a href="#price">Prices</a></li>
                        <li><a href="#testimonial">Testimoni</a></li>
                        <li><a href="#download">Download</a></li>
-->

                        <li><a href="#masuk">Masuk</a></li>

                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>

        <!--Home page style-->
        <header id="home" class="home">

            <div class="container-fluid">

                <div class="row">

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="home-content sections" style="padding-top: 0px;">
                            <%--<img src="assets/images/logo1.png" alt="" />--%>
                            
                            <h1><span style="color: navajowhite; font-size: 90px;">Internal Control Effectiveness</span></h1>
                            <br />
                            <h1><span style="color: blue; font-size: smaller">PT. Tri Sapta Jaya</span></h1>
                            
                            <div class="row">
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <p style="color: White">Sistem untuk melakukan kontrol proses-proses kerja yang sudah dilakukan, apakah sudah sesuai dengan standard yang sudah ditetapkan guna menghindari timbulnya penurunan efektifitas proses kerja, kualitas, kerugian, penyimpangan dan lainnya.</p>
                                </div>
                            </div>


                            <!--                            <a target="_blank" href="#"><button class="btn btn-default btn-lg">Download</button></a>
                            <a target="_blank" href="#"><button class="btn btn-primary btn-lg">Learn More</button></a>
-->
<!--                            <div class="available">
                                <p>
                                    Available on :
                                    <a href="#"><i class="fa fa-apple"></i></a> 
                                    <a href="#"><i class="fa fa-android"></i></a>
                                </p>
                            </div>-->

                        </div>
                    </div>
                </div>

            </div>

        </header>

        <!-- Sections -->
        <section id="features" class="sections lightbg">
            <div class="container text-center">

                <div class="heading-content">
                    <h3>Rangkuman Fitur ICE</h3>
                    <h5>Beberapa fitur yang disediakan di ICE</h5>-->
                </div>

                <!-- Example row of columns -->
                <div class="row">

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="features-content">
                            <i class="fa fa-cloud"></i>
                            <h5>Web</h5>
                            <p>ICE dapat diakses melalui browser Firefox dan Chrome, gunakan versi terakhir pada browser</p>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="features-content">
                            <i class="fa fa-refresh"></i>
                            <h5>Flexible</h5>
                            <p>Template ICE yang dapat di generate secara fleksible dan di-regenerate melalui sistem</p>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="features-content">
                            <i class="fa fa-newspaper-o"></i>
                            <h5>Report</h5>
                            <p>Laporan disajikan dalam bentuk tabular yang dapat di export ke dalam file Excel</p>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="features-content">
                            <i class="fa fa-line-chart"></i>
                            <h5>Graph</h5>
                            <p>Untuk memudahkan analisa, data disajikan dalam bentuk grafik</p>
                        </div>
                    </div>

                </div>
            </div> <!-- /container -->       
        </section>

        <!-- Sections -->
        <section id="gallery" class="sections">
            <div class="container text-center">

                <div class="heading-content">
                    <h3>Gallery</h3>
                    <h5>Gallery implementasi ICE</h5>
                </div>

                <!-- Example row of columns -->
                <div class="row">



                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="gallery-item">
                            <div class="gallery">
                                <img src="assets/images/gallery/1.png" alt="" />
                                <h6>LIF ICE Project - Hedgehog</h6>

                                <div class="img-overlay"></div>

                                <div class="gallery-icon">
                                    <a class="gallery-img" href="assets/images/gallery/1.png ">View More</a>

                                </div>
                            </div>

                            <p>Ketua: Rustam T (IA), Anggota: Dede S (RBM), Anton B (CI), Hendro W (IA), Bambang BA (IT), Herman N (CHD), Novi K (IA), Hariyadi DP (IA), Stefanus ES (IT) dan Gomarus AI (HRD)</p>

                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="gallery-item">
                            <div class="gallery">
                                <img src="assets/images/gallery/2.png" alt="" />
                                <h6>Sosialisasi ICE - Cabang</h6>

                                <div class="img-overlay"></div>

                                <div class="gallery-icon">

                                    <a class="gallery-img" href="assets/images/gallery/2.png">View More</a>
                                </div>
                            </div>

                            <p>Sosialisasi ICE - Cabang tahap awal diadakan di Cabang Jakarta 1 (3 Mei 2017) dan Cabang Surabaya 2 (17 Mei 2017)</p>

                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="gallery-item">
                            <div class="gallery">
                                <img src="assets/images/gallery/3.png" alt="" />
                                <h6>Sosialisasi ICE - Pusat</h6>

                                <div class="img-overlay"></div>

                                <div class="gallery-icon">

                                    <a class="gallery-img" href="assets/images/gallery/3.png">View More</a>
                                </div>
                            </div>

                            <p>Sosialisasi ICE - Pusat tahap awal diadakan bersama Team Auditor (29 Mei 2017( dan Team HRGACI (13 Desember 2017)</p>

                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="gallery-item">
                            <div class="gallery">
                                <img src="assets/images/gallery/4.png" alt="" />
                                <h6>Sosialisasi ICE - Jabotabekdung</h6>

                                <div class="img-overlay"></div>

                                <div class="gallery-icon">
                                    <a class="gallery-img" href="assets/images/gallery/4.png">View More</a>
                                </div>

                            </div>

                            <p>Sosialisasi ICE - Jabotabekdung diadakan di Kantor EPM Pusat pada tanggal 8-10 Agustus 2017</p>

                        </div>
                    </div>



                </div>
            </div> <!-- /container -->       
        </section>

<!--
        <!-- Sections ->
        <section id="video" class="video">
            <div class="overlay sections">
                <div class="container text-center">
                    <!-- Example row of columns ->
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                            <div class="video-content">

                                <a href="http://www.youtube.com/embed/ee1_2gA8UZY?autoplay=1&amp;.)wmode=opaque&amp;.)fs=1" class="youtube-media"><i class="fa fa-play"></i></a>
                                <h1>Watch the best Technology in <span>Action</span></h1>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget nunc vitae tellus luctus ullamcorper. Nam porttitor ullamcorper felis at convallis. Aenean ornare vestibulum nisi fringilla lacinia. Nullam pulvinar sollicitudin velit id laoreet. Quisque non rhoncus sem.</p>

                            </div>
                        </div>
                    </div>
                </div> <!-- /container ->
            </div>		
        </section>

        <!-- Sections ->
        <section id="price" class="sections">
            <div class="container text-center">

                <div class="heading-content">
                    <h3>choose your price</h3>
                    <h5>summarise what your product is all about </h5>
                </div>

                <!-- Example row of columns ->
                <div class="row">

                    <div class="col-md-4 col-sm-12 col-xs-12">
                        <div class="price-category basic">
                            <h4>Basic Package</h4>
                            <h2>20$</h2>
                            <ul>
                                <li>Nullam suscipit vitae</li>
                                <li>Etiam faucibus</li>
                                <li>Vivamus viverra</li>

                            </ul>
                            <a href="#" class="btn-price">Purchase</a>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-12 col-xs-12">
                        <div class="price-category professional">
                            <h4>Professional Package</h4>
                            <h2>25$</h2>
                            <ul>
                                <li>Nullam suscipit vitae</li>
                                <li>Etiam faucibus</li>
                                <li>Vivamus viverra</li>
                                <li>Praesent pharetra</li>
                            </ul>
                            <a href="#" class="btn-price">Purchase</a>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-12 col-xs-12">
                        <div class="price-category advance">
                            <h4>Advanced Package</h4>
                            <h2>30$</h2>
                            <ul>
                                <li>Nullam suscipit vitae</li>
                                <li>Etiam faucibus</li>
                                <li>Vivamus viverra</li>

                            </ul>
                            <a href="#" class="btn-price">Purchase</a>
                        </div>
                    </div>

                </div>
            </div> <!-- /container ->       
        </section>
-->


        <!-- Sections ->
        <section id="testimonial" class="sections lightbg">
            <div class="container text-center">

                <div class="heading-content">
                    <h3>Testimoni</h3>
                </div>

                <!-- Example row of columns ->
                <div class="row">

                    <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">

                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="false">
                            <!-- Indicators ->
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                            </ol>

                            <!-- Wrapper for slides ->
                            <div class="carousel-inner" role="listbox">

                                <div class="item active">

                                    <div class="col-md-4">
                                        <div class="testimonial-photo">
                                            <img src="assets/images/gallery/1.png" alt="" />
                                        </div>
                                    </div>

                                    <div class="col-md-8">
                                        <div class="testimonial-content">
                                            <p>Proses registrasinya tidak memakan waktu lama...</p>
                                            <h6><strong>Apoteker <br />Outlet Apotik</strong></h6>
                                        </div>
                                    </div>

                                </div>

                                <div class="item">

                                    <div class="col-md-4">
                                        <div class="testimonial-photo">
                                            <img src="assets/images/gallery/2.png" alt="" />
                                        </div>
                                    </div>

                                    <div class="col-md-8">
                                        <div class="testimonial-content">
                                            <p> Cukup oke, keliatannya cukup canggih programnya...</p>
                                            <h6><strong>Buyer <br />Minimarket</strong></h6>
                                        </div>
                                    </div>

                                </div>

                                <div class="item">

                                    <div class="col-md-4">
                                        <div class="testimonial-photo">
                                            <img src="assets/images/gallery/4.png" alt="" />
                                        </div>
                                    </div>

                                    <div class="col-md-8">
                                        <div class="testimonial-content">
                                            <p> Hebat, proses registrasinya lancar...</p>
                                            <h6><strong>Pemilik <br />Toko Kelontong</strong></h6>
                                        </div>
                                    </div>

                                </div>


                            </div>


                        </div>


                    </div>

                </div>	


            </div> <!-- /container ->       
        </section>





        <!-- Sections ->
        <section id="download" class="section colorsbg download">
            <div class="container text-center">

                <!-- Example row of columns ->
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="download-content">
                            <h1>Download NOO Mobs versi Android</h1>
                            <button class="btn btn-primary">Download NOO Mobile</button>
                            &nbsp;&nbsp;&nbsp;
                            <button class="btn btn-primary">Download Approval Mobile</button>
                        </div>
                    </div>
                </div>
            </div> <!-- /container ->       
        </section>

-->


                <!-- Sections -->
        <section id="masuk" >

            
           <!--<div id="map"></div>-->

            <div id="test" style="height:450px" ></div>
            

            <div style="background-color: grey"> <!--class="container text-center">-->

                <!-- Example row of columns -->
                <div class="row">
                    <div class="col-md-4 col-md-offset-4 col-sm-12 col-xs-12">
                        <div class="contact-form-area">
                            <form id="form1" runat="server">
                                <%--<h3><img src="image/digital.png" width="106" height="62" /></h3>--%>	
                                <img src="assets/images/logo.png">
                                <div class="form-group">

                                    <input id="txtUserName" runat="server" type="text" class="form-control" placeholder="User Akses" />
                                </div>

                                <div class="form-group">
                                    <input id="txtPassword" runat="server" type="password" class="form-control" placeholder="Kata Sandi" />
                                </div>

                                <asp:Button id="BtnLogiin" runat="server"
                                     type="button" class="btn btn-primary contact-btn" text="MASUK" />
                            </form>
                        </div>	
                    </div>
                </div>
            </div> <!-- /container -->       
        </section>



        <div class="scroll-top">

            <div class="scrollup">
                <a href="#home"><i class="fa fa-angle-double-up"></i></a>
            </div>

        </div>




        <!--Footer-->
        <footer id="footer" class="footer colorsbg">
            <div class="container">
                <div class="row">

                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="copyright text-left">
                            <p>Hak Cipta (c) 2017 PT. Enseval Putera MegaTrading Tbk.</p>
                        </div>
                    </div>


                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="copyright text-right">
                            <p>Internal Audit & IT Department Collaboration | Admin : <a href="mailto:hendro.wibowo@enseval.com <Hendro Wibowo>">Audit</a> - <a href="mailto:bbayuaji@enseval.com <Bambang Bayu Aji>">System</a></p>
                        </div>
                    </div>

                </div>
            </div>
        </footer>


                <%--<img src="image/digital.png" alt="" style="position: absolute; top: 90px; left: 10px; width: 352px; height: 192px" />--%>


        <script src="assets/js/vendor/jquery-1.11.2.min.js"></script>
        <script src="assets/js/vendor/bootstrap.min.js"></script>


<!--        <script src="http://maps.google.com/maps/api/js"></script>-->
        <script src="assets/js/gmaps.min.js"></script>


        <script src="assets/js/jquery.magnific-popup.js"></script>

        <script src="assets/js/plugins.js"></script>
        <script src="assets/js/main.js"></script>

        <script>
            function nodatauser() {
                alert("Username atau Password salah!!!");
            }
        </script>
        

    </body>
</html>
