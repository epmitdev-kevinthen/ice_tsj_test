﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="brwfolder.aspx.vb" Inherits="brwfolder" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <title>Internal Control Effectiveness</title>

    <link rel="icon" href="image/Logoplikasi_Layer 2.png">


</head>
<body>
    Untuk membuka folder dokumen, copy link text dibawah ini dan gunakan pada browser Anda (Chrome,Mozilla,IE,dsb) :
    <p>
  <textarea style="width: 200px" class="js-copytextarea"><%= Session("myses") %></textarea>
</p>

<p>
  <button class="js-textareacopybtn">Copy & Close</button>
</p>
    <script type="text/javascript">
        var copyTextareaBtn = document.querySelector('.js-textareacopybtn');

copyTextareaBtn.addEventListener('click', function(event) {
  var copyTextarea = document.querySelector('.js-copytextarea');
  copyTextarea.select();

    try {

        //var myshell = new ActiveXObject("WScript.shell");
        //myshell.run("file://e-share/PublicRW/", 1, true);


    var successful = document.execCommand('copy');
    var msg = successful ? 'successful' : 'unsuccessful';
    console.log('Copying text command was ' + msg);
  } catch (err) {
    console.log('Oops, unable to copy');
  }

    window.close();

});
    </script>

</body>
</html>
