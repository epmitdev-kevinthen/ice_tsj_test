﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class master_sub_activity
    Inherits System.Web.UI.Page

    Public Shared ConnStrAudit As String = ConfigurationManager.ConnectionStrings("SQLICE").ConnectionString
    Public Shared cmd As New SqlCommand

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        If Not Page.IsPostBack Then
            loadDDLMainActivity()
            tampilTable()
        End If
    End Sub
    Sub loadDDLMainActivity()
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                Dim da As New SqlDataAdapter("select * from MSTMAIN_ACTIVITY order by kode", con)
                Dim dt As New DataTable
                da.Fill(dt)
                If dt.Rows.Count > 0 Then
                    For i = 0 To dt.Rows.Count - 1
                        ddlMainActivity.Items.Add(New ListItem(dt.Rows(i).Item("kode") & "-" & dt.Rows(i).Item("main_activity"), dt.Rows(i).Item("kode")))
                    Next
                End If
                con.Close()
            End Using
        Catch ex As Exception
            lbl_msg.Text = ex.Message.ToString
        End Try
    End Sub
    Protected Sub tampilTable()
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                Dim da As New SqlDataAdapter("select * from MSTSUB_ACTIVITY order by kode_main_activity, kode_sub_activity", con)
                Dim dt As New DataTable
                da.Fill(dt)
                GridView1.DataSource = dt
                GridView1.DataBind()
                GridView1.HeaderRow.TableSection = TableRowSection.TableHeader
                con.Close()
            End Using
        Catch ex As Exception
            lbl_msg.Text = ex.Message.ToString
        End Try

    End Sub
    Protected Sub GridView1_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        tampilTable()
    End Sub
    Protected Sub clearData_Click(sender As Object, e As EventArgs)
        Response.Redirect("master_sub_activity.aspx")
    End Sub
    Protected Sub insertData_Click(sender As Object, e As EventArgs)
        Dim sqlstr As String = " INSERT INTO [dbo].[MSTSUB_ACTIVITY] ([kode_main_activity] ,[kode_sub_activity] ,[sub_activity] ,[created_by] ,[creation_date] ,[last_update_by] ,[last_update_date]) VALUES ('" & ddlMainActivity.SelectedValue & "' ,'" & txtKodeSubActivity.Text & "' ,'" & txtNamaSubActivity.Text & "' ,'" & Session("sUsername") & "', current_timestamp, '" & Session("sUsername") & "', current_timestamp)"
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                cmd = New SqlCommand(sqlstr, con)
                cmd.ExecuteNonQuery()
                con.Close()
            End Using

            div_lbl.Visible = True
            lbl_msg.Text = "Insert Successful!"
            tampilTable()
        Catch ex As Exception
            div_lbl.Visible = True
            lbl_msg.Text = ex.Message.ToString
        End Try
    End Sub
    Protected Sub updateData_Click(sender As Object, e As EventArgs)
        Dim sqlstr As String = "update MSTSUB_ACTIVITY set kode_main_activity = '" & ddlMainActivity.SelectedValue & "', kode_sub_activity = '" & txtKodeSubActivity.Text & "', sub_activity = '" & txtNamaSubActivity.Text & "', last_update_by = '" & Session("sUsername") & "', last_update_date = current_timestamp where id='" & hdnID.Value & "'"
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                cmd = New SqlCommand(sqlstr, con)
                cmd.ExecuteNonQuery()
                con.Close()
            End Using

            div_lbl.Visible = True
            lbl_msg.Text = "Update Successful!"
            tampilTable()
        Catch ex As Exception
            div_lbl.Visible = True
            lbl_msg.Text = ex.Message.ToString
        End Try
    End Sub
    Protected Sub deleteData_Click(sender As Object, e As EventArgs)
        Dim sqlstr As String = "delete from MSTSUB_ACTIVITY where id = '" & hdnID.Value & "'"
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                cmd = New SqlCommand(sqlstr, con)
                cmd.ExecuteNonQuery()
                con.Close()
            End Using

            div_lbl.Visible = True
            lbl_msg.Text = "Delete Successful!"
            tampilTable()
        Catch ex As Exception
            div_lbl.Visible = True
            lbl_msg.Text = ex.Message.ToString
        End Try
    End Sub
    Protected Sub GridView1_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GridView1.RowCommand

    End Sub

End Class
