﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="PHHasil.aspx.vb" Inherits="PHHasil" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <br />



    <asp:Label ID="lblHead" runat="server" Text="Detail - Master Hasil Penilaian"
        style="font-size: 20px;"
        ></asp:Label>

<style>

    .exportExcel{
  padding: 5px;
  border: 1px solid grey;
  margin: 5px;
  cursor: pointer;
}

</style>

<script  type="text/javascript">
    $(document).ready(function () {
        var table = $('#mytable1').DataTable({
            scrollY: "300px",
            scrollX: "800px",
            scrollCollapse: true,
            paging: true,
            bProcessing: true, // shows 'processing' label
            bStateSave: true, // presumably saves state for reloads
            fixedColumns: {
                leftColumns: 3
            }
        });
    });  




    function pclear1() {
        document.getElementById("txtkode_activity11").value = "";
    };

    function pclear3() {
        document.getElementById("txtkode_activity13").value = "";
    };

    function fncBrowse(p) {
        var pn = "";
        pn = document.getElementById('txtkode_activity1' + p).value;
        window.open('brwMaster.aspx?pn=' + pn + '&p=' + p, 'mywindow', 'left=10,top=10,width=450,height=450,scrollbars=yes,resizable=no');
    }

    function openw(p) {
        window.open('brwfolder.aspx?p=' + p  , 'mywindow', 'left=100,top=100,width=450,height=200,scrollbars=yes,resizable=no');
    };

    function fnExcelReport() {
        var tab_text = "<table border='2px'><tr bgcolor='#87AFC6'>";
        var textRange; var j = 0;
        tab = document.getElementById('mytable1'); // id of table

        for (j = 0 ; j < tab.rows.length ; j++) {
            tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
            //tab_text=tab_text+"</tr>";
        }

        tab_text = tab_text + "</table>";
        tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
        tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
        tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
        {
            txtArea1.document.open("txt/html", "replace");
            txtArea1.document.write(tab_text);
            txtArea1.document.close();
            txtArea1.focus();
            sa = txtArea1.document.execCommand("SaveAs", true, "Say Thanks to Sumit.xls");
        }
        else                 //other browser not tested on IE 11
            sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

        return (sa);
    }

</script>

    <br />
    <div style="font-size: 20px;">
        <b>
    <asp:Label ID="lblkodetmp" runat="server"></asp:Label> - <asp:Label ID="lblnamatmp" runat="server"></asp:Label>
            </b>

    </div>

    <div style="margin:30px;">
    <button type='button' class='button2xx' data-book-id='|' data-target='#formModalNew' data-toggle='modal'>Add New</button>
        <asp:PlaceHolder ID="myPlaceH" runat="server"></asp:PlaceHolder>
            </div>

    <div id="formModalEdit" tabindex="-1" role="dialog" aria-hidden="true" style="display: none ">
        <table style="position: fixed; top:100px ; left:100px; background-color: ButtonFace; border: 5px; border-radius: 15px; padding: 20px; box-shadow: 5px 5px 5px gray;">

            <tr><td>
           <label for="TxtNilai">Nilai &nbsp;&nbsp;</label></td><td>
           <asp:TextBox ID="TxtNilai" runat="server" MaxLength="10" Width="100px" ClientIDMode="Static" style="background-color: lightgray"></asp:TextBox></td>
            </tr><tr><td>
           <label for="TxtKeterangan">Keterangan &nbsp;&nbsp;</label></td><td>
           <asp:TextBox ID="TxtKet" runat="server" MaxLength="100" Width="200px" ClientIDMode="Static"></asp:TextBox></td>
           </tr>

           <tr><td colspan="2" style="text-align: center;">&nbsp;
               </td>
           </tr>
           <tr><td colspan="2" style="text-align: center;">
               <asp:Button ID="btnUpdate1" CssClass="btn1" runat="server" text="Update"/> &nbsp;&nbsp;
               <asp:Button ID="btnCancel1" CssClass="btn1" runat="server" text="Cancel"/>
               </td>
           </tr>
       </table>
    </div>

    <div id="formModalDelete" tabindex="-1" role="dialog" aria-hidden="true"style="display: none ">
        <table style="position: fixed; top:100px ; left:100px; background-color: ButtonFace; border: 5px; border-radius: 15px; padding: 20px; box-shadow: 5px 5px 5px gray;">

             <tr><td>
           <label for="TxtNilai2">Nilai &nbsp;&nbsp;</label></td><td>
           <asp:TextBox ID="TxtNilai2" runat="server" MaxLength="10" Width="100px" ClientIDMode="Static" style="background-color: lightgray"></asp:TextBox></td>
            </tr><tr><td>
           <label for="TxtKet2">Keterangan &nbsp;&nbsp;</label></td><td>
           <asp:TextBox ID="TxtKet2" runat="server" MaxLength="100" Width="200px" ClientIDMode="Static" style="background-color: lightgray"></asp:TextBox></td>
           </tr>

           <tr><td colspan="2" style="text-align: center;">&nbsp;
               </td>
           </tr>
           <tr><td colspan="2" style="text-align: center;">
               <asp:Button ID="btnDelete2" CssClass="btn1" runat="server" text="Delete"/> &nbsp;&nbsp;
               <asp:Button ID="btnCancel2" CssClass="btn1" runat="server" text="Cancel"/>
               </td>
           </tr>
       </table>
    </div>

    <div id="formModalNew" tabindex="-1" role="dialog" aria-hidden="true"style="display: none ">
        <table style="position: fixed; top:100px ; left:100px; background-color: ButtonFace; border: 5px; border-radius: 15px; padding: 20px; box-shadow: 5px 5px 5px gray;">

            <tr><td>
           <label for="TxtNilai3">Nilai &nbsp;&nbsp;</label></td><td>
           <asp:TextBox ID="TxtNilai3" runat="server" MaxLength="10" Width="100px" ClientIDMode="Static"></asp:TextBox></td>
            </tr><tr><td>
           <label for="TxtKet3">Keterangan &nbsp;&nbsp;</label></td><td>
           <asp:TextBox ID="TxtKet3" runat="server" MaxLength="100" Width="200px" ClientIDMode="Static"></asp:TextBox></td>
           </tr>


           <tr><td colspan="2" style="text-align: center;">&nbsp;
               </td>
           </tr>
           <tr><td colspan="2" style="text-align: center;">
               <asp:Button ID="btnInsert3" CssClass="btn1" runat="server" text="Insert"/> &nbsp;&nbsp;
               <asp:Button ID="btnCancel3" CssClass="btn1" runat="server" text="Cancel"/>
               </td>
           </tr>
       </table>
    </div>






    <script type="text/javascript">

        $(function () {
            var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "personal";
            $('#Tabs a[href="#' + tabName + '"]').tab('show');
            $("#Tabs a").click(function () {
                $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
            });
        });



        $('#formModalEdit').on('show.bs.modal', function (e) {
            var bookId = $(e.relatedTarget).data('book-id');
            var arrString = []

            arrString = bookId.split("|")

            $(e.currentTarget).find("#<%=TxtNilai.ClientID%>").val(arrString[0]);
            $(e.currentTarget).find("#<%=TxtKet.ClientID%>").val(arrString[1]);
            
            document.getElementById("TxtNilai").onkeydown = function () { return false; }

        });

        $('#formModalDelete').on('show.bs.modal', function (e) {
            var bookId = $(e.relatedTarget).data('book-id');
            var arrString = []

            arrString = bookId.split("|")

            $(e.currentTarget).find("#<%=TxtNilai2.ClientID%>").val(arrString[0]);
            $(e.currentTarget).find("#<%=TxtKet2.ClientID%>").val(arrString[1]);

            document.getElementById("TxtNilai2").onkeydown = function () { return false; }
            document.getElementById("TxtKet2").onkeydown = function () { return false; }

        });

        $('#formModalNew').on('show.bs.modal', function (e) {
            var bookId = $(e.relatedTarget).data('book-id');
            var arrString = []

            arrString = bookId.split("|")

            $(e.currentTarget).find("#<%=TxtNilai3.ClientID%>").val(arrString[0]);
            $(e.currentTarget).find("#<%=TxtKet3.ClientID%>").val(arrString[1]);

        });

    </script>

    <asp:Label ID="infoerror" runat="server" style="color: red" ></asp:Label>


</asp:Content>

