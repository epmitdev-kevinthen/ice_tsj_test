﻿
Partial Class PHasil
    Inherits System.Web.UI.Page
    Dim myChef As New Chef

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click


        Try

            infoerror.Text = ""
            infoerror.Text = myChef.DELNILAIHDR2(TxtKode2.Text)
            Response.Redirect(HttpContext.Current.Request.Url.ToString(), True)
        Catch ex As Exception
            infoerror.Text = ex.Message
        End Try


    End Sub

    Private Sub btnInsert_Click(sender As Object, e As EventArgs) Handles btnInsert.Click

        Try

            infoerror.Text = ""

            Dim mpLabel As Label
            Dim myusername As String = ""
            mpLabel = CType(Master.FindControl("lblusername"), Label)
            If Not mpLabel Is Nothing Then
                myusername = mpLabel.Text
            End If
            infoerror.Text = myChef.INSNILAIHDR2(TxtKode3.Text, TxtKet3.Text, Val(TxtPengali3.Text))
            Response.Redirect(HttpContext.Current.Request.Url.ToString(), True)
        Catch ex As Exception
            infoerror.Text = ex.Message
        End Try

    End Sub

    Private Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click

        Try

            infoerror.Text = ""

            Dim mpLabel As Label
            Dim myusername As String = ""
            mpLabel = CType(Master.FindControl("lblusername"), Label)
            If Not mpLabel Is Nothing Then
                myusername = mpLabel.Text
            End If
            infoerror.Text = myChef.UPDNILAIHDR2(TxtKode.Text, TxtKet.Text, Val(TxtPengali.Text))
            Response.Redirect(HttpContext.Current.Request.Url.ToString(), True)
        Catch ex As Exception
            infoerror.Text = ex.Message
        End Try


    End Sub

    Private Sub strCheckList_Init(sender As Object, e As EventArgs) Handles Me.Init
        Dim myData As Data.DataSet
        Dim myhtml As New StringBuilder


        If Session("sUsername") Is Nothing Then
            Response.Redirect("Default.aspx")
        End If


        If Session("levelakses") Is Nothing Then
            Response.Redirect("hakakses.aspx")
        ElseIf IsDBNull(Session("levelakses")) Then
            Response.Redirect("hakakses.aspx")
        ElseIf Session("levelakses") <> "1" And Session("levelakses") <> "2" Then
            Response.Redirect("hakakses.aspx")
        End If


        myData = myChef.NILAIHDR2
        myhtml.Append(" <table id='mytable' class='stripe row-border order-column' cellspacing='0' width='100%'> ")
        myhtml.Append("   <thead>")
        myhtml.Append("      <tr>")
        myhtml.Append("         <th>Kode</th>")
        myhtml.Append("         <th></th>")
        myhtml.Append("         <th></th>")
        myhtml.Append("         <th>Keterangan</th>")
        myhtml.Append("         <th>Pengali</th>")
        myhtml.Append("         <th></th>")
        myhtml.Append("      </tr>")
        myhtml.Append("   </thead>")
        myhtml.Append("   <tbody>")
        For i = 0 To myData.Tables(0).Rows.Count - 1
            myhtml.Append("      <tr>")


            Dim Temps As String = myData.Tables(0).Rows(i).Item("kode") & "|" &
                                  myData.Tables(0).Rows(i).Item("keterangan") & "|" &
                                  myData.Tables(0).Rows(i).Item("pengali")

            myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("kode") & "</td>")

            myhtml.Append("<td class='text-right'>")
            myhtml.Append("<button type='button' class='button2xx' onClick='btnClick(" & Chr(34) & myData.Tables(0).Rows(i).Item("kode") & Chr(34) & ")'>Detail</button>")
            myhtml.Append("</td>")

            myhtml.Append("<td class='text-right'>")
            myhtml.Append("<button type='button' class='button2xx' data-book-id='" & Temps & "' data-target='#formModalEdit' data-toggle='modal'>Edit</button>")
            myhtml.Append("</td>")

            myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("Keterangan") & "</td>")
            myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("pengali") & "</td>")

            myhtml.Append("<td class='text-right'>")
            myhtml.Append("<button type='button' class='button2xx' data-book-id='" & Temps & "' data-target='#formModalDelete' data-toggle='modal'>Delete</button>")
            myhtml.Append("</td>")

            myhtml.Append("      </tr>")
        Next
        myhtml.Append("   </tbody>")
        myhtml.Append(" </table> ")

        myPlaceH.Controls.Add(New Literal() With {
          .Text = myhtml.ToString()
         })
    End Sub




End Class
