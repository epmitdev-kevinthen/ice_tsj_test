﻿Imports System.Data
Imports System.Data.SqlClient


Partial Class user
    Inherits System.Web.UI.Page

    Public Shared ConnStrAudit As String = ConfigurationManager.ConnectionStrings("SQLICE").ConnectionString
    Public Shared cmd As New SqlCommand

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        If Not Page.IsPostBack Then
            userEdit.Text = Request.QueryString("username")
            If userEdit.Text = "" Then
                tampilTable()
                tampilJabatan()
                tampilCabang()
                tampilLevelAkses()
                tampilKodeDept()
            Else
                tampilTable()
                tampilJabatan()
                tampilCabang()
                tampilLevelAkses()
                tampilKodeDept()
                tampil()
                'insertData.Style("display") = "none"
            End If

        End If
    End Sub
    Protected Sub tampilTable()
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                Dim da As New SqlDataAdapter("select * from MSTUSER order by username ", con)
                Dim dt As New DataTable
                da.Fill(dt)
                GridView1.DataSource = dt
                GridView1.DataBind()
                GridView1.HeaderRow.TableSection = TableRowSection.TableHeader
                con.Close()
            End Using
        Catch ex As Exception

        End Try

    End Sub
    Protected Sub tampilJabatan()
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                Dim da As New SqlDataAdapter("select * from MSTJABATAN order by jabatan", con)
                Dim dt As New DataTable
                da.Fill(dt)
                If dt.Rows.Count > 0 Then
                    For i = 0 To dt.Rows.Count - 1
                        ddlJabatan.Items.Add(New ListItem(dt.Rows(i).Item("jabatan"), dt.Rows(i).Item("kode") & "-" & dt.Rows(i).Item("jabatan")))
                    Next
                End If
                con.Close()
            End Using
        Catch ex As Exception

        End Try

    End Sub
    Protected Sub tampilCabang()
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                Dim da As New SqlDataAdapter("select * from MSTCAB", con)
                Dim dt As New DataTable
                da.Fill(dt)
                If dt.Rows.Count > 0 Then
                    For i = 0 To dt.Rows.Count - 1
                        ddlCabang.Items.Add(New ListItem(dt.Rows(i).Item("kodecab") & "-" & dt.Rows(i).Item("namacab"), dt.Rows(i).Item("kodecab")))
                    Next
                End If
                con.Close()
            End Using
        Catch ex As Exception

        End Try

    End Sub
    Protected Sub tampilLevelAkses()
        If Session("levelakses") = 1 Then
            ddlLvlAkses.Items.Add(New ListItem("1 - IT ADMINISTRATOR", "1"))
            ddlLvlAkses.Items.Add(New ListItem("2 - AUDIT ADMINISTRATOR", "2"))
            ddlLvlAkses.Items.Add(New ListItem("3 - AUDITOR", "3"))
            ddlLvlAkses.Items.Add(New ListItem("4 - AUDITEE", "4"))
            ddlLvlAkses.Items.Add(New ListItem("5 - USER", "5"))
        Else
            ddlLvlAkses.Items.Add(New ListItem("2 - AUDIT ADMINISTRATOR", "2"))
            ddlLvlAkses.Items.Add(New ListItem("3 - AUDITOR", "3"))
            ddlLvlAkses.Items.Add(New ListItem("4 - AUDITEE", "4"))
            ddlLvlAkses.Items.Add(New ListItem("5 - USER", "5"))
        End If

    End Sub
    Protected Sub tampilKodeDept()
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                Dim da As New SqlDataAdapter("select * from MSTDIREKTORAT", con)
                Dim dt As New DataTable
                da.Fill(dt)
                If dt.Rows.Count > 0 Then
                    For i = 0 To dt.Rows.Count - 1
                        ddlKodeDept.Items.Add(New ListItem(dt.Rows(i).Item("kode") & "-" & dt.Rows(i).Item("direktorat"), dt.Rows(i).Item("kode") & "-" & dt.Rows(i).Item("direktorat")))
                    Next
                End If
                con.Close()
            End Using
        Catch ex As Exception

        End Try

    End Sub
    Protected Sub GridView1_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        tampilTable()
    End Sub
    Protected Sub insertData_Click(sender As Object, e As EventArgs)
        Dim selectedjabatan As String = ddlJabatan.SelectedValue
        Dim selectedDept As String = ddlKodeDept.SelectedValue
        Dim kodejabatan As String = selectedjabatan.Substring(0, selectedjabatan.IndexOf("-"))
        Dim jabatan As String = selectedjabatan.Substring(selectedjabatan.IndexOf("-") + 1, selectedjabatan.Count - 1 - selectedjabatan.IndexOf("-"))
        Dim kodedept As String = Nothing
        Dim dept As String = Nothing
        Dim ice As String = aksesIce.SelectedValue
        Dim sensus As String = aksesSensus.SelectedValue
        If selectedDept <> "" Then
            kodedept = selectedDept.Substring(0, selectedDept.IndexOf("-"))
            dept = selectedDept.Substring(selectedDept.IndexOf("-") + 1, selectedDept.Count - 1 - selectedDept.IndexOf("-"))
        End If
        Dim sqlstr As String = "insert into mstuser(username,userpass,fullusername,jk,kodejabatan,jabatan,kodecab,email,levelakses,created_by,creation_date,last_update_by,last_update_date,kodedept,dept,nik,no_wa, flag_akses_ice, flag_akses_sensus) values ('" & txtUserName.Text & "', '" & txtPass.Text & "', '" & txtFullUserName.Text & "', '" & ddlJK.SelectedValue & "', '" & kodejabatan & "', '" & jabatan & "', '" & ddlCabang.SelectedValue & "', '" & txtEmail.Text & "', '" & ddlLvlAkses.SelectedValue & "', '" & Session("sUsername") & "', current_timestamp, '" & Session("sUsername") & "', current_timestamp, '" & kodedept & "', '" & dept & "','" & txtNIK.Text & "','" & txtNoWA.Text & "','" & ice & "','" & sensus & "')"
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                cmd = New SqlCommand(sqlstr, con)
                cmd.ExecuteNonQuery()
                con.Close()
            End Using

            div_lbl.Visible = True
            lbl_msg.Text = "Insert Successful!"
            tampilTable()
        Catch ex As Exception
            div_lbl.Visible = True
            lbl_msg.Text = ex.Message.ToString
        End Try
    End Sub
    'Protected Sub clearData_Click(sender As Object, e As EventArgs)
    '    txtUserName.Text = ""
    '    txtPass.Text = ""
    '    txtFullUserName.Text = ""
    '    txtNIK.Text = ""
    '    ddlJabatan.SelectedValue = ""
    '    ddlCabang.SelectedValue = ""
    '    txtEmail.Text = ""
    '    ddlKodeDept.SelectedValue = ""
    '    txtNoWA.Text = ""
    '    createdBy.InnerText = ""
    '    createdDate.InnerText = ""
    '    lastUpdateBy.InnerText = ""
    '    lastUpdateDate.InnerText = ""
    'End Sub
    Protected Sub GridView1_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GridView1.RowCommand
        If e.CommandName = "cmdsubmit" Then
            Dim username As String = e.CommandArgument
            Response.Redirect("user.aspx?username=" & username & "")
        ElseIf e.CommandName = "cmddel" Then


        End If
    End Sub
    Protected Sub tampil()
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                Dim da As New SqlDataAdapter("select * from mstuser where username = '" & userEdit.Text & "'", con)
                Dim dt As New DataTable
                da.Fill(dt)
                If dt.Rows.Count > 0 Then
                    txtUserName.Text = dt.Rows(0).Item("username")
                    txtFullUserName.Text = dt.Rows(0).Item("fullusername")
                    txtPass.Text = "password"
                    ddlJK.SelectedValue = dt.Rows(0).Item("jk")
                    ddlJabatan.SelectedValue = dt.Rows(0).Item("kodejabatan") & "-" & dt.Rows(0).Item("jabatan")
                    ddlCabang.SelectedValue = dt.Rows(0).Item("kodecab")
                    txtEmail.Text = dt.Rows(0).Item("email")
                    ddlLvlAkses.SelectedValue = dt.Rows(0).Item("levelakses")
                    ddlKodeDept.SelectedValue = dt.Rows(0).Item("kodedept") & "-" & dt.Rows(0).Item("dept")
                    aksesIce.SelectedValue = dt.Rows(0).Item("flag_akses_ice")
                    aksesSensus.SelectedValue = dt.Rows(0).Item("flag_akses_sensus")
                    createdBy.InnerText = dt.Rows(0).Item("created_by")
                    createdDate.InnerText = dt.Rows(0).Item("creation_date")
                    lastUpdateBy.InnerText = dt.Rows(0).Item("last_update_by")
                    lastUpdateDate.InnerText = dt.Rows(0).Item("last_update_date")
                End If
                    con.Close()
            End Using
        Catch ex As Exception

        End Try

    End Sub
    Protected Sub updateData_Click(sender As Object, e As EventArgs)
        Dim selectedjabatan As String = ddlJabatan.SelectedValue
        Dim selectedDept As String = ddlKodeDept.SelectedValue
        Dim kodejabatan As String = selectedjabatan.Substring(0, selectedjabatan.IndexOf("-"))
        Dim jabatan As String = selectedjabatan.Substring(selectedjabatan.IndexOf("-") + 1, selectedjabatan.Count - 1 - selectedjabatan.IndexOf("-"))
        Dim kodedept As String = Nothing
        Dim dept As String = Nothing
        Dim ice As String = aksesIce.SelectedValue
        Dim sensus As String = aksesSensus.SelectedValue
        If selectedDept <> "" Then
            kodedept = selectedDept.Substring(0, selectedDept.IndexOf("-"))
            dept = selectedDept.Substring(selectedDept.IndexOf("-") + 1, selectedDept.Count - 1 - selectedDept.IndexOf("-"))
        End If
        Dim sqlstr As String = "UPDATE MSTUSER SET username = '" & txtUserName.Text & "',userpass = '" & txtPass.Text & "',fullusername = '" & txtFullUserName.Text & "',nik='" & txtNIK.Text & "',no_wa = '" & txtNoWA.Text & "', jk = '" & ddlJK.SelectedValue & "',kodejabatan = '" & kodejabatan & "',jabatan = '" & jabatan & "',kodecab = '" & ddlCabang.SelectedValue & "',email = '" & txtEmail.Text & "',levelakses = '" & ddlLvlAkses.SelectedValue & "',last_update_by = '" & Session("sUsername") & "',last_update_date = current_timestamp,kodedept = '" & kodedept & "',dept = '" & dept & "', flag_akses_ice = '" & ice & "', flag_akses_sensus = '" & sensus & "' WHERE username = '" & txtUserName.Text & "'"
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                cmd = New SqlCommand(sqlstr, con)
                cmd.ExecuteNonQuery()
                con.Close()
            End Using

            div_lbl.Visible = True
            lbl_msg.Text = "Update Successful!"
            tampilTable()
        Catch ex As Exception
            div_lbl.Visible = True
            lbl_msg.Text = ex.Message.ToString
        End Try
    End Sub
    Protected Sub deleteData_Click(sender As Object, e As EventArgs)
        Dim sqlstr As String = "delete from MSTUSER where username = '" & txtUserName.Text & "'"
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                cmd = New SqlCommand(sqlstr, con)
                cmd.ExecuteNonQuery()
                con.Close()
            End Using

            div_lbl.Visible = True
            lbl_msg.Text = "Delete Successful!"
            tampilTable()
        Catch ex As Exception
            div_lbl.Visible = True
            lbl_msg.Text = ex.Message.ToString
        End Try
    End Sub
End Class
