﻿
Partial Class ListUser
    Inherits System.Web.UI.Page
    Dim myChef As New Chef

    Private Sub ListUser_Init(sender As Object, e As EventArgs) Handles Me.Init
        Dim myData As Data.DataSet
        Dim myhtml As New StringBuilder

        If Session("levelakses") Is Nothing Then
            Response.Redirect("hakakses.aspx")
        ElseIf IsDBNull(Session("levelakses")) Then
            Response.Redirect("hakakses.aspx")
        ElseIf Session("levelakses") <> "1" And Session("levelakses") <> "2" Then
            Response.Redirect("hakakses.aspx")
        End If



        If Session("sUsername") Is Nothing Then
            Response.Redirect("Default.aspx")
        End If


        If Session("levelakses") Is Nothing Then
            Response.Redirect("hakakses.aspx")
        ElseIf IsDBNull(Session("levelakses")) Then
            Response.Redirect("hakakses.aspx")
        ElseIf Session("levelakses") <> "1" And Session("levelakses") <> "2" Then
            Response.Redirect("hakakses.aspx")
        End If



        myData = myChef.SQLLISTUSER
        myhtml.Append(" <table id='mytable' class='stripe row-border order-column' cellspacing='0' width='100%'> ")
        myhtml.Append("   <thead>")
        myhtml.Append("      <tr>")
        myhtml.Append("         <th>User Name</th>")
        myhtml.Append("         <th>Password</th>")
        myhtml.Append("         <th>Full Name</th>")
        myhtml.Append("         <th>Jabatan</th>")
        myhtml.Append("         <th>Cabang</th>")
        myhtml.Append("         <th>Email</th>")
        myhtml.Append("         <th>Level Akses</th>")
        myhtml.Append("         <th>Div</th>")
        myhtml.Append("      </tr>")
        myhtml.Append("   </thead>")
        myhtml.Append("   <tbody>")
        For i = 0 To myData.Tables(0).Rows.Count - 1
            myhtml.Append("      <tr>")

            myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("username") & "</td>")
            myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("userpass") & "</td>")
            myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("fullusername") & "</td>")
            myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("jabatan") & "</td>")
            myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("kodecab") & "</td>")
            myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("email") & "</td>")
            myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("levelakses") & "</td>")
            myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("kodedept") & "</td>")

            myhtml.Append("      </tr>")
        Next
        myhtml.Append("   </tbody>")
        myhtml.Append(" </table> ")

        myPlaceH.Controls.Add(New Literal() With {
          .Text = myhtml.ToString()
         })
    End Sub




End Class
