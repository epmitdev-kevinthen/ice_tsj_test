﻿
Partial Class MasterPage
    Inherits System.Web.UI.MasterPage

    Private Sub MasterPage_Init(sender As Object, e As EventArgs) Handles Me.Init
        lblinfobar.Text = ""
        If Not IsDBNull(Session("sUsername")) Then
            If Not IsNothing(Session("sUsername")) Then
                If Session("sUsername") <> "" Then
                    lblinfobar.Text = Session("FULLNAME") & " | " & Session("sUsername") & " | Logon at " & Now.ToString("dd-MMM-yyyy HH:mm:ss") & Session("kodedept")
                    lblusername.Text = Session("sUsername")
                End If
            End If
        End If
    End Sub

    Private Sub MasterPage_Load(sender As Object, e As EventArgs) Handles Me.Load
        masterSensus.Visible = False
        masterFTA_Subsidiaries.Visible = False
        If lblinfobar.Text = "" Then
            Response.Redirect("Default.aspx")
        End If
        If Session("levelakses") > 2 Then
            masterTable.Visible = False
            masterAudit.Visible = False
        End If

        If (Session("jabatan") IsNot Nothing) Then
            If Session("sCabang").ToString() <> "PST" And (Session("jabatan").ToString() = "ACC SPV" OrElse Session("jabatan").ToString().Contains("AUDIT") OrElse Session("jabatan").ToString() = "ABM") Then
                masterSensus.Visible = True
            End If

            If Session("levelakses") <= 3 And Session("sCabang").ToString().Equals("PST") Then
                masterSensus.Visible = True
            End If
        ElseIf Session("sUsername").ToString = "SUPERADMIN" Then
            masterSensus.Visible = True
        End If

        If (Session("region") = "SUBSIDIARIES") Then
            masterICE.Visible = False
            masterFTA.Visible = False
            masterFTA_Subsidiaries.Visible = True
        End If
    End Sub

End Class

