﻿
Imports System.Data
Imports System.Data.SqlClient

Partial Class fta_rekap
    Inherits System.Web.UI.Page



    Protected Sub GridView1_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then

        End If
    End Sub

    Private Sub fta_rekap_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            loadDDLFilter()
            'loadKelompokPemeriksa()
            'loadRekap()
        End If
    End Sub
    Sub loadDDLFilter()
        Dim dt As New DataTable

        dt = GetDataSql("select * from mstcab order by namacab")
        If dt.Rows.Count > 0 Then
            ddlBranch.Items.Add(New ListItem("All", "%"))
            For i = 0 To dt.Rows.Count - 1
                ddlBranch.Items.Add(New ListItem(dt.Rows(i).Item("namacab"), dt.Rows(i).Item("kodecab")))
            Next
        End If

        dt = GetDataSql("select * from MSTCASECATEGORY order by keterangan")
        If dt.Rows.Count > 0 Then
            ddlCaseCategory.Items.Add(New ListItem("All", "%"))
            For i = 0 To dt.Rows.Count - 1
                ddlCaseCategory.Items.Add(New ListItem(dt.Rows(i).Item("keterangan"), dt.Rows(i).Item("kode")))
            Next
        End If

        dt = GetDataSql("select * from MSTFINDINGCATEGORY order by namacategory")
        If dt.Rows.Count > 0 Then
            ddlFindingCategory.Items.Add(New ListItem("All", "%"))
            For i = 0 To dt.Rows.Count - 1
                ddlFindingCategory.Items.Add(New ListItem(dt.Rows(i).Item("namacategory"), dt.Rows(i).Item("kodecategory")))
            Next
        End If

        dt = GetDataSql("SELECT * FROM mst_periode where flag_aktif='A' order by start_date,nama_prd,kode_prd desc")
        If dt.Rows.Count > 0 Then
            ddlPeriode.Items.Add(New ListItem("All", "%"))
            For i = 0 To dt.Rows.Count - 1
                ddlPeriode.Items.Add(New ListItem(dt.Rows(i).Item("nama_prd"), dt.Rows(i).Item("kode_prd")))
            Next
        End If

        dt = GetDataSql("select * from MST_FTA_STATUS")
        If dt.Rows.Count > 0 Then
            ddlStatus.Items.Add(New ListItem("All", "%"))
            For i = 0 To dt.Rows.Count - 1
                ddlStatus.Items.Add(New ListItem(dt.Rows(i).Item("namastatus") + " (" & dt.Rows(i).Item("kodestatus") & ")", dt.Rows(i).Item("id")))
            Next
        End If

        dt = GetDataSql("select * from MSTKELOMPOKPEMERIKSA order by kodekel")
        If dt.Rows.Count > 0 Then
            ddlKelompokPemeriksa.Items.Add(New ListItem("All", "%"))
            For i = 0 To dt.Rows.Count - 1
                ddlKelompokPemeriksa.Items.Add(New ListItem(dt.Rows(i).Item(1), dt.Rows(i).Item(0)))
            Next
        End If

    End Sub
    Sub loadKelompokPemeriksa()
        Dim dt As DataTable = GetDataSql("select * from MSTKELOMPOKPEMERIKSA order by kodekel")
        'ddlKelompokPemeriksa.Items.Add(New ListItem("Pilih Kelompok Pemeriksa", ""))
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                ddlKelompokPemeriksa.Items.Add(New ListItem(dt.Rows(i).Item(1), dt.Rows(i).Item(0)))
            Next
        End If
    End Sub

    Sub loadRekap(ByVal param_branch As String, ByVal param_caseCat As String, ByVal param_finCat As String, ByVal param_periode As String, ByVal param_status As String, ByVal param_kelompokpemeriksa As String)
        Dim sb As String
        '        sb = "SELECT a.*, " & vbCrLf &
        '"       CASE " & vbCrLf &
        '"         WHEN isr = 100 THEN 'Done' " & vbCrLf &
        '"         ELSE 'Not Done' " & vbCrLf &
        '"       END AS status_follow_up " & vbCrLf &
        '"FROM   (SELECT a.*, " & vbCrLf &
        '"               CASE " & vbCrLf &
        '"                 WHEN a.due_for_implement <> '0' THEN Cast( " & vbCrLf &
        '"                 ( " & vbCrLf &
        '"                 Cast(a.implemented AS DECIMAL) * 100 / Cast( " & vbCrLf &
        '"                 a.due_for_implement AS DECIMAL) " & vbCrLf &
        '"                 ) " & vbCrLf &
        '"                                                      AS DECIMAL) " & vbCrLf &
        '"                 ELSE 0 " & vbCrLf &
        '"               END AS ISR " & vbCrLf &
        '"        FROM   (SELECT a.fta_no, " & vbCrLf &
        '"                       a.kel_pemeriksa, " & vbCrLf &
        '"                       a.kodecab, " & vbCrLf &
        '"                       a.class, " & vbCrLf &
        '"                       a.region, " & vbCrLf &
        '"                       a.nama_prd, " & vbCrLf &
        '"                       b.follow_up_recommendation, " & vbCrLf &
        '"                       b.not_implemented, " & vbCrLf &
        '"                       b.not_applicable, " & vbCrLf &
        '"                       b.in_progress, " & vbCrLf &
        '"                       b.not_due, " & vbCrLf &
        '"                       b.implemented, " & vbCrLf &
        '"                       b.due_for_implement, " & vbCrLf &
        '"                       CASE " & vbCrLf &
        '"                         WHEN auditor IS NULL THEN " & vbCrLf &
        '"                         (SELECT auditor " & vbCrLf &
        '"                          FROM   (SELECT fta_no, " & vbCrLf &
        '"                                         auditor " & vbCrLf &
        '"                                  FROM   fta_checklist_hdr a " & vbCrLf &
        '"                                         INNER JOIN (SELECT " & vbCrLf &
        '"                                         a.kode_prd, " & vbCrLf &
        '"                                         a.kode_tmp, " & vbCrLf &
        '"                                         a.kodecab, " & vbCrLf &
        '"                                         ( CASE " & vbCrLf &
        '"                                             WHEN b3.nama " & vbCrLf &
        '"                                             IS NULL THEN " & vbCrLf &
        '"                                             b1.nama + ',' " & vbCrLf &
        '"                                             + b2.nama " & vbCrLf &
        '"                                             ELSE b1.nama " & vbCrLf &
        '"                                           + ',' + b2.nama " & vbCrLf &
        '"                                           + ',' + " & vbCrLf &
        '"                                                  b3.nama " & vbCrLf &
        '"                                           END ) AS auditor " & vbCrLf &
        '"                                                     FROM " & vbCrLf &
        '"                                         checklist_entryhdr a " & vbCrLf &
        '"                         LEFT JOIN mstauditor " & vbCrLf &
        '"                                   b3 " & vbCrLf &
        '"                                ON a.auditor_3 = " & vbCrLf &
        '"                                   b3.nik, " & vbCrLf &
        '"                         mstauditor b1, " & vbCrLf &
        '"                         mstauditor b2 " & vbCrLf &
        '"                                     WHERE " & vbCrLf &
        '"                         a.auditor_1 = b1.nik " & vbCrLf &
        '"                         AND auditor_2 = b2.nik) b " & vbCrLf &
        '"                                 ON a.kodecab = " & vbCrLf &
        '"                                    b.kodecab " & vbCrLf &
        '"                                    AND " & vbCrLf &
        '"                         a.kode_tmp = b.kode_tmp " & vbCrLf &
        '"                                    AND " & vbCrLf &
        '"                         a.kode_prd = b.kode_prd) e " & vbCrLf &
        '"                          WHERE  e.fta_no = a.fta_no) " & vbCrLf &
        '"                         ELSE auditor " & vbCrLf &
        '"                       END AS auditor " & vbCrLf &
        '"                FROM   (SELECT a.fta_no, " & vbCrLf &
        '"                               a.kel_pemeriksa, " & vbCrLf &
        '"                               a.kode_tmp, " & vbCrLf &
        '"                               a.kodecab, " & vbCrLf &
        '"                               b.class, " & vbCrLf &
        '"                               b.region, " & vbCrLf &
        '"                               a.kode_prd, " & vbCrLf &
        '"                               c.nama_prd " & vbCrLf &
        '"                        FROM   (SELECT a.kodecab, " & vbCrLf &
        '"                                       a.kode_prd, " & vbCrLf &
        '"                                       a.kode_tmp, " & vbCrLf &
        '"                                       a.fta_no, " & vbCrLf &
        '"                                       a.kel_pemeriksa " & vbCrLf &
        '"                                FROM   fta_checklist_hdr a " & vbCrLf &
        '"                                       INNER JOIN (SELECT " & vbCrLf &
        '"                                       kodecab, " & vbCrLf &
        '"                                       status, " & vbCrLf &
        '"                                       Max(fta_no) fta_no_last " & vbCrLf &
        '"                                                   FROM   fta_checklist_hdr " & vbCrLf &
        '"                                                   WHERE  status = '2' " & vbCrLf &
        '"                                                   GROUP  BY kodecab, " & vbCrLf &
        '"                                                             status) b " & vbCrLf &
        '"                                               ON a.kodecab = b.kodecab " & vbCrLf &
        '"                                                  AND a.fta_no = b.fta_no_last " & vbCrLf &
        '"                                                  AND a.status = b.status) a " & vbCrLf &
        '"                               LEFT JOIN mstcab b " & vbCrLf &
        '"                                      ON a.kodecab = b.kodecab " & vbCrLf &
        '"                               LEFT JOIN mst_periode c " & vbCrLf &
        '"                                      ON a.kode_prd = c.kode_prd) a " & vbCrLf &
        '"                       LEFT JOIN (SELECT fta_no, " & vbCrLf &
        '"                                         Count(1) AS [Follow_Up_Recommendation], " & vbCrLf &
        '"                                         Sum(CASE " & vbCrLf &
        '"                                               WHEN status = 1 THEN 1 " & vbCrLf &
        '"                                               ELSE 0 " & vbCrLf &
        '"                                             END) AS [Not_Implemented], " & vbCrLf &
        '"                                         Sum(CASE " & vbCrLf &
        '"                                               WHEN status = 2 THEN 1 " & vbCrLf &
        '"                                               ELSE 0 " & vbCrLf &
        '"                                             END) AS [Not_Applicable], " & vbCrLf &
        '"                                         Sum(CASE " & vbCrLf &
        '"                                               WHEN status = 3 THEN 1 " & vbCrLf &
        '"                                               ELSE 0 " & vbCrLf &
        '"                                             END) AS [In_Progress], " & vbCrLf &
        '"                                         Sum(CASE " & vbCrLf &
        '"                                               WHEN status = 4 THEN 1 " & vbCrLf &
        '"                                               ELSE 0 " & vbCrLf &
        '"                                             END) AS [Not_Due], " & vbCrLf &
        '"                                         Sum(CASE " & vbCrLf &
        '"                                               WHEN status = 5 THEN 1 " & vbCrLf &
        '"                                               ELSE 0 " & vbCrLf &
        '"                                             END) AS [Implemented], " & vbCrLf &
        '"                                         Sum(CASE " & vbCrLf &
        '"                                               WHEN status = 1 THEN 1 " & vbCrLf &
        '"                                               WHEN status = 3 THEN 1 " & vbCrLf &
        '"                                               WHEN status = 5 THEN 1 " & vbCrLf &
        '"                                               ELSE 0 " & vbCrLf &
        '"                                             END) AS [Due_For_Implement] " & vbCrLf &
        '"                                  FROM   fta_checklist_dtl " & vbCrLf &
        '"                                  GROUP  BY fta_no) b " & vbCrLf &
        '"                              ON a.fta_no = b.fta_no " & vbCrLf &
        '"                       LEFT JOIN (SELECT fta_no, " & vbCrLf &
        '"                                         ( CASE " & vbCrLf &
        '"                                             WHEN b3.nama IS NULL THEN " & vbCrLf &
        '"                                             b1.nama + ',' + b2.nama " & vbCrLf &
        '"                                             ELSE b1.nama + ',' + b2.nama + ',' " & vbCrLf &
        '"                                                  + " & vbCrLf &
        '"                                                  b3.nama " & vbCrLf &
        '"                                           END ) AS auditor " & vbCrLf &
        '"                                  FROM   fta_checklist_auditor a " & vbCrLf &
        '"                                         LEFT JOIN mstauditor b3 " & vbCrLf &
        '"                                                ON a.auditor_3 = b3.nik, " & vbCrLf &
        '"                                         mstauditor b1, " & vbCrLf &
        '"                                         mstauditor b2 " & vbCrLf &
        '"                                  WHERE  a.auditor_1 = b1.nik " & vbCrLf &
        '"                                         AND auditor_2 = b2.nik) c " & vbCrLf &
        '"                              ON a.fta_no = c.fta_no) a) a "

        If Request.QueryString("q") <> "" Or hdnSelectedKelompokPemeriksa.Value.Contains("002") Or hdnSelectedKelompokPemeriksa.Value.Contains("003") Or hdnSelectedKelompokPemeriksa.Value.Contains("004") Then
            sb = "SELECT a.*, CASE WHEN isr = 100 THEN 'Done' ELSE 'Not Done' END AS status_follow_up FROM ( SELECT a.*, CASE WHEN a.due_for_implement <> '0' THEN Cast( ( Cast(a.implemented AS DECIMAL) * 100 / Cast(a.due_for_implement AS DECIMAL) ) AS DECIMAL ) ELSE 0 END AS ISR FROM ( SELECT a.fta_no, a.kel_pemeriksa, a.kodecab, a.class, a.region, a.nama_prd, b.follow_up_recommendation, b.not_implemented, b.not_applicable, b.in_progress, b.not_due, b.implemented, b.due_for_implement, CASE WHEN auditor IS NULL THEN ( SELECT auditor FROM ( SELECT fta_no, a.Kode_Prd, auditor FROM fta_checklist_hdr a INNER JOIN ( SELECT a.kode_prd, a.kode_tmp, a.kodecab, ( CASE WHEN b3.nama IS NULL THEN b1.nama + ',' + b2.nama ELSE b1.nama + ',' + b2.nama + ',' + b3.nama END ) AS auditor FROM checklist_entryhdr a LEFT JOIN mstauditor b3 ON a.auditor_3 = b3.nik, mstauditor b1, mstauditor b2 WHERE a.auditor_1 = b1.nik AND auditor_2 = b2.nik ) b ON a.kodecab = b.kodecab AND a.kode_tmp = b.kode_tmp AND a.kode_prd = b.kode_prd ) e WHERE e.fta_no = a.fta_no and e.kode_prd = a.kode_prd ) ELSE auditor END AS auditor FROM ( SELECT a.fta_no, a.tahun, a.kel_pemeriksa, a.kode_tmp, a.kodecab, b.class, b.region, a.kode_prd, c.nama_prd FROM ( SELECT a.kodecab, a.kode_prd, a.kode_tmp, a.fta_no, a.tahun, a.kel_pemeriksa FROM fta_checklist_hdr a INNER JOIN ( SELECT a.fta_no, a.kodecab, status,  tahun FROM fta_checklist_hdr a INNER JOIN ( SELECT kodecab, kel_pemeriksa FROM fta_checklist_hdr GROUP BY kodecab, kel_pemeriksa ) b ON a.kodecab = b.kodecab AND a.kel_pemeriksa = b.kel_pemeriksa WHERE status = '2' AND " & param_kelompokpemeriksa & " GROUP BY a.kodecab, status, tahun, a.fta_no ) b ON a.kodecab = b.kodecab AND a.fta_no = b.fta_no AND a.status = b.status ) a LEFT JOIN mstcab b ON a.kodecab = b.kodecab LEFT JOIN mst_periode c ON a.kode_prd = c.kode_prd ) a INNER JOIN ( SELECT fta_no, Kode_Prd, Count(1) AS [Follow_Up_Recommendation], Sum(CASE WHEN status = 1 THEN 1 ELSE 0 END) AS [Not_Implemented], Sum(CASE WHEN status = 2 THEN 1 ELSE 0 END) AS [Not_Applicable], Sum(CASE WHEN status = 3 THEN 1 ELSE 0 END) AS [In_Progress], Sum(CASE WHEN status = 4 THEN 1 ELSE 0 END) AS [Not_Due], Sum(CASE WHEN status = 5 THEN 1 ELSE 0 END) AS [Implemented], Sum( CASE WHEN status = 1 THEN 1 WHEN status = 3 THEN 1 WHEN status = 5 THEN 1 ELSE 0 END ) AS [Due_For_Implement] FROM fta_checklist_dtl where " & param_branch & " and " & param_periode & " and " & param_caseCat & " and " & param_finCat & " and " & param_status & " GROUP BY fta_no, Kode_Prd ) b ON a.fta_no = b.fta_no and a.Kode_Prd = b.Kode_Prd LEFT JOIN ( SELECT fta_no, ( CASE WHEN b3.nama IS NULL THEN b1.nama + ',' + b2.nama ELSE b1.nama + ',' + b2.nama + ',' + b3.nama END ) AS auditor, tahun FROM fta_checklist_auditor a LEFT JOIN mstauditor b3 ON a.auditor_3 = b3.nik, mstauditor b1, mstauditor b2 WHERE a.auditor_1 = b1.nik AND auditor_2 = b2.nik ) c ON a.fta_no = c.fta_no AND a.tahun = c.tahun ) a ) a UNION SELECT '9999999' AS fta_no, '' AS kel_pemeriksa, 'Total' AS kodecab, '' AS class, '' AS region, '' AS nama_prd, Sum(follow_up_recommendation) AS Follow_Up_Recommendation, Sum(not_implemented) AS Not_Implemented, Sum(not_applicable) AS Not_Applicable, Sum(in_progress) AS In_Progress, Sum(not_due) AS not_due, Sum(implemented) AS Implemented, Sum(due_for_implement) AS Due_For_Implement, '' AS auditor, CASE WHEN Sum(a.due_for_implement) <> '0' THEN Cast( ( Cast( Sum(a.implemented) AS DECIMAL ) * 100 / Cast( Sum(a.due_for_implement) AS DECIMAL ) ) AS DECIMAL ) ELSE 0 END AS ISR, '' AS status_follow_up FROM ( SELECT a.*, CASE WHEN isr = 100 THEN 'Done' ELSE 'Not Done' END AS status_follow_up FROM ( SELECT a.*, CASE WHEN a.due_for_implement <> '0' THEN Cast( ( Cast(a.implemented AS DECIMAL) * 100 / Cast(a.due_for_implement AS DECIMAL) ) AS DECIMAL ) ELSE 0 END AS ISR FROM ( SELECT a.fta_no, a.tahun, a.kel_pemeriksa, a.kodecab, a.class, a.region, a.nama_prd, b.follow_up_recommendation, b.not_implemented, b.not_applicable, b.in_progress, b.not_due, b.implemented, b.due_for_implement, CASE WHEN auditor IS NULL THEN ( SELECT auditor FROM ( SELECT fta_no, auditor FROM fta_checklist_hdr a INNER JOIN ( SELECT a.kode_prd, a.kode_tmp, a.kodecab, ( CASE WHEN b3.nama IS NULL THEN b1.nama + ',' + b2.nama ELSE b1.nama + ',' + b2.nama + ',' + b3.nama END ) AS auditor FROM checklist_entryhdr a LEFT JOIN mstauditor b3 ON a.auditor_3 = b3.nik, mstauditor b1, mstauditor b2 WHERE a.auditor_1 = b1.nik AND auditor_2 = b2.nik ) b ON a.kodecab = b.kodecab AND a.kode_tmp = b.kode_tmp AND a.kode_prd = b.kode_prd ) e WHERE e.fta_no = a.fta_no ) ELSE auditor END AS auditor FROM ( SELECT a.fta_no, a.tahun, a.kel_pemeriksa, a.kode_tmp, a.kodecab, b.class, b.region, a.kode_prd, c.nama_prd FROM ( SELECT a.kodecab, a.kode_prd, a.kode_tmp, a.fta_no, a.tahun, a.kel_pemeriksa FROM fta_checklist_hdr a INNER JOIN ( SELECT a.kodecab, status, a.fta_no, tahun FROM fta_checklist_hdr a INNER JOIN ( SELECT kodecab, kel_pemeriksa FROM fta_checklist_hdr GROUP BY kodecab, kel_pemeriksa ) b ON a.kodecab = b.kodecab AND a.kel_pemeriksa = b.kel_pemeriksa WHERE status = '2' AND " & param_kelompokpemeriksa & " GROUP BY a.fta_no, a.kodecab, status, tahun ) b ON a.kodecab = b.kodecab AND a.fta_no = b.fta_no AND a.status = b.status ) a LEFT JOIN mstcab b ON a.kodecab = b.kodecab LEFT JOIN mst_periode c ON a.kode_prd = c.kode_prd ) a INNER JOIN ( SELECT fta_no, kode_prd, Count(1) AS [Follow_Up_Recommendation], Sum(CASE WHEN status = 1 THEN 1 ELSE 0 END) AS [Not_Implemented], Sum(CASE WHEN status = 2 THEN 1 ELSE 0 END) AS [Not_Applicable], Sum(CASE WHEN status = 3 THEN 1 ELSE 0 END) AS [In_Progress], Sum(CASE WHEN status = 4 THEN 1 ELSE 0 END) AS [Not_Due], Sum(CASE WHEN status = 5 THEN 1 ELSE 0 END) AS [Implemented], Sum( CASE WHEN status = 1 THEN 1 WHEN status = 3 THEN 1 WHEN status = 5 THEN 1 ELSE 0 END ) AS [Due_For_Implement] FROM fta_checklist_dtl where " & param_branch & " and " & param_periode & " and " & param_caseCat & " and " & param_finCat & " and " & param_status & " GROUP BY fta_no, Kode_Prd ) b ON a.fta_no = b.fta_no and a.Kode_Prd = b.Kode_Prd LEFT JOIN ( SELECT fta_no, tahun, ( CASE WHEN b3.nama IS NULL THEN b1.nama + ',' + b2.nama ELSE b1.nama + ',' + b2.nama + ',' + b3.nama END ) AS auditor FROM fta_checklist_auditor a LEFT JOIN mstauditor b3 ON a.auditor_3 = b3.nik, mstauditor b1, mstauditor b2 WHERE a.auditor_1 = b1.nik AND auditor_2 = b2.nik ) c ON a.fta_no = c.fta_no AND a.tahun = c.tahun ) a ) a ) a "
        Else
            sb = "SELECT a.*, CASE WHEN isr = 100 THEN 'Done' ELSE 'Not Done' END AS status_follow_up FROM ( SELECT a.*, CASE WHEN a.due_for_implement <> '0' THEN Cast( ( Cast(a.implemented AS DECIMAL) * 100 / Cast(a.due_for_implement AS DECIMAL) ) AS DECIMAL ) ELSE 0 END AS ISR FROM ( SELECT a.fta_no, a.kel_pemeriksa, a.kodecab, a.class, a.region, a.nama_prd, b.follow_up_recommendation, b.not_implemented, b.not_applicable, b.in_progress, b.not_due, b.implemented, b.due_for_implement, CASE WHEN auditor IS NULL THEN ( SELECT auditor FROM ( SELECT fta_no, a.Kode_Prd, auditor FROM fta_checklist_hdr a INNER JOIN ( SELECT a.kode_prd, a.kode_tmp, a.kodecab, ( CASE WHEN b3.nama IS NULL THEN b1.nama + ',' + b2.nama ELSE b1.nama + ',' + b2.nama + ',' + b3.nama END ) AS auditor FROM checklist_entryhdr a LEFT JOIN mstauditor b3 ON a.auditor_3 = b3.nik, mstauditor b1, mstauditor b2 WHERE a.auditor_1 = b1.nik AND auditor_2 = b2.nik ) b ON a.kodecab = b.kodecab AND a.kode_tmp = b.kode_tmp AND a.kode_prd = b.kode_prd ) e WHERE e.fta_no = a.fta_no and e.kode_prd = a.kode_prd ) ELSE auditor END AS auditor FROM ( SELECT a.fta_no, a.tahun, a.kel_pemeriksa, a.kode_tmp, a.kodecab, b.class, b.region, a.kode_prd, c.nama_prd FROM ( SELECT a.kodecab, a.kode_prd, a.kode_tmp, a.fta_no, a.tahun, a.kel_pemeriksa FROM fta_checklist_hdr a INNER JOIN ( SELECT a.kodecab, status, Max(fta_no) AS fta_no_last, tahun FROM fta_checklist_hdr a INNER JOIN ( SELECT Max(tahun) last_tahun, kodecab, kel_pemeriksa FROM fta_checklist_hdr GROUP BY kodecab, kel_pemeriksa ) b ON a.kodecab = b.kodecab AND a.tahun = b.last_tahun AND a.kel_pemeriksa = b.kel_pemeriksa WHERE status = '2' AND " & param_kelompokpemeriksa & " GROUP BY a.kodecab, status, tahun ) b ON a.kodecab = b.kodecab AND a.fta_no = b.fta_no_last AND a.status = b.status ) a LEFT JOIN mstcab b ON a.kodecab = b.kodecab LEFT JOIN mst_periode c ON a.kode_prd = c.kode_prd ) a INNER JOIN ( SELECT fta_no, Kode_Prd, Count(1) AS [Follow_Up_Recommendation], Sum(CASE WHEN status = 1 THEN 1 ELSE 0 END) AS [Not_Implemented], Sum(CASE WHEN status = 2 THEN 1 ELSE 0 END) AS [Not_Applicable], Sum(CASE WHEN status = 3 THEN 1 ELSE 0 END) AS [In_Progress], Sum(CASE WHEN status = 4 THEN 1 ELSE 0 END) AS [Not_Due], Sum(CASE WHEN status = 5 THEN 1 ELSE 0 END) AS [Implemented], Sum( CASE WHEN status = 1 THEN 1 WHEN status = 3 THEN 1 WHEN status = 5 THEN 1 ELSE 0 END ) AS [Due_For_Implement] FROM fta_checklist_dtl where " & param_branch & " and " & param_periode & " and " & param_caseCat & " and " & param_finCat & " and " & param_status & " GROUP BY fta_no, Kode_Prd ) b ON a.fta_no = b.fta_no and a.Kode_Prd = b.Kode_Prd LEFT JOIN ( SELECT fta_no, ( CASE WHEN b3.nama IS NULL THEN b1.nama + ',' + b2.nama ELSE b1.nama + ',' + b2.nama + ',' + b3.nama END ) AS auditor, tahun FROM fta_checklist_auditor a LEFT JOIN mstauditor b3 ON a.auditor_3 = b3.nik, mstauditor b1, mstauditor b2 WHERE a.auditor_1 = b1.nik AND auditor_2 = b2.nik ) c ON a.fta_no = c.fta_no AND a.tahun = c.tahun ) a ) a UNION SELECT '9999999' AS fta_no, '' AS kel_pemeriksa, 'Total' AS kodecab, '' AS class, '' AS region, '' AS nama_prd, Sum(follow_up_recommendation) AS Follow_Up_Recommendation, Sum(not_implemented) AS Not_Implemented, Sum(not_applicable) AS Not_Applicable, Sum(in_progress) AS In_Progress, Sum(not_due) AS not_due, Sum(implemented) AS Implemented, Sum(due_for_implement) AS Due_For_Implement, '' AS auditor, CASE WHEN Sum(a.due_for_implement) <> '0' THEN Cast( ( Cast( Sum(a.implemented) AS DECIMAL ) * 100 / Cast( Sum(a.due_for_implement) AS DECIMAL ) ) AS DECIMAL ) ELSE 0 END AS ISR, '' AS status_follow_up FROM ( SELECT a.*, CASE WHEN isr = 100 THEN 'Done' ELSE 'Not Done' END AS status_follow_up FROM ( SELECT a.*, CASE WHEN a.due_for_implement <> '0' THEN Cast( ( Cast(a.implemented AS DECIMAL) * 100 / Cast(a.due_for_implement AS DECIMAL) ) AS DECIMAL ) ELSE 0 END AS ISR FROM ( SELECT a.fta_no, a.tahun, a.kel_pemeriksa, a.kodecab, a.class, a.region, a.nama_prd, b.follow_up_recommendation, b.not_implemented, b.not_applicable, b.in_progress, b.not_due, b.implemented, b.due_for_implement, CASE WHEN auditor IS NULL THEN ( SELECT auditor FROM ( SELECT fta_no, auditor FROM fta_checklist_hdr a INNER JOIN ( SELECT a.kode_prd, a.kode_tmp, a.kodecab, ( CASE WHEN b3.nama IS NULL THEN b1.nama + ',' + b2.nama ELSE b1.nama + ',' + b2.nama + ',' + b3.nama END ) AS auditor FROM checklist_entryhdr a LEFT JOIN mstauditor b3 ON a.auditor_3 = b3.nik, mstauditor b1, mstauditor b2 WHERE a.auditor_1 = b1.nik AND auditor_2 = b2.nik ) b ON a.kodecab = b.kodecab AND a.kode_tmp = b.kode_tmp AND a.kode_prd = b.kode_prd ) e WHERE e.fta_no = a.fta_no ) ELSE auditor END AS auditor FROM ( SELECT a.fta_no, a.tahun, a.kel_pemeriksa, a.kode_tmp, a.kodecab, b.class, b.region, a.kode_prd, c.nama_prd FROM ( SELECT a.kodecab, a.kode_prd, a.kode_tmp, a.fta_no, a.tahun, a.kel_pemeriksa FROM fta_checklist_hdr a INNER JOIN ( SELECT a.kodecab, status, Max(fta_no) AS fta_no_last, tahun FROM fta_checklist_hdr a INNER JOIN ( SELECT Max(tahun) last_tahun, kodecab, kel_pemeriksa FROM fta_checklist_hdr GROUP BY kodecab, kel_pemeriksa ) b ON a.kodecab = b.kodecab AND a.tahun = b.last_tahun AND a.kel_pemeriksa = b.kel_pemeriksa WHERE status = '2' AND " & param_kelompokpemeriksa & " GROUP BY a.kodecab, status, tahun ) b ON a.kodecab = b.kodecab AND a.fta_no = b.fta_no_last AND a.status = b.status ) a LEFT JOIN mstcab b ON a.kodecab = b.kodecab LEFT JOIN mst_periode c ON a.kode_prd = c.kode_prd ) a INNER JOIN ( SELECT fta_no, kode_prd, Count(1) AS [Follow_Up_Recommendation], Sum(CASE WHEN status = 1 THEN 1 ELSE 0 END) AS [Not_Implemented], Sum(CASE WHEN status = 2 THEN 1 ELSE 0 END) AS [Not_Applicable], Sum(CASE WHEN status = 3 THEN 1 ELSE 0 END) AS [In_Progress], Sum(CASE WHEN status = 4 THEN 1 ELSE 0 END) AS [Not_Due], Sum(CASE WHEN status = 5 THEN 1 ELSE 0 END) AS [Implemented], Sum( CASE WHEN status = 1 THEN 1 WHEN status = 3 THEN 1 WHEN status = 5 THEN 1 ELSE 0 END ) AS [Due_For_Implement] FROM fta_checklist_dtl where " & param_branch & " and " & param_periode & " and " & param_caseCat & " and " & param_finCat & " and " & param_status & " GROUP BY fta_no, Kode_Prd ) b ON a.fta_no = b.fta_no and a.Kode_Prd = b.Kode_Prd LEFT JOIN ( SELECT fta_no, tahun, ( CASE WHEN b3.nama IS NULL THEN b1.nama + ',' + b2.nama ELSE b1.nama + ',' + b2.nama + ',' + b3.nama END ) AS auditor FROM fta_checklist_auditor a LEFT JOIN mstauditor b3 ON a.auditor_3 = b3.nik, mstauditor b1, mstauditor b2 WHERE a.auditor_1 = b1.nik AND auditor_2 = b2.nik ) c ON a.fta_no = c.fta_no AND a.tahun = c.tahun ) a ) a ) a "
        End If

        Dim dt As DataTable = GetDataSql(sb)
        GridView1.DataSource = dt
        GridView1.DataBind()
        If dt.Rows.Count > 0 Then
            btnExportExcel.Visible = True
        End If
        lblMsg.Text = sb
    End Sub

    Protected Function GetDataSql(ByVal query As String) As DataTable
        Dim strConn As String = ConfigurationManager.ConnectionStrings("SQLICE").ConnectionString
        Try
            Using con As New SqlConnection(strConn)
                con.Open()
                Dim da As New SqlDataAdapter(query, con)
                Dim dt As New DataTable
                da.Fill(dt)
                con.Close()
                Return dt
            End Using
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Protected Sub btnFind_Click(sender As Object, e As EventArgs)
        Dim selected_branch As String = hdnSelectedBranch.Value
        Dim selected_caseCat As String = hdnSelectedCaseCategory.Value
        Dim selected_finCat As String = hdnSelectedFindingCategory.Value
        Dim selected_periode As String = hdnSelectedPeriode.Value
        Dim selected_status As String = hdnSelectedStatus.Value
        Dim selected_kelompokpemeriksa As String = hdnSelectedKelompokPemeriksa.Value

        Dim list_selected_branch As String() = selected_branch.Split(",")
        Dim list_selected_caseCat As String() = selected_caseCat.Split(",")
        Dim list_selected_finCat As String() = selected_finCat.Split(",")
        Dim list_selected_periode As String() = selected_periode.Split(",")
        Dim list_selected_status As String() = selected_status.Split(",")
        Dim list_selected_kelompokpemeriksa As String() = selected_kelompokpemeriksa.Split(",")

        Dim param_branch As String = ""
        Dim param_caseCat As String = ""
        Dim param_finCat As String = ""
        Dim param_periode As String = ""
        Dim param_status As String = ""
        Dim param_kelompokpemeriksa As String = ""

        If selected_branch = "%" Or selected_branch = "" Then
            param_branch = "kodecab like '%%'"
        Else
            param_branch = "kodecab in ('" & selected_branch.Replace(",", "','") & "')"
        End If

        If selected_caseCat = "%" Or selected_caseCat = "" Then
            param_caseCat = "case_category like '%%'"
        Else
            param_caseCat = "case_category in ('" & selected_caseCat.Replace(",", "','") & "')"
        End If

        If selected_finCat = "%" Or selected_finCat = "" Then
            param_finCat = "fin_category like '%%'"
        Else
            param_finCat = "fin_category in ('" & selected_finCat.Replace(",", "','") & "')"
        End If

        If selected_periode = "%" Or selected_periode = "" Then
            param_periode = "Kode_Prd like '%%'"
        Else
            param_periode = "Kode_Prd in ('" & selected_periode.Replace(",", "','") & "')"
        End If

        If selected_status = "%" Or selected_status = "" Then
            param_status = "status like '%%'"
        Else
            param_status = "status in ('" & selected_status.Replace(",", "','") & "')"
        End If

        If selected_kelompokpemeriksa = "%" Or selected_kelompokpemeriksa = "" Then
            param_kelompokpemeriksa = "a.kel_pemeriksa like '%%'"
        Else
            param_kelompokpemeriksa = "a.kel_pemeriksa in ('" & selected_kelompokpemeriksa.Replace(",", "','") & "')"
        End If

        'loadTable(param_branch, param_caseCat, param_finCat, param_periode, param_status, param_kelompokpemeriksa)
        loadRekap(param_branch, param_caseCat, param_finCat, param_periode, param_status, param_kelompokpemeriksa)
        'loadRekap(param_branch, param_caseCat, param_finCat, param_periode, param_kelompokpemeriksa)
    End Sub
End Class
