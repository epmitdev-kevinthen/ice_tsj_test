﻿Imports System.Data
Imports System.Data.SqlClient


Partial Class KelPemeriksa
    Inherits System.Web.UI.Page

    Public Shared ConnStrAudit As String = ConfigurationManager.ConnectionStrings("SQLICE").ConnectionString
    Public Shared cmd As New SqlCommand

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        If Not Page.IsPostBack Then
            userEdit.Text = Request.QueryString("kodekel")
            If userEdit.Text = "" Then
                tampilTable()
                loadApproverUser()
            Else
                tampilTable()
                tampil()
                'insertData.Style("display") = "none"
            End If

        End If
    End Sub

    Protected Sub loadApproverUser()
        ddlApprover1.Items.Add(" ")
        ddlApprover2.Items.Add(" ")
        Dim dt As DataTable = GetDataSql("select * from MSTUSER where jabatan in ('AUDITOR MGR','AUDITOR DEVELOPMENT')")
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                If dt.Rows(i).Item("jabatan") = "AUDITOR DEVELOPMENT" Then
                    ddlApprover1.Items.Add(New ListItem(dt.Rows(i).Item("fullusername"), dt.Rows(i).Item("username")))
                ElseIf dt.Rows(i).Item("jabatan") = "AUDITOR MGR" Then
                    ddlApprover2.Items.Add(New ListItem(dt.Rows(i).Item("fullusername"), dt.Rows(i).Item("username")))
                End If
            Next
        End If
    End Sub

    Public Shared Function GetDataSql(ByVal query As String) As DataTable
        Dim con As New SqlConnection(ConnStrAudit)
        Try
            con.Open()
            Dim da As New SqlDataAdapter(query, con)
            Dim dt As New DataTable
            da.Fill(dt)
            con.Close()
            Return dt
        Catch ex As Exception
            con.Close()
            Return Nothing
        End Try
    End Function

    Protected Sub tampilTable()
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                Dim da As New SqlDataAdapter("select * from MSTKELOMPOKPEMERIKSA order by kodekel", con)
                Dim dt As New DataTable
                da.Fill(dt)
                GridView1.DataSource = dt
                GridView1.DataBind()
                GridView1.HeaderRow.TableSection = TableRowSection.TableHeader
                con.Close()
            End Using
        Catch ex As Exception

        End Try

    End Sub
    Protected Sub GridView1_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        tampilTable()
    End Sub
    Protected Sub clearData_Click(sender As Object, e As EventArgs)
        Response.Redirect("KelPemeriksa.aspx")
    End Sub
    Protected Sub insertData_Click(sender As Object, e As EventArgs)
        Dim con As New SqlConnection(ConnStrAudit)
        Dim sqlstr As String = "insert into MSTKELOMPOKPEMERIKSA(kodekel, namakel, approver1, approver2, createdby, createdat, lasteditby, lasteditat) values('" & txtKodeKel.Text & "', '" & txtNamaKel.Text & "', '" & ddlApprover1.SelectedValue & "', '" & ddlApprover2.SelectedValue & "', '" & Session("sUsername") & "', current_timestamp, '" & Session("sUsername") & "', current_timestamp)"
        Try
            con.Open()
            cmd = New SqlCommand(sqlstr, con)
            cmd.ExecuteNonQuery()
            con.Close()

            div_lbl.Visible = True
            lbl_msg.Text = "Insert Successful!"
            tampilTable()
        Catch ex As Exception
            con.Close()
            div_lbl.Visible = True
            lbl_msg.Text = ex.Message.ToString
        End Try
    End Sub
    Protected Sub updateData_Click(sender As Object, e As EventArgs)
        Dim sqlstr As String = "update MSTKELOMPOKPEMERIKSA set kodekel = '" & txtKodeKel.Text & "', namakel = '" & txtNamaKel.Text & "', lasteditby = '" & Session("sUsername") & "', lasteditat = current_timestamp, approver1 = '" & ddlApprover1.SelectedValue & "', approver2 = '" & ddlApprover2.SelectedValue & "' where kodekel='" & txtKodeKel.Text & "'"
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                cmd = New SqlCommand(sqlstr, con)
                cmd.ExecuteNonQuery()
                con.Close()
            End Using

            div_lbl.Visible = True
            lbl_msg.Text = "Update Successful!"
            tampilTable()
        Catch ex As Exception
            div_lbl.Visible = True
            lbl_msg.Text = ex.Message.ToString
        End Try
    End Sub
    Protected Sub deleteData_Click(sender As Object, e As EventArgs)
        Dim sqlstr As String = "delete from MSTKELOMPOKPEMERIKSA where kodekel = '" & txtKodeKel.Text & "'"
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                cmd = New SqlCommand(sqlstr, con)
                cmd.ExecuteNonQuery()
                con.Close()
            End Using

            div_lbl.Visible = True
            lbl_msg.Text = "Delete Successful!"
            tampilTable()
        Catch ex As Exception
            div_lbl.Visible = True
            lbl_msg.Text = ex.Message.ToString
        End Try
    End Sub
    Protected Sub GridView1_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GridView1.RowCommand
        If e.CommandName = "cmdsubmit" Then
            Dim kode As String = e.CommandArgument
            Response.Redirect("KelPemeriksa.aspx?kodekel=" & kode & "")
        ElseIf e.CommandName = "cmddel" Then


        End If
    End Sub
    Protected Sub tampil()
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                Dim da As New SqlDataAdapter("select * from MSTKELOMPOKPEMERIKSA where kodekel = '" & userEdit.Text & "'", con)
                Dim dt As New DataTable
                da.Fill(dt)
                If dt.Rows.Count > 0 Then
                    txtKodeKel.Text = dt.Rows(0).Item("kodekel")
                    txtNamaKel.Text = dt.Rows(0).Item("namakel")
                    createdBy.InnerText = dt.Rows(0).Item("createdby")
                    createdDate.InnerText = dt.Rows(0).Item("createdat")
                    lastUpdateBy.InnerText = dt.Rows(0).Item("lasteditby")
                    lastUpdateDate.InnerText = dt.Rows(0).Item("lasteditat")
                End If
                con.Close()
            End Using
        Catch ex As Exception

        End Try

    End Sub
End Class
