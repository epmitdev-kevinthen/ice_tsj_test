﻿
Partial Class PHHasil
    Inherits System.Web.UI.Page
    Dim myChef As New Chef
    Dim j, i As Integer

    Private Sub hdrCheckList_Init(sender As Object, e As EventArgs) Handles Me.Init
        lblkodetmp.Text = Session("kodetmp")
        Dim myData As Data.DataSet
        Dim myhtml As New StringBuilder

        myData = myChef.NILAIHDR2Q(lblkodetmp.Text)
        If myData.Tables(0).Rows.Count > 0 Then
            lblnamatmp.Text = myData.Tables(0).Rows(0).Item("keterangan")
        End If

        myData = myChef.NILAIDTL2(lblkodetmp.Text)
        myhtml.Append(" <table id='mytable1' class='cell-border' cellspacing='0' width='100%' style=' margin: 0 auto; width: 100%; clear: both; border-collapse: collapse; table-layout: fixed; word-wrap:break-word; '> ")
        myhtml.Append("   <thead>")
        myhtml.Append("      <tr>")
        myhtml.Append("         <th>Nilai</th>")
        myhtml.Append("         <th></th>")
        myhtml.Append("         <th>Keterangan</th>")
        myhtml.Append("         <th></th>")
        myhtml.Append("      </tr>")
        myhtml.Append("   </thead>")
        myhtml.Append("   <tbody>")
        For i = 0 To myData.Tables(0).Rows.Count - 1
            myhtml.Append("      <tr>")

            Dim Temps As String = myData.Tables(0).Rows(i).Item("nilai") & "|" & myData.Tables(0).Rows(i).Item("keterangan") 

            myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("nilai") & "</td>")

            myhtml.Append("<td class='text-right'>")
            myhtml.Append("<button type='button' class='button2xx' data-book-id='" & Temps & "' data-target='#formModalEdit' data-toggle='modal'>Edit</button>")
            myhtml.Append("</td>")

            myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("keterangan") & "</td>")

            myhtml.Append("<td class='text-right'>")
            myhtml.Append("<button type='button' class='button2xx' data-book-id='" & Temps & "' data-target='#formModalDelete' data-toggle='modal'>Delete</button>")
            myhtml.Append("</td>")

            myhtml.Append("      </tr>")
        Next
        myhtml.Append("   </tbody>")
        myhtml.Append(" </table> ")

        myPlaceH.Controls.Add(New Literal() With {
          .Text = myhtml.ToString()
         })

    End Sub


    Private Sub btnInsert3_Click(sender As Object, e As EventArgs) Handles btnInsert3.Click

        Try

            infoerror.Text = ""

            Dim mpLabel As Label
            Dim myusername As String = ""
            mpLabel = CType(Master.FindControl("lblusername"), Label)
            If Not mpLabel Is Nothing Then
                myusername = mpLabel.Text
            End If

            Dim mytmpSTr As String = TxtNilai3.Text.ToUpper().Trim()
            If mytmpSTr <> "N/A" Then
                mytmpSTr = (Val(mytmpSTr)).ToString().Trim()
            End If

            infoerror.Text = myChef.INSNILAIDTL2(lblkodetmp.Text, mytmpSTr, TxtKet3.Text)
            Response.Redirect(HttpContext.Current.Request.Url.ToString(), True)
        Catch ex As Exception
            infoerror.Text = ex.Message
        End Try

    End Sub

    Private Sub btnDelete2_Click(sender As Object, e As EventArgs) Handles btnDelete2.Click


        Try

            infoerror.Text = ""
            infoerror.Text = myChef.DELNILAIDTL2(lblkodetmp.Text, TxtNilai2.Text)
            Response.Redirect(HttpContext.Current.Request.Url.ToString(), True)
        Catch ex As Exception
            infoerror.Text = ex.Message
        End Try


    End Sub

    Private Sub btnUpdate1_Click(sender As Object, e As EventArgs) Handles btnUpdate1.Click

        Try

            infoerror.Text = ""

            Dim mpLabel As Label
            Dim myusername As String = ""
            mpLabel = CType(Master.FindControl("lblusername"), Label)
            If Not mpLabel Is Nothing Then
                myusername = mpLabel.Text
            End If

            Dim mytmpSTr As String = TxtNilai.Text.ToUpper().Trim()
            If mytmpSTr <> "N/A" Then
                mytmpSTr = (Val(mytmpSTr)).ToString().Trim()
            End If

            infoerror.Text = myChef.UPDNILAIDTL2(lblkodetmp.Text, mytmpSTr, TxtKet.Text)
            Response.Redirect(HttpContext.Current.Request.Url.ToString(), True)
        Catch ex As Exception
            infoerror.Text = ex.Message
        End Try


    End Sub

End Class
