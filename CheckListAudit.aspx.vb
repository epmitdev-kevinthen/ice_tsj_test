﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI

Partial Class CheckListAudit
    Inherits System.Web.UI.Page
    Dim myChef As New Chef
    Dim myhtml As New StringBuilder

    Dim myhtml1 As New StringBuilder
    Public MyHTMLString As String

    Dim PublicPengali As String
    Public Shared strConn As String = ConfigurationManager.ConnectionStrings("SQLICE").ConnectionString
    Dim cmd As New SqlCommand

    Private Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click

        'Try
        Dim myData As Data.DataSet
        myData = myChef.dataNILAI(_kode.Text)
        If myData.Tables(0).Rows.Count > 0 Then
            PublicPengali = Val(myData.Tables(0).Rows(0).Item("pengali")).ToString
        Else
            If doBranch.SelectedValue = "PST" Then
                PublicPengali = "1"
            Else
                PublicPengali = "2"
            End If
        End If
        infoerror.Text = ""

        Dim mpLabel As Label
        Dim myusername As String = ""
        mpLabel = CType(Master.FindControl("lblusername"), Label)
        If Not mpLabel Is Nothing Then
            myusername = mpLabel.Text
        End If

        Dim check_nilai_kriteria As Boolean = checkPNilai(_kode.Text)
        Dim vdoStatus As String
        'If txtDone1.Text = "1" And txtDone2.Text = "1" Then
        If hdnDone1.Value = "N/A" Or hdnDone2.Value = "N/A" Then
            vdoStatus = "Not Applicable"
        ElseIf (check_nilai_kriteria And Integer.Parse(hdnDone1.Value) + Integer.Parse(hdnDone2.Value) = Integer.Parse(hdnMaxNilai.Value)) Or (Not check_nilai_kriteria And Integer.Parse(hdnDone1.Value) + Integer.Parse(hdnDone2.Value) = Integer.Parse(hdnMaxNilai.Value) * 2) Then
            vdoStatus = "OK"
        Else
            vdoStatus = "Not OK"
        End If

        If (hdnDone1.Value = "N/A" Or hdnDone2.Value = "N/A") And hdnDone1.Value <> hdnDone2.Value Then

            Dim message As String = "Done Control tidak boleh hanya salah satu yang statusnya TIDAK DINILAI"
            Dim sb As New System.Text.StringBuilder()
            sb.Append("<script type = 'text/javascript'>")
            sb.Append("window.onload=function(){")
            sb.Append("alert('")
            sb.Append(message)
            sb.Append("')};")
            sb.Append("</script>")
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "alert", sb.ToString())

        Else
            infoerror.Text = myChef.SQLchecklist_update(_kode.Text, TxtNo.Text, _cabang.Text,
                         _periode.Text, hdnDone1.Value, hdnDone2.Value, vdoStatus,
                         txtHasilAudit.Text, txtBuktiObyektif.Text, txtRecomendation.Text,
                         txtBranchFU.Text, doImplemenRecom.Text, myusername)

        End If

        'Catch ex As Exception
        '    infoerror.Text = ex.Message
        'End Try

        Call sub_refresh2()

    End Sub

    Protected Sub cekConfirmAuditor(ByVal kode_tmp As String, ByVal kodecab As String, ByVal kode_prd As String)
        Try
            Using con As New SqlConnection(strConn)
                con.Open()
                Dim da As New SqlDataAdapter("select auditor_1 from checklist_entryhdr where kode_tmp = '" & kode_tmp & "' and kodecab = '" & kodecab & "' and kode_prd = '" & kode_prd & "'", con)
                Dim dt As New DataTable
                da.Fill(dt)
                If dt.Rows.Count > 0 Then
                    btnSaveAuditor.Enabled = False
                Else
                    btnSaveAuditor.Enabled = True
                End If
                con.Close()
            End Using
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnFind_Click(sender As Object, e As EventArgs) Handles btnFind.Click
        If doBranch.Text <> "" And doTemplate.Text <> "" And doPeriode.Text <> "" Then
            _cabang.Text = doBranch.Text
            _kode.Text = doTemplate.Text
            _periode.Text = doPeriode.Text

            _statusFind.Text = doStatusFind.Text
            _entryFind.Text = doEntryFind.Text

            Call sub_Refresh1()

            ddlAuditor1.SelectedValue = " "
            ddlAuditor2.SelectedValue = " "
            ddlAuditor3.SelectedValue = " "

            cekConfirmAuditor(_kode.Text, _cabang.Text, _periode.Text)

            If btnSaveAuditor.Enabled = True Then
                Dim dt As DataTable = GetDataSql("select * from CHECKLIST_ENTRY_AUDITOR_TEMP where kode_tmp = '" & _kode.Text & "' and kodecab = '" & _cabang.Text & "' and Kode_Prd = '" & _periode.Text & "'")
                If dt.Rows.Count > 0 Then
                    Try
                        ddlAuditor1.SelectedValue = dt.Rows(0).Item("auditor_1")
                        ddlAuditor2.SelectedValue = dt.Rows(0).Item("auditor_2")
                        ddlAuditor3.SelectedValue = dt.Rows(0).Item("auditor_3")
                    Catch ex As Exception

                    End Try

                End If
            Else
                Dim dt As DataTable = GetDataSql("select * from checklist_entryhdr where kode_tmp = '" & _kode.Text & "' and kodecab = '" & _cabang.Text & "' and Kode_Prd = '" & _periode.Text & "'")
                If dt.Rows.Count > 0 Then
                    Try
                        ddlAuditor1.SelectedValue = dt.Rows(0).Item("auditor_1")
                        ddlAuditor2.SelectedValue = dt.Rows(0).Item("auditor_2")
                        ddlAuditor3.SelectedValue = dt.Rows(0).Item("auditor_3")
                    Catch ex As Exception

                    End Try

                End If
            End If





            infoerror1.Text = ""
        Else
            infoerror1.Text = "Kriteria tidak boleh ada yang kosong"
        End If
    End Sub


    'Private Sub sub_Refresh()
    '    Dim myData As Data.DataSet
    '    Dim myhtml As New StringBuilder

    '    myData = myChef.SQLCHECKLIST
    '    myhtml.Append(" <table id='mytable' class='stripe row-border order-column' cellspacing='0' width='100%'> ")
    '    myhtml.Append("   <thead>")
    '    myhtml.Append("      <tr>")
    '    myhtml.Append("         <th rowspan='2'>No</th>")
    '    myhtml.Append("         <th colspan='3'>Standar</th>")
    '    myhtml.Append("         <th colspan='3'>1st Line Defence</th>")
    '    myhtml.Append("         <th colspan='3'>2nd Line Defence</th>")
    '    myhtml.Append("         <th rowspan='2'>Element Control</th>")
    '    myhtml.Append("         <th rowspan='2'>Done Control</th>")
    '    myhtml.Append("         <th rowspan='2'>% Done</th>")
    '    myhtml.Append("         <th colspan='7'>Working Paper</th>")
    '    myhtml.Append("      </tr>")
    '    myhtml.Append("      <tr>")
    '    myhtml.Append("         <th>ISO</th>")
    '    myhtml.Append("         <th>Policy</th>")
    '    myhtml.Append("         <th>Lainnya</th>")
    '    myhtml.Append("         <th>Activity</th>")
    '    myhtml.Append("         <th>PIC</th>")
    '    myhtml.Append("         <th>Done Control</th>")
    '    myhtml.Append("         <th>Activity</th>")
    '    myhtml.Append("         <th>PIC</th>")
    '    myhtml.Append("         <th>Done Control</th>")
    '    myhtml.Append("         <th>Status</th>")
    '    myhtml.Append("         <th>Hasil Audit</th>")
    '    myhtml.Append("         <th>Bukti Objektif</th>")
    '    myhtml.Append("         <th>Recommendation</th>")
    '    myhtml.Append("         <th>Branches Follow-Up</th>")
    '    myhtml.Append("         <th>Implementasi Recommendation</th>")
    '    myhtml.Append("         <th>% Done</th>")
    '    myhtml.Append("      </tr>")
    '    myhtml.Append("   </thead>")
    '    myhtml.Append("   <tbody>")
    '    myhtml.Append("   </tbody>")
    '    myhtml.Append(" </table> ")

    '    myPlaceH.Controls.Add(New Literal() With {
    '      .Text = myhtml.ToString()
    '     })
    'End Sub

    Private Sub CheckListAudit_Init(sender As Object, e As EventArgs) Handles Me.Init
        If Not Page.IsPostBack Then
            Dim myData As Data.DataSet
            Dim i As Integer
            Dim j As Integer

            Dim MyDT As DateTime = Now
            Dim MyDTString As String

            If Request.QueryString("a") <> "" Then
                myData = myChef.ICE_LOG_LOGIN_SEL(Request.QueryString("a"))
                If myData.Tables(0).Rows.Count > 0 Then
                    sUsername_.Text = myData.Tables(0).Rows(0).Item(1)
                    levelakses_.Text = myData.Tables(0).Rows(0).Item(2)
                    jabatan_.Text = myData.Tables(0).Rows(0).Item(3)
                    sCabang_.Text = myData.Tables(0).Rows(0).Item(4)
                    kodedept_.Text = myData.Tables(0).Rows(0).Item(5)
                Else
                    Response.Redirect("Default.aspx")
                End If
            ElseIf Session("sUsername") Is Nothing Then
                Response.Redirect("Default.aspx")
            Else
                MyDTString = Session("sUsername") & MyDT.ToString("MMddyyyyHHmmss")
                myChef.ICE_LOG_LOGIN_INS(MyDTString, Session("sUsername"), Session("levelakses"), Session("jabatan"), Session("sCabang"), Session("kodedept"))
                Response.Redirect("CheckListAudit.aspx?a=" & MyDTString)
            End If

            updatepilhannilai(_kode.Text)
            hdnPengali.Value = PublicPengali
            'If Not IsPostBack Then

            'If sUsername_.Text = "" Then
            '    If Session("sUsername") Is Nothing Then
            '        Response.Redirect("Default.aspx")
            '    Else
            '        sUsername_.Text = Session("sUsername")
            '        levelakses_.Text = Session("levelakses")
            '        jabatan_.Text = Session("jabatan")
            '        sCabang_.Text = Session("sCabang")
            '        kodedept_.Text = Session("kodedept")
            '    End If
            'End If

            'End If

            '        If Session("levelakses") <> "1" And Session("levelakses") <> "2" Then
            If levelakses_.Text = "1" Or levelakses_.Text = "2" Or myChef.SQLJABATANUNAPPRV(jabatan_.Text) Then
                btnUnconfirm.Enabled = True
            Else
                btnUnconfirm.Enabled = False
            End If
            If levelakses_.Text = "1" Or levelakses_.Text = "2" Or myChef.SQLJABATANAPPRV(jabatan_.Text) Then
                btnConfirm.Enabled = True
            Else
                btnConfirm.Enabled = False
            End If



            txtDone1.Attributes.Add("onchange", "DropOnChange1();")
            txtDone2.Attributes.Add("onchange", "DropOnChange2();")


            myData = myChef.SQLLISTTEMPLATE
            j = 1
            doTemplate.Items.Add("")
            For i = 0 To myData.Tables(0).Rows.Count - 1

                If (myData.Tables(0).Rows(i).Item("flag_selasses") = "Y" And sCabang_.Text <> "PST") Or
                sCabang_.Text = "PST" Then

                    'myData.Tables(0).Rows(i).Item("kodedept") = "" Or
                    '    If UCase(sUsername_.Text) = "SUPERADMIN" Or
                    '    kodedept_.Text = "3501" Or
                    'myData.Tables(0).Rows(i).Item("kodedept") = kodedept_.Text Then

                    doTemplate.Items.Add(myData.Tables(0).Rows(i).Item("nama_tmp"))
                    doTemplate.Items(j).Value = myData.Tables(0).Rows(i).Item("kode")
                    j = j + 1

                    'End If
                End If
            Next
            myData = myChef.SQLMSTPERIODEAKTIF
            j = 1
            doPeriode.Items.Add("")
            For i = 0 To myData.Tables(0).Rows.Count - 1

                doPeriode.Items.Add(myData.Tables(0).Rows(i).Item("nama_prd"))
                doPeriode.Items(j).Value = myData.Tables(0).Rows(i).Item("kode_prd")
                j = j + 1

            Next
            myData = myChef.SQLLISTCAB
            j = 1
            doBranch.Items.Add("")
            For i = 0 To myData.Tables(0).Rows.Count - 1

                If sCabang_.Text = "PST" Or sCabang_.Text = myData.Tables(0).Rows(i).Item("kodecab") Then

                    doBranch.Items.Add(myData.Tables(0).Rows(i).Item("namacab"))
                    doBranch.Items(j).Value = myData.Tables(0).Rows(i).Item("kodecab")
                    j = j + 1

                End If
            Next

            loadAuditor()

            If Session("kodedept") <> "3501" And Session("sUsername") <> "SUPERADMIN" Then
                divAuditor.Visible = False
            End If

            'txtDone1.Items.Add("Belum Dijalankan")
            'txtDone1.Items(0).Value = "0"
            'txtDone1.Items.Add("Sudah Dijalankan")
            'txtDone1.Items(1).Value = "1"
            'txtDone1.Items.Add("Tidak Dinilai")
            'txtDone1.Items(2).Value = "N/A"
            'txtDone2.Items.Add("Belum Dijalankan")
            'txtDone2.Items(0).Value = "0"
            'txtDone2.Items.Add("Sudah Dijalankan")
            'txtDone2.Items(1).Value = "1"
            'txtDone2.Items.Add("Tidak Dinilai")
            'txtDone2.Items(2).Value = "N/A"


            'txtDone1__1.Items.Add("Belum Dijalankan")
            'txtDone1__1.Items(0).Value = "0"
            'txtDone1__1.Items.Add("Sudah Dijalankan")
            'txtDone1__1.Items(1).Value = "1"
            'txtDone1__1.Items.Add("Tidak Dinilai")
            'txtDone1__1.Items(2).Value = "N/A"
            'txtDone2__2.Items.Add("Belum Dijalankan")
            'txtDone2__2.Items(0).Value = "0"
            'txtDone2__2.Items.Add("Sudah Dijalankan")
            'txtDone2__2.Items(1).Value = "1"
            'txtDone2__2.Items.Add("Tidak Dinilai")
            'txtDone2__2.Items(2).Value = "N/A"


            'doStatus.Items.Add("")
            'doStatus.Items.Add("OK")
            'doStatus.Items.Add("Not OK")
            'doStatus.Items.Add("Not Applicable")
            'doStatus.Items.Add("Observasi")

            doVerifikasi_Audit_Status__5.Items.Add("")
            doVerifikasi_Audit_Status__5.Items.Add("OK")
            doVerifikasi_Audit_Status__5.Items.Add("Not OK")




            doImplemenRecom.Items.Add("")
            doImplemenRecom.Items.Add("Done")
            doImplemenRecom.Items.Add("On Progress")
            doImplemenRecom.Items.Add("Not Follow-Up")
            doImplemenRecom.Items.Add("Not Applicable")


            doImplemenRecom__3.Items.Add("")
            doImplemenRecom__3.Items.Add("Done")
            doImplemenRecom__3.Items.Add("On Progress")
            doImplemenRecom__3.Items.Add("Not Follow-Up")
            doImplemenRecom__3.Items.Add("Not Applicable")


            doStatusFind.Items.Add("")
            doStatusFind.Items.Add("OK")
            doStatusFind.Items.Add("Not Applicable")
            doStatusFind.Items.Add("Not OK")

            doEntryFind.Items.Add("")
            doEntryFind.Items.Add("Complete")
            doEntryFind.Items.Add("InComplete")

            'Call sub_Refresh1()
        End If

    End Sub

    Protected Sub loadAuditor()
        ddlAuditor1.Items.Add(" ")
        ddlAuditor2.Items.Add(" ")
        ddlAuditor3.Items.Add(" ")

        Try
            Using con As New SqlConnection(strConn)
                con.Open()
                Dim da As New SqlDataAdapter("select * from mstauditor order by nama", con)
                Dim dt As New DataTable
                da.Fill(dt)
                If dt.Rows.Count > 0 Then
                    For i = 0 To dt.Rows.Count - 1
                        ddlAuditor1.Items.Add(New ListItem(dt.Rows(i).Item("nama"), dt.Rows(i).Item("nik")))
                        ddlAuditor2.Items.Add(New ListItem(dt.Rows(i).Item("nama"), dt.Rows(i).Item("nik")))
                        ddlAuditor3.Items.Add(New ListItem(dt.Rows(i).Item("nama"), dt.Rows(i).Item("nik")))
                    Next
                End If
                con.Close()
            End Using
        Catch ex As Exception

        End Try
    End Sub

    Private Sub myaddstr(ByVal mystr As String)
        myhtml.Append(mystr)
        myhtml1.Append(mystr)

    End Sub

    Private Sub sub_Refresh1()
        Dim myData As Data.DataSet
        '    Dim myData1 As Data.DataSet

        Dim i As Integer
        Dim myrownum As Integer = 0

        Dim vEntryDoneControl As Integer = 0

        updatepilhannilai(_kode.Text)
        hdnPengali.Value = PublicPengali
        Dim checkConf As Boolean = myChef.SQLCONFIRMCHCK(_kode.Text, _periode.Text, _cabang.Text)

        Dim vflag_selasses As String = myChef.SQLCHECKLIST_SELASSES(_kode.Text)

        'myData = myChef.SQLCHECKLIST

        myhtml.Clear()
        myhtml1.Clear()

        myhtml.Append(" <table id='mytable' class='cell-border table table-striped table-bordered' cellspacing='0' width='100%'> ")
        myhtml1.Append("<div  aria-hidden='true' style='display: none '>")
        myhtml1.Append(" <table id='mytable1' cellspacing='0' width='100%'> ")
        myaddstr("   <thead>")
        myaddstr("      <tr>")
        myhtml.Append("         <th rowspan='2'></th>")
        myaddstr("         <th rowspan='2'>No</th>")
        myaddstr("         <th colspan='3'>Standar</th>")

        'myaddstr("         <th colspan='4'>1st Line Defence</th>")

        myhtml1.Append("         <th colspan='3'>1st Line Defence</th>")
        myhtml.Append("         <th colspan='4'>1st Line Defence</th>")

        myaddstr("         <th colspan='3'>2nd Line Defence</th>")
        myhtml.Append("         <th rowspan='2'></th>")
        myaddstr("         <th rowspan='2'>Element Control</th>")
        If PublicPengali = 1 Then
            myaddstr("         <th rowspan='2'>Done Control</th>")
        Else
            myaddstr("         <th rowspan='2'>Total Poin</br>Done Control</th>")
        End If
        myaddstr("         <th rowspan='2'>% Done</th>")

        'myaddstr("         <th colspan='7'>Working Paper</th>")

        myhtml1.Append("         <th colspan='3'>Working Paper</th>")
        myhtml.Append("         <th colspan='3'>Working Paper</th>")

        myhtml.Append("         <th rowspan='2'></th>")

        If vflag_selasses = "Y" And checkConf Then
            myhtml.Append("         <th rowspan='2'></th>")
            myaddstr("         <th rowspan='2'></th>")
            myaddstr("         <th rowspan='2'></th>")

        End If

        myaddstr("      </tr>")
        myaddstr("      <tr>")
        myaddstr("         <th>ISO</th>")
        myaddstr("         <th>Policy</th>")
        myaddstr("         <th>Lainnya</th>")
        myaddstr("         <th>Activity</th>")
        myaddstr("         <th>PIC</th>")
        If PublicPengali = 1 Then
            myaddstr("         <th>Done Control</th>")
        Else
            myaddstr("         <th>Poin Done</br>Control</th>")
        End If

        myhtml.Append("         <th></th>")
        myaddstr("         <th>Activity</th>")
        myaddstr("         <th>PIC</th>")
        If PublicPengali = 1 Then
            myaddstr("         <th>Done Control</th>")
        Else
            myaddstr("         <th>Poin Done</br>Control</th>")
        End If
        myaddstr("         <th>Status</th>")
        myaddstr("         <th>Hasil Audit</th>")
        myaddstr("         <th>Bukti Objektif</th>")
        myhtml.Append("         <th></th>")
        myaddstr("         <th class='large-width'>Guidance</th>")
        myaddstr("      </tr>")
        myaddstr("   </thead>")
        myaddstr("   <tbody>")

        Dim MyNo As String = ""
        Dim sql_doneChecklist As String = "select * from checklist_entry  " &
            "where kode_tmp='" & _kode.Text & "' and kodecab='" & _cabang.Text & "' and kode_prd='" & _periode.Text & "' and chk_status<>'' and chk_status is not null  " &
            " and done_maincontrol is not null and done_secondcontrol is not null and ((((Bukti_Obyektif <> '' and Bukti_Obyektif is not null) or (Bukti_Obyektif_2 <> '' and Bukti_Obyektif_2 is not null)) and ((Hasil_Audit <> '' and Hasil_Audit is not null) or (Hasil_Audit_2 <> '' and Hasil_Audit_2 is not null)) and chk_status <> 'Not Applicable') or chk_status =  'Not Applicable')"

        Dim sql_jabatan_audit As Boolean = myChef.SQLJABATANAUDIT(jabatan_.Text)
        Dim check_nilai_kriteria As Boolean = checkPNilai(_kode.Text)
        myData = myChef.SQLCHECKLISTDTL_NEW_NEW(_kode.Text, _cabang.Text, _periode.Text, _statusFind.Text, _entryFind.Text)

        Dim pic1_2 As String = "%"

        If levelakses_.Text <> "1" And levelakses_.Text <> "2" And Not myChef.SQLJABATANAPPRV(jabatan_.Text) And Not Session("sUsername") = "SUPERADMIN" Then
            pic1_2 = Session("kodejabatan").ToString()
        End If

        Dim strsql_mainActivity As String = "select * " &
"from ( " &
"	select * " &
"	from TMP_CHECKLIST_DTL  " &
"	where typehd = 'H' " &
"	and no_parent = '' " &
"	and kode = '" & _kode.Text & "' " &
") hdr " &
"where hdr.no in( " &
"		select a.no_parent " &
"		from " &
"		( " &
"		select distinct kode, no, no_parent " &
"		from TMP_CHECKLIST_DTL  " &
"		where typehd = 'H' " &
"		and no_parent <> '' " &
"		and kode = '" & _kode.Text & "' " &
"		)a, " &
"		( " &
"		select distinct kode, no_parent " &
"		from TMP_CHECKLIST_DTL  " &
"		where 1=1 " &
"		and typehd = 'D' " &
"		and pic1 <> '' " &
"		and kode = '" & _kode.Text & "' " &
"		and (pic1 like '" & pic1_2 & "' or pic2 like '" & pic1_2 & "') " &
"		)b " &
"		where a.kode = b.kode " &
"		and a.no = b.no_parent " &
")  "

        Dim strsql_subActivity As String = "select distinct a.no_parent " &
"from TMP_CHECKLIST_DTL a " &
"where 1=1 " &
"and typehd = 'D' " &
"and pic1 <> '' " &
"and kode = '" & _kode.Text & "' " &
"and (a.pic1 like '" & pic1_2 & "' or a.pic2 like '" & pic1_2 & "') "

        Dim dt_MainActivity As DataTable = GetDataSql(strsql_mainActivity)
        Dim dt_SubActivity As DataTable = GetDataSql(strsql_subActivity)

        Dim dt_kriteria_penilaian As DataTable = GetDataSql("select * from MST_KRITERIA_PENILAIAN_DETAIL")
        Dim dt_jabatan_induk As DataTable = GetDataSql("select * from jabatan_induk_v")
        Dim dt_DoneChecklist As DataTable = GetDataSql(sql_doneChecklist)
        For i = 0 To myData.Tables(0).Rows.Count - 1
            Dim sql_jabatan_induk_pica1entry As Boolean = False
            Dim sql_jabatan_induk_pica2entry As Boolean = False
            Dim sql_no_mainActivity As Boolean = False
            Dim sql_no_subActivity As Boolean = False

            Dim qry_pica1entry = From dr As DataRow In dt_jabatan_induk.AsEnumerable()
                                 Where dr.Field(Of String)("jabatan_induk") = myData.Tables(0).Rows(i).Item("PICA1ENTRY") And dr.Field(Of String)("jabatan") = jabatan_.Text
                                 Select dr

            Dim qry_pica2entry = From dr As DataRow In dt_jabatan_induk.AsEnumerable()
                                 Where dr.Field(Of String)("jabatan_induk") = myData.Tables(0).Rows(i).Item("PICA2ENTRY") And dr.Field(Of String)("jabatan") = jabatan_.Text
                                 Select dr

            Dim qry_no_mainActivity = From dr As DataRow In dt_MainActivity.AsEnumerable()
                                      Where dr.Field(Of String)("no") = myData.Tables(0).Rows(i).Item("no")
                                      Select dr

            Dim qry_no_subActivity = From dr As DataRow In dt_SubActivity.AsEnumerable()
                                     Where dr.Field(Of String)("no_parent") = myData.Tables(0).Rows(i).Item("no")
                                     Select dr

            If qry_pica1entry.Any() Then
                sql_jabatan_induk_pica1entry = True
            End If

            If qry_pica2entry.Any() Then
                sql_jabatan_induk_pica2entry = True
            End If

            If qry_no_mainActivity.Any() Then
                sql_no_mainActivity = True
            End If

            If qry_no_subActivity.Any() Then
                sql_no_subActivity = True
            End If

            If vflag_selasses = "N" Or (vflag_selasses = "Y" And (myData.Tables(0).Rows(i).Item("PICA1ENTRY").ToString() = jabatan_.Text Or
                    sql_jabatan_induk_pica1entry Or (myData.Tables(0).Rows(i).Item("typeHD") = "H" And (sql_no_mainActivity Or sql_no_subActivity)) Or sql_jabatan_audit) Or levelakses_.Text = "1" Or levelakses_.Text = "2" Or myChef.SQLJABATANAPPRV(jabatan_.Text)) Or (vflag_selasses = "Y" And (myData.Tables(0).Rows(i).Item("PICA2ENTRY").ToString() = jabatan_.Text Or
                    sql_jabatan_induk_pica2entry Or (myData.Tables(0).Rows(i).Item("typeHD") = "H" And (sql_no_mainActivity Or sql_no_subActivity)) Or sql_jabatan_audit) Or levelakses_.Text = "1" Or levelakses_.Text = "2" Or myChef.SQLJABATANAPPRV(jabatan_.Text)) Then

                If check_nilai_kriteria Then
                    PublicPengali = getMaxNilai(myData.Tables(0).Rows(i).Item("Kode_Pilihan_Nilai1").ToString, myData.Tables(0).Rows(i).Item("Kode_Pilihan_Nilai2").ToString, dt_kriteria_penilaian)
                End If

                MyNo = myData.Tables(0).Rows(i).Item("no")
                vEntryDoneControl = 0



                If myData.Tables(0).Rows(i).Item("typeHD") = "H" Then
                    myrownum = 0
                Else
                    'myData1 = myChef.SQLchecklist_entry(_kode.Text, myData.Tables(0).Rows(i).Item("no"), _cabang.Text, _periode.Text)
                    If IsDBNull(myData.Tables(0).Rows(i).Item("kode_tmp")) Then
                        myrownum = 0
                    Else
                        myrownum = 1
                    End If
                End If



                myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("no") & "</td>")
                myaddstr("         <td>" & myData.Tables(0).Rows(i).Item("no_report") & "</td>")
                If myData.Tables(0).Rows(i).Item("std_iso") = "Y" Then

                    If myData.Tables(0).Rows(i).Item("dir_iso") = "" Then
                        myaddstr("         <td>" & myData.Tables(0).Rows(i).Item("std_iso") & "</td>")
                    Else
                        myaddstr("         <td><a href=" & Chr(34) & "#" & Chr(34) & " onclick=" & Chr(34) & "openw('" & myData.Tables(0).Rows(i).Item("dir_ISO") & "');return false;" & Chr(34) & ">" & myData.Tables(0).Rows(i).Item("std_ISO") & "</a></td>")
                    End If

                Else
                    myaddstr("         <td></td>")
                End If
                If myData.Tables(0).Rows(i).Item("std_policy") = "Y" Then

                    If myData.Tables(0).Rows(i).Item("dir_policy") = "" Then
                        myaddstr("         <td>" & myData.Tables(0).Rows(i).Item("std_policy") & "</td>")
                    Else
                        myaddstr("         <td><a href=" & Chr(34) & "#" & Chr(34) & " onclick=" & Chr(34) & "openw('" & myData.Tables(0).Rows(i).Item("dir_policy") & "');return false;" & Chr(34) & ">" & myData.Tables(0).Rows(i).Item("std_policy") & "</a></td>")
                    End If

                Else
                    myaddstr("         <td></td>")
                End If
                If myData.Tables(0).Rows(i).Item("std_lain") = "Y" Then

                    'myaddstr("         <td>" & myData.Tables(0).Rows(i).Item("std_lain") & "</td>")

                    If myData.Tables(0).Rows(i).Item("dir_lain") = "" Then
                        myaddstr("         <td>" & myData.Tables(0).Rows(i).Item("std_lain") & "</td>")
                    Else
                        myaddstr("         <td><a href=" & Chr(34) & "#" & Chr(34) & " onclick=" & Chr(34) & "openw('" & myData.Tables(0).Rows(i).Item("dir_lain") & "');return false;" & Chr(34) & ">" & myData.Tables(0).Rows(i).Item("std_lain") & "</a></td>")
                    End If

                Else
                    myaddstr("         <td></td>")
                End If

                myaddstr("         <td>" & myData.Tables(0).Rows(i).Item("activity1") & "</td>")
                myaddstr("         <td>" & myData.Tables(0).Rows(i).Item("PICA1") & "</td>")
                If myrownum = 0 Then
                    myaddstr("         <td></td>")
                    vEntryDoneControl = vEntryDoneControl + 1
                Else
                    myaddstr("         <td>" & myData.Tables(0).Rows(i).Item("Done_MainControl") & "</td>")
                    If Not IsDBNull(myData.Tables(0).Rows(i).Item("Done_MainControl")) Then
                        vEntryDoneControl = vEntryDoneControl + 100
                    End If
                End If

                Dim Temps As String
                If myrownum = 0 Then
                    Temps = _kode.Text & "|" & myData.Tables(0).Rows(i).Item("no") & "|" &
                        _cabang.Text & "|" & _periode.Text & "|" &
                        "||||||||" &
                    myData.Tables(0).Rows(i).Item("Activity1") & "|" &
                    myData.Tables(0).Rows(i).Item("PICA1") & "|" &
                    myData.Tables(0).Rows(i).Item("Activity2") & "|" &
                    myData.Tables(0).Rows(i).Item("PICA2") & "||||||" &
                    myData.Tables(0).Rows(i).Item("Kode_Pilihan_Nilai1") & "|" &
                    myData.Tables(0).Rows(i).Item("Kode_Pilihan_Nilai2") & "|" &
                    PublicPengali

                Else
                    Temps = _kode.Text & "|" & myData.Tables(0).Rows(i).Item("no") & "|" &
                        _cabang.Text & "|" & _periode.Text & "|" &
                        myData.Tables(0).Rows(i).Item("Done_MainControl") & "|" &
                        myData.Tables(0).Rows(i).Item("Done_SecondControl") & "|" &
                        myData.Tables(0).Rows(i).Item("chk_status") & "|" &
                        myData.Tables(0).Rows(i).Item("hasil_audit") & "|" &
                        myData.Tables(0).Rows(i).Item("bukti_obyektif") & "|" &
                        myData.Tables(0).Rows(i).Item("Recommendation") & "|" &
                        myData.Tables(0).Rows(i).Item("Branch_FU") & "|" &
                        myData.Tables(0).Rows(i).Item("Implemen_Recom") & "|" &
                    myData.Tables(0).Rows(i).Item("Activity1") & "|" &
                    myData.Tables(0).Rows(i).Item("PICA1") & "|" &
                    myData.Tables(0).Rows(i).Item("Activity2") & "|" &
                    myData.Tables(0).Rows(i).Item("PICA2") & "|" &
                    myData.Tables(0).Rows(i).Item("verifikasi_audit_status") & "|" &
                    myData.Tables(0).Rows(i).Item("verifikasi_audit_remark") & "|" &
                    myData.Tables(0).Rows(i).Item("hasil_audit_2") & "|" &
                        myData.Tables(0).Rows(i).Item("bukti_obyektif_2") & "|" &
                        myData.Tables(0).Rows(i).Item("Branch_FU_2") & "|" &
                        myData.Tables(0).Rows(i).Item("Kode_Pilihan_Nilai1") & "|" &
                        myData.Tables(0).Rows(i).Item("Kode_Pilihan_Nilai2") & "|" &
                        PublicPengali

                End If

                Dim Temps_1 As String = Temps

                If myData.Tables(0).Rows(i).Item("typeHD") = "H" Or checkConf Then
                    myhtml.Append("         <td></td>")
                ElseIf vflag_selasses = "Y" Then
                    If myData.Tables(0).Rows(i).Item("PICA1ENTRY") = jabatan_.Text Or
                        sql_jabatan_induk_pica1entry Then

                        'If IsDBNull(myData.Tables(0).Rows(i).Item("verifikasi_audit_status")) Then
                        '    myhtml.Append("<td class='text-right'>")
                        '    myhtml.Append("<button type='button' class='button2xx' data-book-id='" & Temps_1 & "' data-target='#formSelfReg01' data-toggle='modal'>Update</button>")
                        '    myhtml.Append("</td>")
                        'ElseIf myData.Tables(0).Rows(i).Item("verifikasi_audit_status") <> "OK" Then
                        '    myhtml.Append("<td class='text-right'>")
                        '    myhtml.Append("<button type='button' class='button2xx' data-book-id='" & Temps_1 & "' data-target='#formSelfReg01' data-toggle='modal'>Update</button>")
                        '    myhtml.Append("</td>")
                        'Else
                        '    myhtml.Append("         <td></td>")
                        'End If

                        If IsDBNull(myData.Tables(0).Rows(i).Item("verifikasi_audit_status")) OrElse myData.Tables(0).Rows(i).Item("verifikasi_audit_status").ToString() <> "OK" Then
                            myhtml.Append("<td class='text-right'>")
                            myhtml.Append("<button type='button' class='button2xx' data-book-id='" & Temps_1 & "' data-target='#formSelfReg01' data-toggle='modal'>Update</button>")
                            myhtml.Append("</td>")
                        Else
                            myhtml.Append("         <td></td>")
                        End If
                    Else
                        myhtml.Append("         <td></td>")
                    End If
                Else
                    myhtml.Append("         <td></td>")
                End If



                myaddstr("         <td>" & myData.Tables(0).Rows(i).Item("activity2") & "</td>")
                myaddstr("         <td>" & myData.Tables(0).Rows(i).Item("PICA2") & "</td>")

                If myrownum = 0 Then
                    myaddstr("         <td></td>")
                    vEntryDoneControl = vEntryDoneControl + 1
                Else
                    myaddstr("         <td>" & myData.Tables(0).Rows(i).Item("Done_SecondControl") & "</td>")
                    If Not IsDBNull(myData.Tables(0).Rows(i).Item("Done_SecondControl")) Then
                        vEntryDoneControl = vEntryDoneControl + 100
                    End If
                End If


                If myData.Tables(0).Rows(i).Item("typeHD") = "H" Or checkConf Then
                    myhtml.Append("         <td></td>")
                ElseIf vflag_selasses = "Y" Then
                    If myrownum <> 0 Then
                        If myData.Tables(0).Rows(i).Item("PICA2ENTRY") = jabatan_.Text Or
                            sql_jabatan_induk_pica2entry Then

                            'If IsDBNull(myData.Tables(0).Rows(i).Item("verifikasi_audit_status")) Then
                            '    myhtml.Append("<td class='text-right'>")
                            '    myhtml.Append("<button type='button' class='button2xx' data-book-id='" & Temps & "' data-target='#formModalEdit_2' data-toggle='modal'>Update</button>")
                            '    myhtml.Append("</td>")
                            'ElseIf myData.Tables(0).Rows(i).Item("verifikasi_audit_status") <> "OK" Then
                            '    myhtml.Append("<td class='text-right'>")
                            '    myhtml.Append("<button type='button' class='button2xx' data-book-id='" & Temps & "' data-target='#formModalEdit_2' data-toggle='modal'>Update</button>")
                            '    myhtml.Append("</td>")
                            'Else
                            '    myhtml.Append("         <td></td>")
                            'End If

                            If IsDBNull(myData.Tables(0).Rows(i).Item("verifikasi_audit_status")) OrElse myData.Tables(0).Rows(i).Item("verifikasi_audit_status").ToString() <> "OK" Then
                                myhtml.Append("<td class='text-right'>")
                                myhtml.Append("<button type='button' class='button2xx' data-book-id='" & Temps & "' data-target='#formModalEdit_2' data-toggle='modal'>Update</button>")
                                myhtml.Append("</td>")
                            Else
                                myhtml.Append("         <td></td>")
                            End If

                        Else
                            myhtml.Append("         <td></td>")
                        End If
                    Else
                        myhtml.Append("         <td></td>")
                    End If
                Else
                    myhtml.Append("<td class='text-right'>")
                    myhtml.Append("<button type='button' class='button2xx' data-book-id='" & Temps & "' data-target='#formModalEdit' data-toggle='modal'>Update</button>")
                    myhtml.Append("</td>")
                End If


                If myrownum = 0 Then
                    ' myaddstr("         <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>")
                    myhtml1.Append("         <td></td><td></td><td></td><td></td><td></td><td></td>")
                    myhtml.Append("         <td></td><td></td><td></td><td></td><td></td><td></td>")
                Else
                    Dim vEleControl As Integer = 0
                    Dim vDoneControl As Integer = 0
                    Dim vDonePresent As Double = 0
                    Dim tmpEleControl As String
                    Dim tmpDoneControl As String
                    Dim tmpDonePersent As String
                    If IsDBNull(myData.Tables(0).Rows(i).Item("Done_MainControl")) Then
                        vEleControl = vEleControl + 1
                    Else
                        If myData.Tables(0).Rows(i).Item("Done_MainControl") <> "N/A" Then
                            vEleControl = vEleControl + 1
                        End If
                    End If
                    If IsDBNull(myData.Tables(0).Rows(i).Item("Done_SecondControl")) Then
                        vEleControl = vEleControl + 1
                    Else
                        If myData.Tables(0).Rows(i).Item("Done_SecondControl") <> "N/A" Then
                            vEleControl = vEleControl + 1
                        End If
                    End If
                    If Not IsDBNull(myData.Tables(0).Rows(i).Item("Done_MainControl")) Then
                        'If myData.Tables(0).Rows(i).Item("Done_MainControl") = "1" Then
                        'If myData.Tables(0).Rows(i).Item("Done_MainControl") = PublicPengali Then
                        '    vDoneControl = vDoneControl + 1
                        'End If
                        vDoneControl = vDoneControl + Val(myData.Tables(0).Rows(i).Item("Done_MainControl"))
                    End If
                    If Not IsDBNull(myData.Tables(0).Rows(i).Item("Done_SecondControl")) Then
                        'If myData.Tables(0).Rows(i).Item("Done_SecondControl") = "1" Then
                        'If myData.Tables(0).Rows(i).Item("Done_SecondControl") = PublicPengali Then
                        '    vDoneControl = vDoneControl + 1
                        'End If
                        vDoneControl = vDoneControl + Val(myData.Tables(0).Rows(i).Item("Done_SecondControl"))
                    End If

                    If vEleControl = 0 Then
                        vDonePresent = 0
                    Else
                        'vDonePresent = vDoneControl / vEleControl * 100
                        If check_nilai_kriteria Then
                            vDonePresent = vDoneControl / Val(PublicPengali) * 100
                        Else
                            vDonePresent = vDoneControl / (vEleControl * Val(PublicPengali)) * 100
                        End If
                    End If
                    If vEleControl = 0 Then
                        tmpEleControl = "N/A"
                        tmpDoneControl = "N/A"
                        tmpDonePersent = ""
                    Else
                        tmpEleControl = vEleControl
                        tmpDoneControl = vDoneControl
                        tmpDonePersent = vDonePresent & "%"
                    End If

                    myaddstr("         <td style='text-align:right'>" & tmpEleControl & "</td><td style='text-align:right'>" & tmpDoneControl & "</td><td style='text-align:right'>" & tmpDonePersent & "</td>")
                    myaddstr("         <td>" & myData.Tables(0).Rows(i).Item("chk_status") & "</td>")

                    myaddstr("         <td>")
                    If myData.Tables(0).Rows(i).Item("hasil_audit") <> "" Then
                        myaddstr("<b>1st:</b> " & myData.Tables(0).Rows(i).Item("hasil_audit") & "<br /><br />")
                    End If
                    If Not IsDBNull(myData.Tables(0).Rows(i).Item("hasil_audit_2")) Then
                        myaddstr("<b>2nd:</b> " & myData.Tables(0).Rows(i).Item("hasil_audit_2"))
                    End If
                    myaddstr("</td>")

                    myaddstr("         <td>")
                    If myData.Tables(0).Rows(i).Item("bukti_obyektif") <> "" Then
                        myaddstr("<b>1st:</b> <a href='bo/" & myData.Tables(0).Rows(i).Item("bukti_obyektif") & "'>" & myData.Tables(0).Rows(i).Item("bukti_obyektif") & "</a><br /><br />")
                    End If
                    If Not IsDBNull(myData.Tables(0).Rows(i).Item("bukti_obyektif_2")) Then
                        myaddstr("<b>2nd:</b> <a href='bo/" & myData.Tables(0).Rows(i).Item("bukti_obyektif_2") & "'>" & myData.Tables(0).Rows(i).Item("bukti_obyektif_2") & "</a>")
                    End If
                    myaddstr("</td>")

                End If


                If myData.Tables(0).Rows(i).Item("typeHD") = "D" Then
                    'If vEntryDoneControl = 200 Then
                    Dim result_Check_DoneChecklist = dt_DoneChecklist.AsEnumerable().Where(Function(myRow) myRow.Field(Of String)("no") = myData.Tables(0).Rows(i).Item("no"))

                    If IsDBNull(myData.Tables(0).Rows(i).Item("chk_status")) Then
                        myhtml.Append("         <td>EntryInComplete</td>")
                    ElseIf (myData.Tables(0).Rows(i).Item("chk_status")).ToString.Length = 0 Then
                        myhtml.Append("         <td>EntryInComplete</td>")
                    ElseIf Not result_Check_DoneChecklist.Any() Then
                        myhtml.Append("         <td>EntryInComplete</td>")
                    Else
                        myhtml.Append("         <td>EntryComplete</td>")
                    End If
                Else
                    myhtml.Append("         <td></td>")
                End If






                If vflag_selasses = "Y" And checkConf Then

                    If myrownum = 0 Or myData.Tables(0).Rows(i).Item("typeHD") = "H" Then
                        myhtml.Append("         <td></td>")
                        myaddstr("         <td></td>")
                        myaddstr("         <td></td>")
                    ElseIf sql_jabatan_audit Then

                        myhtml.Append("<td class='text-right'>")
                        myhtml.Append("<button type='button' class='button2xx' data-book-id='" & Temps & "' data-target='#formModalEdit_5' data-toggle='modal'>Verifikasi<br/>Audit</button>")
                        myhtml.Append("</td>")

                        'myData.Tables(0).Rows(i).Item("no")

                        myhtml.Append("         <td><asp:label id='" & MyNo & "' runat='server'>" & myData.Tables(0).Rows(i).Item("verifikasi_audit_status") & "<asp:label></td>")
                        myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("verifikasi_audit_remark") & "</td>")
                        myhtml1.Append("         <td>" & myData.Tables(0).Rows(i).Item("verifikasi_audit_status") & "</td>")
                        myhtml1.Append("         <td>" & myData.Tables(0).Rows(i).Item("verifikasi_audit_remark") & "</td>")

                    Else
                        myhtml.Append("         <td></td>")
                        myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("verifikasi_audit_status") & "</td>")
                        myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("verifikasi_audit_remark") & "</td>")
                        myhtml1.Append("         <td>" & myData.Tables(0).Rows(i).Item("verifikasi_audit_status") & "</td>")
                        myhtml1.Append("         <td>" & myData.Tables(0).Rows(i).Item("verifikasi_audit_remark") & "</td>")

                    End If

                End If

                If myData.Tables(0).Rows(i).Item("typeHD") = "H" Or checkConf Then
                    myhtml.Append("         <td></td>")
                ElseIf vflag_selasses = "Y" Then
                    If myData.Tables(0).Rows(i).Item("PICA1ENTRY") = jabatan_.Text Or
                        myData.Tables(0).Rows(i).Item("PICA2ENTRY") = jabatan_.Text Or
                        sql_jabatan_induk_pica1entry Or
                        sql_jabatan_induk_pica2entry Then

                        If IsDBNull(myData.Tables(0).Rows(i).Item("verifikasi_audit_status")) Then

                            myhtml.Append("<td class='text-right'>")

                            'myhtml.Append("<button type='button' class='button2xx' data-book-id='" & Temps & "' data-target='#formModalEdit_4' data-toggle='modal'>Update</button>")

                            If myData.Tables(0).Rows(i).Item("PICA1ENTRY") = jabatan_.Text Or
                                sql_jabatan_induk_pica1entry Then

                                myhtml.Append("<button type='button' class='button2xx' data-book-id='" & Temps & "' data-target='#formModalEdit_4' data-toggle='modal'>Update</button>")
                            End If

                            If myData.Tables(0).Rows(i).Item("PICA2ENTRY") = jabatan_.Text Or
                                sql_jabatan_induk_pica2entry Then

                                myhtml.Append("<br /><br />")
                                myhtml.Append("<button type='button' class='button2xx' data-book-id='" & Temps & "' data-target='#formModalEdit_6' data-toggle='modal'>Update</button>")
                            End If
                            myhtml.Append("</td>")

                        ElseIf myData.Tables(0).Rows(i).Item("verifikasi_audit_status") <> "OK" Then

                            myhtml.Append("<td class='text-right'>")

                            'myhtml.Append("<button type='button' class='button2xx' data-book-id='" & Temps & "' data-target='#formModalEdit_4' data-toggle='modal'>Update</button>")

                            If myData.Tables(0).Rows(i).Item("PICA1ENTRY") = jabatan_.Text Or
                                sql_jabatan_induk_pica1entry Then

                                myhtml.Append("<button type='button' class='button2xx' data-book-id='" & Temps & "' data-target='#formModalEdit_4' data-toggle='modal'>Update</button>")
                            End If

                            If myData.Tables(0).Rows(i).Item("PICA2ENTRY") = jabatan_.Text Or
                                sql_jabatan_induk_pica2entry Then

                                myhtml.Append("<br /><br />")
                                myhtml.Append("<button type='button' class='button2xx' data-book-id='" & Temps & "' data-target='#formModalEdit_6' data-toggle='modal'>Update</button>")
                            End If
                            myhtml.Append("</td>")


                        Else
                            myhtml.Append("         <td></td>")
                        End If

                    Else
                        myhtml.Append("         <td></td>")
                    End If
                Else
                    myhtml.Append("         <td></td>")
                End If


                myhtml.Append("<td class='large-width'>" & Regex.Replace(myData.Tables(0).Rows(i).Item("detail_guidance").ToString(), "\r\n?|\n", "<br />") & "</td>")


                myaddstr("      </tr>")

            End If


        Next

        myaddstr("   </tbody>")
        myaddstr(" </table> ")
        myhtml1.Append("</div>")

        'myHTMLTable.Text = myhtml.ToString() & "<br/>" & myhtml1.ToString()
        'MyHTMLString = myhtml.ToString() & "<br/>" & myhtml1.ToString()

        myPlaceH.Controls.Add(New Literal() With {
          .Text = myhtml.ToString() & "<br/>" & myhtml1.ToString()
         })

    End Sub

    Function checkPNilai(ByVal kode_tmp As String) As Boolean
        Dim dt As DataTable = GetDataSql("select pnilai from TMP_CHECKLIST_HDR where kode = '" & kode_tmp & "'")
        If dt.Rows.Count > 0 Then
            If dt.Rows(0)(0).ToString = "0" Then
                Return True
            Else
                Return False
            End If
        End If
        Return False
    End Function

    Function getMaxNilai(ByVal kode_kriteria1 As String, ByVal kode_kriteria2 As String, ByVal dt As DataTable) As Integer
        'Dim dt As DataTable = GetDataSql("select cast((select max(nilai) nilai_max from MST_KRITERIA_PENILAIAN_DETAIL where kode_kriteria = '" & kode_kriteria1 & "') as int) + cast((select max(nilai) nilai_max from MST_KRITERIA_PENILAIAN_DETAIL where kode_kriteria = '" & kode_kriteria2 & "') as int)")        
        If dt.Rows.Count > 0 Then
            Dim result_kriteria1 As Int32 = 0
            Dim result_kriteria2 As Int32 = 0

            Dim rows_kriteria1 = From dr As DataRow In dt.AsEnumerable()
                                 Where dr.Field(Of String)("kode_kriteria") = kode_kriteria1

            If rows_kriteria1.Any() Then
                result_kriteria1 = rows_kriteria1.Max(Function(r) r.Field(Of Int32)("nilai"))
            End If

            Dim rows_kriteria2 = From dr As DataRow In dt.AsEnumerable()
                                 Where dr.Field(Of String)("kode_kriteria") = kode_kriteria2


            If rows_kriteria2.Any() Then
                result_kriteria2 = rows_kriteria2.Max(Function(r) r.Field(Of Int32)("nilai"))
            End If


            Return result_kriteria1 + result_kriteria2

        End If
        Return 0
    End Function

    Private Sub sub_refresh2()

        Call sub_Refresh1()

        'MyHTMLString = myHTMLTable.Text
        'myPlaceH.Controls.Add(New Literal() With {
        '  .Text = MyHTMLString
        ' })

    End Sub


    Private Sub btnCancel1_Click(sender As Object, e As EventArgs) Handles btnCancel1.Click
        _cabang.Text = doBranch.Text
        _kode.Text = doTemplate.Text
        _periode.Text = doPeriode.Text
        Call sub_refresh2()
        infoerror1.Text = ""

    End Sub

    Private Sub btnConfirm_Click(sender As Object, e As EventArgs) Handles btnConfirm.Click
        Dim mystringview As String
        If doBranch.Text <> "" And doTemplate.Text <> "" And doPeriode.Text <> "" Then

            mystringview = myChef.SQLCONFIRM(_kode.Text, _periode.Text, _cabang.Text, ddlAuditor1.SelectedValue, ddlAuditor2.SelectedValue, ddlAuditor3.SelectedValue)
            _cabang.Text = doBranch.Text
            _kode.Text = doTemplate.Text
            _periode.Text = doPeriode.Text
            Call sub_refresh2()
            infoerror1.Text = mystringview
        Else
            infoerror1.Text = "Kriteria tidak boleh ada yang kosong"
        End If

    End Sub

    Private Sub btnUnconfirm_Click(sender As Object, e As EventArgs) Handles btnUnconfirm.Click
        Dim mystringview As String
        If doBranch.Text <> "" And doTemplate.Text <> "" And doPeriode.Text <> "" Then

            mystringview = myChef.SQLUNCONFIRM(_kode.Text, _periode.Text, _cabang.Text)
            _cabang.Text = doBranch.Text
            _kode.Text = doTemplate.Text
            _periode.Text = doPeriode.Text
            Call sub_refresh2()
            infoerror1.Text = mystringview
        Else
            infoerror1.Text = "Kriteria tidak boleh ada yang kosong"
        End If

    End Sub

    Private Sub btnCancel1__1_Click(sender As Object, e As EventArgs) Handles btnCancel1__1.Click
        _cabang.Text = doBranch.Text
        _kode.Text = doTemplate.Text
        _periode.Text = doPeriode.Text
        Call sub_refresh2()
        infoerror1.Text = ""

    End Sub

    Private Sub btnCancel1__2_Click(sender As Object, e As EventArgs) Handles btnCancel1__2.Click
        _cabang.Text = doBranch.Text
        _kode.Text = doTemplate.Text
        _periode.Text = doPeriode.Text
        Call sub_refresh2()
        infoerror1.Text = ""

    End Sub

    Private Sub btnCancel1__3_Click(sender As Object, e As EventArgs) Handles btnCancel1__3.Click
        _cabang.Text = doBranch.Text
        _kode.Text = doTemplate.Text
        _periode.Text = doPeriode.Text
        Call sub_refresh2()
        infoerror1.Text = ""

    End Sub

    Private Sub btnCancel1__4_Click(sender As Object, e As EventArgs) Handles btnCancel1__4.Click
        _cabang.Text = doBranch.Text
        _kode.Text = doTemplate.Text
        _periode.Text = doPeriode.Text
        Call sub_refresh2()
        infoerror1.Text = ""

    End Sub


    Private Sub btnCancel1__6_Click(sender As Object, e As EventArgs) Handles btnCancel1__6.Click
        _cabang.Text = doBranch.Text
        _kode.Text = doTemplate.Text
        _periode.Text = doPeriode.Text
        Call sub_refresh2()
        infoerror1.Text = ""

    End Sub




    Private Sub btnCancel1__5_Click(sender As Object, e As EventArgs) Handles btnCancel1__5.Click
        _cabang.Text = doBranch.Text
        _kode.Text = doTemplate.Text
        _periode.Text = doPeriode.Text
        Call sub_refresh2()
        infoerror1.Text = ""

    End Sub


    Private Sub btnUpdate__1_Click(sender As Object, e As EventArgs) Handles btnUpdate__1.Click
        Dim myData1 As Data.DataSet

        Try

            infoerror.Text = ""
            Dim mpLabel As Label
            Dim myusername As String = ""
            mpLabel = CType(Master.FindControl("lblusername"), Label)
            If Not mpLabel Is Nothing Then
                myusername = mpLabel.Text
            End If

            Dim vDone2 As String
            myData1 = myChef.SQLchecklist_entry(_kode.Text, hdnDone1__1.Value, _cabang.Text, _periode.Text)
            If hdnDone1__1.Value = "N/A" Then
                vDone2 = "N/A"
            Else
                'ElseIf myData1.Tables(0).Rows.Count = 0 Then
                vDone2 = ""
                'ElseIf IsDBNull(myData1.Tables(0).Rows(0).Item("Done_SecondControl")) Then
                '    vDone2 = "N/A"
                'Else
                '    vDone2 = myData1.Tables(0).Rows(0).Item("Done_SecondControl")
            End If

            Dim vdoStatus As String
            If vDone2 = "" Then
                vdoStatus = ""
                '            ElseIf txtDone1__1.Text = "1" And vDone2 = "1" Then
            ElseIf hdnDone1__1.Value = PublicPengali And vDone2 = PublicPengali Then
                vdoStatus = "OK"
            ElseIf hdnDone1__1.Value = "N/A" Or vDone2 = "N/A" Then
                vdoStatus = "Not Applicable"
            Else
                vdoStatus = "Not OK"
            End If


            'infoerror.Text = myChef.SQLchecklist_update12(_kode.Text, TxtNo__1.Text, _cabang.Text,
            '_periode.Text, txtDone1__1.Text, vDone2, vdoStatus, myusername)

            infoerror.Text = myChef.SQLchecklist_hanya1st(_kode.Text, TxtNo__1.Text, _cabang.Text,
                             _periode.Text, hdnDone1__1.Value, vDone2, vdoStatus, myusername)

        Catch ex As Exception
            infoerror.Text = ex.Message
        End Try

        Call sub_refresh2()


    End Sub


    Private Sub btnUpdate__2_Click(sender As Object, e As EventArgs) Handles btnUpdate__2.Click
        Dim myData1 As Data.DataSet

        Try
            'updatepilhannilai(_kode.Text)
            infoerror.Text = ""
            Dim mpLabel As Label
            Dim myusername As String = ""
            mpLabel = CType(Master.FindControl("lblusername"), Label)
            If Not mpLabel Is Nothing Then
                myusername = mpLabel.Text
            End If

            Dim vDone1 As String = ""
            myData1 = myChef.SQLchecklist_entry(_kode.Text, TxtNo__2.Text, _cabang.Text, _periode.Text)
            If myData1.Tables(0).Rows(0).Item("Done_MainControl") = "N/A" Then
                hdnDone2__2.Value = "N/A"
            ElseIf hdnDone2__2.Value = "N/A" And myData1.Tables(0).Rows(0).Item("Done_MainControl") <> "N/A" Then
                hdnDone2__2.Value = "0"
            End If
            If myData1.Tables(0).Rows.Count = 0 Then
                vDone1 = "0"
            Else
                vDone1 = myData1.Tables(0).Rows(0).Item("Done_MainControl")
            End If

            Dim check_nilai_kriteria As Boolean = checkPNilai(_kode.Text)
            Dim vdoStatus As String

            'If txtDone2__2.Text = "1" And vDone1 = "1" Then
            If (check_nilai_kriteria And Integer.Parse(hdnDone2__2.Value) + Integer.Parse(vDone1) = Integer.Parse(hdnMaxNilai.Value)) Or (Not check_nilai_kriteria And Integer.Parse(hdnDone2__2.Value) + Integer.Parse(vDone1) = Integer.Parse(hdnMaxNilai.Value) * 2) Then
                vdoStatus = "OK"
            ElseIf hdnDone2__2.Value = "N/A" Or vDone1 = "N/A" Then
                vdoStatus = "Not Applicable"
            Else
                vdoStatus = "Not OK"
            End If


            infoerror.Text = myChef.SQLchecklist_update12(_kode.Text, TxtNo__2.Text, _cabang.Text,
                             _periode.Text, vDone1, hdnDone2__2.Value, vdoStatus, myusername)


        Catch ex As Exception
            infoerror.Text = ex.Message
        End Try

        Call sub_refresh2()


    End Sub


    Private Sub btnUpdate__3_Click(sender As Object, e As EventArgs) Handles btnUpdate__3.Click
        Dim myData1 As Data.DataSet

        Try

            infoerror.Text = ""
            Dim mpLabel As Label
            Dim myusername As String = ""
            mpLabel = CType(Master.FindControl("lblusername"), Label)
            If Not mpLabel Is Nothing Then
                myusername = mpLabel.Text
            End If

            myData1 = myChef.SQLchecklist_entry(_kode.Text, TxtNo__3.Text, _cabang.Text, _periode.Text)

            Dim v_txtHasilAudit__3 As String
            Dim v_txtBuktiObyektif__3 As String
            Dim v_txtRecomendation__3 As String
            Dim v_doImplemenRecom__3 As String

            If myData1.Tables(0).Rows.Count = 0 Then

                infoerror.Text = "Data sebelumnya tidak ada"

            Else

                v_txtHasilAudit__3 = txtHasilAudit__3.Text
                v_txtBuktiObyektif__3 = txtBuktiObyektif__3.Text
                v_txtRecomendation__3 = txtRecomendation__3.Text
                v_doImplemenRecom__3 = doImplemenRecom__3.Text

                If v_txtHasilAudit__3 = "" Then
                    v_txtHasilAudit__3 = myData1.Tables(0).Rows(0).Item("Hasil_Audit")
                End If
                'If v_txtBuktiObyektif__3 = "" Then
                'v_txtBuktiObyektif__3 = myData1.Tables(0).Rows(0).Item("Bukti_Obyektif")
                'End If
                If v_txtRecomendation__3 = "" Then
                    v_txtRecomendation__3 = myData1.Tables(0).Rows(0).Item("Recomendation")
                End If
                If v_doImplemenRecom__3 = "" Then
                    v_doImplemenRecom__3 = myData1.Tables(0).Rows(0).Item("Implemen_Recom")
                End If

                infoerror.Text = myChef.SQLchecklist_update3(_kode.Text, TxtNo__3.Text, _cabang.Text,
                             _periode.Text, v_txtHasilAudit__3, v_txtBuktiObyektif__3, v_txtRecomendation__3, v_doImplemenRecom__3, myusername)

            End If

        Catch ex As Exception
            infoerror.Text = ex.Message
        End Try

        Call sub_refresh2()


    End Sub

    Private Sub btnUpdate__4_Click(sender As Object, e As EventArgs) Handles btnUpdate__4.Click
        Dim myData1 As Data.DataSet

        Try

            infoerror.Text = ""
            Dim mpLabel As Label
            Dim myusername As String = ""
            mpLabel = CType(Master.FindControl("lblusername"), Label)
            If Not mpLabel Is Nothing Then
                myusername = mpLabel.Text
            End If

            myData1 = myChef.SQLchecklist_entry(_kode.Text, TxtNo__4.Text, _cabang.Text, _periode.Text)

            Dim v_txtHasilAudit__4 As String

            Dim v_txtBuktiObyektif__4 As String
            Dim v_txtBranchFU__4 As String

            If myData1.Tables(0).Rows.Count = 0 Then

                infoerror.Text = "Data sebelumnya tidak ada"

            Else

                v_txtBuktiObyektif__4 = txtBuktiObyektif__4.Text
                v_txtBranchFU__4 = txtBranchFU__4.Text
                v_txtHasilAudit__4 = txtHasilAudit__4.Text

                If v_txtHasilAudit__4 = "" Then
                    v_txtHasilAudit__4 = myData1.Tables(0).Rows(0).Item("Hasil_Audit")
                End If

                'If v_txtBuktiObyektif__4 = "" Then
                'v_txtBuktiObyektif__4 = myData1.Tables(0).Rows(0).Item("Bukti_Obyektif")
                'End If
                If v_txtBranchFU__4 = "" Then
                    v_txtBranchFU__4 = myData1.Tables(0).Rows(0).Item("Branch_FU")
                End If

                infoerror.Text = myChef.SQLchecklist_update4(_kode.Text, TxtNo__4.Text, _cabang.Text,
                             _periode.Text, v_txtHasilAudit__4, v_txtBuktiObyektif__4, v_txtBranchFU__4, myusername)

            End If

        Catch ex As Exception
            infoerror.Text = ex.Message
        End Try

        Call sub_refresh2()


    End Sub






    Private Sub btnUpdate__6_Click(sender As Object, e As EventArgs) Handles btnUpdate__6.Click
        Dim myData1 As Data.DataSet

        Try

            infoerror.Text = ""
            Dim mpLabel As Label
            Dim myusername As String = ""
            mpLabel = CType(Master.FindControl("lblusername"), Label)
            If Not mpLabel Is Nothing Then
                myusername = mpLabel.Text
            End If

            myData1 = myChef.SQLchecklist_entry(_kode.Text, TxtNo__6.Text, _cabang.Text, _periode.Text)

            Dim v_txtHasilAudit__6 As String

            Dim v_txtBuktiObyektif__6 As String
            Dim v_txtBranchFU__6 As String

            If myData1.Tables(0).Rows.Count = 0 Then

                infoerror.Text = "Data sebelumnya tidak ada"

            Else

                v_txtBuktiObyektif__6 = txtBuktiObyektif__6.Text
                v_txtBranchFU__6 = txtBranchFU__6.Text
                v_txtHasilAudit__6 = txtHasilAudit__6.Text

                If v_txtHasilAudit__6 = "" Then
                    v_txtHasilAudit__6 = myData1.Tables(0).Rows(0).Item("Hasil_Audit")
                End If

                'If v_txtBuktiObyektif__6 = "" Then
                'v_txtBuktiObyektif__6 = myData1.Tables(0).Rows(0).Item("Bukti_Obyektif")
                'End If
                If v_txtBranchFU__6 = "" Then
                    v_txtBranchFU__6 = myData1.Tables(0).Rows(0).Item("Branch_FU")
                End If

                infoerror.Text = myChef.SQLchecklist_update6(_kode.Text, TxtNo__6.Text, _cabang.Text,
                             _periode.Text, v_txtHasilAudit__6, v_txtBuktiObyektif__6, v_txtBranchFU__6, myusername)

            End If

        Catch ex As Exception
            infoerror.Text = ex.Message
        End Try

        Call sub_refresh2()


    End Sub







    Private Sub btnUpdate__5_Click(sender As Object, e As EventArgs) Handles btnUpdate__5.Click
        Dim myData1 As Data.DataSet

        Try

            infoerror.Text = ""
            Dim mpLabel As Label
            Dim myusername As String = ""
            mpLabel = CType(Master.FindControl("lblusername"), Label)
            If Not mpLabel Is Nothing Then
                myusername = mpLabel.Text
            End If

            myData1 = myChef.SQLchecklist_entry(_kode.Text, TxtNo__5.Text, _cabang.Text, _periode.Text)


            If myData1.Tables(0).Rows.Count = 0 Then

                infoerror.Text = "Data sebelumnya tidak ada"

            Else

                infoerror.Text = myChef.SQLchecklist_update5(_kode.Text, TxtNo__5.Text, _cabang.Text,
                             _periode.Text, doVerifikasi_Audit_Status__5.Text, txtVerifikasi_Audit_Remark__5.Text, myusername)

            End If


            'Dim myLabel As Label
            'myLabel = FindControl(TxtNo__5.Text & "_lbl_verifikasi_audit_status")
            'myLabel.Text = doVerifikasi_Audit_Status__5.Text
            'myLabel = FindControl(TxtNo__5.Text & "_lbl_verifikasi_audit_remark")
            'myLabel.Text = txtVerifikasi_Audit_Remark__5.Text




        Catch ex As Exception
            infoerror.Text = ex.Message
        End Try

        Call sub_refresh2()


    End Sub






    Protected Sub btnValidate_Click(sender As Object, e As EventArgs) Handles btnValidate.Click

        Dim mystringview As String
        If doBranch.Text <> "" And doTemplate.Text <> "" And doPeriode.Text <> "" Then

            mystringview = myChef.SQLVALIDATE(_kode.Text, _periode.Text, _cabang.Text)
            _cabang.Text = doBranch.Text
            _kode.Text = doTemplate.Text
            _periode.Text = doPeriode.Text
            Call sub_refresh2()
            infoerror1.Text = mystringview
        Else
            infoerror1.Text = "Kriteria tidak boleh ada yang kosong"
        End If

    End Sub

    Sub updatepilhannilai(ByVal p_kode As String)

        Dim myData As Data.DataSet
        myData = myChef.dataNILAI(p_kode)
        If myData.Tables(0).Rows.Count > 0 Then
            divTxtDone1__1.Visible = False
            txtDone1__1.Visible = True
            divTxtDone2__2.Visible = False
            txtDone2__2.Visible = True
            divTxtDone1.Visible = False
            txtDone1.Visible = True
            divTxtDone2.Visible = False
            txtDone2.Visible = True

            PublicPengali = Val(myData.Tables(0).Rows(0).Item("pengali")).ToString
            txtDone1.Items.Clear()
            txtDone2.Items.Clear()
            txtDone1__1.Items.Clear()
            txtDone2__2.Items.Clear()
            For i = 0 To myData.Tables(0).Rows.Count - 1
                txtDone1.Items.Add(myData.Tables(0).Rows(i).Item("keterangan"))
                txtDone1.Items(i).Value = myData.Tables(0).Rows(i).Item("nilai")
                txtDone2.Items.Add(myData.Tables(0).Rows(i).Item("keterangan"))
                txtDone2.Items(i).Value = myData.Tables(0).Rows(i).Item("nilai")
                txtDone1__1.Items.Add(myData.Tables(0).Rows(i).Item("keterangan"))
                txtDone1__1.Items(i).Value = myData.Tables(0).Rows(i).Item("nilai")
                txtDone2__2.Items.Add(myData.Tables(0).Rows(i).Item("keterangan"))
                txtDone2__2.Items(i).Value = myData.Tables(0).Rows(i).Item("nilai")
            Next
            hdnDone1__1.Value = txtDone1__1.SelectedValue
            hdnDone2__2.Value = txtDone2__2.SelectedValue
        Else
            divTxtDone1__1.Visible = True
            txtDone1__1.Visible = False
            divTxtDone2__2.Visible = True
            txtDone2__2.Visible = False
            divTxtDone1.Visible = True
            txtDone1.Visible = False
            divTxtDone2.Visible = True
            txtDone2.Visible = False

            If doBranch.SelectedValue = "PST" Then
                PublicPengali = "1"
            Else
                PublicPengali = "2"
            End If
        End If
    End Sub


    Protected Sub btnSaveAuditor_Click(sender As Object, e As EventArgs)
        Dim dt As New DataTable
        If ddlAuditor1.Text <> ddlAuditor2.Text And ddlAuditor1.Text <> ddlAuditor3.Text And ddlAuditor3.Text <> ddlAuditor2.Text Then
            dt = GetDataSql("select * from CHECKLIST_ENTRY_AUDITOR_TEMP where kode_tmp = '" & _kode.Text & "' and kodecab = '" & _cabang.Text & "' and Kode_Prd = '" & _periode.Text & "'")
            If dt.Rows.Count > 0 Then
                InsertUpdateCommandSql("update CHECKLIST_ENTRY_AUDITOR_TEMP set auditor_1 = '" & ddlAuditor1.SelectedValue & "', auditor_2 = '" & ddlAuditor2.SelectedValue & "', auditor_3 = '" & ddlAuditor3.SelectedValue & "' where kode_tmp = '" & _kode.Text & "' and kodecab = '" & _cabang.Text & "' and Kode_Prd = '" & _periode.Text & "'")
            Else
                InsertUpdateCommandSql("insert into CHECKLIST_ENTRY_AUDITOR_TEMP(kode_tmp,Kodecab,Kode_Prd,auditor_1,auditor_2,auditor_3) values ('" & _kode.Text & "','" & _cabang.Text & "','" & _periode.Text & "','" & ddlAuditor1.SelectedValue & "','" & ddlAuditor2.SelectedValue & "','" & ddlAuditor3.SelectedValue & "')")
            End If
        Else
            Dim message As String = "Auditor tidak boleh sama"
            Dim sb As New System.Text.StringBuilder()
            sb.Append("<script type = 'text/javascript'>")
            sb.Append("window.onload=function(){")
            sb.Append("alert('")
            sb.Append(message)
            sb.Append("')};")
            sb.Append("</script>")
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "alert", sb.ToString())
            dt = GetDataSql("select * from CHECKLIST_ENTRY_AUDITOR_TEMP where kode_tmp = '" & _kode.Text & "' and kodecab = '" & _cabang.Text & "' and Kode_Prd = '" & _periode.Text & "'")
            If dt.Rows.Count > 0 Then
                ddlAuditor1.SelectedValue = dt.Rows(0).Item("auditor_1")
                ddlAuditor2.SelectedValue = dt.Rows(0).Item("auditor_2")
                ddlAuditor3.SelectedValue = dt.Rows(0).Item("auditor_3")
            End If

        End If
        Call sub_Refresh1()

    End Sub

    Protected Sub InsertUpdateCommandSql(ByVal query As String)
        Try
            Using con As New SqlConnection(strConn)
                con.Open()
                cmd = New SqlCommand(query, con)
                cmd.ExecuteNonQuery()
                con.Close()
            End Using
        Catch ex As Exception
            Dim a As String = ex.Message.ToString
        End Try
    End Sub

    Protected Function GetDataSql(ByVal query As String) As DataTable
        Try
            Using con As New SqlConnection(strConn)
                con.Open()
                Dim da As New SqlDataAdapter(query, con)
                Dim dt As New DataTable
                da.Fill(dt)
                Return dt
                con.Close()
            End Using
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function getKriteriaPenilaian(ByVal kode_kriteria As String) As String
        Dim return_string As String = "["
        Try
            Using con As New SqlConnection(strConn)
                con.Open()
                Dim da As New SqlDataAdapter("select * from MST_KRITERIA_PENILAIAN_DETAIL where kode_kriteria = '" & kode_kriteria & "' order by nilai desc", con)
                Dim dt As New DataTable
                da.Fill(dt)
                con.Close()
                If dt.Rows.Count > 0 Then
                    For i = 0 To dt.Rows.Count - 1
                        return_string = return_string + "{""nilai"": """ & dt.Rows(i)("nilai") & """, ""penjelasan"": """ & dt.Rows(i)("nilai") & " - " & dt.Rows(i)("penjelasan") & """},"
                    Next
                    return_string = return_string.Remove(return_string.Length - 1, 1)
                End If
            End Using
        Catch ex As Exception

        End Try
        return_string = return_string & "]"
        Return return_string
    End Function
End Class
