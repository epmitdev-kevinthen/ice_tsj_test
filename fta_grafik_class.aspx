﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="fta_grafik_class.aspx.vb" Inherits="fta_grafik_class" MasterPageFile="~/MasterPage.master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <br />
    <link href="css/Chart.css" rel="stylesheet" />
    <link href="css/Chart.min.css" rel="stylesheet" />
    <style>
        /* The Modal (background) */
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        /* Modal Content */
        .modal-content {
            position: relative;
            background-color: #fefefe;
            margin: auto;
            padding: 0;
            border: 1px solid #888;
            width: 80%;
            box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
            -webkit-animation-name: animatetop;
            -webkit-animation-duration: 0.4s;
            animation-name: animatetop;
            animation-duration: 0.4s
        }

        /* Add Animation */
        @-webkit-keyframes animatetop {
            from {
                top: -300px;
                opacity: 0
            }

            to {
                top: 0;
                opacity: 1
            }
        }

        @keyframes animatetop {
            from {
                top: -300px;
                opacity: 0
            }

            to {
                top: 0;
                opacity: 1
            }
        }

        /* The Close Button */
        .close {
            color: white;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

            .close:hover,
            .close:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }

        .modal-header {
            padding: 2px 16px;
            background-color: #5cb85c;
            color: white;
        }

        .modal-body {
            padding: 2px 16px;
        }

        .modal-footer {
            padding: 2px 16px;
            background-color: #5cb85c;
            color: white;
        }

        .none {
            display: none;
        }

        .bg-red{
            background-color: red;
        }

        .bg-yellow{
            background-color: yellow;
        }

        .bg-purple{
            background-color: purple;
            color: white;
        }

        .bg-green{
            background-color: green;
            color: white;
        }

        .bg-deepskyblue{
            background-color: deepskyblue;
        }

        .bg-pink{
            background-color: pink;
        }

        .bg-greenyellow{
            background-color: greenyellow;
        }

        .normalTable{
            width:auto !important;
            margin:0 !important;
        }
        .align-right{
            text-align: right;
        }
        .select2-selection__rendered, .select2-results__options{
            background-color:white !important;
        }
    </style>
    <script>
        $(document).ready(function () {
            var table = $('#ContentPlaceHolder1_GridView1').DataTable({
                paging: true,
                bProcessing: true, // shows 'processing' label
                bStateSave: true, // presumably saves state for reloads
            });
        });
    </script>

    <asp:Label ID="lblHead" runat="server" Text="Grafik FTA by Class"
        style="font-size: 20px;"
        ></asp:Label>
    <asp:Label ID="lblMsg" runat="server" style="display:block;"></asp:Label>    
    <br />
    <table>
        <tr>
            <td>Branch</td>
            <td>:</td>
            <td>
                <asp:DropDownList multiple ID="ddlBranch" runat="server"></asp:DropDownList><asp:HiddenField ID="hdnSelectedBranch" runat="server" />
            </td>
        </tr>
        <tr>
            <td>Case Category</td>
            <td>:</td>
            <td>
                <asp:DropDownList multiple ID="ddlCaseCategory" runat="server"></asp:DropDownList><asp:HiddenField runat="server" ID="hdnSelectedCaseCategory" />
            </td>
        </tr>
        <tr>
            <td>Finding Category</td>
            <td>:</td>
            <td>
                <asp:DropDownList multiple ID="ddlFindingCategory" runat="server"></asp:DropDownList><asp:HiddenField runat="server" ID="hdnSelectedFindingCategory" />
            </td>
        </tr>
        <tr>
            <td>Periode</td>
            <td>:</td>
            <td>
                <asp:DropDownList multiple ID="ddlPeriode" runat="server"></asp:DropDownList><asp:HiddenField runat="server" ID="hdnSelectedPeriode" />
            </td>
        </tr>
        <tr>
            <td>Status</td>
            <td>:</td>
            <td>
                <asp:DropDownList multiple ID="ddlStatus" runat="server"></asp:DropDownList><asp:HiddenField runat="server" ID="hdnSelectedStatus" />
            </td>
        </tr>
        <tr>
            <td>Kelompok Pemeriksa</td>
            <td>:</td>
            <td>
                <asp:DropDownList multiple ID="ddlKelompokPemeriksa" runat="server"></asp:DropDownList><asp:HiddenField runat="server" ID="hdnSelectedKelompokPemeriksa" />
            </td>
        </tr>
    </table>
    <asp:Button ID="btnFind" runat="server" Text="Find" CssClass="button2" OnClick="btnFind_Click" OnClientClick="ShowProgress();"/>

<div style="margin:30px;">
    <asp:Button id="btnExportExcel" runat="server" Text="Export Excel" CssClass="button2" Visible="false"/>
    <asp:GridView ID="GridView1" runat="server" CssClass="stripe row-border cell-border order-column normalTable table2excel_with_colors" ShowHeaderWhenEmpty="False" AllowPaging="False" AutoGenerateColumns="False" GridLines="None" OnRowDataBound="GridView1_RowDataBound">
        <Columns>
            <asp:BoundField DataField="class" HeaderText="Class" HeaderStyle-CssClass="bg-pink"/>                        
            <asp:BoundField DataField="follow_up_recommendation" HeaderText="Follow Up Recommendation" HeaderStyle-CssClass="bg-pink" ItemStyle-CssClass="align-right"/>
            <asp:BoundField DataField="not_implemented" HeaderText="N/I" HeaderStyle-CssClass="bg-red" ItemStyle-CssClass="align-right"/>
            <asp:BoundField DataField="not_applicable" HeaderText="N/A" HeaderStyle-CssClass="bg-yellow" ItemStyle-CssClass="align-right"/>
            <asp:BoundField DataField="in_progress" HeaderText="I/P" HeaderStyle-CssClass="bg-purple" ItemStyle-CssClass="align-right"/>
            <asp:BoundField DataField="not_due" HeaderText="N/D" HeaderStyle-CssClass="bg-green" ItemStyle-CssClass="align-right"/>
            <asp:BoundField DataField="implemented" HeaderText="I" HeaderStyle-CssClass="bg-deepskyblue" ItemStyle-CssClass="align-right"/>
            <asp:BoundField DataField="ISR" HeaderText="%ISR" HeaderStyle-CssClass="bg-pink" ItemStyle-CssClass="align-right"/>
            <asp:BoundField DataField="due_for_implement" HeaderText="#Due For Implement" HeaderStyle-CssClass="none noExl" ItemStyle-CssClass="none noExl"/>            
            
            
<%--            <asp:TemplateField HeaderText="Action">
                <ItemTemplate>
                    <asp:Button id="btnConfirm" runat="server" Text="Confirm"/>
                    <asp:Button id="btnApprove1" runat="server" Text="Approve"/>
                    <asp:Button id="btnApprove2" runat="server" Text="Approve"/>
                </ItemTemplate>
            </asp:TemplateField>--%>
        </Columns>
    </asp:GridView>
    <br />
    <canvas id="myChart" width="400" height="400" style="background-color:white;"></canvas>
</div>    
    <script src="js/Chart.js"></script>
    <script src="js/Chart.min.js"></script>
    <script>
        $(document).ready(function () {
            $("#<%= btnExportExcel.ClientID %>").on('click', function () {
                //console.log("asd");
                var table = $("#<%= GridView1.ClientID %>");
                if (table && table.length) {
                    var preserveColors = (table.hasClass('table2excel_with_colors') ? true : false);
                    $(table).table2excel({
                        exclude: ".noExl",
                        name: "Excel Document Name",
                        filename: "FTA_Class_<%= DateTime.Now.ToString("dd_MMM_yyyy") %>.xls",
                        fileext: ".xls",
                        exclude_img: true,
                        exclude_links: true,
                        exclude_inputs: true,
                        preserveColors: preserveColors
                    });
                }
                return false;
            });
        });
        
        function loadGrafik() {
            console.log("asd");
            $.ajax({
                type: "POST",
                url: "fta_grafik_class.aspx/TableLoad",
                data: '',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    loadChartData(JSON.parse(response.d));
                    console.log(response.d);
                },
                error: function () { alert("Server Error!!"); }
            });
        }

        function loadChartData(data) {
            try {
                //Saldo chart
                var jsonfile = {
                    "jsonarray": data
                };

                var labels = jsonfile.jsonarray.map(function (e) {
                    return e.class;
                });
                var data = jsonfile.jsonarray.map(function (e) {
                    return e.isr;
                });
                var color = jsonfile.jsonarray.map(function (e) {
                    return e.color;
                });

                var ctx = document.getElementById("myChart");
                if (ctx) {
                    ctx.height = 300;
                    mychart = new Chart(ctx, {
                        type: 'bar',
                        data: {
                            labels: labels,
                            datasets: [{
                                label: '%ISR',
                                data: data,
                                backgroundColor: color,
                                borderColor: color,
                                borderWidth: 1
                            }]
                        },
                        options: {
                            responsive: false,
                            scales: {
                                xAxes: [{
                                    ticks: {
                                        
                                    }
                                }],
                                yAxes: [{
                                    ticks: {
                                        beginAtZero: true,
                                        suggestedMin: 0,
                                        suggestedMax: 100
                                    }
                                }]
                            }
                        }
                    });
                }
            } catch (error) {
                console.log(error);
            }
        }

        $(function () {

            $("#<%= ddlBranch.ClientID %>").select2();
            $("#<%= ddlCaseCategory.ClientID %>").select2();
            $("#<%= ddlFindingCategory.ClientID %>").select2();
            $("#<%= ddlPeriode.ClientID %>").select2();
            $("#<%= ddlStatus.ClientID %>").select2();
            $("#<%= ddlKelompokPemeriksa.ClientID %>").select2();

            var hdnSelectedBranch = $("#<%= hdnSelectedBranch.ClientID %>").val();
            var hdnSelectedCaseCategory = $("#<%= hdnSelectedCaseCategory.ClientID %>").val();
            var hdnSelectedFindingCategory = $("#<%= hdnSelectedFindingCategory.ClientID %>").val();
            var hdnSelectedPeriode = $("#<%= hdnSelectedPeriode.ClientID %>").val();
            var hdnSelectedStatus = $("#<%= hdnSelectedStatus.ClientID %>").val();
            var hdnSelectedKelompokPemeriksa = $("#<%= hdnSelectedKelompokPemeriksa.ClientID %>").val();
            console.log(hdnSelectedBranch.split(","));
            $("#<%= ddlBranch.ClientID %>").val(hdnSelectedBranch.split(",")).change();
            $("#<%= ddlCaseCategory.ClientID %>").val(hdnSelectedCaseCategory.split(",")).change();
            $("#<%= ddlFindingCategory.ClientID %>").val(hdnSelectedFindingCategory.split(",")).change();
            $("#<%= ddlPeriode.ClientID %>").val(hdnSelectedPeriode.split(",")).change();
            $("#<%= ddlStatus.ClientID %>").val(hdnSelectedStatus.split(",")).change();
            $("#<%= ddlKelompokPemeriksa.ClientID %>").val(hdnSelectedKelompokPemeriksa.split(",")).change();
        });
        <%--$(function () {
            $('#<%= ddlBranch.ClientID %>').click(function (e) {
                var selected = $(e.target).val();
                console.log(selected);
                //if (selected == 'all') {
                //    $('#fruits > option').prop("selected", false);
                //    $(e.target).prop("selected", true);
                //};
            });
        });--%>
        $('#<%= ddlBranch.ClientID %>').on('change', function (e) {
            var optionSelected = $("option:selected", this);
            var valueSelected = $(this).val();
            var selectedBefore = $("#<%= hdnSelectedBranch.ClientID %>").val();
            console.log(selectedBefore);
            console.log(valueSelected);
            if (valueSelected.indexOf("%") > -1 && selectedBefore.indexOf("%") == -1) {
                //console.log($("option",e.target));
                //console.log($(e.target).select2("val"));
                $(optionSelected).prop("selected", false);
                $(e.target).find('[value="%"]').prop("selected", true);
                //$(e.target).prop("selected", true);
            } else {
                $(e.target).find('[value="%"]').prop("selected", false);
            }
            $("#<%= hdnSelectedBranch.ClientID %>").val($(this).val());
        });

        $('#<%= ddlCaseCategory.ClientID %>').on('change', function (e) {
            var optionSelected = $("option:selected", this);
            var valueSelected = $(this).val();
            var selectedBefore = $("#<%= hdnSelectedCaseCategory.ClientID %>").val();
            //console.log(selectedBefore);
            //console.log(optionSelected);
            if (valueSelected.indexOf("%") > -1 && selectedBefore.indexOf("%") == -1) {
                //console.log($("option",e.target));
                //console.log($(e.target).select2("val"));
                $(optionSelected).prop("selected", false);
                $(e.target).find('[value="%"]').prop("selected", true);
                //$(e.target).prop("selected", true);
            } else {
                $(e.target).find('[value="%"]').prop("selected", false);
            }
            $("#<%= hdnSelectedCaseCategory.ClientID %>").val($(this).val());
        });

        $('#<%= ddlFindingCategory.ClientID %>').on('change', function (e) {
            var optionSelected = $("option:selected", this);
            var valueSelected = $(this).val();
            var selectedBefore = $("#<%= hdnSelectedFindingCategory.ClientID %>").val();
            //console.log(selectedBefore);
            //console.log(optionSelected);
            if (valueSelected.indexOf("%") > -1 && selectedBefore.indexOf("%") == -1) {
                //console.log($("option",e.target));
                //console.log($(e.target).select2("val"));
                $(optionSelected).prop("selected", false);
                $(e.target).find('[value="%"]').prop("selected", true);
                //$(e.target).prop("selected", true);
            } else {
                $(e.target).find('[value="%"]').prop("selected", false);
            }
            $("#<%= hdnSelectedFindingCategory.ClientID %>").val($(this).val());
        });

        $('#<%= ddlPeriode.ClientID %>').on('change', function (e) {
            var optionSelected = $("option:selected", this);
            var valueSelected = $(this).val();
            var selectedBefore = $("#<%= hdnSelectedPeriode.ClientID %>").val();
            //console.log(selectedBefore);
            //console.log(optionSelected);
            if (valueSelected.indexOf("%") > -1 && selectedBefore.indexOf("%") == -1) {
                //console.log($("option",e.target));
                //console.log($(e.target).select2("val"));
                $(optionSelected).prop("selected", false);
                $(e.target).find('[value="%"]').prop("selected", true);
                //$(e.target).prop("selected", true);
            } else {
                $(e.target).find('[value="%"]').prop("selected", false);
            }
            $("#<%= hdnSelectedPeriode.ClientID %>").val($(this).val());
        });

        $('#<%= ddlStatus.ClientID %>').on('change', function (e) {
            var optionSelected = $("option:selected", this);
            var valueSelected = $(this).val();
            var selectedBefore = $("#<%= hdnSelectedStatus.ClientID %>").val();
            //console.log(selectedBefore);
            //console.log(optionSelected);
            if (valueSelected.indexOf("%") > -1 && selectedBefore.indexOf("%") == -1) {
                //console.log($("option",e.target));
                //console.log($(e.target).select2("val"));
                $(optionSelected).prop("selected", false);
                $(e.target).find('[value="%"]').prop("selected", true);
                //$(e.target).prop("selected", true);
            } else {
                $(e.target).find('[value="%"]').prop("selected", false);
            }
            $("#<%= hdnSelectedStatus.ClientID %>").val($(this).val());
        });

        $('#<%= ddlKelompokPemeriksa.ClientID %>').on('change', function (e) {
            var optionSelected = $("option:selected", this);
            var valueSelected = $(this).val();
            var selectedBefore = $("#<%= hdnSelectedKelompokPemeriksa.ClientID %>").val();
            //console.log(selectedBefore);
            //console.log(optionSelected);
            if (valueSelected.indexOf("%") > -1 && selectedBefore.indexOf("%") == -1) {
                //console.log($("option",e.target));
                //console.log($(e.target).select2("val"));
                $(optionSelected).prop("selected", false);
                $(e.target).find('[value="%"]').prop("selected", true);
                //$(e.target).prop("selected", true);
            } else {
                $(e.target).find('[value="%"]').prop("selected", false);
            }
            $("#<%= hdnSelectedKelompokPemeriksa.ClientID %>").val($(this).val());
        });
    </script>

    <asp:Label ID="infoerror" runat="server" style="color: red" ></asp:Label>

</asp:Content>


