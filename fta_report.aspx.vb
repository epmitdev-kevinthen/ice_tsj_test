﻿
Imports System.Data
Imports System.Data.SqlClient

Partial Class fta_report
    Inherits System.Web.UI.Page
    Private Sub fta_report_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            loadDDLFilter()
        End If
    End Sub
    Sub loadDDLFilter()
        Dim dt As New DataTable

        dt = GetDataSql("select * from mstcab order by namacab")
        If dt.Rows.Count > 0 Then
            ddlBranch.Items.Add(New ListItem("All", "%"))
            For i = 0 To dt.Rows.Count - 1
                ddlBranch.Items.Add(New ListItem(dt.Rows(i).Item("namacab"), dt.Rows(i).Item("kodecab")))
            Next
        End If

        dt = GetDataSql("select * from MSTCASECATEGORY order by keterangan")
        If dt.Rows.Count > 0 Then
            ddlCaseCategory.Items.Add(New ListItem("All", "%"))
            For i = 0 To dt.Rows.Count - 1
                ddlCaseCategory.Items.Add(New ListItem(dt.Rows(i).Item("keterangan"), dt.Rows(i).Item("kode")))
            Next
        End If

        dt = GetDataSql("select * from MSTFINDINGCATEGORY order by namacategory")
        If dt.Rows.Count > 0 Then
            ddlFindingCategory.Items.Add(New ListItem("All", "%"))
            For i = 0 To dt.Rows.Count - 1
                ddlFindingCategory.Items.Add(New ListItem(dt.Rows(i).Item("namacategory"), dt.Rows(i).Item("kodecategory")))
            Next
        End If

        dt = GetDataSql("SELECT * FROM mst_periode where flag_aktif='A' order by start_date,nama_prd,kode_prd desc")
        If dt.Rows.Count > 0 Then
            ddlPeriode.Items.Add(New ListItem("All", "%"))
            For i = 0 To dt.Rows.Count - 1
                ddlPeriode.Items.Add(New ListItem(dt.Rows(i).Item("nama_prd"), dt.Rows(i).Item("kode_prd")))
            Next
        End If

        dt = GetDataSql("select * from MST_FTA_STATUS")
        If dt.Rows.Count > 0 Then
            ddlStatus.Items.Add(New ListItem("All", "%"))
            For i = 0 To dt.Rows.Count - 1
                ddlStatus.Items.Add(New ListItem(dt.Rows(i).Item("namastatus") + " (" & dt.Rows(i).Item("kodestatus") & ")", dt.Rows(i).Item("id")))
            Next
        End If

        dt = GetDataSql("select * from MSTKELOMPOKPEMERIKSA order by kodekel")
        If dt.Rows.Count > 0 Then
            ddlKelompokPemeriksa.Items.Add(New ListItem("All", "%"))
            For i = 0 To dt.Rows.Count - 1
                ddlKelompokPemeriksa.Items.Add(New ListItem(dt.Rows(i).Item(1), dt.Rows(i).Item(0)))
            Next
        End If

    End Sub
    Protected Sub btnFind_Click(sender As Object, e As EventArgs)
        Dim selected_branch As String = hdnSelectedBranch.Value
        Dim selected_caseCat As String = hdnSelectedCaseCategory.Value
        Dim selected_finCat As String = hdnSelectedFindingCategory.Value
        Dim selected_periode As String = hdnSelectedPeriode.Value
        Dim selected_status As String = hdnSelectedStatus.Value
        Dim selected_kelompokpemeriksa As String = hdnSelectedKelompokPemeriksa.Value

        Dim list_selected_branch As String() = selected_branch.Split(",")
        Dim list_selected_caseCat As String() = selected_caseCat.Split(",")
        Dim list_selected_finCat As String() = selected_finCat.Split(",")
        Dim list_selected_periode As String() = selected_periode.Split(",")
        Dim list_selected_status As String() = selected_status.Split(",")
        Dim list_selected_kelompokpemeriksa As String() = selected_kelompokpemeriksa.Split(",")

        Dim param_branch As String = ""
        Dim param_caseCat As String = ""
        Dim param_finCat As String = ""
        Dim param_periode As String = ""
        Dim param_status As String = ""
        Dim param_kelompokpemeriksa As String = ""

        If selected_branch = "%" Or selected_branch = "" Then
            param_branch = "fcd.kodecab like '%%'"
        Else
            param_branch = "fcd.kodecab in ('" & selected_branch.Replace(",", "','") & "')"
        End If

        If selected_caseCat = "%" Or selected_caseCat = "" Then
            param_caseCat = "fcd.case_category like '%%'"
        Else
            param_caseCat = "fcd.case_category in ('" & selected_caseCat.Replace(",", "','") & "')"
        End If

        If selected_finCat = "%" Or selected_finCat = "" Then
            param_finCat = "fcd.fin_category like '%%'"
        Else
            param_finCat = "fcd.fin_category in ('" & selected_finCat.Replace(",", "','") & "')"
        End If

        If selected_periode = "%" Or selected_periode = "" Then
            param_periode = "fcd.Kode_Prd like '%%'"
        Else
            param_periode = "fcd.Kode_Prd in ('" & selected_periode.Replace(",", "','") & "')"
        End If

        If selected_status = "%" Or selected_status = "" Then
            param_status = "fcd.status like '%%'"
        Else
            param_status = "fcd.status in ('" & selected_status.Replace(",", "','") & "')"
        End If

        If selected_kelompokpemeriksa = "%" Or selected_kelompokpemeriksa = "" Then
            param_kelompokpemeriksa = "fcd.kel_pemeriksa like '%%'"
        Else
            param_kelompokpemeriksa = "fcd.kel_pemeriksa in ('" & selected_kelompokpemeriksa.Replace(",", "','") & "')"
        End If

        loadTable(param_branch, param_caseCat, param_finCat, param_periode, param_status, param_kelompokpemeriksa)

        btnExport.Visible = True
    End Sub

    Protected Sub loadTable(ByVal param_branch As String, ByVal param_caseCat As String, ByVal param_finCat As String, ByVal param_periode As String, ByVal param_status As String, ByVal param_kelompokpemeriksa As String)
        Dim Chrs As String = Chr(34)
        Dim table_html As String
        Dim month As String = ""
        Dim year As String = ""
        Dim sub_activity As String
        Dim main_activity As String

        Dim report_no As String = ""

        table_html = "<table id='mytable' class='cell-border table table-striped table-bordered datatable-multi-row' cellspacing='0' width='100%'>" +
            "<thead>" +
                "<tr>" +
                    "<th rowspan='2'>FTA No.</th>" +
                    "<th rowspan='2'>Branch</th>" +
                    "<th colspan='2'>Period</th>" +
                    "<th rowspan='2'>Report No</th>" +
                    "<th rowspan='2'>Main Activity</th>" +
                    "<th rowspan='2'>Sub Activity</th>" +
                    "<th rowspan='2'>Findings Category</th>" +
                    "<th rowspan='2'>Case Category</th>" +
                    "<th rowspan='2' class='large-width'>Audit Result</th>" +
                    "<th rowspan='2'>Bukti Obyektif</th>" +
                    "<th rowspan='2'>Suspect</th>" +
                    "<th rowspan='2'> </th>" +
                    "<th rowspan='2'>Responsibility</th>" +
                    "<th colspan='3'>Follow Up by Auditee</th>" +
                    "<th rowspan='2' class='large-width'>Recommendation</th>" +
                    "<th rowspan='2'>Start Date</th>" +
                    "<th rowspan='2'>Due Date</th>" +
                    "<th colspan='5'>Verification by Auditor</th>" +
                "</tr>" +
                "<tr>" +
                    "<th>Month</th>" +
                    "<th>Year</th>" +
                    "<th class='large-width'>Corrective Action</th>" +
                    "<th>PIC</th>" +
                    "<th>Evidence</th>" +
                    "<th class='large-width'>Verification Result-1</th>" +
                    "<th class='large-width'>Verification Result-2</th>" +
                    "<th>Status*</th>" +
                    "<th>Finish Date</th>" +
                    "<th>PIC</th>" +
                "</tr>" +
            "</thead>" +
            "<tbody>"

        Dim strsql As String = "select fcd.*,mfs.back_color,mfs.text_color,mfs.namastatus,(select nama_prd from MST_PERIODE mp where fcd.Kode_Prd_Checklist = mp.Kode_Prd) as nama_prd, (select no_report from TMP_CHECKLIST_DTL tcd where tcd.kode = fcd.Kode_tmp and tcd.no = fcd.no) as no_report_chcklist, (select namacategory from MSTFINDINGCATEGORY where kodecategory = fcd.fin_category) as nama_fin_category, (select keterangan from MSTCASECATEGORY where kode = fcd.case_category) as nama_case_category, (select jabatan from MSTJABATAN where kode = fcd.position) as nama_position, (select jabatan from MSTJABATAN where kode = fcd.position2) as nama_position2, (select nama from mstauditor where nik = fcd.pic_auditor) as nama_pic_auditor, (select no + '-' + Activity1 from TMP_CHECKLIST_DTL where kode = fcd.Kode_tmp and no = (select no_parent from TMP_CHECKLIST_DTL where kode = fcd.Kode_tmp and no = fcd.No)) as nama_sub_activity from FTA_CHECKLIST_DTL fcd left join MST_FTA_STATUS mfs on fcd.status = mfs.id where " & param_branch & " and " & param_periode & " and " & param_caseCat & " and " & param_finCat & " and " & param_status & " and " & param_kelompokpemeriksa

        Dim dt As DataTable = GetDataSql(strsql)
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                'If UCase(nik) = ddlAuditor1.SelectedValue Or UCase(nik) = ddlAuditor2.SelectedValue Or UCase(nik) = ddlAuditor3.SelectedValue Or Session("sUsername").ToString = "SUPERADMIN" Or Session("jabatan") = "AUDITOR MGR" Or Session("jabatan") = "AUDITOR DEVELOPMENT" Or Session("jabatan") = "ABM" Or (status_approval = 2 And Session("kodejabatan") = dt.Rows(i).Item("position").ToString) Then
                'If UCase(nik) = ddlAuditor1.SelectedValue Or UCase(nik) = ddlAuditor2.SelectedValue Or UCase(nik) = ddlAuditor3.SelectedValue Or Session("sUsername").ToString = "SUPERADMIN" Or Session("jabatan") = "AUDITOR MGR" Or Session("jabatan") = "AUDITOR DEVELOPMENT" Or Session("jabatan") = "ABM" Or (status_approval = 2 And Session("kodejabatan") <> "") Then

                table_html = table_html + "<tr>"

                month = dt.Rows(i).Item("nama_prd").ToString.Split(" ")(0)
                year = dt.Rows(i).Item("nama_prd").ToString.Split(" ")(1)

                report_no = If(dt.Rows(i).Item("manual_insertion_flag") = "1", dt.Rows(i).Item("no"), dt.Rows(i).Item("no_report_chcklist"))
                sub_activity = If(dt.Rows(i).Item("manual_insertion_flag").ToString = "1", dt.Rows(i).Item("sub_activity").ToString(), dt.Rows(i).Item("nama_sub_activity").ToString())
                main_activity = If(dt.Rows(i).Item("manual_insertion_flag") = "1", dt.Rows(i).Item("main_activity").ToString(), getMainSubActivity(dt.Rows(i).Item("kode_tmp"), sub_activity.Split("-")(0)))

                Dim finding_category As String = dt.Rows(i).Item("nama_fin_category")
                Dim case_category As String = dt.Rows(i).Item("nama_case_category")


                Dim evidence_path As String = ""
                If dt.Rows(i).Item("evidence").ToString = "" Or dt.Rows(i).Item("evidence").ToString Is Nothing Then
                    evidence_path = ""
                Else
                    evidence_path = "<a href='" & dt.Rows(i).Item("evidence").ToString & "'>Download</a>"
                End If

                Dim evidence_path2 As String = ""
                If dt.Rows(i).Item("evidence2").ToString = "" Or dt.Rows(i).Item("evidence2").ToString Is Nothing Then
                    evidence_path2 = ""
                Else
                    evidence_path2 = "<a href='" & dt.Rows(i).Item("evidence2").ToString & "'>Download</a>"
                End If

                Dim audit_result_path As String = ""
                If dt.Rows(i).Item("Hasil_Audit_Attachment").ToString = "" Or dt.Rows(i).Item("Hasil_Audit_Attachment").ToString Is Nothing Then
                    audit_result_path = ""
                    If dt.Rows(i).Item("manual_insertion_flag").ToString = "0" Then
                        audit_result_path = cekAudit_result_attachment_ice(dt.Rows(i).Item("kode_prd_checklist"), dt.Rows(i).Item("kode_tmp"), dt.Rows(i).Item("kodecab"), dt.Rows(i).Item("no"))
                        If audit_result_path <> "" Then
                            audit_result_path = "<a href=bo/" & audit_result_path & ">Download</a>"
                        End If
                    End If

                Else
                    audit_result_path = "<a href='" & dt.Rows(i).Item("Hasil_Audit_Attachment").ToString & "'>Download</a>"
                End If

                Dim start_date As Date = dt.Rows(i).Item("start_date")
                Dim finish_date As Date
                If Not IsDBNull(dt.Rows(i).Item("finish_date")) Then
                    finish_date = dt.Rows(i).Item("finish_date")
                End If

                table_html = table_html + "<td data-datatable-multi-row-rowspan='2'>" & dt.Rows(i).Item("fta_no") & "</td>"
                table_html = table_html + "<td data-datatable-multi-row-rowspan='2'>" & dt.Rows(i).Item("Kodecab") & "</td>"
                table_html = table_html + "<td data-datatable-multi-row-rowspan='2'>" & month & "</td>"
                table_html = table_html + "<td data-datatable-multi-row-rowspan='2'>" & year & "</td>"
                table_html = table_html + "<td data-datatable-multi-row-rowspan='2'>" & report_no & "</td>"
                table_html = table_html + "<td data-datatable-multi-row-rowspan='2'>" & If(dt.Rows(i).Item("manual_insertion_flag") = "1", main_activity, main_activity.Split("-")(1)) & "</td>"
                table_html = table_html + "<td data-datatable-multi-row-rowspan='2'>" & If(dt.Rows(i).Item("manual_insertion_flag") = "1", sub_activity, sub_activity.Split("-")(1)) & "</td>"
                table_html = table_html + "<td data-datatable-multi-row-rowspan='2'>" & finding_category & "</td>"
                table_html = table_html + "<td data-datatable-multi-row-rowspan='2'>" & case_category & "</br>" & "</td>"
                table_html = table_html + "<td data-datatable-multi-row-rowspan='2' class='large-width'>" & dt.Rows(i).Item("Hasil_audit").ToString.Replace(Environment.NewLine, "<br/>") & "</br>" & "</td>"
                table_html = table_html + "<td data-datatable-multi-row-rowspan='2'>" & audit_result_path & "</br>" & "</td>"
                table_html = table_html + "<td data-datatable-multi-row-rowspan='2'>" & dt.Rows(i).Item("suspect").ToString.Replace(Environment.NewLine, "<br/>") & "</br>"
                table_html = table_html + "<script type='x/template' class='extra-row-content'>"
                table_html = table_html + "<tr>"
                table_html = table_html + "<td><b>2nd: </b>"
                table_html = table_html + "<td>" & dt.Rows(i).Item("nama_position2").ToString & "</br>" & "</td>"
                table_html = table_html + "<td class='large-width'>" & dt.Rows(i).Item("corrective_action2").ToString.Replace(Environment.NewLine, "<br/>") & "</br>" & "</td>"
                table_html = table_html + "<td>" & dt.Rows(i).Item("pic_auditee2").ToString & "</br>" & "</td>"
                table_html = table_html + "<td>" & evidence_path2 & "</br>" & "</td>"
                table_html = table_html + "</tr>"
                table_html = table_html + "</script></td>"
                table_html = table_html + "<td><b>1st: </b></td>"
                table_html = table_html + "<td>" & dt.Rows(i).Item("nama_position").ToString & "</br>" & "</td>"
                table_html = table_html + "<td class='large-width'>" & dt.Rows(i).Item("corrective_action").ToString.Replace(Environment.NewLine, "<br/>") & "</br>" & "</td>"
                table_html = table_html + "<td>" & dt.Rows(i).Item("pic_auditee").ToString & "</br>" & "</td>"
                table_html = table_html + "<td>" & evidence_path & "</br>" & "</td>"
                table_html = table_html + "<td data-datatable-multi-row-rowspan='2' class='large-width'>" & dt.Rows(i).Item("recommendation").ToString.Replace(Environment.NewLine, "<br/>") & "</br>" & "</td>"
                table_html = table_html + "<td data-datatable-multi-row-rowspan='2'>" & start_date.ToString("dd-MMM-yy") & "</td>"
                table_html = table_html + "<td data-datatable-multi-row-rowspan='2'>" & If(IsDBNull(dt.Rows(i).Item("due_date")), "", CType(dt.Rows(i).Item("due_date"), Date).ToString("dd-MMM-yy")) & "</td>"
                table_html = table_html + "<td data-datatable-multi-row-rowspan='2' class='large-width'>" & dt.Rows(i).Item("verif_result1").ToString.Replace(Environment.NewLine, "<br/>") & "</br>" & "</td>"
                table_html = table_html + "<td data-datatable-multi-row-rowspan='2' class='large-width'>" & dt.Rows(i).Item("verif_result2").ToString.Replace(Environment.NewLine, "<br/>") & "</br>" & "</td>"
                table_html = table_html + "<td data-datatable-multi-row-rowspan='2' style='background-color: " & dt.Rows(i).Item("back_color") & "; color: " & dt.Rows(i).Item("text_color") & "'>" & dt.Rows(i).Item("namastatus").ToString & "</br>" & "</td>"
                table_html = table_html + "<td data-datatable-multi-row-rowspan='2'>" & If(IsDBNull(dt.Rows(i).Item("finish_date")), dt.Rows(i).Item("finish_date").ToString, finish_date.ToString("dd MMM yyyy")) & "</td>"
                table_html = table_html + "<td data-datatable-multi-row-rowspan='2'>" & dt.Rows(i).Item("nama_pic_auditor").ToString & "</br>" & "</td>"

                table_html = table_html + "</tr>"



            Next
        End If
        table_html = table_html + "</tbody>"
        table_html = table_html + "</table>"
        divTable.InnerHtml = table_html
        divTableHidden.InnerHtml = table_html.Replace("mytable", "mytable1")
    End Sub
    Protected Function getNamaStatus(ByVal kode As String) As String
        Dim dt As DataTable = GetDataSql("select namastatus from MST_FTA_STATUS where id = '" & kode & "'")
        If dt.Rows.Count > 0 Then
            Return dt.Rows(0).Item(0).ToString
        End If
        Return ""
    End Function
    Protected Function getStyleStatus(ByVal kode As String) As String
        Dim dt As DataTable = GetDataSql("select back_color,text_color from MST_FTA_STATUS where id = '" & kode & "'")
        If dt.Rows.Count > 0 Then
            Return "background-color: " & dt.Rows(0).Item(0) & "; color: " & dt.Rows(0).Item(1) & ""
        End If
        Return ""
    End Function
    Protected Function getNamaPosition(ByVal kode As String) As String
        Dim dt As DataTable = GetDataSql("select jabatan from MSTJABATAN where kode = '" & kode & "'")
        If dt.Rows.Count > 0 Then
            Return dt.Rows(0).Item(0).ToString
        End If
        Return ""
    End Function

    Protected Function getNamaAuditor(ByVal nik As String) As String
        Dim dt As DataTable = GetDataSql("select nama from mstauditor where nik = '" & nik & "'")
        If dt.Rows.Count > 0 Then
            Return dt.Rows(0).Item(0).ToString
        End If
        Return ""
    End Function
    Function cekAudit_result_attachment_ice(ByVal kode_prd As String, ByVal kode_tmp As String, ByVal kodecab As String, ByVal no As String) As String
        Dim dt As DataTable = GetDataSql("select bukti_obyektif, bukti_obyektif_2 from CHECKLIST_ENTRY where Kode_Prd = '" & kode_prd & "' and Kode_tmp = '" & kode_tmp & "' and no = '" & no & "' and Kodecab = '" & kodecab & "' ")
        If dt.Rows.Count > 0 Then
            If IsDBNull(dt.Rows(0).Item(0)) And IsDBNull(dt.Rows(0).Item(1)) Then
                Return ""
            End If

            If dt.Rows(0).Item(0).ToString <> "" Then
                Return dt.Rows(0).Item(0).ToString
            ElseIf dt.Rows(0).Item(1).ToString <> "" Then
                Return dt.Rows(0).Item(1).ToString
            Else
                Return ""
            End If

        Else
            Return ""
        End If
    End Function
    Protected Function cekJenisCategory(ByVal kode As String, ByVal jenis_category As String) As String
        Dim dt As New DataTable
        Select Case jenis_category
            Case ""
                Return ""
            Case "finding_category"
                dt = GetDataSql("select namacategory from MSTFINDINGCATEGORY where kodecategory = '" & kode & "'")
                If dt.Rows.Count > 0 Then
                    Return dt.Rows(0).Item(0)
                Else
                    Return ""
                End If
            Case "case_category"
                dt = GetDataSql("select keterangan, due_date from MSTCASECATEGORY where kode = '" & kode & "'")
                If dt.Rows.Count > 0 Then
                    Return dt.Rows(0).Item(0) + "-" + dt.Rows(0).Item(1)
                Else
                    Return ""
                End If
        End Select
        Return ""
    End Function
    Protected Function getReportNo(ByVal kode_tmp As String, ByVal no As String) As String
        Dim dt As DataTable = GetDataSql("select no_report from TMP_CHECKLIST_DTL where kode = '" & kode_tmp & "' and no = '" & no & "'")
        If dt.Rows.Count > 0 Then
            Return dt.Rows(0).Item(0).ToString
        Else
            Return no
        End If
    End Function
    Protected Function getMainSubActivity(ByVal kode As String, ByVal no As String) As String
        Dim dt As DataTable = GetDataSql("select no, Activity1 from TMP_CHECKLIST_DTL where kode = '" & kode & "' and no = (select no_parent from TMP_CHECKLIST_DTL where kode = '" & kode & "' and no = '" & no & "')")
        If dt.Rows.Count > 0 Then
            Return dt.Rows(0).Item(0) + "-" + dt.Rows(0).Item(1)
        Else
            Return ""
        End If
    End Function
    Protected Sub GridView1_RowDataBound(sender As Object, e As GridViewRowEventArgs)

    End Sub

    Public Shared Function GetDataSql(ByVal query As String) As DataTable
        Try
            Dim strConn As String = ConfigurationManager.ConnectionStrings("SQLICE").ConnectionString
            Using con As New SqlConnection(strConn)
                con.Open()
                Dim da As New SqlDataAdapter(query, con)
                Dim dt As New DataTable
                da.Fill(dt)
                con.Close()
                Return dt
            End Using
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
End Class
