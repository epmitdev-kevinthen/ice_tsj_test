﻿
Partial Class changepass
    Inherits System.Web.UI.Page

    Dim myChef As New Chef


    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Response.Write("<script language='javascript'>window.close()</script>")
    End Sub

    Private Sub btnsimpan_Click(sender As Object, e As EventArgs) Handles btnsimpan.Click
        If idold.Text = "" Or idnew.Text = "" Or idconfirm.Text = "" Then
            Dim asd As String = ""
            lblerror.Text = "Password harus diisi semua"
        ElseIf idnew.Text <> idconfirm.Text Then
            lblerror.Text = "Confirm new password harus sama dong"
        Else
            Dim mycheck As String = myChef.SQLGETUSER(Session("sUsername"), idold.Text)
            If mycheck <> "1" Then
                lblerror.Text = "Old Password tidak valid"
            Else
                If idnew.Text.Length < 8 Then
                    lblerror.Text = "Minimum passwod 8 char"
                Else
                    myChef.SQLupdpass(Session("sUsername"), idnew.Text, idold.Text)
                    lblerror.Text = "Sukses ubah password"
                End If
            End If
        End If
    End Sub
End Class
