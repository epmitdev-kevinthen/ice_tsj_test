﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CheckListAudit_byPIC.aspx.vb" Inherits="CheckListAudit_byPIC" MasterPageFile="~/MasterPage.master"%>




<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>
        .large-width {
            min-width: 500px !important;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <br />


    <asp:HiddenField id="hdnMaxNilai" runat="server"/>
    <asp:Label ID="lblHead" runat="server" Text="CheckList ICE"
        style="font-size: 20px;"
        ></asp:Label>
    <br />
    <div style="font-size: 20px;">
        <b>
            <Table>
                <tr>
                    <td><asp:Label ID="Lblbranch" runat="server">Cabang </asp:Label>&nbsp;&nbsp;</td>
                    <td><asp:Dropdownlist id="doBranch" runat="server" width="250px"></asp:Dropdownlist></td>
                    <td rowspan="3"> &nbsp;&nbsp;&nbsp; <asp:Button ID="btnFind" CssClass="button2" runat="server" Text="Find" /></td>

                    <td rowspan="3"> &nbsp;&nbsp;&nbsp; <button id="btnExport" type="button" class="button2" onclick="fnExcelReport();">Export to Excel</button></td>
                    

                    <td rowspan="3"> &nbsp;&nbsp;&nbsp; <asp:label ID="infoerror1" runat="server" Font-Bold="true"  ></asp:label>   </td>
                </tr>
                <tr>
                    <td><asp:Label ID="lblTemplate" runat="server">CheckList </asp:Label>&nbsp;&nbsp;</td>
                    <td><asp:Dropdownlist id="doTemplate" runat="server" width="400px"></asp:Dropdownlist></td>
                </tr>
                <tr>
                    <td><asp:Label ID="lblPeriode" runat="server">Periode </asp:Label>&nbsp;&nbsp;</td>
                    <td><asp:Dropdownlist id="doPeriode" runat="server" width="400px"></asp:Dropdownlist></td>
                </tr>
                <tr>
                    <td><asp:Label ID="Label1" runat="server">Status </asp:Label>&nbsp;&nbsp;</td>
                    <td><asp:Dropdownlist id="doStatusFind" runat="server" width="200px"></asp:Dropdownlist></td>
                </tr>
                <tr>
                    <td><asp:Label ID="Label2" runat="server">Entry </asp:Label>&nbsp;&nbsp;</td>
                    <td><asp:Dropdownlist id="doEntryFind" runat="server" width="200px"></asp:Dropdownlist></td>
                </tr>
                
    
                </Table>
            </b>
        <div id="divAuditor" runat="server" style="border: double; display: inline-block;">
            <b>AUDITOR</b> <br />
                    <asp:Label ID="Label3" runat="server">Auditor - 1 </asp:Label>&nbsp;&nbsp;
                    <asp:Dropdownlist id="ddlAuditor1" runat="server" width="200px"></asp:Dropdownlist> <br />
                
                
                    <asp:Label ID="Label4" runat="server">Auditor - 2 </asp:Label>&nbsp;&nbsp;
                    <asp:Dropdownlist id="ddlAuditor2" runat="server" width="200px"></asp:Dropdownlist> <br />
                
                
                    <asp:Label ID="Label5" runat="server">Auditor - 3 </asp:Label>&nbsp;&nbsp;
                    <asp:Dropdownlist id="ddlAuditor3" runat="server" width="200px"></asp:Dropdownlist> <br />
            <asp:Button id="btnSaveAuditor" runat="server" OnClick="btnSaveAuditor_Click" Text="Save Auditor" CssClass="button2" Visible="false"/>
                
        </div>
    </div>


    
<script  type="text/javascript">
    $(document).ready(function () {
        var table = $('#mytable').DataTable({
            scrollY: "1000px",
            scrollX: "800px",
            scrollCollapse: true,
            bProcessing: true, // shows 'processing' label
            bStateSave: true, // presumably saves state for reloads
            paging: true,
            pagelangth: 5,
            lengthMenu: [
                [ 5, 10, 25, 50, -1 ],
                [ '5 rows', '10 rows', '25 rows', '50 rows', 'Show all' ]
            ],
            fixedColumns: {
                leftColumns: 2
            }
        });
    });  

    function btnClick(args) {
        window.location.href = "r.aspx?a=1&b=" + args;
    }

    function openw(p) {
        window.open('brwfolder.aspx?p=' + p, 'mywindow', 'left=100,top=100,width=1300,height=1000,scrollbars=yes,resizable=no');
    };

    function fncBrowseF() {
        window.open('frmBrowseFile.aspx', 'mywindow', 'left=100,top=100,width=450,height=300,scrollbars=yes,resizable=no');
    }

    function fncBrowseClear() {
        document.getElementById("txtBuktiObyektif").value = "";
    }


    function fncBrowseF__3() {
        window.open('frmBrowseFile__3.aspx', 'mywindow', 'left=100,top=100,width=450,height=300,scrollbars=yes,resizable=no');
    }

    function fncBrowseClear__3() {
        document.getElementById("txtBuktiObyektif__3").value = "";
    }

    function fncBrowseF__4() {
        window.open('frmBrowseFile__4.aspx', 'mywindow', 'left=100,top=100,width=450,height=300,scrollbars=yes,resizable=no');
    }

    function fncBrowseClear__4() {
        document.getElementById("txtBuktiObyektif__4").value = "";
    }


    function fncBrowseF__6() {
        window.open('frmBrowseFile__6.aspx', 'mywindow', 'left=100,top=100,width=450,height=300,scrollbars=yes,resizable=no');
    }

    function fncBrowseClear__6() {
        document.getElementById("txtBuktiObyektif__6").value = "";
    }




    function DropOnChange1()
        {
        if (document.getElementById("txtDone1").value == "N/A") { document.getElementById("txtDone2").value = "N/A" };
        }

    function DropOnChange2() {
        if (document.getElementById("txtDone2").value == "N/A") { document.getElementById("txtDone1").value = "N/A" };
    }

    function fnExcelReport() {
        <%--var table = $("#<%= GridView2.ClientID %>");
        if (table && table.length) {
            $(table).table2excel({
                exclude: ".noExl",
                name: "Excel Document Name",
                filename: "ICE by PIC.xls",
                fileext: ".xls",
                exclude_img: true,
                exclude_links: true,
                exclude_inputs: true,
                preserveColors: true
            });
        }
        return false;--%>

        var tab_text = "<table border='2px'><tr bgcolor='#87AFC6'>";
        var textRange; var j = 0;
        tab = document.getElementById('<%= GridView2.ClientID %>'); // id of table

        for (j = 0; j < tab.rows.length; j++) {
            tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
            //tab_text=tab_text+"</tr>";
        }

        tab_text = tab_text + "</table>";
        tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
        tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
        tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
        {
            txtArea1.document.open("txt/html", "replace");
            txtArea1.document.write(tab_text);
            txtArea1.document.close();
            txtArea1.focus();
            sa = txtArea1.document.execCommand("SaveAs", true, "Say Thanks to Sumit.xls");
        }
        else                 //other browser not tested on IE 11
            sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

        return (sa);
    }

</script>

<div style="margin:30px;">

    <!--<button type='button' class='button2xx' data-book-id='||||A|A' data-target='#formModalNew' data-toggle='modal'>Add New</button>
    -->
    
    <asp:Label ID="_periode" style="display: none " runat="server"></asp:Label>
    <asp:Label ID="_cabang" style="display: none " runat="server"></asp:Label>
    <asp:Label ID="_kode" style="display: none " runat="server"></asp:Label>

    <asp:Label ID="_statusFind" style="display: none " runat="server"></asp:Label>
    <asp:Label ID="_entryFind" style="display: none " runat="server"></asp:Label>

    <asp:PlaceHolder ID="myPlaceH" runat="server"></asp:PlaceHolder>
    <asp:GridView ID="GridView1" runat="server" CssClass="stripe row-border order-column" DataKeyNames="kode" ShowHeaderWhenEmpty="False" AllowPaging="False" AutoGenerateColumns="False" GridLines="None" OnRowDataBound="GridView1_RowDataBound">
        <Columns>
            <asp:BoundField DataField="kode" HeaderText="kode" HeaderStyle-CssClass="hidden noExl" ItemStyle-CssClass="hidden noExl" />
            <asp:BoundField DataField="jabatan" HeaderText="Jabatan" HeaderStyle-CssClass="hidden noExl" ItemStyle-CssClass="hidden noExl" />
            <asp:TemplateField>
                <ItemTemplate>                    
                    <div style="width: 100%; text-align: center">
                        <b><%# Eval("Jabatan") %></b>
                        <br />
                        
                        <div style="width: 49%; float: left; border: 1px solid black;">
                            <asp:GridView ID="GridView_FirstLine" runat="server" CssClass="stripe row-border cell-border order-column" ShowHeaderWhenEmpty="true" AllowPaging="False" AutoGenerateColumns="False" GridLines="None" ShowFooter="true" OnRowDataBound="GridView_FirstLine_RowDataBound" Width="100%">
                                <Columns>
                                    <asp:BoundField DataField="No_Report" HeaderText="No." />
                                    <asp:BoundField DataField="Activity1" HeaderText="Activity 1st Line" />
                                    <asp:BoundField DataField="PICA1" HeaderText="PIC" />
                                    <asp:BoundField DataField="Element_Control_1" HeaderText="Element Control" />
                                    <asp:BoundField DataField="Poin_Element_Control_1" HeaderText="Poin Element Control" />
                                    <asp:BoundField DataField="Done_MainControl" HeaderText="Poin Done Control" />
                                    <asp:BoundField DataField="score_control" HeaderText="Score" />
                                </Columns>
                            </asp:GridView>
                        </div>

                        <div style="width: 49%; float: right; border: 1px solid black;">
                            <asp:GridView ID="GridView_SecondLine" runat="server" CssClass="stripe row-border cell-border order-column" ShowHeaderWhenEmpty="true" AllowPaging="False" AutoGenerateColumns="False" GridLines="None" ShowFooter="true" OnRowDataBound="GridView_SecondLine_RowDataBound" Width="100%">
                                <Columns>
                                    <asp:BoundField DataField="No_Report" HeaderText="No." />
                                    <asp:BoundField DataField="Activity2" HeaderText="Activity 2nd Line" />
                                    <asp:BoundField DataField="PICA2" HeaderText="PIC" />
                                    <asp:BoundField DataField="Element_Control_2" HeaderText="Element Control" />
                                    <asp:BoundField DataField="Poin_Element_Control_2" HeaderText="Poin Element Control" />
                                    <asp:BoundField DataField="Done_SecondControl" HeaderText="Poin Done Control" />
                                    <asp:BoundField DataField="score_control" HeaderText="Score" />
                                </Columns>
                            </asp:GridView>
                        </div>

                    </div>
                    <br />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>

    <div class="hidden">
        <asp:GridView ID="GridView2" runat="server" CssClass="stripe row-border order-column" DataKeyNames="kode" ShowHeaderWhenEmpty="False" AllowPaging="False" AutoGenerateColumns="False" GridLines="None" OnRowDataBound="GridView1_RowDataBound">
        <Columns>
            <asp:BoundField DataField="kode" HeaderText="kode" HeaderStyle-CssClass="hidden noExl" ItemStyle-CssClass="hidden noExl" Visible="false"/>
            <asp:BoundField DataField="jabatan" HeaderText="Jabatan" HeaderStyle-CssClass="hidden noExl" ItemStyle-CssClass="hidden noExl" />
            <asp:TemplateField>
                <ItemTemplate>                    
                    <div style="width: 100%; text-align: center">                        
                        <table>                            
                            <tr>
                                <td style="width: 48%; float: left; border: 1px solid black;">
                                    <asp:GridView ID="GridView_FirstLine" runat="server" CssClass="stripe row-border cell-border order-column" ShowHeaderWhenEmpty="true" AllowPaging="False" AutoGenerateColumns="False" GridLines="None" ShowFooter="true" OnRowDataBound="GridView_FirstLine_RowDataBound" Width="100%">
                                        <Columns>
                                            <asp:BoundField DataField="No_Report" HeaderText="No." />
                                            <asp:BoundField DataField="Activity1" HeaderText="Activity 1st Line" />
                                            <asp:BoundField DataField="PICA1" HeaderText="PIC" />
                                            <asp:BoundField DataField="Element_Control_1" HeaderText="Element Control" />
                                            <asp:BoundField DataField="Poin_Element_Control_1" HeaderText="Poin Element Control" />
                                            <asp:BoundField DataField="Done_MainControl" HeaderText="Poin Done Control" />
                                            <asp:BoundField DataField="score_control" HeaderText="Score" />
                                        </Columns>
                                    </asp:GridView>
                                </td>
                                <td style="width: 48%; float: left; border: 1px solid black;">
                                    <asp:GridView ID="GridView_SecondLine" runat="server" CssClass="stripe row-border cell-border order-column" ShowHeaderWhenEmpty="true" AllowPaging="False" AutoGenerateColumns="False" GridLines="None" ShowFooter="true" OnRowDataBound="GridView_SecondLine_RowDataBound" Width="100%">
                                        <Columns>
                                            <asp:BoundField DataField="No_Report" HeaderText="No." />
                                            <asp:BoundField DataField="Activity2" HeaderText="Activity 2nd Line" />
                                            <asp:BoundField DataField="PICA2" HeaderText="PIC" />
                                            <asp:BoundField DataField="Element_Control_2" HeaderText="Element Control" />
                                            <asp:BoundField DataField="Poin_Element_Control_2" HeaderText="Poin Element Control" />
                                            <asp:BoundField DataField="Done_SecondControl" HeaderText="Poin Done Control" />
                                            <asp:BoundField DataField="score_control" HeaderText="Score" />
                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>

                    </div>
                    <br />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    </div>
    
    <table id="tblHidden">
        <thead>

        </thead>
    </table>
</div>

    <asp:Label ID="infoerror" runat="server" style="color: red" ></asp:Label>



    <script type="text/javascript">

        $(function () {
            var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "personal";
            $('#Tabs a[href="#' + tabName + '"]').tab('show');
            $("#Tabs a").click(function () {
                $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
            });

            $("#<%= GridView1.ClientID %>").DataTable({
                paging: true,
                bProcessing: true, // shows 'processing' label
                bStateSave: true, // presumably saves state for reloads
            });

            <%--$("#<%= GridView2.ClientID %>").DataTable({
                paging: false
            });--%>
        });


    </script>


    <div style="display:none;">
        <asp:Label ID="myHTMLTable" runat="server"></asp:Label>

            <asp:textbox id="sUsername_" runat="server"></asp:textbox>
        <asp:textbox id="levelakses_" runat="server"></asp:textbox>
        <asp:textbox id="jabatan_" runat="server"></asp:textbox>
        <asp:textbox id="sCabang_" runat="server"></asp:textbox>
        <asp:textbox id="kodedept_" runat="server"></asp:textbox>
        
    </div>


    <asp:HiddenField id="hdnPengali" runat="server"/>

</asp:Content>

