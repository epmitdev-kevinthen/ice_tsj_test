﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeFile="fta_list.aspx.vb" Inherits="fta_list" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <br />
    <style>
        /* Create two equal columns that floats next to each other */
.column {
  float: left;  
  padding: 10px;
  
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Responsive layout - makes the two columns stack on top of each other instead of next to each other */
@media screen and (max-width: 600px) {
  .column {
    width: 100%;
  }
}
        /* The Modal (background) */
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        /* Modal Content */
        .modal-content {
            position: relative;
            background-color: #fefefe;
            margin: auto;
            padding: 0;
            border: 1px solid #888;
            width: 80%;
            box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
            -webkit-animation-name: animatetop;
            -webkit-animation-duration: 0.4s;
            animation-name: animatetop;
            animation-duration: 0.4s
        }

        /* Add Animation */
        @-webkit-keyframes animatetop {
            from {
                top: -300px;
                opacity: 0
            }

            to {
                top: 0;
                opacity: 1
            }
        }

        @keyframes animatetop {
            from {
                top: -300px;
                opacity: 0
            }

            to {
                top: 0;
                opacity: 1
            }
        }

        /* The Close Button */
        .close {
            color: white;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

            .close:hover,
            .close:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }

        .modal-header {
            padding: 2px 16px;
            background-color: #5cb85c;
            color: white;
        }

        .modal-body {
            padding: 2px 16px;
        }

        .modal-footer {
            padding: 2px 16px;
            background-color: #5cb85c;
            color: white;
        }
    </style>
    <script>
        $(document).ready(function () {
            var table = $('#ContentPlaceHolder1_GridView1').DataTable({
                paging: true,
                bProcessing: true, // shows 'processing' label
                bStateSave: true, // presumably saves state for reloads
            });
        });
    </script>

    <asp:Label ID="lblHead" runat="server" Text="List FTA"
        style="font-size: 20px;"
        ></asp:Label>
    <asp:Label ID="lblMsg" runat="server" style="display:none;"></asp:Label>
    <asp:Button id="btnNewFta" runat="server" Text="Create New FTA" CssClass="button2" data-toggle='modal' data-target='#modalInsertFTA' OnClientClick="return false;"/>
    

<div style="margin:30px;">

    <asp:GridView ID="GridView1" runat="server" CssClass="stripe row-border order-column" ShowHeaderWhenEmpty="False" AllowPaging="False" AutoGenerateColumns="False" GridLines="None" OnRowDataBound="GridView1_RowDataBound">
        <Columns>
            <asp:TemplateField HeaderText="Action">
                <ItemTemplate>
                    <a href="fta.aspx?fta_no=<%# Eval("fta_no").ToString %>&kode_prd=<%# Eval("kode_prd").ToString %>">View</a>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="fta_no" HeaderText="FTA No."/>            
            <asp:BoundField DataField="Kodecab" HeaderText="Kode Cabang"/>
            <asp:BoundField DataField="prd_month" HeaderText="Month"/>      
            <asp:BoundField DataField="tahun" HeaderText="Year"/>
            <asp:BoundField DataField="kel_pemeriksa" HeaderText="Kelompok Pemeriksa"/>
            <asp:BoundField DataField="status" HeaderText="Status"/>
            <asp:BoundField DataField="approve1_by" HeaderText="Approve 1"/>
            <asp:BoundField DataField="approve1_date" HeaderText="Approve 1 Date"/>
            <asp:BoundField DataField="approve2_by" HeaderText="Approve 2"/>
            <asp:BoundField DataField="approve2_date" HeaderText="Approve 2 Date"/>
            <asp:BoundField DataField="created_date" HeaderText="Created Date"/>
            <asp:BoundField DataField="kode_prd" HeaderText="kode_prd" HeaderStyle-CssClass="hidden" ItemStyle-CssClass="hidden" />
            <asp:TemplateField HeaderText="Action">
                <ItemTemplate>
                    <asp:Button id="btnDeleteFTA" runat="server" Text="Delete" OnClick="btnDeleteFTA_Click"/>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>

</div>
    <div id="modalInsertFTA" class="modal">

            <!-- Modal content -->
            <div class="modal-content">
                <div class="modal-header">
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                    <h2>Insert New FTA</h2>
                </div>
                <div class="modal-body">
                    <table>
                        <tr>
                            <td>Cabang</td>
                            <td><asp:DropDownList ID="doBranch" runat="server" Width="250px"></asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td>Periode</td>
                            <td><asp:DropDownList ID="doPeriode" runat="server" Width="400px"></asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td>Kelompok Pemeriksa</td>
                            <td><asp:DropDownList ID="ddlKelompokPemeriksa" runat="server" Width="400px"></asp:DropDownList></td>
                        </tr>
                    </table>
                    <div id="divAuditor" runat="server" style="border: double; display:inline-block;" class="">
            <b>AUDITOR</b>
            <br />
            <asp:Label ID="Label3" runat="server">Auditor - 1 </asp:Label>&nbsp;&nbsp;
                    <asp:DropDownList ID="ddlAuditor1" runat="server" Width="200px" Enabled="true"></asp:DropDownList>
            <br />


            <asp:Label ID="Label4" runat="server">Auditor - 2 </asp:Label>&nbsp;&nbsp;
                    <asp:DropDownList ID="ddlAuditor2" runat="server" Width="200px" Enabled="true"></asp:DropDownList>
            <br />


            <asp:Label ID="Label5" runat="server">Auditor - 3 </asp:Label>&nbsp;&nbsp;
                    <asp:DropDownList ID="ddlAuditor3" runat="server" Width="200px" Enabled="true"></asp:DropDownList>
            <br />
            <asp:Button id="btnSaveAuditor" runat="server" OnClick="btnSaveAuditor_Click" Text="Save Auditor" CssClass="button2" Visible="false"/>
        </div>
                    <asp:Button id="btnInsertFTA" runat="server" Text="Insert" OnClick="btnInsertFTA_Click" CssClass="button2" />
                </div>
            </div>

        </div>

    <script type="text/javascript">



    </script>

    <asp:Label ID="infoerror" runat="server" style="color: red" ></asp:Label>

</asp:Content>


