﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="fta.aspx.vb" Inherits="fta" MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .d-none{
            
        }
        /* Create two equal columns that floats next to each other */
        .column {
            float: left;
            padding: 10px;
        }

        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        /* Responsive layout - makes the two columns stack on top of each other instead of next to each other */
        @media screen and (max-width: 600px) {
            .column {
                width: 100%;
            }
        }
        /* The Modal (background) */
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        /* Modal Content */
        .modal-content {
            position: relative;
            background-color: #fefefe;
            margin: auto;
            padding: 0;
            border: 1px solid #888;
            width: 80%;
            box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
            -webkit-animation-name: animatetop;
            -webkit-animation-duration: 0.4s;
            animation-name: animatetop;
            animation-duration: 0.4s
        }

        /* Add Animation */
        @-webkit-keyframes animatetop {
            from {
                top: -300px;
                opacity: 0
            }

            to {
                top: 0;
                opacity: 1
            }
        }

        @keyframes animatetop {
            from {
                top: -300px;
                opacity: 0
            }

            to {
                top: 0;
                opacity: 1
            }
        }

        /* The Close Button */
        .close {
            color: white;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

            .close:hover,
            .close:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }

        .modal-header {
            padding: 2px 16px;
            background-color: #5cb85c;
            color: white;
        }

        .modal-body {
            padding: 2px 16px;
        }

        .modal-footer {
            padding: 2px 16px;
            background-color: #5cb85c;
            color: white;
        }

        .large-width {
            min-width: 800px !important;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />


    <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <asp:Label ID="lblHead" runat="server" Text="FTA ICE"
        Style="font-size: 20px;"></asp:Label>
    <br />
    <asp:Label ID="lblerrorasd" runat="server" ForeColor="Red"></asp:Label>
    <div style="font-size: 20px;">
        <asp:HiddenField ID="hdnKode_Tmp" runat="server" />
        <b>
            <table>
                <tr>
                    <td>
                        <asp:Label ID="Label1" runat="server">FTA No   </asp:Label>&nbsp;&nbsp;</td>
                    <td>
                        <asp:TextBox ID="txtFtaNo" runat="server" Enabled="false"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Lblbranch" runat="server">Cabang </asp:Label>&nbsp;&nbsp;</td>
                    <td>
                        <asp:DropDownList ID="doBranch" runat="server" Width="250px"></asp:DropDownList></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblPeriode" runat="server">Periode </asp:Label>&nbsp;&nbsp;</td>
                    <td>
                        <asp:DropDownList ID="doPeriode" runat="server" Width="400px"></asp:DropDownList></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label2" runat="server">Kelompok Pemeriksa </asp:Label>&nbsp;&nbsp;</td>
                    <td>
                        <asp:DropDownList ID="ddlKelompokPemeriksa" runat="server" Width="400px"></asp:DropDownList></td>
                </tr>
            </table>
        </b>
        <div class="row">
            <div id="divAuditor" runat="server" style="border: double; display: inline-block;" class="column">
                <b>AUDITOR</b>
                <br />
                <asp:Label ID="Label3" runat="server">Auditor - 1 </asp:Label>&nbsp;&nbsp;
                    <asp:DropDownList ID="ddlAuditor1" runat="server" Width="200px" Enabled="false"></asp:DropDownList>
                <br />


                <asp:Label ID="Label4" runat="server">Auditor - 2 </asp:Label>&nbsp;&nbsp;
                    <asp:DropDownList ID="ddlAuditor2" runat="server" Width="200px" Enabled="false"></asp:DropDownList>
                <br />


                <asp:Label ID="Label5" runat="server">Auditor - 3 </asp:Label>&nbsp;&nbsp;
                    <asp:DropDownList ID="ddlAuditor3" runat="server" Width="200px" Enabled="false"></asp:DropDownList>
                <br />
                <asp:Button ID="btnSaveAuditor" runat="server" OnClick="btnSaveAuditor_Click" Text="Save Auditor" CssClass="button2" Visible="false" />
            </div>
            <div id="divRejected" runat="server" style="border: double; display: inline-block; color: red;" class="column">
                <b>REJECTED</b>
                <table>
                    <tr>
                        <td>Reject Reason</td>
                        <td>:</td>
                        <td>
                            <asp:Label ID="lblRejectReason" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>Reject Date</td>
                        <td>:</td>
                        <td>
                            <asp:Label ID="lblRejectDate" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>Reject By</td>
                        <td>:</td>
                        <td>
                            <asp:Label ID="lblRejectBy" runat="server"></asp:Label></td>
                    </tr>
                </table>

            </div>

        </div>



        <div style="display: none;">
            <asp:Label ID="myHTMLTable" runat="server"></asp:Label>

            <asp:TextBox ID="sUsername_" runat="server"></asp:TextBox>
            <asp:TextBox ID="levelakses_" runat="server"></asp:TextBox>
            <asp:TextBox ID="jabatan_" runat="server"></asp:TextBox>
            <asp:TextBox ID="sCabang_" runat="server"></asp:TextBox>
            <asp:TextBox ID="kodedept_" runat="server"></asp:TextBox>

        </div>
        <%--<button onclick="getTableData(); return false;">Find</button>--%>
        <div style="float: right; margin-left: 15px;">
            <asp:Button ID="btnImportChecklist" runat="server" Text="Import UnFinish Checklist" CssClass="button2" OnClick="btnImportChecklist_Click" />
            <asp:Button ID="btnConfirm" runat="server" OnClick="btnConfirm_Click" Text="CONFIRM" CssClass="button2" />
            <asp:Button ID="btnApproveChecklist" runat="server" OnClick="btnApproveChecklist_Click" Text="APPROVE" CssClass="button2" />
            <asp:Button ID="btnRejectModal" runat="server" Text="REJECT" CssClass="button2" data-toggle='modal' data-target='#modalReject' OnClientClick="return false;" />
            <asp:Button ID="btnExport" runat="server" CssClass="button2" Text="Export to Excel" OnClientClick="fnExcelReport(); return false;" />
        </div>
        <asp:Button ID="btnFindData" runat="server" OnClick="btnFindData_Click" Text="FIND DATA" CssClass="button2" Visible="false" /><br />
        <div id="divStatistik" runat="server">
        </div>
        <asp:Button ID="btnInsertChecklist" runat="server" OnClientClick="return false;" Text="Insert New Checklist" data-toggle='modal' data-target='#modalInsertChecklist' CssClass="button2" />
        <asp:Button ID="btnImportExcel" runat="server" OnClientClick="return false;" Text="Import From Excel" data-toggle="modal" data-target="#modalImport" CssClass="button2" />
        <div id="divTable" runat="server">
        </div>
        <div id="divTableHidden" runat="server" style="display: none;">
        </div>
        <asp:HiddenField ID="hdnIdChklist" runat="server" />
        <asp:HiddenField ID="hdnJenisUpdate" runat="server" />
        <asp:HiddenField id="scroll_position" runat="server"/>

        <%--Modal Import Excel--%>
        <div id="modalImport" class="modal">

            <!-- Modal content -->
            <div class="modal-content">
                <div class="modal-header">
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                    <h2>Import Excel</h2>
                </div>
                <div class="modal-body">

                    <span>File Excel : </span>
                    <asp:FileUpload ID="fuExcelImport" runat="server" /><br />
                    <asp:Button ID="btnUploadExcel" runat="server" Text="Upload Excel" OnClick="btnUploadExcel_Click" />
                </div>
            </div>

        </div>

        <%--Modal Finding Category--%>
        <div id="modalFinCategory" class="modal">

            <!-- Modal content -->
            <div class="modal-content">
                <div class="modal-header">
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                    <h2>Finding Category</h2>
                </div>
                <div class="modal-body">

                    <span>Finding Category : </span>
                    <asp:DropDownList ID="ddlFindingCategory" runat="server"></asp:DropDownList><br />
                    <asp:Button ID="btnUpdateFinCategory" runat="server" Text="Update" OnClick="btnUpdateFinCategory_Click" />
                </div>
            </div>

        </div>

        <%--Modal Case Category--%>
        <div id="modalCaseCategory" class="modal">

            <!-- Modal content -->
            <div class="modal-content">
                <div class="modal-header">
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                    <h2>Case Category</h2>
                </div>
                <div class="modal-body">
                    <span>Case Category : </span>
                    <asp:DropDownList ID="ddlCaseCategory" runat="server"></asp:DropDownList><br />
                    <asp:Button ID="btnUpdateCaseCategory" runat="server" Text="Update" OnClick="btnUpdateCaseCategory_Click" />
                </div>
            </div>

        </div>

        <%--Modal Suspect--%>
        <div id="modalSuspect" class="modal">

            <!-- Modal content -->
            <div class="modal-content">
                <div class="modal-header">
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                    <h2>Suspect</h2>
                </div>
                <div class="modal-body">
                    <span>Suspect : </span>
                    <asp:TextBox ID="txtSuspect" runat="server" TextMode="multiline" Columns="50" Rows="5" onkeyDown="return checkTextAreaMaxLength(this,event,'100');" onblur="checkLength(this, '100')"></asp:TextBox><br />
                    <asp:Button ID="btnUpdateSuspect" runat="server" Text="Update" OnClick="btnUpdateSuspect_Click" />
                </div>
            </div>

        </div>

        <%--Modal Position--%>
        <div id="modalPosition" class="modal">

            <!-- Modal content -->
            <div class="modal-content">
                <div class="modal-header">
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                    <h2>Position</h2>
                </div>
                <div class="modal-body">
                    <span>Position : </span>
                    <asp:DropDownList ID="ddlPosition" runat="server"></asp:DropDownList>
                    <asp:Button ID="btnUpdatePosition" runat="server" Text="Update" OnClick="btnUpdatePosition_Click" />
                </div>
            </div>

        </div>

        <%--Modal Corrective Action--%>
        <div id="modalCorrective" class="modal">

            <!-- Modal content -->
            <div class="modal-content">
                <div class="modal-header">
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                    <h2>Corrective Action</h2>
                </div>
                <div class="modal-body">
                    <span>Corrective Action : </span>
                    <asp:TextBox ID="txtCorrective" runat="server" TextMode="multiline" Columns="50" Rows="5"></asp:TextBox><br />
                    <asp:Button ID="btnUpdateCorrective" runat="server" Text="Update" OnClick="btnUpdateCorrective_Click" />
                </div>
            </div>

        </div>

        <%--Modal PIC Auditee--%>
        <div id="modalPICAuditee" class="modal">

            <!-- Modal content -->
            <div class="modal-content">
                <div class="modal-header">
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                    <h2>PIC</h2>
                </div>
                <div class="modal-body">
                    <span>PIC : </span>
                    <asp:TextBox ID="txtPIC_Auditee" runat="server" MaxLength="500"></asp:TextBox><br />
                    <asp:Button ID="btnUpdatePIC_Auditee" runat="server" Text="Update" OnClick="btnUpdatePIC_Auditee_Click" />
                </div>
            </div>

        </div>

        <%--Modal Evidence Auditee--%>
        <div id="modalEvidenceAuditee" class="modal">

            <!-- Modal content -->
            <div class="modal-content">
                <div class="modal-header">
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                    <h2>Evidence</h2>
                </div>
                <div class="modal-body">
                    <span>Evidence : </span>
                    <asp:FileUpload ID="file_evidence" runat="server" /><br />
                    <asp:Button ID="btnUpdateFileEvidence" runat="server" Text="Update" OnClick="btnUpdateFileEvidence_Click" />
                </div>
            </div>

        </div>

        <%--Modal Position--%>
        <div id="modalPosition2" class="modal">

            <!-- Modal content -->
            <div class="modal-content">
                <div class="modal-header">
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                    <h2>Position</h2>
                </div>
                <div class="modal-body">
                    <span>Position : </span>
                    <asp:DropDownList ID="ddlPosition2" runat="server"></asp:DropDownList>
                    <asp:Button ID="btnUpdatePosition2" runat="server" Text="Update" OnClick="btnUpdatePosition2_Click" />
                </div>
            </div>

        </div>

        <%--Modal Corrective Action--%>
        <div id="modalCorrective2" class="modal">

            <!-- Modal content -->
            <div class="modal-content">
                <div class="modal-header">
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                    <h2>Corrective Action</h2>
                </div>
                <div class="modal-body">
                    <span>Corrective Action : </span>
                    <asp:TextBox ID="txtCorrective2" runat="server" TextMode="multiline" Columns="50" Rows="5"></asp:TextBox><br />
                    <asp:Button ID="btnUpdateCorrective2" runat="server" Text="Update" OnClick="btnUpdateCorrective2_Click" />
                </div>
            </div>

        </div>

        <%--Modal PIC Auditee--%>
        <div id="modalPICAuditee2" class="modal">

            <!-- Modal content -->
            <div class="modal-content">
                <div class="modal-header">
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                    <h2>PIC</h2>
                </div>
                <div class="modal-body">
                    <span>PIC : </span>
                    <asp:TextBox ID="txtPIC_Auditee2" runat="server" MaxLength="500"></asp:TextBox><br />
                    <asp:Button ID="btnUpdatePIC_Auditee2" runat="server" Text="Update" OnClick="btnUpdatePIC_Auditee2_Click" />
                </div>
            </div>

        </div>

        <%--Modal Evidence Auditee--%>
        <div id="modalEvidenceAuditee2" class="modal">

            <!-- Modal content -->
            <div class="modal-content">
                <div class="modal-header">
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                    <h2>Evidence</h2>
                </div>
                <div class="modal-body">
                    <span>Evidence : </span>
                    <asp:FileUpload ID="file_evidence2" runat="server" /><br />
                    <asp:Button ID="btnUpdateFileEvidence2" runat="server" Text="Update" OnClick="btnUpdateFileEvidence2_Click" />
                </div>
            </div>

        </div>



        <%--Modal Recommendation--%>
        <div id="modalRecommendation" class="modal">

            <!-- Modal content -->
            <div class="modal-content">
                <div class="modal-header">
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                    <h2>Recommendation</h2>
                </div>
                <div class="modal-body">
                    <span>Recommendation : </span>
                    <asp:TextBox ID="txtRecommendation" runat="server" TextMode="multiline" Columns="50" Rows="5" Style="width: 800px; height: 100px;"></asp:TextBox><br />
                    <asp:Button ID="btnUpdateRecommendation" runat="server" Text="Update" OnClick="btnUpdateRecommendation_Click" />
                </div>
            </div>

        </div>
        <%--Modal Hasil Audit--%>
        <div id="modalHasilAudit" class="modal">

            <!-- Modal content -->
            <div class="modal-content">
                <div class="modal-header">
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                    <h2>Hasil Audit</h2>
                </div>
                <div class="modal-body">
                    <span>Hasil Audit : </span>
                    <asp:TextBox ID="txtHasilAudit" runat="server" TextMode="multiline" Columns="50" Rows="5" Style="width: 800px; height: 100px;"></asp:TextBox><br />
                    <span>Hasil Audit Attachment: </span>
                    <asp:FileUpload ID="audit_result_attachment" runat="server" /><br />
                    <asp:Button ID="btnUpdateHasilAudit" runat="server" Text="Update" OnClick="btnUpdateHasilAudit_Click" />
                </div>
            </div>

        </div>
        <%--Modal Hasil Audit--%>
        <div id="modalHasilAuditAttachment" class="modal">

            <!-- Modal content -->
            <div class="modal-content">
                <div class="modal-header">
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                    <h2>Bukti Obyektif</h2>
                </div>
                <div class="modal-body">
                    <span>Hasil Audit Attachment: </span>
                    <asp:FileUpload ID="audit_result_attachment_edit" runat="server" /><br />
                    <asp:Button ID="btnUpdateHasilAuditAttachment" runat="server" Text="Update" OnClick="btnUpdateHasilAuditAttachment_Click" />
                </div>
            </div>

        </div>
        <%--Modal Verif 1--%>
        <div id="modalVerif1" class="modal">

            <!-- Modal content -->
            <div class="modal-content">
                <div class="modal-header">
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                    <h2>Verification Result 1</h2>
                </div>
                <div class="modal-body">
                    <span>Verification Result 1 : </span>
                    <asp:TextBox ID="txtVerif1" runat="server" TextMode="multiline" Columns="50" Rows="5"></asp:TextBox><br />
                    <asp:Button ID="btnUpdateVerif1" runat="server" Text="Update" OnClick="btnUpdateVerif1_Click" />
                </div>
            </div>

        </div>

        <%--Modal Verif 2--%>
        <div id="modalVerif2" class="modal">

            <!-- Modal content -->
            <div class="modal-content">
                <div class="modal-header">
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                    <h2>Verification Result 2</h2>
                </div>
                <div class="modal-body">
                    <span>Verification Result 2 : </span>
                    <asp:TextBox ID="txtVerif2" runat="server" TextMode="multiline" Columns="50" Rows="5"></asp:TextBox><br />
                    <asp:Button ID="btnUpdateVerif2" runat="server" Text="Update" OnClick="btnUpdateVerif2_Click" />
                </div>
            </div>

        </div>

        <%--Modal Status--%>
        <div id="modalStatus" class="modal">

            <!-- Modal content -->
            <div class="modal-content">
                <div class="modal-header">
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                    <h2>Status</h2>
                </div>
                <div class="modal-body">
                    <span>Status : </span>
                    <asp:DropDownList ID="ddlStatus" runat="server">
                    </asp:DropDownList><br />
                    <asp:Button ID="btnUpdateStatus" runat="server" Text="Update" OnClick="btnUpdateStatus_Click" />
                </div>
            </div>

        </div>

        <%--Modal PIC Auditor--%>
        <div id="modalPIC_Auditor" class="modal">

            <!-- Modal content -->
            <div class="modal-content">
                <div class="modal-header">
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                    <h2>PIC Auditor</h2>
                </div>
                <div class="modal-body">
                    <span>PIC Auditor : </span>
                    <asp:DropDownList ID="ddlPIC_Auditor" runat="server"></asp:DropDownList><br />
                    <asp:Button ID="btnUpdatePIC_Auditor" runat="server" Text="Update" OnClick="btnUpdatePIC_Auditor_Click" />
                </div>
            </div>

        </div>

        <%--Modal Delete--%>
        <div id="modalDelete" class="modal">

            <!-- Modal content -->
            <div class="modal-content">
                <div class="modal-header">
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                    <h2>Delete Checklist</h2>
                </div>
                <div class="modal-body">
                    <span>Delete Checklist : </span>
                    <br />
                    <asp:Button ID="btnDeleteChecklist" runat="server" Text="Yes" OnClick="btnDeleteChecklist_Click" />
                    <asp:Button ID="btnCancelDelete" runat="server" Text="No" OnClientClick="return false;" data-dismiss="modal" aria-label="Close" />
                </div>
            </div>

        </div>

        <div id="modalReject" class="modal">

            <!-- Modal content -->
            <div class="modal-content">
                <div class="modal-header">
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                    <h2>Reject FTA</h2>
                </div>
                <div class="modal-body">
                    <span>Reject Reason : </span>
                    <br />
                    <asp:TextBox ID="txtRejectReason" runat="server" TextMode="multiline" Columns="50" Rows="5" onkeyDown="return checkTextAreaMaxLength(this,event,'500');" onblur="checkLength(this, '500')"></asp:TextBox>
                    <asp:Button ID="btnReject" runat="server" Text="Reject" OnClick="btnReject_Click" />
                    <asp:Button ID="Button2" runat="server" Text="Cancel" OnClientClick="return false;" data-dismiss="modal" aria-label="Close" />
                </div>
            </div>

        </div>


        <%--Modal Insert Checklist--%>
        <div id="modalInsertChecklist" class="modal">

            <!-- Modal content -->
            <div class="modal-content">
                <div class="modal-header">
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                    <h2>Insert New Checklist</h2>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="updPan1" runat="server">
                        <ContentTemplate>
                            <table>
                                <%--<tr>
                            <td>Checklist No</td>
                            <td><asp:TextBox ID="txtChecklistNo_NewChecklist" runat="server" MaxLength="500"></asp:TextBox></td>
                        </tr>--%>
                                <tr>
                                    <td>Main Activity</td>
                                    <%--<td><asp:TextBox ID="txtMainActivity_NewChecklist" runat="server" MaxLength="500"></asp:TextBox></td>--%>
                                    <td>
                                        <asp:DropDownList ID="ddlMainActivity_NewChecklist" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlMainActivity_NewChecklist_SelectedIndexChanged"></asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td>Sub Activity</td>
                                    <!--<td><%--<asp:TextBox ID="txtSubActivity_NewChecklist" runat="server" MaxLength="500"></asp:TextBox>--%></td> -->
                                    <td>
                                        <asp:DropDownList ID="ddlSubActivity_NewChecklist" runat="server"></asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td>Findings Category</td>
                                    <td>
                                        <asp:DropDownList ID="ddlFindingCategory_NewChecklist" runat="server"></asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td>Case Category</td>
                                    <td>
                                        <asp:DropDownList ID="ddlCaseCategory_NewChecklist" runat="server"></asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td>Audit Result</td>
                                    <td>
                                        <asp:TextBox ID="txtAuditResult_NewChecklist" runat="server" TextMode="multiline" Columns="50" Rows="5" Style="width: 800px; height: 100px;"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td>Audit Result Attachment</td>
                                    <td>
                                        <asp:FileUpload ID="audit_result_attachment_NewChecklist" runat="server" /></td>
                                </tr>
                                <tr>
                                    <td>Suspect</td>
                                    <td>
                                        <asp:TextBox ID="txtSuspect_NewChecklist" runat="server" TextMode="multiline" Columns="50" Rows="5" onkeyDown="return checkTextAreaMaxLength(this,event,'100');" onblur="checkLength(this, '100')"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td>Responsibility 1st</td>
                                    <td>
                                        <asp:DropDownList ID="ddlPosition_NewChecklist" runat="server"></asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td>Responsibility 2nd</td>
                                    <td>
                                        <asp:DropDownList ID="ddlPosition2_NewChecklist" runat="server"></asp:DropDownList></td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>


                    <asp:Button ID="btnInsert_NewChecklist" runat="server" Text="Insert" OnClick="btnInsert_NewChecklist_Click" CssClass="button2" />
                </div>
            </div>

        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            var table = $('#mytable').DataTable({
                scrollX: "2000px",
                scrollCollapse: true,
                bProcessing: true, // shows 'processing' label
                bStateSave: true, // presumably saves state for reloads
                paging: true,
                pagelength: 5,
                lengthMenu: [
                    [5, 10, 25, 50, -1],
                    ['5 rows', '10 rows', '25 rows', '50 rows', 'Show all']
                ],
                fixedColumns: {
                    leftColumns: 2
                },
                "fnDrawCallback": function () {

                    $table = $(this);

                    // only apply this to specific tables
                    if ($table.closest(".datatable-multi-row").length) {

                        // for each row in the table body...
                        $table.find("tbody>tr").each(function () {
                            var $tr = $(this);

                            // get the "extra row" content from the <script> tag.
                            // note, this could be any DOM object in the row.
                            var extra_row = $tr.find(".extra-row-content").html();

                            // in case draw() fires multiple times, 
                            // we only want to add new rows once.
                            if (!$tr.next().hasClass('dt-added')) {
                                $tr.after(extra_row);
                                $tr.find("td").each(function () {

                                    // for each cell in the top row,
                                    // set the "rowspan" according to the data value.
                                    var $td = $(this);
                                    var rowspan = parseInt($td.data("datatable-multi-row-rowspan"), 10);
                                    if (rowspan) {
                                        $td.attr('rowspan', rowspan);
                                    }
                                });
                            }

                        });

                    } // end if the table has the proper class
                } // end fnDrawCallback()
            });

            $('#mytable1 > tbody a').each(function () {
                $(this).attr('href', this.href);
            });

            var table_hidden = $('#mytable1').DataTable({
                paging: false,
                "fnDrawCallback": function () {

                    $table = $(this);

                    // only apply this to specific tables
                    if ($table.closest(".datatable-multi-row").length) {

                        // for each row in the table body...
                        $table.find("tbody>tr").each(function () {
                            var $tr = $(this);

                            // get the "extra row" content from the <script> tag.
                            // note, this could be any DOM object in the row.
                            var extra_row = $tr.find(".extra-row-content").html();

                            // in case draw() fires multiple times, 
                            // we only want to add new rows once.
                            if (!$tr.next().hasClass('dt-added')) {
                                $tr.after(extra_row);
                                $tr.find("td").each(function () {

                                    // for each cell in the top row,
                                    // set the "rowspan" according to the data value.
                                    var $td = $(this);
                                    var rowspan = parseInt($td.data("datatable-multi-row-rowspan"), 10);
                                    if (rowspan) {
                                        $td.attr('rowspan', rowspan);
                                    }
                                });
                            }

                        });

                    } // end if the table has the proper class
                } // end fnDrawCallback()
            });
            // Get the modal
            $('#mytable1').remove("class");
        });

        window.onload = function () {
            var div = $(".dataTables_scrollBody");
            var div_position = $("#<%= scroll_position.ClientID %>");
            var position = parseInt($("#<%= scroll_position.ClientID %>").val());
            if (isNaN(position)) {
                position = 0;
            }
            div.scrollLeft(position);
            div.scroll(function () {
                div_position.val(div.scrollLeft());
            });
        };

        $("#<%= ddlMainActivity_NewChecklist.ClientID%>").change(function () {
            //console.log("tes1");
            $("#<%= ddlMainActivity_NewChecklist.ClientID%> > option:selected").each(function () {
                //console.log(this.value);
                if (this.value != "-") {
                    //loadSubActivity(this.value);
                }
            });
        });
        function loadSubActivity(kode_main_activity) {
            $.ajax({
                type: "POST",
                url: "fta.aspx/LoadSubActivity",
                data: '{kode: "' + kode_main_activity + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    //$("#<%= ddlSubActivity_NewChecklist.ClientID%>").empty();
                    //alert(response.d);
                    drawDdlSubActivity(JSON.parse(response.d));
                    //turnToDatatable();
                },
                error: function () { alert("Error! Tidak bisa mendapatkan data sub activity"); delete_gameboard(); }
            });
        }
        function drawDdlSubActivity(data) {
            $("#<%= ddlSubActivity_NewChecklist.ClientID%>").empty();
            $.each(data, function (index, item) {
                $("#<%= ddlSubActivity_NewChecklist.ClientID%>").append('<option value=' + item.value + '>' + item.text + '</option>');
                //console.log(item.CompID + "-" + item.CompName);
            });
        }
        function fnExcelReport() {
            var tab_text = "<table border='2px'><tr bgcolor='#87AFC6'>";
            var textRange; var j = 0;
            tab = document.getElementById('mytable1'); // id of table

            var tab_header = $("table.cell-border.table.table-striped.table-bordered.datatable-multi-row.dataTable.no-footer")[0];

            for (j = 0; j < tab_header.rows.length; j++) {
                tab_text = tab_text + tab_header.rows[j].innerHTML + "</tr>";
                //tab_text=tab_text+"</tr>";
            }

            for (j = 2; j < tab.rows.length; j++) {
                tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
                //tab_text=tab_text+"</tr>";
            }

            tab_text = tab_text + "</table>";
            tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
            tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
            tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params
            tab_text = tab_text.replace(/<button[^>]*>|<\/button>/gi, ""); // reomves input params
            tab_text = tab_text.replace(/<br>Update/g, ""); // reomves input params
            //console.log(tab_text);
            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");

            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
            {
                txtArea1.document.open("txt/html", "replace");
                txtArea1.document.write(tab_text);
                txtArea1.document.close();
                txtArea1.focus();
                sa = txtArea1.document.execCommand("SaveAs", true, "Say Thanks to Sumit.xls");
            }
            else                 //other browser not tested on IE 11
                sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

            return (sa);
        }

        function deleteChecklist(id) {
            $("#ContentPlaceHolder1_hdnIdChklist").val(id);
        }

        function setUpdateStatus(id, jenisupdate, status_value) {
            $("#ContentPlaceHolder1_hdnIdChklist").val(id);
            $("#ContentPlaceHolder1_hdnJenisUpdate").val(jenisupdate);

            switch (jenisupdate) {
                case "fin_category":
                    $("#ContentPlaceHolder1_ddlFindingCategory").val(status_value).change();
                    break;
                case "case_category":
                    $("#ContentPlaceHolder1_ddlCaseCategory").val(status_value).change();
                    break;
                case "audit_result":
                    $("#<%= txtHasilAudit.ClientID%>").val(status_value).change();
                    break;
                case "suspect":
                    $("#ContentPlaceHolder1_txtSuspect").val(status_value);
                    break;
                case "position":
                    $("#ContentPlaceHolder1_ddlPosition").val(status_value).change();
                    break;
                case "corrective":
                    $("#ContentPlaceHolder1_txtCorrective").val(status_value);
                    break;
                case "pic_auditee":
                    $("#ContentPlaceHolder1_txtPIC_Auditee").val(status_value);
                    break;
                case "evidence":
                    //$("#ContentPlaceHolder1_txtCorrective").val(status_value);
                    break;
                case "position2":
                    $("#ContentPlaceHolder1_ddlPosition2").val(status_value).change();
                    break;
                case "corrective2":
                    $("#ContentPlaceHolder1_txtCorrective2").val(status_value);
                    break;
                case "pic_auditee2":
                    $("#ContentPlaceHolder1_txtPIC_Auditee2").val(status_value);
                    break;
                case "evidence2":
                    //$("#ContentPlaceHolder1_txtCorrective").val(status_value);
                    break;
                case "recommendation":
                    $("#ContentPlaceHolder1_txtRecommendation").val(status_value);
                    break;
                case "verif1":
                    $("#ContentPlaceHolder1_txtVerif1").val(status_value);
                    break;
                case "verif2":
                    $("#ContentPlaceHolder1_txtVerif2").val(status_value);
                    break;
                case "status":
                    $("#ContentPlaceHolder1_ddlStatus").val(status_value);
                    break;
                case "pic_auditor":
                    $("#ContentPlaceHolder1_ddlPIC_Auditor").val(status_value).change();
                    break;
            }
        }

        function getTableData() {
            //var no_awb = $("#ContentPlaceHolder1_doBranch").val();
            var branch = $("#ContentPlaceHolder1_doBranch").val();
            var periode = $("#ContentPlaceHolder1_doPeriode").val();
            console.log("asd");
            var table = $('#mytable').DataTable({
                scrollY: "1000px",
                scrollX: "2000px",
                scrollCollapse: true,
                bProcessing: true, // shows 'processing' label
                bStateSave: true, // presumably saves state for reloads
                paging: true,
                pagelangth: 5,
                lengthMenu: [
                    [5, 10, 25, 50, -1],
                    ['5 rows', '10 rows', '25 rows', '50 rows', 'Show all']
                ],
                fixedColumns: {
                    leftColumns: 2
                },
                ajax: {
                    type: "POST",
                    url: "fta.aspx/TableLoad",
                    data: function (d) {
                        return "{ branch: '" + branch + "', periode: '" + periode + "' }";
                    },
                    contentType: "application/json; charset=utf-8",
                    dataType: "json"
                },
                columns: [
                    { "data": "Kodecab" },
                    { "data": "month" },
                    { "data": "year" },
                    { "data": "No" },
                    { "data": "main_activity" },
                    { "data": "sub_activity" },
                    { "data": "fin_category" },
                    { "data": "case_category" },
                    { "data": "Hasil_audit" },
                    { "data": "suspect" },
                    { "data": "position" },
                    { "data": "corrective_action" },
                    { "data": "pic_auditee" },
                    { "data": "evidence" },
                    { "data": "recommendation" },
                    { "data": "start_date" },
                    { "data": "due_date" },
                    { "data": "verif_result1" },
                    { "data": "verif_result2" },
                    { "data": "status" },
                    { "data": "finish_date" },
                    { "data": "pic_auditor" }
                ]
            });
            //$.ajax({
            //    type: "POST",
            //    url: "fta.aspx/TableLoad",
            //    data: '{branch: "' + branch + '", periode: "' + periode +'"}',
            //    contentType: "application/json; charset=utf-8",
            //    dataType: "json",
            //    success: function (response) {
            //        delete_gameboard()
            //        //alert(response.d);
            //        drawTable(JSON.parse(response.d));
            //        //turnToDatatable();
            //    },
            //    error: function () { alert("Checklist Tidak Ditemukan/Belum di Approve"); delete_gameboard(); }
            //});
        }

        function turnToDatatable() {

            var table = $('#mytable').DataTable({
                scrollY: "1000px",
                scrollX: "800px",
                scrollCollapse: true,
                bProcessing: true, // shows 'processing' label
                bStateSave: true, // presumably saves state for reloads
                paging: true,
                pagelangth: 5,
                lengthMenu: [
                    [5, 10, 25, 50, -1],
                    ['5 rows', '10 rows', '25 rows', '50 rows', 'Show all']
                ],
                fixedColumns: {
                    leftColumns: 2
                }
            });

        }

        function delete_gameboard() {
            var table = document.getElementById("mytable");
            var rowCount = table.rows.length;

            while (table.rows.length > 1) {
                table.deleteRow(1);
            }
        }

        function drawTable(data) {
            for (var i = 0; i < data.length; i++) {
                drawRow(data[i]);
            }
        }

        function drawRow(rowData) {
            var row = $("<tr/>")
            $("#mytable").append(row); //this will append tr element to table... keep its reference for a while since we will add cels into it
            row.append($("<td>" + rowData.Kodecab + "</td>"));
            row.append($("<td>" + rowData.month + "</td>"));
            row.append($("<td>" + rowData.year + "</td>"));
            row.append($("<td>" + rowData.No + "</td>"));
            row.append($("<td>" + rowData.main_activity + "</td>"));
            row.append($("<td>" + rowData.sub_activity + "</td>"));
            row.append($("<td>" + rowData.fin_category + "</td>"));
            row.append($("<td>" + rowData.case_category + "</td>"));
            row.append($("<td>" + rowData.Hasil_audit + "</td>"));
            row.append($("<td>" + rowData.suspect + "</td>"));
            row.append($("<td>" + rowData.position + "</td>"));
            row.append($("<td>" + rowData.corrective_action + "</td>"));
            row.append($("<td>" + rowData.pic_auditee + "</td>"));
            row.append($("<td>" + rowData.evidence + "</td>"));
            row.append($("<td>" + rowData.recommendation + "</td>"));
            row.append($("<td>" + rowData.start_date + "</td>"));
            row.append($("<td>" + rowData.due_date + "</td>"));
            row.append($("<td>" + rowData.verif_result1 + "</td>"));
            row.append($("<td>" + rowData.verif_result2 + "</td>"));
            row.append($("<td>" + rowData.status + "</td>"));
            row.append($("<td>" + rowData.finish_date + "</td>"));
            row.append($("<td>" + rowData.pic_auditor + "</td>"));
        }
        function checkTextAreaMaxLength(textBox, e, length) {

            var mLen = length;

            var maxLength = parseInt(mLen);
            if (!checkSpecialKeys(e)) {
                if (textBox.value.length > maxLength - 1) {
                    if (window.event)//IE
                    {
                        e.returnValue = false;
                        return false;
                    }
                    else//Firefox
                        e.preventDefault();
                }
            }
        }

        function checkSpecialKeys(e) {

            if (e.keyCode != 9 && e.keyCode != 8 && e.keyCode != 46 &&
                e.keyCode != 35 && e.keyCode != 36 && e.keyCode != 37 &&
                e.keyCode != 38 && e.keyCode != 39 && e.keyCode != 40)
                return false;
            else
                return true;
        }
        function checkLength(textBox, length) {
            var mLen = length;

            var maxLength = parseInt(mLen);

            if (textBox.value.length > maxLength) {
                textBox.value = textBox.value.substring(0, maxLength);
            }
        }
    </script>
</asp:Content>
