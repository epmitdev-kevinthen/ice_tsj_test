﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="SubKelPemeriksaan.aspx.vb" Inherits="SubKelPemeriksaan" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="script/jquery-1.4.1.min.js"></script>
    <script>
        $(document).ready(function () {
            var table = $("#ContentPlaceHolder1_GridView1").DataTable({
                paging: true,
                searching: true,
                lengthChange: false,
            });
            $('#ContentPlaceHolder1_GridView1 tbody').on('click', 'tr', function () {
                var data = table.row(this).data();

                $("#ContentPlaceHolder1_txtSubKode").val(data[0]);
                $("#ContentPlaceHolder1_txtSubKelPer").val(data[1]);
                $("#ContentPlaceHolder1_subcreatedBy").text(data[2]);
                $("#ContentPlaceHolder1_subcreatedDate").text(data[3]);
                $("#ContentPlaceHolder1_sublastUpdateBy").text(data[4]);
                $("#ContentPlaceHolder1_sublastUpdateDate").text(data[5]);
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <asp:Label ID="lblHead" runat="server" Text="Master Sub Kelompok Pemeriksaan"
        Style="font-size: 20px;"></asp:Label>
    <div style="display: flex;">
        <table>
            <tr>
                <td>Kode</td>
                <td>:</td>
                <td>
                    <asp:label ID="lblKodeCab" runat="server"></asp:label></td>
            </tr>
            <tr>
                <td>Kelompok Pemeriksaan</td>
                <td>:</td>
                <td>
                    <asp:label ID="lblKelPer" runat="server"></asp:label></td>
            </tr>
        </table>
        <div style="float: right; margin-left: 15px;">
            Created By: <span id="createdBy" runat="server"></span>
            <br />
            Creation Date: <span id="createdDate" runat="server"></span>
            <br />
            Last Update By: <span id="lastUpdateBy" runat="server"></span>
            <br />
            Last Update Date:<span id="lastUpdateDate" runat="server"></span><br />
        </div>
    </div>
    <br />
    <div style="display: flex;">
        <table>
            <tr>
                <td>Sub Kode</td>
                <td>:</td>
                <td>
                    <asp:TextBox ID="txtSubKode" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Sub Kelompok Pemeriksaan</td>
                <td>:</td>
                <td>
                    <asp:TextBox ID="txtSubKelPer" runat="server"></asp:TextBox></td>
            </tr>
        </table>
        <div style="float: right; margin-left: 15px;">
            Created By: <span id="subcreatedBy" runat="server"></span>
            <br />
            Creation Date: <span id="subcreatedDate" runat="server"></span>
            <br />
            Last Update By: <span id="sublastUpdateBy" runat="server"></span>
            <br />
            Last Update Date:<span id="sublastUpdateDate" runat="server"></span><br />
        </div>
    </div>
    <br />
    <br />
    <asp:Button id="btnBackKel" runat="server" Text="Back To Kelompok Pemeriksaan" OnClick="btnBackKel_Click" CssClass="button2"/>
    <div id="div_lbl" runat="server">
        <asp:Label ID="lbl_msg" runat="server" ForeColor="Red"></asp:Label>
    </div>
    <asp:Button ID="clearData" runat="server" Text="Clear Data" OnClick="clearData_Click" CssClass="button2"/>
    <asp:Button ID="insertData" runat="server" Text="Insert Data" OnClick="insertData_Click" CssClass="button2"/>
    <asp:Button ID="updateData" runat="server" Text="Update Data" OnClick="updateData_Click" CssClass="button2"/>
    <asp:Button ID="deleteData" runat="server" Text="Delete Data" onclick="deleteData_Click" CssClass="button2"/>

    <div>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" AllowPaging="false" AllowSorting="false"
            PageSize="10" CssClass="dataTable">
            <Columns>
                <asp:BoundField DataField="subkode" HeaderText="Sub Kode" />
                <asp:BoundField DataField="subkelperiksa" HeaderText="Sub Kelompok Pemeriksaan" />
                <asp:Boundfield DataField="created_by" HeaderText="RBM" >
                    <HeaderStyle CssClass="hidden"></HeaderStyle>

                        <ItemStyle CssClass="hidden"></ItemStyle>
                </asp:Boundfield>
                <asp:Boundfield DataField="creation_date" HeaderText="RBM" >
                    <HeaderStyle CssClass="hidden"></HeaderStyle>

                        <ItemStyle CssClass="hidden"></ItemStyle>
                </asp:Boundfield>
                <asp:Boundfield DataField="last_update_by" HeaderText="RBM" >
                    <HeaderStyle CssClass="hidden"></HeaderStyle>

                        <ItemStyle CssClass="hidden"></ItemStyle>
                </asp:Boundfield>
                <asp:Boundfield DataField="last_update_date" HeaderText="RBM" >
                    <HeaderStyle CssClass="hidden"></HeaderStyle>

                        <ItemStyle CssClass="hidden"></ItemStyle>
                </asp:Boundfield>
            </Columns>
            <PagerStyle CssClass="paginate_button" />
        </asp:GridView>
    </div>
</asp:Content>


