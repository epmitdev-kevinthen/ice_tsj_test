﻿
Partial Class strCheckList
    Inherits System.Web.UI.Page
    Dim myChef As New Chef

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click


        Try

            infoerror.Text = ""
            infoerror.Text = myChef.SQLDelCHECKLIST(TxtKode2.Text)
            Response.Redirect(HttpContext.Current.Request.Url.ToString(), True)
        Catch ex As Exception
            infoerror.Text = ex.Message
        End Try


    End Sub

    Private Sub btnInsert_Click(sender As Object, e As EventArgs) Handles btnInsert.Click

        Try

            infoerror.Text = ""

            Dim mpLabel As Label
            Dim myusername As String = ""
            mpLabel = CType(Master.FindControl("lblusername"), Label)
            If Not mpLabel Is Nothing Then
                myusername = mpLabel.Text
            End If
            infoerror.Text = myChef.SQLNewCHECKLIST(TxtKode3.Text, TxtNama_Tmp3.Text,
                               txtStartDate3.Text, TxtEndDate3.Text, "I", "I", myusername, txtSelfAssesment3.Text, txtdept3.Text, txtNNILAI3.Text, txtPNILAI3.Text, ddlNotif_New.SelectedValue)
            Response.Redirect(HttpContext.Current.Request.Url.ToString(), True)
        Catch ex As Exception
            infoerror.Text = ex.Message
        End Try

    End Sub

    Private Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click

        Try

            infoerror.Text = ""

            Dim mpLabel As Label
            Dim myusername As String = ""
            mpLabel = CType(Master.FindControl("lblusername"), Label)
            If Not mpLabel Is Nothing Then
                myusername = mpLabel.Text
            End If
            infoerror.Text = myChef.SQLEditCHECKLIST(TxtKode.Text, TxtNama_Tmp.Text,
                               txtStartDate.Text, TxtEndDate.Text, TxtFlagEntry.Text, TxtFlagReport.Text, myusername,
                                                     txtSelfAssesment.Text, txtDept.Text,
                                                     txtNNILAI.Text, txtPNILAI.Text, ddlNotif_Edit.SelectedValue
                                )
            Response.Redirect(HttpContext.Current.Request.Url.ToString(), True)
        Catch ex As Exception
            infoerror.Text = ex.Message
        End Try


    End Sub

    Private Sub strCheckList_Init(sender As Object, e As EventArgs) Handles Me.Init
        Dim myData As Data.DataSet
        Dim myhtml As New StringBuilder


        If Session("sUsername") Is Nothing Then
            Response.Redirect("Default.aspx")
        End If


        If Session("levelakses") Is Nothing Then
            Response.Redirect("hakakses.aspx")
        ElseIf IsDBNull(Session("levelakses")) Then
            Response.Redirect("hakakses.aspx")
        ElseIf Session("levelakses") <> "1" And Session("levelakses") <> "2" Then
            Response.Redirect("hakakses.aspx")
        End If



        TxtFlagEntry3.Items.Add("A")
        TxtFlagEntry3.Items.Add("I")
        TxtFlagReport3.Items.Add("A")
        TxtFlagReport3.Items.Add("I")
        TxtFlagEntry.Items.Add("A")
        TxtFlagEntry.Items.Add("I")
        TxtFlagReport.Items.Add("A")
        TxtFlagReport.Items.Add("I")

        txtSelfAssesment.Items.Add("Y")
        txtSelfAssesment.Items.Add("N")
        txtSelfAssesment3.Items.Add("Y")
        txtSelfAssesment3.Items.Add("N")

        myData = myChef.SQLDIVISION
        txtDept.Items.Add("")
        txtdept3.Items.Add("")
        txtDept.Items(txtDept.Items.Count - 1).Value = ""
        txtdept3.Items(txtDept.Items.Count - 1).Value = ""
        For i = 0 To myData.Tables(0).Rows.Count - 1
            txtDept.Items.Add(myData.Tables(0).Rows(i).Item(0) & "-" & myData.Tables(0).Rows(i).Item(1))
            txtDept.Items(txtDept.Items.Count - 1).Value = myData.Tables(0).Rows(i).Item(0)
            txtdept3.Items.Add(myData.Tables(0).Rows(i).Item(0) & "-" & myData.Tables(0).Rows(i).Item(1))
            txtdept3.Items(txtdept3.Items.Count - 1).Value = myData.Tables(0).Rows(i).Item(0)
        Next


        myData = myChef.SQLCHECKLIST
        myhtml.Append(" <table id='mytable' class='stripe row-border order-column' cellspacing='0' width='100%'> ")
        myhtml.Append("   <thead>")
        myhtml.Append("      <tr>")
        myhtml.Append("         <th>Kode</th>")
        myhtml.Append("         <th></th>")
        myhtml.Append("         <th></th>")
        myhtml.Append("         <th>Nama Template</th>")
        myhtml.Append("         <th>Start Date</th>")
        myhtml.Append("         <th>End Date</th>")
        myhtml.Append("         <th>Flag Entry? / Flag Report?</th>")
        myhtml.Append("         <th>Flag Notification</th>")
        'myhtml.Append("         <th>Flag Report?</th>")
        myhtml.Append("         <th>Self Assesment</th>")
        myhtml.Append("         <th>Division</th>")
        myhtml.Append("         <th>Hasil</br>Penilaian</th>")
        myhtml.Append("         <th>Pilihan</br>Penilaian</th>")
        myhtml.Append("         <th></th>")
        myhtml.Append("      </tr>")
        myhtml.Append("   </thead>")
        myhtml.Append("   <tbody>")
        For i = 0 To myData.Tables(0).Rows.Count - 1
            myhtml.Append("      <tr>")

            Dim mydate1 As String = Format(myData.Tables(0).Rows(i).Item("start_date"), "d-MMM-yyyy")
            Dim mydate2 As String = Format(myData.Tables(0).Rows(i).Item("end_date"), "d-MMM-yyyy")

            Dim Temps As String = myData.Tables(0).Rows(i).Item("kode") & "|" &
                                  myData.Tables(0).Rows(i).Item("nama_tmp") & "|" &
                                  mydate1 & "|" & mydate2 & "|" &
                                  myData.Tables(0).Rows(i).Item("flag_entry") & "|" &
                                  myData.Tables(0).Rows(i).Item("flag_report") & "|" &
                                  myData.Tables(0).Rows(i).Item("flag_selasses") & "|" &
                                  myData.Tables(0).Rows(i).Item("kodedept") & "|" &
                                  myData.Tables(0).Rows(i).Item("NNILAI") & "|" &
                                  myData.Tables(0).Rows(i).Item("PNILAI") & "|" &
                                  myData.Tables(0).Rows(i).Item("flag_notif")

            myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("kode") & "</td>")

            myhtml.Append("<td class='text-right'>")
            myhtml.Append("<button type='button' class='button2xx' onClick='btnClick(" & Chr(34) & myData.Tables(0).Rows(i).Item("kode") & Chr(34) & ")'>Detail</button>")
            myhtml.Append("</td>")

            myhtml.Append("<td class='text-right'>")
            myhtml.Append("<button type='button' class='button2xx' data-book-id='" & Temps & "' data-target='#formModalEdit' data-toggle='modal'>Edit</button>")
            myhtml.Append("</td>")

            myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("nama_tmp") & "</td>")
            myhtml.Append("         <td>" & mydate1 & "</td>")
            myhtml.Append("         <td>" & mydate2 & "</td>")

            myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("flag_entry") & " / " & myData.Tables(0).Rows(i).Item("flag_report") & "</td>")
            myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("flag_notif") & "</td>")
            'myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("flag_report") & "</td>")
            myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("flag_selasses") & "</td>")
            myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("kodedept") & "</td>")

            myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("NNILAI") & "</td>")
            myhtml.Append("         <td>" & myData.Tables(0).Rows(i).Item("PNILAI") & "</td>")

            'myhtml.Append("         <td> </td>")

            myhtml.Append("<td class='text-right'>")
            myhtml.Append("<button type='button' class='button2xx' data-book-id='" & Temps & "' data-target='#formModalDelete' data-toggle='modal'>Delete</button>")
            myhtml.Append("</td>")

            myhtml.Append("      </tr>")
        Next
        myhtml.Append("   </tbody>")
        myhtml.Append(" </table> ")

        myPlaceH.Controls.Add(New Literal() With {
          .Text = myhtml.ToString()
         })
    End Sub




End Class
