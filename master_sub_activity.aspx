﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="master_sub_activity.aspx.vb" Inherits="master_sub_activity" MasterPageFile="~/MasterPage.master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .none{
            display:none;
        }
    </style>
    <script type="text/javascript" src="script/jquery-1.4.1.min.js"></script>
    <script>
        $(document).ready(function () {
            var table = $("#ContentPlaceHolder1_GridView1").DataTable({
                paging: true,
                searching: true,
                lengthChange: false,
            });
            $('#ContentPlaceHolder1_GridView1 tbody').on('click', 'tr', function () {
                var data = table.row(this).data();
                console.log(data);
                $("#<%= hdnID.ClientID%>").val(data[0]);
                $("#<%= ddlMainActivity.ClientID%>").val(data[1]).change();
                $("#<%= txtKodeSubActivity.ClientID%>").val(data[2]);
                $("#<%= txtNamaSubActivity.ClientID%>").val(data[3]);
                $("#ContentPlaceHolder1_createdBy").text(data[4]);
                $("#ContentPlaceHolder1_createdDate").text(data[5]);
                $("#ContentPlaceHolder1_lastUpdateBy").text(data[6]);
                $("#ContentPlaceHolder1_lastUpdateDate").text(data[7]);
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <asp:Label ID="lblHead" runat="server" Text="Master Region"
        Style="font-size: 20px;"></asp:Label>
    <div>
        <asp:Label ID="userEdit" runat="server"></asp:Label>
    </div>
    <div style="display: flex;">
        <table>
            <tr>
                <td>Main Activity</td>
                <td>:</td>
                <td>
                    <asp:DropDownList ID="ddlMainActivity" runat="server"></asp:DropDownList></td>
            </tr>
            <tr>
                <td>Kode Sub Activity</td>
                <td>:</td>
                <td>
                    <asp:TextBox ID="txtKodeSubActivity" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Nama Sub Activity</td>
                <td>:</td>
                <td>
                    <asp:TextBox ID="txtNamaSubActivity" runat="server"></asp:TextBox></td>
            </tr>
        </table>
        <div style="float: right; margin-left: 15px;">
            Created By: <span id="createdBy" runat="server"></span>
            <br />
            Creation Date: <span id="createdDate" runat="server"></span>
            <br />
            Last Update By: <span id="lastUpdateBy" runat="server"></span>
            <br />
            Last Update Date:<span id="lastUpdateDate" runat="server"></span><br />
        </div>
        <asp:HiddenField id="hdnID" runat="server"/>
    </div>
    <br />
    <br />
    <div id="div_lbl" runat="server">
        <asp:Label ID="lbl_msg" runat="server"></asp:Label>
    </div>
    <asp:Button ID="clearData" runat="server" Text="Clear Data" OnClick="clearData_Click" CssClass="button2"/>
    <asp:Button ID="insertData" runat="server" Text="Insert Data" OnClick="insertData_Click" CssClass="button2"/>
    <asp:Button ID="updateData" runat="server" Text="Update Data" OnClick="updateData_Click" CssClass="button2"/>
    <asp:Button ID="deleteData" runat="server" Text="Delete Data" OnClick="deleteData_Click" CssClass="button2"/>

    <div>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" AllowPaging="false" AllowSorting="false"
            PageSize="10" CssClass="dataTable">
            <Columns>
                <asp:BoundField DataField="id" HeaderText="id" HeaderStyle-CssClass="none" ItemStyle-CssClass="none"/>
                <asp:BoundField DataField="kode_main_activity" HeaderText="Kode Main Activity" />
                <asp:BoundField DataField="kode_sub_activity" HeaderText="Kode Sub Activity" />
                <asp:BoundField DataField="sub_activity" HeaderText="Sub Activity" />
                <asp:Boundfield DataField="created_by" HeaderText="RBM" >
                    <HeaderStyle CssClass="hidden"></HeaderStyle>

                        <ItemStyle CssClass="hidden"></ItemStyle>
                </asp:Boundfield>
                <asp:Boundfield DataField="creation_date" HeaderText="RBM" >
                    <HeaderStyle CssClass="hidden"></HeaderStyle>

                        <ItemStyle CssClass="hidden"></ItemStyle>
                </asp:Boundfield>
                <asp:Boundfield DataField="last_update_by" HeaderText="RBM" >
                    <HeaderStyle CssClass="hidden"></HeaderStyle>

                        <ItemStyle CssClass="hidden"></ItemStyle>
                </asp:Boundfield>
                <asp:Boundfield DataField="last_update_date" HeaderText="RBM" >
                    <HeaderStyle CssClass="hidden"></HeaderStyle>

                        <ItemStyle CssClass="hidden"></ItemStyle>
                </asp:Boundfield>
            </Columns>
            <PagerStyle CssClass="paginate_button" />
        </asp:GridView>
    </div>
</asp:Content>


