﻿Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.IO
Imports System.Net.Mail
Imports System.Net.Http

Partial Class fta
    Inherits System.Web.UI.Page

    Public Shared strConn As String = ConfigurationManager.ConnectionStrings("SQLICE").ConnectionString
    Dim cmd As New SqlCommand
    Dim myChef As New Chef

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim myData As Data.DataSet
            Dim MyDTString As String
            Dim MyDT As DateTime = Now
            Dim j As Integer


            If Session("sUsername") Is Nothing Then
                Response.Redirect("Default.aspx")
            Else
                sUsername_.Text = Session("sUsername")
                levelakses_.Text = Session("levelakses")
                jabatan_.Text = Session("jabatan")
                sCabang_.Text = Session("sCabang")
                kodedept_.Text = Session("kodedept")
            End If

            myData = myChef.SQLMSTPERIODEAKTIF

            j = 1
            doPeriode.Items.Add("")
            For i = 0 To myData.Tables(0).Rows.Count - 1

                doPeriode.Items.Add(myData.Tables(0).Rows(i).Item("nama_prd"))
                doPeriode.Items(j).Value = myData.Tables(0).Rows(i).Item("kode_prd")
                j = j + 1

            Next
            myData = myChef.SQLLISTCAB
            j = 1
            doBranch.Items.Add("")
            For i = 0 To myData.Tables(0).Rows.Count - 1

                'If sCabang_.Text = "PST" Or sCabang_.Text = myData.Tables(0).Rows(i).Item("kodecab") Then

                doBranch.Items.Add(myData.Tables(0).Rows(i).Item("namacab"))
                doBranch.Items(j).Value = myData.Tables(0).Rows(i).Item("kodecab")
                j = j + 1

                'End If
            Next
            btnConfirm.Visible = False
            btnApproveChecklist.Visible = False
            btnRejectModal.Visible = False
            btnImportChecklist.Visible = False
            divRejected.Visible = False
            loadAuditor()
            loadAllDropdownList()

            Dim dt As New DataTable
            If Request.QueryString("fta_no") <> "" And Request.QueryString("kode_prd") <> "" Then
                dt = GetDataSql("select * from FTA_CHECKLIST_HDR where fta_no = '" & Request.QueryString("fta_no") & "' and kode_prd = '" & Request.QueryString("kode_prd") & "'")
                If dt.Rows.Count > 0 Then
                    doBranch.SelectedValue = dt.Rows(0).Item("Kodecab")
                    doPeriode.SelectedValue = dt.Rows(0).Item("Kode_Prd")
                    ddlKelompokPemeriksa.SelectedValue = dt.Rows(0).Item("kel_pemeriksa").ToString
                    hdnKode_Tmp.Value = dt.Rows(0).Item("Kode_tmp").ToString
                    loadTable()
                End If
                doBranch.Enabled = False
                doPeriode.Enabled = False
                ddlKelompokPemeriksa.Enabled = False
            End If

            loadFindingCaseCategory()
            loadMainActivity()

            'sendNotif()
            'kirimEmail("admin_fta@enseval.com", "kevin.then@enseval.com", "Tes", "tes", "")
        End If
    End Sub
    Sub loadFindingCaseCategory()
        Dim dt As DataTable = GetDataSql("select * from MSTFINDINGCATEGORY where kel_pemeriksa like '%" & ddlKelompokPemeriksa.SelectedValue & "%' or kel_pemeriksa is null or kel_pemeriksa = '' order by namacategory")
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                ddlFindingCategory.Items.Add(New ListItem(dt.Rows(i).Item("namacategory"), dt.Rows(i).Item("kodecategory")))
                ddlFindingCategory_NewChecklist.Items.Add(New ListItem(dt.Rows(i).Item("namacategory"), dt.Rows(i).Item("kodecategory")))
            Next
        End If

        dt = GetDataSql("select * from MSTCASECATEGORY where kel_pemeriksa like '%" & ddlKelompokPemeriksa.SelectedValue & "%' or kel_pemeriksa is null or kel_pemeriksa = '' order by keterangan")
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                ddlCaseCategory.Items.Add(New ListItem(dt.Rows(i).Item("keterangan"), dt.Rows(i).Item("kode")))
                ddlCaseCategory_NewChecklist.Items.Add(New ListItem(dt.Rows(i).Item("keterangan"), dt.Rows(i).Item("kode")))
            Next
        End If
    End Sub
    Sub loadMainActivity()
        Dim dt As DataTable = GetDataSql("select * from MSTMAIN_ACTIVITY order by kode")
        If dt.Rows.Count > 0 Then
            ddlMainActivity_NewChecklist.Items.Add(New ListItem("-", "-"))
            For i = 0 To dt.Rows.Count - 1
                ddlMainActivity_NewChecklist.Items.Add(New ListItem(dt.Rows(i).Item("kode") & "-" & dt.Rows(i).Item("main_activity"), dt.Rows(i).Item("kode")))
            Next
        End If
    End Sub
    Protected Sub loadStatistik()
        Dim Not_Implemented As Integer = 0
        Dim Not_Applicable As Integer = 0
        Dim In_Progress As Integer = 0
        Dim Not_Due As Integer = 0
        Dim Implemented As Integer = 0
        Dim total As Integer = 0
        Dim total_percent As Integer = 0
        Dim table_html As String = "<table id='tableStatistik' border='1' style='text-align: center;width: 25%;'>" +
            "<thead>" +
                "<tr>" +
                    "<th>Status</th>" +
                    "<th>∑</th>" +
                    "<th>%</th>" +
                "</tr>" +
            "</thead>" +
            "<tbody>"
        Dim dt As DataTable = GetDataSql("select * from (select sum(case when status = 1 then 1 else 0 end) Not_Implemented, sum(case when status = 2 then 1 else 0 end) Not_Applicable, sum(case when status = 3 then 1 else 0 end) In_Progress, sum(case when status = 4 then 1 else 0 end) Not_Due, sum(case when status = 5 then 1 else 0 end) Implemented from FTA_CHECKLIST_DTL where fta_no = '" & txtFtaNo.Text & "' and kode_prd = '" & Request.QueryString("kode_prd") & "') status_table")
        If dt.Rows.Count > 0 Then
            Not_Implemented = dt.Rows(0).Item("Not_Implemented")
            Not_Applicable = dt.Rows(0).Item("Not_Applicable")
            In_Progress = dt.Rows(0).Item("In_Progress")
            Not_Due = dt.Rows(0).Item("Not_Due")
            Implemented = dt.Rows(0).Item("Implemented")

            total = Not_Implemented + Not_Applicable + In_Progress + Not_Due + Implemented
            total_percent = Not_Implemented + In_Progress + Implemented

            table_html = table_html + "<tr style='" & getStyleStatus(1) & "'><td>Not Implemented</td><td>" & Not_Implemented & "</td><td>" & If(total_percent <> 0, Math.Round((Not_Implemented / total_percent) * 100, 2, MidpointRounding.AwayFromZero), 0) & "%</td></tr>"
            table_html = table_html + "<tr style='" & getStyleStatus(2) & "'><td>Not Applicable</td><td>" & Not_Applicable & "</td><td>0%</td></tr>"
            table_html = table_html + "<tr style='" & getStyleStatus(3) & "'><td>In Progress</td><td>" & In_Progress & "</td><td>" & If(total_percent <> 0, Math.Round((In_Progress / total_percent) * 100, 2, MidpointRounding.AwayFromZero), 0) & "%</td></tr>"
            table_html = table_html + "<tr style='" & getStyleStatus(4) & "'><td>Not Due</td><td>" & Not_Due & "</td><td>0%</td></tr>"
            table_html = table_html + "<tr style='" & getStyleStatus(5) & "'><td>Implemented</td><td>" & Implemented & "</td><td>" & If(total_percent <> 0, Math.Round((Implemented / total_percent) * 100, 2, MidpointRounding.AwayFromZero), 0) & "%</td></tr>"
            table_html = table_html + "<tr><td>Total Recommendation</td><td>" & total & "</td><td>" & If(total_percent <> 0, Math.Round((total_percent / total_percent) * 100, MidpointRounding.AwayFromZero), 0) & "%</td></tr>"
        End If

        table_html = table_html + "</tbody>"
        table_html = table_html + "</table>"
        divStatistik.InnerHtml = table_html
    End Sub

    Protected Sub loadAllDropdownList()

        Dim dt As DataTable = GetDataSql("select * from MST_FTA_STATUS")
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                ddlStatus.Items.Add(New ListItem(dt.Rows(i).Item("namastatus") + " (" & dt.Rows(i).Item("kodestatus") & ")", dt.Rows(i).Item("id")))
            Next
        End If

        dt = GetDataSql("select kode, jabatan, flag_cabangpusat from MSTJABATAN order by flag_cabangpusat,jabatan")
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                ddlPosition.Items.Add(New ListItem(dt.Rows(i).Item(2) + " - " + dt.Rows(i).Item(1), dt.Rows(i).Item(0)))
                ddlPosition2.Items.Add(New ListItem(dt.Rows(i).Item(2) + " - " + dt.Rows(i).Item(1), dt.Rows(i).Item(0)))
                ddlPosition_NewChecklist.Items.Add(New ListItem(dt.Rows(i).Item(2) + " - " + dt.Rows(i).Item(1), dt.Rows(i).Item(0)))
                ddlPosition2_NewChecklist.Items.Add(New ListItem(dt.Rows(i).Item(2) + " - " + dt.Rows(i).Item(1), dt.Rows(i).Item(0)))
            Next
        End If

        dt = GetDataSql("select * from MSTKELOMPOKPEMERIKSA order by kodekel")
        ddlKelompokPemeriksa.Items.Add(New ListItem("", ""))
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                ddlKelompokPemeriksa.Items.Add(New ListItem(dt.Rows(i).Item(1), dt.Rows(i).Item(0)))
            Next
        End If
    End Sub

    Public Shared Function GetDataSql(ByVal query As String) As DataTable
        Try
            Using con As New SqlConnection(strConn)
                con.Open()
                Dim da As New SqlDataAdapter(query, con)
                Dim dt As New DataTable
                da.Fill(dt)
                con.Close()
                Return dt
            End Using
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Protected Sub btnFindData_Click(sender As Object, e As EventArgs)
        loadTable()
    End Sub

    Protected Sub loadTable()
        Dim Chrs As String = Chr(34)
        Dim table_html As String
        Dim month As String = ""
        Dim year As String = ""
        Dim sub_activity As String
        Dim main_activity As String
        Dim status_approval As Integer = cekApproval()
        Dim status_rejected As Integer = cekRejected()
        Dim nik As String = If(Session("nik") Is Nothing, "", Session("nik").ToString)
        Dim report_no As String = ""
        table_html = "<table id='mytable' class='cell-border table table-striped table-bordered datatable-multi-row' cellspacing='0' width='100%'>" +
            "<thead>" +
                "<tr>" +
                    "<th rowspan='2'>Action</th>" +
                    "<th rowspan='2'>No.</th>" +
                    "<th rowspan='2'>Branch</th>" +
                    "<th colspan='2'>Period</th>" +
                    "<th rowspan='2'>Report No</th>" +
                    "<th rowspan='2'>Main Activity</th>" +
                    "<th rowspan='2'>Sub Activity</th>" +
                    "<th rowspan='2'>Findings Category</th>" +
                    "<th rowspan='2'>Case Category</th>" +
                    "<th rowspan='2' class='large-width'>Audit Result</th>" +
                    "<th rowspan='2'>Bukti Obyektif</th>" +
                    "<th rowspan='2'>Suspect</th>" +
                    "<th rowspan='2'> </th>" +
                    "<th rowspan='2'>Responsibility</th>" +
                    "<th colspan='3'>Follow Up by Auditee</th>" +
                    "<th rowspan='2' class='large-width'>Recommendation</th>" +
                    "<th rowspan='2'>Start Date</th>" +
                    "<th rowspan='2'>Due Date</th>" +
                    "<th colspan='5'>Verification by Auditor</th>" +
                "</tr>" +
                "<tr>" +
                    "<th>Month</th>" +
                    "<th>Year</th>" +
                    "<th class='large-width'>Corrective Action</th>" +
                    "<th>PIC</th>" +
                    "<th>Evidence</th>" +
                    "<th class='large-width'>Verification Result-1</th>" +
                    "<th class='large-width'>Verification Result-2</th>" +
                    "<th>Status*</th>" +
                    "<th>Finish Date</th>" +
                    "<th>PIC</th>" +
                "</tr>" +
            "</thead>" +
            "<tbody>"

        'cekFtaNo()
        txtFtaNo.Text = Request.QueryString("fta_no")
        loadSavedAuditor()

        If status_rejected = 1 Then
            divRejected.Visible = True
        Else
            divRejected.Visible = False
        End If

        If status_approval = -1 And (nik = ddlAuditor1.SelectedValue Or Session("sUsername").ToString = "SUPERADMIN" Or Session("kodejabatan") = "J0015") Then
            btnConfirm.Visible = True
        ElseIf status_approval = 0 And (Session("jabatan") = "AUDITOR DEVELOPMENT" Or Session("sUsername").ToString = "SUPERADMIN" Or Session("kodejabatan") = "J0015") Then
            btnApproveChecklist.Visible = True
            btnRejectModal.Visible = True
        ElseIf status_approval = 1 And (Session("jabatan") = "AUDITOR MGR" Or Session("sUsername").ToString = "SUPERADMIN" Or Session("kodejabatan") = "J0015") Then
            btnApproveChecklist.Visible = True
            btnRejectModal.Visible = True
        End If

        btnInsertChecklist.Visible = False
        btnImportExcel.Visible = False
        btnExport.Visible = False
        ddlAuditor1.Enabled = False
        ddlAuditor2.Enabled = False
        ddlAuditor3.Enabled = False

        If status_approval = 2 Then
            btnExport.Visible = True
            loadStatistik()
        ElseIf status_approval = -1 Or status_approval = 0 Then
            cekImportHistory()
        End If

        If (status_approval = -1 And (nik = ddlAuditor1.SelectedValue Or nik = ddlAuditor2.SelectedValue Or nik = ddlAuditor3.SelectedValue Or Session("sUsername") = "SUPERADMIN" Or Session("kodejabatan") = "J0015")) Or (status_approval = 0 And (Session("jabatan") = "AUDITOR DEVELOPMENT" Or Session("sUsername") = "SUPERADMIN" Or Session("kodejabatan") = "J0015")) Or (status_approval = 1 And (Session("jabatan") = "AUDITOR MGR" Or Session("sUsername") = "SUPERADMIN" Or Session("kodejabatan") = "J0015")) Then
            'If ddlKelompokPemeriksa.SelectedValue <> "001" Then
            '    ddlAuditor1.Enabled = True
            '    ddlAuditor2.Enabled = True
            '    ddlAuditor3.Enabled = True
            '    btnSaveAuditor.Visible = True
            'End If
            btnInsertChecklist.Visible = True
            btnImportExcel.Visible = True
        End If
        Dim list_responsibility As String = getListResponsibility(Session("kodejabatan"))
        Dim dt As DataTable = GetDataSql("select fcd.*,mfs.back_color,mfs.text_color,mfs.namastatus,(select nama_prd from MST_PERIODE mp where fcd.Kode_Prd_Checklist = mp.Kode_Prd) as nama_prd, (select no_report from TMP_CHECKLIST_DTL tcd where tcd.kode = fcd.Kode_tmp and tcd.no = fcd.no) as no_report_chcklist, (select namacategory from MSTFINDINGCATEGORY where kodecategory = fcd.fin_category) as nama_fin_category, (select keterangan from MSTCASECATEGORY where kode = fcd.case_category) as nama_case_category, (select jabatan from MSTJABATAN where kode = fcd.position) as nama_position, (select jabatan from MSTJABATAN where kode = fcd.position2) as nama_position2, (select nama from mstauditor where nik = fcd.pic_auditor) as nama_pic_auditor, (select no + '-' + Activity1 from TMP_CHECKLIST_DTL where kode = fcd.Kode_tmp and no = (select no_parent from TMP_CHECKLIST_DTL where kode = fcd.Kode_tmp and no = fcd.No)) as nama_sub_activity from FTA_CHECKLIST_DTL fcd left join MST_FTA_STATUS mfs on fcd.status = mfs.id where fcd.fta_no = '" & txtFtaNo.Text & "' and fcd.kode_prd = '" & Request.QueryString("kode_prd").ToString & "'")
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                'If UCase(nik) = ddlAuditor1.SelectedValue Or UCase(nik) = ddlAuditor2.SelectedValue Or UCase(nik) = ddlAuditor3.SelectedValue Or Session("sUsername").ToString = "SUPERADMIN" Or Session("jabatan") = "AUDITOR MGR" Or Session("jabatan") = "AUDITOR DEVELOPMENT" Or Session("jabatan") = "ABM" Or (status_approval = 2 And Session("kodejabatan") = dt.Rows(i).Item("position").ToString) Then
                'If UCase(nik) = ddlAuditor1.SelectedValue Or UCase(nik) = ddlAuditor2.SelectedValue Or UCase(nik) = ddlAuditor3.SelectedValue Or Session("sUsername").ToString = "SUPERADMIN" Or Session("jabatan") = "AUDITOR MGR" Or Session("jabatan") = "AUDITOR DEVELOPMENT" Or Session("jabatan") = "ABM" Or (status_approval = 2 And Session("kodejabatan") <> "") Then
                If UCase(nik) = ddlAuditor1.SelectedValue Or UCase(nik) = ddlAuditor2.SelectedValue Or UCase(nik) = ddlAuditor3.SelectedValue Or Session("sUsername").ToString = "SUPERADMIN" Or Session("kodejabatan") = "J0015" Or Session("jabatan") = "AUDITOR MGR" Or Session("jabatan") = "AUDITOR DEVELOPMENT" Or Session("jabatan") = "ABM" Or Session("kodejabatan") = "J0055" Or Session("kodejabatan") = "J0196" Or (status_approval = 2 And (Session("kodejabatan") = dt.Rows(i).Item("position").ToString) Or Session("kodejabatan") = dt.Rows(i).Item("position2").ToString Or list_responsibility.Contains(dt.Rows(i).Item("position").ToString) Or (list_responsibility.Contains(dt.Rows(i).Item("position2").ToString) And dt.Rows(i).Item("position2").ToString <> "")) Then
                    table_html = table_html + "<tr>"

                    month = dt.Rows(i).Item("nama_prd").ToString.Split(" ")(0)
                    year = dt.Rows(i).Item("nama_prd").ToString.Split(" ")(1)

                    report_no = If(dt.Rows(i).Item("manual_insertion_flag") = "1", dt.Rows(i).Item("no"), dt.Rows(i).Item("no_report_chcklist"))
                    sub_activity = If(dt.Rows(i).Item("manual_insertion_flag").ToString = "1", dt.Rows(i).Item("sub_activity").ToString(), dt.Rows(i).Item("nama_sub_activity").ToString())
                    main_activity = If(dt.Rows(i).Item("manual_insertion_flag") = "1", dt.Rows(i).Item("main_activity").ToString(), getMainSubActivity(dt.Rows(i).Item("kode_tmp"), sub_activity.Split("-")(0)))

                    Dim finding_category As String = dt.Rows(i).Item("nama_fin_category").ToString
                    Dim case_category As String = dt.Rows(i).Item("nama_case_category").ToString

                    Dim edit_fin_category As String = ""
                    Dim edit_case_category As String = ""
                    Dim edit_audit_result As String = ""
                    Dim edit_audit_result_attachment As String = ""
                    Dim edit_suspect As String = ""
                    Dim edit_position As String = ""
                    Dim edit_corrective As String = ""
                    Dim edit_pic_auditee As String = ""
                    Dim edit_evidence_auditee As String = ""
                    Dim edit_position2 As String = ""
                    Dim edit_corrective2 As String = ""
                    Dim edit_pic_auditee2 As String = ""
                    Dim edit_evidence_auditee2 As String = ""
                    Dim edit_recommendation As String = ""
                    Dim edit_verif1 As String = ""
                    Dim edit_verif2 As String = ""
                    Dim edit_status As String = ""
                    Dim edit_pic_auditor As String = ""
                    Dim delete_chechklist As String = ""

                    If IsDBNull(dt.Rows(i).Item("finish_date")) Or (Session("jabatan") = "AUDITOR DEVELOPMENT" Or Session("jabatan") = "AUDITOR MGR" Or Session("sUsername") = "SUPERADMIN" Or Session("kodejabatan") = "J0015") Or status_approval = -1 Then
                        If (status_approval = -1 And (nik = ddlAuditor1.SelectedValue Or nik = ddlAuditor2.SelectedValue Or nik = ddlAuditor3.SelectedValue Or Session("sUsername") = "SUPERADMIN" Or Session("kodejabatan") = "J0015")) Or (status_approval = 0 And (Session("jabatan") = "AUDITOR DEVELOPMENT" Or Session("sUsername") = "SUPERADMIN" Or Session("kodejabatan") = "J0015")) Or (status_approval = 1 And (Session("jabatan") = "AUDITOR MGR" Or Session("sUsername") = "SUPERADMIN" Or Session("kodejabatan") = "J0015")) Then
                            If (dt.Rows(i).Item("manual_insertion_flag") = "1" And (nik = ddlAuditor1.SelectedValue Or nik = ddlAuditor2.SelectedValue Or nik = ddlAuditor3.SelectedValue Or Session("sUsername") = "SUPERADMIN" Or Session("kodejabatan") = "J0015")) Or (Session("jabatan") = "AUDITOR DEVELOPMENT" Or Session("jabatan") = "AUDITOR MGR" Or Session("sUsername") = "SUPERADMIN" Or Session("kodejabatan") = "J0015") Then
                                delete_chechklist = "<button data-toggle='modal' data-target='#modalDelete' onclick='deleteChecklist(" & Chrs & dt.Rows(i).Item("id") & Chrs & ", " & Chrs & "fin_category" & Chrs & ", " & Chrs & dt.Rows(i).Item("fin_category").ToString & Chrs & "); return false;'>Delete</button>"
                            End If
                            edit_fin_category = "<button data-toggle='modal' data-target='#modalFinCategory' onclick='setUpdateStatus(" & Chrs & dt.Rows(i).Item("id") & Chrs & ", " & Chrs & "fin_category" & Chrs & ", " & Chrs & dt.Rows(i).Item("fin_category").ToString & Chrs & "); return false;'>Update</button>"
                            edit_case_category = "<button data-toggle='modal' data-target='#modalCaseCategory' onclick='setUpdateStatus(" & Chrs & dt.Rows(i).Item("id") & Chrs & ", " & Chrs & "case_category" & Chrs & ", " & Chrs & dt.Rows(i).Item("case_category").ToString & Chrs & "); return false;'>Update</button>"
                            edit_audit_result = "<button data-toggle='modal' data-target='#modalHasilAudit' onclick=""setUpdateStatus('" & dt.Rows(i).Item("id") & "', '" & "audit_result" & "', '" & dt.Rows(i).Item("Hasil_audit").ToString.Replace("""", "\&quot;") & "'); return false;"">Update</button>"
                            edit_audit_result_attachment = "<button data-toggle='modal' data-target='#modalHasilAuditAttachment' onclick=""setUpdateStatus('" & dt.Rows(i).Item("id") & "', '" & "audit_result_attachment" & "', '" & dt.Rows(i).Item("Hasil_audit_attachment").ToString.Replace("""", "\&quot;") & "'); return false;"">Update</button>"
                            edit_suspect = "<button data-toggle='modal' data-target='#modalSuspect' onclick='setUpdateStatus(" & Chrs & dt.Rows(i).Item("id") & Chrs & ", " & Chrs & "suspect" & Chrs & ", " & Chrs & dt.Rows(i).Item("suspect").ToString & Chrs & "); return false;'>Update</button>"
                            edit_position = "<button data-toggle='modal' data-target='#modalPosition' onclick='setUpdateStatus(" & Chrs & dt.Rows(i).Item("id") & Chrs & ", " & Chrs & "position" & Chrs & ", " & Chrs & dt.Rows(i).Item("position").ToString & Chrs & "); return false;'>Update</button>"
                            edit_position2 = "<button data-toggle='modal' data-target='#modalPosition2' onclick='setUpdateStatus(" & Chrs & dt.Rows(i).Item("id") & Chrs & ", " & Chrs & "position2" & Chrs & ", " & Chrs & dt.Rows(i).Item("position2").ToString & Chrs & "); return false;'>Update</button>"
                        End If

                        If status_approval = 2 And (Session("kodejabatan") = dt.Rows(i).Item("position").ToString Or Session("jabatan") = "ABM" Or Session("kodejabatan") = "J0055" Or Session("kodejabatan") = "J0196" Or Session("sUsername") = "SUPERADMIN" Or Session("kodejabatan") = "J0015" Or list_responsibility.Contains(dt.Rows(i).Item("position").ToString)) Then
                            edit_corrective = "<button data-toggle='modal' data-target='#modalCorrective' onclick='setUpdateStatus(" & Chrs & dt.Rows(i).Item("id") & Chrs & ", " & Chrs & "corrective" & Chrs & ", " & Chrs & dt.Rows(i).Item("corrective_action").ToString & Chrs & "); return false;'>Update</button>"
                            edit_pic_auditee = "<button data-toggle='modal' data-target='#modalPICAuditee' onclick='setUpdateStatus(" & Chrs & dt.Rows(i).Item("id") & Chrs & ", " & Chrs & "pic_auditee" & Chrs & ", " & Chrs & dt.Rows(i).Item("pic_auditee").ToString & Chrs & "); return false;'>Update</button>"
                            edit_evidence_auditee = "<button data-toggle='modal' data-target='#modalEvidenceAuditee' onclick='setUpdateStatus(" & Chrs & dt.Rows(i).Item("id") & Chrs & ", " & Chrs & "evidence" & Chrs & ", " & Chrs & dt.Rows(i).Item("evidence").ToString & Chrs & "); return false;'>Update</button>"
                        End If

                        If status_approval = 2 And (Session("kodejabatan") = dt.Rows(i).Item("position2").ToString Or Session("jabatan") = "ABM" Or Session("kodejabatan") = "J0055" Or Session("kodejabatan") = "J0196" Or Session("sUsername") = "SUPERADMIN" Or Session("kodejabatan") = "J0015" Or list_responsibility.Contains(dt.Rows(i).Item("position2").ToString)) Then
                            edit_corrective2 = "<button data-toggle='modal' data-target='#modalCorrective2' onclick='setUpdateStatus(" & Chrs & dt.Rows(i).Item("id") & Chrs & ", " & Chrs & "corrective2" & Chrs & ", " & Chrs & dt.Rows(i).Item("corrective_action2").ToString & Chrs & "); return false;'>Update</button>"
                            edit_pic_auditee2 = "<button data-toggle='modal' data-target='#modalPICAuditee2' onclick='setUpdateStatus(" & Chrs & dt.Rows(i).Item("id") & Chrs & ", " & Chrs & "pic_auditee2" & Chrs & ", " & Chrs & dt.Rows(i).Item("pic_auditee2").ToString & Chrs & "); return false;'>Update</button>"
                            edit_evidence_auditee2 = "<button data-toggle='modal' data-target='#modalEvidenceAuditee2' onclick='setUpdateStatus(" & Chrs & dt.Rows(i).Item("id") & Chrs & ", " & Chrs & "evidence2" & Chrs & ", " & Chrs & dt.Rows(i).Item("evidence2").ToString & Chrs & "); return false;'>Update</button>"
                        End If


                        If nik = ddlAuditor1.SelectedValue Or nik = ddlAuditor2.SelectedValue Or nik = ddlAuditor3.SelectedValue Or Session("sUsername") = "SUPERADMIN" Or Session("kodejabatan") = "J0015" Or Session("jabatan") = "AUDITOR MGR" Or Session("jabatan") = "AUDITOR DEVELOPMENT" Then
                            edit_recommendation = "<button data-toggle='modal' data-target='#modalRecommendation' onclick='setUpdateStatus(" & Chrs & dt.Rows(i).Item("id") & Chrs & ", " & Chrs & "recommendation" & Chrs & ", " & Chrs & dt.Rows(i).Item("recommendation").ToString & Chrs & "); return false;'>Update</button>"
                            edit_verif1 = "<button data-toggle='modal' data-target='#modalVerif1' onclick='setUpdateStatus(" & Chrs & dt.Rows(i).Item("id") & Chrs & ", " & Chrs & "verif1" & Chrs & ", " & Chrs & dt.Rows(i).Item("verif_result1").ToString & Chrs & "); return false;'>Update</button>"
                            edit_verif2 = "<button data-toggle='modal' data-target='#modalVerif2' onclick='setUpdateStatus(" & Chrs & dt.Rows(i).Item("id") & Chrs & ", " & Chrs & "verif2" & Chrs & ", " & Chrs & dt.Rows(i).Item("verif_result2").ToString & Chrs & "); return false;'>Update</button>"
                            edit_status = "<button data-toggle='modal' data-target='#modalStatus' onclick='setUpdateStatus(" & Chrs & dt.Rows(i).Item("id") & Chrs & ", " & Chrs & "status" & Chrs & ", " & Chrs & dt.Rows(i).Item("status").ToString & Chrs & "); return false;'>Update</button>"
                            edit_pic_auditor = "<button data-toggle='modal' data-target='#modalPIC_Auditor' onclick='setUpdateStatus(" & Chrs & dt.Rows(i).Item("id") & Chrs & ", " & Chrs & "pic_auditor" & Chrs & ", " & Chrs & dt.Rows(i).Item("pic_auditor").ToString & Chrs & "); return false;'>Update</button>"
                        End If

                    End If

                    Dim evidence_path As String = ""
                    If dt.Rows(i).Item("evidence").ToString = "" Or dt.Rows(i).Item("evidence").ToString Is Nothing Then
                        evidence_path = ""
                    Else
                        evidence_path = "<a href='" & dt.Rows(i).Item("evidence").ToString & "'>Download</a>"
                    End If

                    Dim evidence_path2 As String = ""
                    If dt.Rows(i).Item("evidence2").ToString = "" Or dt.Rows(i).Item("evidence2").ToString Is Nothing Then
                        evidence_path2 = ""
                    Else
                        evidence_path2 = "<a href='" & dt.Rows(i).Item("evidence2").ToString & "'>Download</a>"
                    End If

                    Dim audit_result_path As String = ""
                    If dt.Rows(i).Item("Hasil_Audit_Attachment").ToString = "" Or dt.Rows(i).Item("Hasil_Audit_Attachment").ToString Is Nothing Then
                        audit_result_path = ""
                        If dt.Rows(i).Item("manual_insertion_flag").ToString = "0" Then
                            audit_result_path = cekAudit_result_attachment_ice(dt.Rows(i).Item("kode_prd_checklist"), dt.Rows(i).Item("kode_tmp"), dt.Rows(i).Item("kodecab"), dt.Rows(i).Item("no"))
                            If audit_result_path <> "" Then
                                audit_result_path = "<a href='bo/" & audit_result_path & "'>Download</a>"
                            End If
                        End If

                    Else
                        audit_result_path = "<a href='" & dt.Rows(i).Item("Hasil_Audit_Attachment").ToString & "'>Download</a>"
                    End If

                    Dim start_date As Date = dt.Rows(i).Item("start_date")
                    Dim finish_date As Date
                    If Not IsDBNull(dt.Rows(i).Item("finish_date")) Then
                        finish_date = dt.Rows(i).Item("finish_date")
                    End If

                    table_html = table_html + "<td data-datatable-multi-row-rowspan='2'>" & delete_chechklist & "</td>"
                    table_html = table_html + "<td data-datatable-multi-row-rowspan='2'>" & i + 1 & "</td>"
                    table_html = table_html + "<td data-datatable-multi-row-rowspan='2'>" & dt.Rows(i).Item("Kodecab") & "</td>"
                    table_html = table_html + "<td data-datatable-multi-row-rowspan='2'>" & month & "</td>"
                    table_html = table_html + "<td data-datatable-multi-row-rowspan='2'>" & year & "</td>"
                    table_html = table_html + "<td data-datatable-multi-row-rowspan='2'>" & report_no & "</td>"
                    table_html = table_html + "<td data-datatable-multi-row-rowspan='2'>" & If(dt.Rows(i).Item("manual_insertion_flag") = "1", main_activity, main_activity.Split("-")(1)) & "</td>"
                    table_html = table_html + "<td data-datatable-multi-row-rowspan='2'>" & If(dt.Rows(i).Item("manual_insertion_flag") = "1", sub_activity, sub_activity.Split("-")(1)) & "</td>"
                    table_html = table_html + "<td data-datatable-multi-row-rowspan='2'>" & finding_category & edit_fin_category & "</td>"
                    table_html = table_html + "<td data-datatable-multi-row-rowspan='2'>" & case_category & "</br>" & edit_case_category & "</td>"
                    table_html = table_html + "<td data-datatable-multi-row-rowspan='2' class='large-width'>" & Regex.Replace(dt.Rows(i).Item("Hasil_audit").ToString, "\r\n?|\n", "<br />") & "</br>" & Regex.Replace(edit_audit_result, "\r\n?|\n", "\n") & "</td>"
                    table_html = table_html + "<td data-datatable-multi-row-rowspan='2'>" & audit_result_path & "</br>" & Regex.Replace(edit_audit_result_attachment, "\r\n?|\n", "\n") & "</td>"
                    table_html = table_html + "<td data-datatable-multi-row-rowspan='2'>" & Regex.Replace(dt.Rows(i).Item("suspect").ToString, "\r\n?|\n", "<br />") & "</br>" & Regex.Replace(edit_suspect, "\r\n?|\n", "\n")
                    table_html = table_html + "<script type='x/template' class='extra-row-content'>"
                    table_html = table_html + "<tr>"
                    table_html = table_html + "<td><b>2nd: </b></td>"
                    table_html = table_html + "<td>" & dt.Rows(i).Item("nama_position2").ToString & "</br>" & edit_position2 & "</td>"
                    table_html = table_html + "<td class='large-width'>" & Regex.Replace(dt.Rows(i).Item("corrective_action2").ToString, "\r\n?|\n", "<br />") & "</br>" & Regex.Replace(edit_corrective2, "\r\n?|\n", "\n") & "</td>"
                    table_html = table_html + "<td>" & dt.Rows(i).Item("pic_auditee2").ToString & "</br>" & edit_pic_auditee2 & "</td>"
                    table_html = table_html + "<td>" & evidence_path2 & "</br>" & edit_evidence_auditee2 & "</td>"
                    table_html = table_html + "</tr>"
                    table_html = table_html + "</script></td>"
                    table_html = table_html + "<td><b>1st: </b>"
                    table_html = table_html + "<td>" & dt.Rows(i).Item("nama_position").ToString & "</br>" & edit_position & "</td>"
                    table_html = table_html + "<td class='large-width'>" & Regex.Replace(dt.Rows(i).Item("corrective_action").ToString, "\r\n?|\n", "<br />") & "</br>" & Regex.Replace(edit_corrective, "\r\n?|\n", "\n") & "</td>"
                    table_html = table_html + "<td>" & dt.Rows(i).Item("pic_auditee").ToString & "</br>" & edit_pic_auditee & "</td>"
                    table_html = table_html + "<td>" & evidence_path & "</br>" & edit_evidence_auditee & "</td>"
                    table_html = table_html + "<td data-datatable-multi-row-rowspan='2' class='large-width'>" & Regex.Replace(dt.Rows(i).Item("recommendation").ToString, "\r\n?|\n", "<br />") & "</br>" & Regex.Replace(edit_recommendation, "\r\n?|\n", "\n") & "</td>"
                    table_html = table_html + "<td data-datatable-multi-row-rowspan='2'>" & start_date.ToString("dd-MMM-yy") & "</td>"
                    table_html = table_html + "<td data-datatable-multi-row-rowspan='2'>" & If(IsDBNull(dt.Rows(i).Item("due_date")), "", CType(dt.Rows(i).Item("due_date"), Date).ToString("dd-MMM-yy")) & "</td>"
                    table_html = table_html + "<td data-datatable-multi-row-rowspan='2' class='large-width'>" & Regex.Replace(dt.Rows(i).Item("verif_result1").ToString, "\r\n?|\n", "<br />") & "</br>" & Regex.Replace(edit_verif1, "\r\n?|\n", "\n") & "</td>"
                    table_html = table_html + "<td data-datatable-multi-row-rowspan='2' class='large-width'>" & Regex.Replace(dt.Rows(i).Item("verif_result2").ToString, "\r\n?|\n", "<br />") & "</br>" & Regex.Replace(edit_verif2, "\r\n?|\n", "\n") & "</td>"
                    table_html = table_html + "<td data-datatable-multi-row-rowspan='2' style='background-color: " & dt.Rows(i).Item("back_color") & "; color: " & dt.Rows(i).Item("text_color") & "'>" & dt.Rows(i).Item("namastatus").ToString & "</br>" & edit_status & "</td>"
                    table_html = table_html + "<td data-datatable-multi-row-rowspan='2'>" & If(IsDBNull(dt.Rows(i).Item("finish_date")), dt.Rows(i).Item("finish_date").ToString, finish_date.ToString("dd MMM yyyy")) & "</td>"
                    table_html = table_html + "<td data-datatable-multi-row-rowspan='2'>" & dt.Rows(i).Item("nama_pic_auditor").ToString & "</br>" & edit_pic_auditor & "</td>"

                    table_html = table_html + "</tr>"


                End If
            Next
        End If
        table_html = table_html + "</tbody>"
        table_html = table_html + "</table>"
        divTable.InnerHtml = table_html
        divTableHidden.InnerHtml = table_html.Replace("mytable", "mytable1")
    End Sub
    Function getListResponsibility(ByVal kode_jabatan As String) As String
        Dim dt As DataTable = GetDataSql("select responsibility from MSTRESPONSIBILITY where user_responsibility = '" & kode_jabatan & "'")
        Dim list_responsibility As String = ""
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                list_responsibility = list_responsibility & dt.Rows(i).Item(0) & ","
            Next
            list_responsibility = list_responsibility.Remove(list_responsibility.Length - 1, 1)
        End If
        Return list_responsibility
    End Function
    Function cekAudit_result_attachment_ice(ByVal kode_prd As String, ByVal kode_tmp As String, ByVal kodecab As String, ByVal no As String) As String
        Dim dt As DataTable = GetDataSql("select bukti_obyektif, bukti_obyektif_2 from CHECKLIST_ENTRY where Kode_Prd = '" & kode_prd & "' and Kode_tmp = '" & kode_tmp & "' and no = '" & no & "' and Kodecab = '" & kodecab & "' ")
        If dt.Rows.Count > 0 Then
            If IsDBNull(dt.Rows(0).Item(0)) And IsDBNull(dt.Rows(0).Item(1)) Then
                Return ""
            End If

            If dt.Rows(0).Item(0).ToString <> "" Then
                Return dt.Rows(0).Item(0).ToString
            ElseIf dt.Rows(0).Item(1).ToString <> "" Then
                Return dt.Rows(0).Item(1).ToString
            Else
                Return ""
            End If

        Else
            Return ""
        End If
    End Function
    Protected Function getStyleStatus(ByVal kode As String) As String
        Dim dt As DataTable = GetDataSql("select back_color,text_color from MST_FTA_STATUS where id = '" & kode & "'")
        If dt.Rows.Count > 0 Then
            Return "background-color: " & dt.Rows(0).Item(0) & "; color: " & dt.Rows(0).Item(1) & ""
        End If
        Return ""
    End Function

    Protected Function getReportNo(ByVal kode_tmp As String, ByVal no As String) As String
        Dim dt As DataTable = GetDataSql("select no_report from TMP_CHECKLIST_DTL where kode = '" & kode_tmp & "' and no = '" & no & "'")
        If dt.Rows.Count > 0 Then
            Return dt.Rows(0).Item(0).ToString
        Else
            Return no
        End If
    End Function

    Protected Sub cekImportHistory()
        Dim dt As DataTable = GetDataSql("select * from FTA_IMPORT_HISTORY where fta_no = '" & txtFtaNo.Text & "' and kel_pemeriksa = '" & ddlKelompokPemeriksa.SelectedValue & "' and kodecab = '" & doBranch.SelectedValue & "' and kode_prd = '" & doPeriode.SelectedValue & "'")
        If dt.Rows.Count > 0 Then
            btnImportChecklist.Visible = False
        Else
            btnImportChecklist.Visible = True
        End If
    End Sub
    Protected Sub cekFtaNo()
        Dim dt As DataTable = GetDataSql("select fta_no from FTA_CHECKLIST_HDR where kel_pemeriksa = '" & ddlKelompokPemeriksa.Text & "' and kode_prd = '" & doPeriode.SelectedValue & "' and kodecab = '" & doBranch.SelectedValue & "'")
        If dt.Rows.Count > 0 Then
            txtFtaNo.Text = dt.Rows(0).Item(0)
        End If
    End Sub

    Protected Function cekApproval() As Integer
        Dim dt As DataTable = GetDataSql("select * from FTA_CHECKLIST_HDR where fta_no = '" & Request.QueryString("fta_no") & "' and kode_prd = '" & Request.QueryString("kode_prd") & "'")
        If dt.Rows.Count > 0 Then
            Return dt.Rows(0).Item("status")
        Else
            Return -1
        End If
    End Function

    Protected Function cekRejected() As Integer
        Dim dt As DataTable = GetDataSql("select * from FTA_CHECKLIST_HDR where fta_no = '" & Request.QueryString("fta_no") & "' and kode_prd = '" & Request.QueryString("kode_prd") & "'")
        If dt.Rows.Count > 0 Then
            lblRejectReason.Text = dt.Rows(0).Item("reject_reason").ToString()
            lblRejectBy.Text = dt.Rows(0).Item("reject_by").ToString()
            lblRejectDate.Text = dt.Rows(0).Item("reject_date").ToString()
            Dim reject_status As Integer = If(IsDBNull(dt.Rows(0).Item("reject_status")), 0, dt.Rows(0).Item("reject_status"))
            Return reject_status
        Else
            Return -1
        End If
    End Function

    Protected Function cekJenisCategory(ByVal kode As String, ByVal jenis_category As String) As String
        Dim dt As New DataTable
        Select Case jenis_category
            Case ""
                Return ""
            Case "finding_category"
                dt = GetDataSql("select namacategory from MSTFINDINGCATEGORY where kodecategory = '" & kode & "'")
                If dt.Rows.Count > 0 Then
                    Return dt.Rows(0).Item(0)
                Else
                    Return ""
                End If
            Case "case_category"
                dt = GetDataSql("select keterangan, due_date from MSTCASECATEGORY where kode = '" & kode & "'")
                If dt.Rows.Count > 0 Then
                    Return dt.Rows(0).Item(0) + "-" + dt.Rows(0).Item(1)
                Else
                    Return ""
                End If
        End Select
        Return ""
    End Function

    Protected Sub loadSavedAuditor()
        Dim dt As New DataTable
        ddlAuditor1.SelectedValue = " "
        ddlAuditor2.SelectedValue = " "
        ddlAuditor3.SelectedValue = " "
        Dim strsql As String = ""
        If ddlKelompokPemeriksa.SelectedValue = "001" Then
            strsql = "select * from FTA_CHECKLIST_AUDITOR where fta_no = '" & txtFtaNo.Text & "' and tahun = (SELECT Parsename(Replace((SELECT nama_prd FROM mst_periode a WHERE a.kode_prd = " & Request.QueryString("kode_prd") & "), ' ', '.'), 1) )"
            dt = GetDataSql(strsql)
            If dt.Rows.Count = 0 OrElse IsDBNull(dt.Rows(0).Item(0)) OrElse dt.Rows.Count = 0 OrElse dt.Rows(0).Item(0) = " " Then
                strsql = "select auditor_1, auditor_2, auditor_3 from CHECKLIST_ENTRYHDR where kodecab = '" & doBranch.SelectedValue & "' and kode_prd = '" & doPeriode.SelectedValue & "'"
            End If
        Else
            strsql = "select * from FTA_CHECKLIST_AUDITOR where fta_no = '" & txtFtaNo.Text & "' and tahun = (SELECT Parsename(Replace((SELECT nama_prd FROM mst_periode a WHERE a.kode_prd = " & Request.QueryString("kode_prd") & "), ' ', '.'), 1) )"
        End If
        dt = GetDataSql(strsql)
        If dt.Rows.Count > 0 Then
            Try
                ddlAuditor1.SelectedValue = dt.Rows(0).Item("auditor_1")
                ddlAuditor2.SelectedValue = dt.Rows(0).Item("auditor_2")
                ddlAuditor3.SelectedValue = dt.Rows(0).Item("auditor_3")

                ddlPIC_Auditor.Items.Clear()

                If Not IsDBNull(dt.Rows(0).Item("auditor_1")) Then
                    ddlPIC_Auditor.Items.Add(New ListItem(getNamaAuditor(dt.Rows(0).Item("auditor_1")), dt.Rows(0).Item("auditor_1")))
                End If

                If Not IsDBNull(dt.Rows(0).Item("auditor_2")) Then
                    ddlPIC_Auditor.Items.Add(New ListItem(getNamaAuditor(dt.Rows(0).Item("auditor_2")), dt.Rows(0).Item("auditor_2")))
                End If

                If Not IsDBNull(dt.Rows(0).Item("auditor_3")) Then
                    ddlPIC_Auditor.Items.Add(New ListItem(getNamaAuditor(dt.Rows(0).Item("auditor_3")), dt.Rows(0).Item("auditor_3")))
                End If
            Catch ex As Exception

            End Try
        End If
    End Sub

    Protected Function getNamaStatus(ByVal kode As String) As String
        Dim dt As DataTable = GetDataSql("select namastatus from MST_FTA_STATUS where id = '" & kode & "'")
        If dt.Rows.Count > 0 Then
            Return dt.Rows(0).Item(0).ToString
        End If
        Return ""
    End Function

    Protected Function getNamaPosition(ByVal kode As String) As String
        Dim dt As DataTable = GetDataSql("select jabatan from MSTJABATAN where kode = '" & kode & "'")
        If dt.Rows.Count > 0 Then
            Return dt.Rows(0).Item(0).ToString
        End If
        Return ""
    End Function

    Protected Function getNamaAuditor(ByVal nik As String) As String
        Dim dt As DataTable = GetDataSql("select nama from mstauditor where nik = '" & nik & "'")
        If dt.Rows.Count > 0 Then
            Return dt.Rows(0).Item(0).ToString
        End If
        Return ""
    End Function

    Protected Function getMainSubActivity(ByVal kode As String, ByVal no As String) As String
        Dim dt As DataTable = GetDataSql("select no, Activity1 from TMP_CHECKLIST_DTL where kode = '" & kode & "' and no = (select no_parent from TMP_CHECKLIST_DTL where kode = '" & kode & "' and no = '" & no & "')")
        If dt.Rows.Count > 0 Then
            Return dt.Rows(0).Item(0) + "-" + dt.Rows(0).Item(1)
        Else
            Return ""
        End If
    End Function

    Protected Sub loadAuditor()
        ddlAuditor1.Items.Add(" ")
        ddlAuditor2.Items.Add(" ")
        ddlAuditor3.Items.Add(" ")

        Try
            Using con As New SqlConnection(strConn)
                con.Open()
                Dim da As New SqlDataAdapter("select * from mstauditor order by nama", con)
                Dim dt As New DataTable
                da.Fill(dt)
                If dt.Rows.Count > 0 Then
                    For i = 0 To dt.Rows.Count - 1
                        ddlAuditor1.Items.Add(New ListItem(dt.Rows(i).Item("nama"), dt.Rows(i).Item("nik")))
                        ddlAuditor2.Items.Add(New ListItem(dt.Rows(i).Item("nama"), dt.Rows(i).Item("nik")))
                        ddlAuditor3.Items.Add(New ListItem(dt.Rows(i).Item("nama"), dt.Rows(i).Item("nik")))
                    Next
                End If
                con.Close()
            End Using
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub InsertUpdateCommandSql(ByVal query As String)
        Try
            Using con As New SqlConnection(strConn)
                con.Open()
                cmd = New SqlCommand(query, con)
                cmd.ExecuteNonQuery()
                con.Close()
            End Using
        Catch ex As Exception
            lblerrorasd.Text = ex.Message.ToString
        End Try
    End Sub

    Protected Function find_due_date() As String
        Dim dt As New DataTable
        Dim due_date As Integer = 0
        Dim start_date As Date
        Dim finish_date As Date
        dt = GetDataSql("select due_date from MSTCASECATEGORY where kode = '" & ddlCaseCategory.SelectedValue & "'")
        If dt.Rows.Count > 0 Then
            due_date = dt.Rows(0).Item(0)
        End If

        dt = GetDataSql("select start_date from FTA_CHECKLIST_DTL where id = '" & hdnIdChklist.Value & "'")
        If dt.Rows.Count > 0 Then
            start_date = dt.Rows(0).Item(0)
        End If

        If due_date <> 0 Then
            finish_date = start_date.AddMonths(due_date)
        End If

        Return finish_date.ToString("yyyy-MM-dd")
    End Function

    Protected Function find_due_date_new() As String
        Dim dt As New DataTable
        Dim due_date As Integer = 0
        Dim start_date As Date
        Dim finish_date As Date
        dt = GetDataSql("select due_date from MSTCASECATEGORY where kode = '" & ddlCaseCategory_NewChecklist.SelectedValue & "'")
        If dt.Rows.Count > 0 Then
            due_date = dt.Rows(0).Item(0)
        End If

        start_date = Date.Now()

        If due_date <> 0 Then
            finish_date = start_date.AddMonths(due_date)
        End If

        Return finish_date.ToString("yyyy-MM-dd")
    End Function

    Protected Sub btnUpdateFinCategory_Click(sender As Object, e As EventArgs)
        InsertUpdateCommandSql("update FTA_CHECKLIST_DTL set fin_category = '" & ddlFindingCategory.SelectedValue & "' where id = '" & hdnIdChklist.Value & "'")
        loadTable()
    End Sub
    Protected Sub btnUpdateCaseCategory_Click(sender As Object, e As EventArgs)
        Dim due_date As String = find_due_date()
        InsertUpdateCommandSql("update FTA_CHECKLIST_DTL set case_category = '" & ddlCaseCategory.SelectedValue & "' , due_date = '" & due_date & "', status = case when current_timestamp < '" & due_date & "' then '4' else '1' end where id = '" & hdnIdChklist.Value & "'")
        loadTable()
    End Sub
    Protected Sub btnUpdateSuspect_Click(sender As Object, e As EventArgs)
        InsertUpdateCommandSql("update FTA_CHECKLIST_DTL set suspect = '" & txtSuspect.Text & "' where id = '" & hdnIdChklist.Value & "'")
        loadTable()
    End Sub

    Protected Sub btnUpdateCorrective_Click(sender As Object, e As EventArgs)
        InsertUpdateCommandSql("update FTA_CHECKLIST_DTL set corrective_action = '" & txtCorrective.Text & "' where id = '" & hdnIdChklist.Value & "'")
        loadTable()
    End Sub
    Protected Sub btnUpdatePIC_Auditee_Click(sender As Object, e As EventArgs)
        InsertUpdateCommandSql("update FTA_CHECKLIST_DTL set pic_auditee = '" & txtPIC_Auditee.Text & "' where id = '" & hdnIdChklist.Value & "'")
        loadTable()
    End Sub
    Protected Sub btnUpdateFileEvidence_Click(sender As Object, e As EventArgs)
        Dim postedFile As HttpPostedFile = file_evidence.PostedFile
        Dim lokasifile As String = Nothing
        Dim filePath As String
        Dim extensionfile As String

        'Check if File is available.
        If postedFile IsNot Nothing And postedFile.ContentLength > 0 Then
            'Save the File.
            Directory.CreateDirectory(Server.MapPath("/Uploads/file/FTA/" & doBranch.SelectedValue + "_" + doPeriode.SelectedValue & "/"))
            extensionfile = Path.GetExtension(postedFile.FileName)
            filePath = Server.MapPath("/Uploads/file/FTA/" & doBranch.SelectedValue + "_" + doPeriode.SelectedValue & "/") + Format(Now(), "yyyyMMddHHmmss") + extensionfile
            lokasifile = "'/Uploads/file/FTA/" & doBranch.SelectedValue + "_" + doPeriode.SelectedValue & "/" & Format(Now(), "yyyyMMddHHmmss") & extensionfile & "'"
            postedFile.SaveAs(filePath)
        Else
            lokasifile = "''"
        End If

        InsertUpdateCommandSql("update FTA_CHECKLIST_DTL set evidence = " & lokasifile & " where id = '" & hdnIdChklist.Value & "'")
        loadTable()
    End Sub
    Protected Sub btnUpdateRecommendation_Click(sender As Object, e As EventArgs)
        InsertUpdateCommandSql("update FTA_CHECKLIST_DTL set recommendation = '" & txtRecommendation.Text & "' where id = '" & hdnIdChklist.Value & "'")
        loadTable()
    End Sub
    Protected Sub btnUpdateVerif1_Click(sender As Object, e As EventArgs)
        Dim corrective_action As String = cekCorrectiveAction(hdnIdChklist.Value)
        Dim pic_auditee As String = cekPICAuditee(hdnIdChklist.Value)
        'Dim evidence As String = cekEvidence(hdnIdChklist.Value)

        If corrective_action = "" Or pic_auditee = "" Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "alertMessage", "alert('Kolom di bagian Auditee ada yang kosong.')", True)
        Else
            InsertUpdateCommandSql("update FTA_CHECKLIST_DTL set verif_result1 = '" & txtVerif1.Text & "' where id = '" & hdnIdChklist.Value & "'")
            loadTable()
        End If




    End Sub
    Protected Sub btnUpdateVerif2_Click(sender As Object, e As EventArgs)
        Dim corrective_action As String = cekCorrectiveAction(hdnIdChklist.Value)
        Dim pic_auditee As String = cekPICAuditee(hdnIdChklist.Value)
        'Dim evidence As String = cekEvidence(hdnIdChklist.Value)

        If corrective_action = "" Or pic_auditee = "" Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "alertMessage", "alert('Kolom di bagian Auditee ada yang kosong.')", True)
        Else
            InsertUpdateCommandSql("update FTA_CHECKLIST_DTL set verif_result2 = '" & txtVerif2.Text & "' where id = '" & hdnIdChklist.Value & "'")
            loadTable()
        End If

    End Sub

    Protected Function cekEvidence(ByVal id As String) As String
        Dim dt As DataTable = GetDataSql("select evidence from FTA_CHECKLIST_DTL where id = '" & id & "'")
        If dt.Rows.Count > 0 Then
            If IsDBNull(dt.Rows(0).Item(0)) Then
                Return ""
            Else
                Return dt.Rows(0).Item(0).ToString
            End If

        Else
            Return ""
        End If
    End Function

    Protected Function cekPICAuditee(ByVal id As String) As String
        Dim dt As DataTable = GetDataSql("select pic_auditee from FTA_CHECKLIST_DTL where id = '" & id & "'")
        If dt.Rows.Count > 0 Then
            If IsDBNull(dt.Rows(0).Item(0)) Then
                Return ""
            Else
                Return dt.Rows(0).Item(0).ToString
            End If

        Else
            Return ""
        End If
    End Function

    Protected Function cekCorrectiveAction(ByVal id As String) As String
        Dim dt As DataTable = GetDataSql("select corrective_action from FTA_CHECKLIST_DTL where id = '" & id & "'")
        If dt.Rows.Count > 0 Then
            If IsDBNull(dt.Rows(0).Item(0)) Then
                Return ""
            Else
                Return dt.Rows(0).Item(0).ToString
            End If

        Else
            Return ""
        End If
    End Function

    Protected Sub btnUpdateStatus_Click(sender As Object, e As EventArgs)
        Try
            Dim textVerif1 As String = cekVerif1(hdnIdChklist.Value)
            Dim textPIC As String = cekPIC(hdnIdChklist.Value)
            'lblerrorasd.Text = textPIC
            If textVerif1 = "" Or textPIC = "" Then

                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "alertMessage", "alert('Mohon isi kolom Verificiation dan PIC Auditor terlebih dahulu.')", True)
            Else
                If ddlStatus.SelectedValue = 2 Or ddlStatus.SelectedValue = 5 Then
                    InsertUpdateCommandSql("update FTA_CHECKLIST_DTL set status = '" & ddlStatus.SelectedValue & "', finish_date = current_timestamp where id = '" & hdnIdChklist.Value & "'")
                Else
                    InsertUpdateCommandSql("update FTA_CHECKLIST_DTL set status = '" & ddlStatus.SelectedValue & "', finish_date = null where id = '" & hdnIdChklist.Value & "'")
                End If
                loadTable()
            End If
        Catch ex As Exception
            lblerrorasd.Text = ex.Message.ToString
        End Try

    End Sub
    Protected Function cekPIC(ByVal id As String) As String
        Dim dt As DataTable = GetDataSql("select pic_auditor from FTA_CHECKLIST_DTL where id = '" & id & "'")
        If dt.Rows.Count > 0 Then
            If IsDBNull(dt.Rows(0).Item(0)) Then
                Return ""
            Else
                Return dt.Rows(0).Item(0).ToString
            End If

        Else
            Return ""
        End If
    End Function
    Protected Function cekVerif1(ByVal id As String) As String
        Dim dt As DataTable = GetDataSql("select verif_result1 from FTA_CHECKLIST_DTL where id = '" & id & "'")
        If dt.Rows.Count > 0 Then
            If IsDBNull(dt.Rows(0).Item(0)) Then
                Return ""
            Else
                Return dt.Rows(0).Item(0).ToString
            End If

        Else
            Return ""
        End If
    End Function
    Protected Sub btnUpdatePIC_Auditor_Click(sender As Object, e As EventArgs)
        InsertUpdateCommandSql("update FTA_CHECKLIST_DTL set pic_auditor = '" & ddlPIC_Auditor.SelectedValue & "' where id = '" & hdnIdChklist.Value & "'")
        loadTable()
    End Sub
    Protected Sub btnUpdatePosition_Click(sender As Object, e As EventArgs)
        InsertUpdateCommandSql("update FTA_CHECKLIST_DTL set position = '" & ddlPosition.SelectedValue & "' where id = '" & hdnIdChklist.Value & "'")
        loadTable()
    End Sub
    Protected Sub btnConfirm_Click(sender As Object, e As EventArgs)
        If cekValidasiFTA(txtFtaNo.Text) = 0 Then
            Dim dt As New DataTable
            Dim fta_no As Integer = 0
            'InsertUpdateCommandSql("insert into FTA_CHECKLIST_HDR (kel_pemeriksa,kodecab,kode_prd,status,created_date) values " & "('" & ddlKelompokPemeriksa.SelectedValue & "','" & doBranch.SelectedValue & "','" & doPeriode.SelectedValue & "','0',current_timestamp)")
            InsertUpdateCommandSql("update FTA_CHECKLIST_HDR set status = '0', reject_status = '0' where fta_no = '" & txtFtaNo.Text & "' and kode_prd = '" & Request.QueryString("kode_prd") & "'")
            'dt = GetDataSql("select * from FTA_CHECKLIST_HDR where kel_pemeriksa = '" & ddlKelompokPemeriksa.SelectedValue & "' and kodecab = '" & doBranch.SelectedValue & "', kode_prd = '" & doPeriode.SelectedValue & "'")
            'If dt.Rows.Count > 0 Then
            '    fta_no = dt.Rows(0).Item(0)
            'End If

            'If fta_no <> 0 Then
            '    InsertUpdateCommandSql("update FTA_CHECKLIST_DTL set fta_no = '" & fta_no & "' where fta_no = '0' and kodecab = '" & doBranch.SelectedValue & "' and kode_prd = '" & doPeriode.SelectedValue & "'")
            'End If

            btnConfirm.Visible = False
            sendNotif_Approval("approver1")
            loadTable()
        Else
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "alertMessage", "alert('Ada FTA yang masih belum diisi dengan lengkap.')", True)
        End If

    End Sub

    Protected Function cekValidasiFTA(ByVal fta_no As String) As Integer
        Dim dt As DataTable = GetDataSql("select count(1) from (select * from FTA_CHECKLIST_DTL where fta_no = '" & fta_no & "' and kode_prd = '" & Request.QueryString("kode_prd") & "') a where fin_category is null or fin_category = '' or case_category is null or case_category = '' or suspect is null or suspect = '' or position is null or position = ''")
        If dt.Rows.Count > 0 Then
            Return dt.Rows(0).Item(0)
        Else
            Return ""
        End If
    End Function

    Protected Sub btnInsert_NewChecklist_Click(sender As Object, e As EventArgs)
        Dim postedFile As HttpPostedFile = audit_result_attachment_NewChecklist.PostedFile
        Dim lokasifile As String = Nothing
        Dim filePath As String
        Dim extensionfile As String

        'Check if File is available.
        If postedFile IsNot Nothing And postedFile.ContentLength > 0 Then
            'Save the File.
            Directory.CreateDirectory(Server.MapPath("/Uploads/file/FTA/" & doBranch.SelectedValue + "_" + doPeriode.SelectedValue & "/"))
            extensionfile = Path.GetExtension(postedFile.FileName)
            filePath = Server.MapPath("/Uploads/file/FTA/" & doBranch.SelectedValue + "_" + doPeriode.SelectedValue & "/") + Format(Now(), "yyyyMMddHHmmss") + extensionfile
            lokasifile = "'/Uploads/file/FTA/" & doBranch.SelectedValue + "_" + doPeriode.SelectedValue & "/" & Format(Now(), "yyyyMMddHHmmss") & extensionfile & "'"
            postedFile.SaveAs(filePath)
        Else
            lokasifile = "''"
        End If
        Dim main_activity_text As String = ddlMainActivity_NewChecklist.SelectedItem.Text
        Dim sub_activity_text As String = ddlSubActivity_NewChecklist.SelectedItem.Text
        main_activity_text = main_activity_text.Replace(main_activity_text.Split("-")(0) & "-", "")
        sub_activity_text = sub_activity_text.Replace(sub_activity_text.Split("-")(0) & "-", "")

        InsertUpdateCommandSql("insert into FTA_CHECKLIST_DTL(fta_no,kel_pemeriksa,manual_insertion_flag,Kode_tmp,No,master_activity_flag,main_activity,sub_activity,Kodecab,Kode_Prd,Kode_Prd_Checklist,fin_category,case_category,Hasil_audit,suspect,position,position2,start_date,due_date,status,hasil_audit_attachment) values('" & txtFtaNo.Text & "','" & ddlKelompokPemeriksa.SelectedValue & "','1','0','" & ddlSubActivity_NewChecklist.SelectedValue & "','1','" & main_activity_text & "','" & sub_activity_text & "','" & doBranch.SelectedValue & "','" & doPeriode.SelectedValue & "','" & doPeriode.SelectedValue & "','" & ddlFindingCategory_NewChecklist.SelectedValue & "','" & ddlCaseCategory_NewChecklist.SelectedValue & "','" & txtAuditResult_NewChecklist.Text & "','" & txtSuspect_NewChecklist.Text & "','" & ddlPosition_NewChecklist.SelectedValue & "','" & ddlPosition2_NewChecklist.SelectedValue & "', current_timestamp,'" & find_due_date_new() & "','4'," & lokasifile & ")")
        'txtChecklistNo_NewChecklist.Text = ""
        ddlFindingCategory_NewChecklist.SelectedIndex = 0
        ddlCaseCategory_NewChecklist.SelectedIndex = 0
        txtAuditResult_NewChecklist.Text = ""
        txtSuspect_NewChecklist.Text = ""
        ddlPosition_NewChecklist.SelectedIndex = 0
        loadTable()
    End Sub
    Protected Sub btnDeleteChecklist_Click(sender As Object, e As EventArgs)
        InsertUpdateCommandSql("delete from FTA_CHECKLIST_DTL where id='" & hdnIdChklist.Value & "'")
        loadTable()
    End Sub
    Protected Sub btnApproveChecklist_Click(sender As Object, e As EventArgs)
        If Session("jabatan") = "AUDITOR DEVELOPMENT" Then
            InsertUpdateCommandSql("update FTA_CHECKLIST_HDR set STATUS = '1', approve1_by = '" & Session("sUsername") & "', approve1_date = current_timestamp where fta_no = '" & txtFtaNo.Text & "' and kode_prd = '" & Request.QueryString("kode_prd") & "'")
            sendNotif_Approval("approver2")
        ElseIf Session("jabatan") = "AUDITOR MGR" Then
            InsertUpdateCommandSql("update FTA_CHECKLIST_HDR set STATUS = '2', approve2_by = '" & Session("sUsername") & "', approve2_date = current_timestamp where fta_no = '" & txtFtaNo.Text & "' and kode_prd = '" & Request.QueryString("kode_prd") & "'")
            'InsertUpdateCommandSql("update FTA_CHECKLIST_DTL set fta_no = '" & txtFtaNo.Text & "' where kel_pemeriksa = '" & ddlKelompokPemeriksa.Text & "' and kode_prd = '" & doPeriode.SelectedValue & "' and kodecab = '" & doBranch.SelectedValue & "' and fta_no = '0'")
            If ddlKelompokPemeriksa.SelectedValue = "003" Then
                sendNotif_Subsidiaries()
            Else
                sendNotif()
            End If
        End If

        If Session("sUsername") = "SUPERADMIN" Or Session("kodejabatan") = "J0015" Then
            InsertUpdateCommandSql("update FTA_CHECKLIST_HDR set STATUS = '2', approve2_by = '" & Session("sUsername") & "', approve2_date = current_timestamp where fta_no = '" & txtFtaNo.Text & "' and kode_prd = '" & Request.QueryString("kode_prd") & "'")
            'InsertUpdateCommandSql("update FTA_CHECKLIST_DTL set fta_no = '" & txtFtaNo.Text & "' where kel_pemeriksa = '" & ddlKelompokPemeriksa.Text & "' and kode_prd = '" & doPeriode.SelectedValue & "' and kodecab = '" & doBranch.SelectedValue & "' and fta_no = '0'")
            If ddlKelompokPemeriksa.SelectedValue = "003" Then
                sendNotif_Subsidiaries()
            Else
                sendNotif()
            End If
        End If
        btnConfirm.Visible = False
        btnApproveChecklist.Visible = False
        btnRejectModal.Visible = False
        loadTable()
    End Sub

    Protected Sub sendNotif_Approval(ByVal approver As String)
        Dim start_date As Date
        Dim end_date As Date = Date.Now

        Dim pic_approve As String = ""
        Dim from_approve As String = ""

        If approver = "approver1" Then
            pic_approve = "Approve 1"
            from_approve = "Entry dan Confirm oleh Auditor."
        ElseIf approver = "approver2" Then
            pic_approve = "Approve 2"
            from_approve = "approve oleh PIC Approve1."
        End If


        Dim receiver_email As String = ""
        Dim receiver_wa As String = ""
        Dim dt As New DataTable

        dt = GetDataSql("select email,no_wa from MSTUSER where username = (select " & approver & " from MSTKELOMPOKPEMERIKSA where kodekel = '" & ddlKelompokPemeriksa.SelectedValue & "')")
        If dt.Rows.Count > 0 Then
            receiver_email = dt.Rows(0).Item(0).ToString
            receiver_wa = dt.Rows(0).Item(1).ToString
        End If

        dt = GetDataSql("select start_date from MST_PERIODE where Kode_Prd = '" & doPeriode.SelectedValue & "'")
        If dt.Rows.Count > 0 Then
            start_date = dt.Rows(0).Item(0)
        End If

        Dim sb As String
        sb = "Yth. Pic " & pic_approve & "<br>" &
"Berikut ini adalah Form Tanggapan Audit ( FTA ) yang sudah di " & from_approve & "<br><br>" &
"FTA No                       : " & txtFtaNo.Text & ".<br>" &
"Bulan & Tahun           : " & start_date.ToString("MMMM yyyy") & ".<br>" &
"Cabang / Subsidiaries : " & doBranch.SelectedItem.Text & ".<br><br>" &
"Silahkan  untuk melakukan approve atau reject (beserta reasonnya) terkait FTA tsb di Aplikasi ICE (http://ice.enseval.com/).<br>" &
"Terima kasih.<br>"

        Dim cc_email_approver As String = getEmailApprover_Approval()
        'Dim cc_email_approver As String = "kevin190598@gmail.com,kevincristian105@yahoo.com"
        If receiver_email <> "" Then
            'kirimEmail("admin_fta@enseval.com", receiver, "Form Tanggapan Auditee (FTA) Online", sb, cc_email_approver)
            'kirimEmail("admin_fta@enseval.com", "merrysa.wiradinata@enseval.com", "Form Tanggapan Auditee (FTA) Online", sb, "kevin.then@enseval.com")
            'lblerrorasd.Text = cc_email_approver + receiver
        End If

        Dim cc_wa_approver As String = getWAApprover_Approval()
        cc_wa_approver = "6281314940096,6281288599507"
        receiver_wa = "6285217419052"
        If receiver_wa <> "" Then
            If receiver_wa.Substring(0, 1) = "0" Then
                receiver_wa = "62" + receiver_wa.Remove(0, 1)
            End If

            If receiver_wa.Substring(0, 2) <> "62" Then
                receiver_wa = "62" + receiver_wa
            End If

            Dim no_hp_cc As String() = cc_wa_approver.Split(",")
            Dim textbody As String
            textbody = "Yth. Pic " & pic_approve & vbCrLf &
"Berikut ini adalah Form Tanggapan Audit ( FTA ) yang  sudah di " & from_approve & vbCrLf & vbCrLf &
"FTA No                       : " & txtFtaNo.Text & "." & vbCrLf &
"Bulan %26 Tahun           : " & start_date.ToString("MMMM yyyy") & "." & vbCrLf &
"Cabang / Subsidiaries : " & doBranch.SelectedItem.Text & "." & vbCrLf & vbCrLf &
"Silahkan  untuk melakukan approve atau reject (beserta reasonnya) terkait FTA tsb di Aplikasi ICE (http://ice.enseval.com/)." & vbCrLf &
"Terima kasih." & vbCrLf

            sendWA_ToTable(receiver_wa, textbody)

            For Each no_hp As String In no_hp_cc
                If no_hp <> "" Then
                    If no_hp.Substring(0, 1) = "0" Then
                        no_hp = "62" + no_hp.Remove(0, 1)
                    End If

                    If no_hp.Substring(0, 2) <> "62" Then
                        no_hp = "62" + no_hp
                    End If
                    sendWA_ToTable(no_hp, textbody)
                End If
            Next

        End If

    End Sub

    Protected Sub sendNotif()
        Dim start_date As Date
        Dim end_date As Date = Date.Now



        Dim receiver_email As String = ""
        Dim receiver_wa As String = ""
        Dim dt As New DataTable

        dt = GetDataSql("select email,no_wa from MSTUSER where jabatan = 'ABM' and kodecab = '" & doBranch.SelectedValue & "'")
        If dt.Rows.Count > 0 Then
            receiver_email = dt.Rows(0).Item(0).ToString
            receiver_wa = dt.Rows(0).Item(1).ToString
        End If

        dt = GetDataSql("select start_date from MST_PERIODE where Kode_Prd = '" & doPeriode.SelectedValue & "'")
        If dt.Rows.Count > 0 Then
            start_date = dt.Rows(0).Item(0)
        End If

        dt = GetDataSql("select created_date from CHECKLIST_ENTRYHDR where kodecab='" & doBranch.SelectedValue & "' and Kode_Prd = '" & doPeriode.SelectedValue & "' ")
        If dt.Rows.Count > 0 Then
            end_date = If(IsDBNull(dt.Rows(0).Item(0)), Date.Now, dt.Rows(0).Item(0))
        End If

        Dim sb As String
        sb = "Yth. ABM Cabang " & doBranch.SelectedItem.Text & "<br>" &
"Di tempat,<br><br>" &
"Dengan hormat,<br>" &
"Berdasarkan hasil pemeriksaan periode " & start_date.ToString("MMMM yyyy") & " s.d " & end_date.ToString("MMMM yyyy") & ", kami informasikan perihal permasalahan-permasalahan yang ditemukan oleh Tim Audit dalam Form Tanggapan Auditee (FTA).<br><br>" &
"Mohon Bapak/ Ibu menindaklanjuti dengan memberikan tanggapan berupa Corrective & Preventive Action beserta bukti objective (lampiran) di Aplikasi FTA Online No. " & txtFtaNo.Text & " (kolom follow up by auditee). <br><br>" &
"Aplikasi FTA Online dapat diakses menggunakan link http://ice.enseval.com. <br>" &
"Login menggunakan user dan password yang sama saat login di Aplikasi Check List ICE.<br><br>" &
"Adapun pengisian tanggapan di Aplikasi FTA Online tersebut, maksimal 7 hari kerja sejak notifikasi (email) ini diterima oleh Bapak/ Ibu.<br><br>" &
"Untuk informasi lebih lanjut, Bapak/ Ibu dapat menghubungi kami di nomor Ext. 2501/ 2506.<br><br>" &
"Demikian informasi yang dapat kami sampaikan, atas perhatian dan kerjasamanya kami ucapkan terima kasih.<br><br>" &
"Hormat kami,<br><br><br>" &
"Rustam Tan<br>" &
"Internal Audit GM<br>"

        Dim cc_email_approver As String = getEmailApprover()
        'Dim cc_email_approver As String = "kevin190598@gmail.com,kevincristian105@yahoo.com"
        If receiver_email <> "" Then
            'kirimEmail("admin_fta@enseval.com", receiver, "Form Tanggapan Auditee (FTA) Online", sb, cc_email_approver)
            'kirimEmail("admin_fta@enseval.com", "merrysa.wiradinata@enseval.com", "Form Tanggapan Auditee (FTA) Online", sb, "kevin.then@enseval.com")
            'lblerrorasd.Text = cc_email_approver + receiver
        End If

        Dim cc_wa_approver As String = getWAApprover()
        cc_wa_approver = "6281314940096,6281288599507"
        receiver_wa = "6285217419052"
        If receiver_wa <> "" Then
            If receiver_wa.Substring(0, 1) = "0" Then
                receiver_wa = "62" + receiver_wa.Remove(0, 1)
            End If

            If receiver_wa.Substring(0, 2) <> "62" Then
                receiver_wa = "62" + receiver_wa
            End If

            Dim no_hp_cc As String() = cc_wa_approver.Split(",")
            Dim textbody As String
            textbody = "Yth. ABM Cabang " & doBranch.SelectedItem.Text & vbCrLf & vbCrLf &
"Di tempat," & vbCrLf & vbCrLf &
"Dengan hormat," & vbCrLf &
"Berdasarkan hasil pemeriksaan periode " & start_date.ToString("MMMM yyyy") & " s.d " & end_date.ToString("MMMM yyyy") & ", kami informasikan perihal permasalahan-permasalahan yang ditemukan oleh Tim Audit dalam Form Tanggapan Auditee (FTA)." & vbCrLf & vbCrLf &
"Mohon Bapak/ Ibu menindaklanjuti dengan memberikan tanggapan berupa Corrective %26 Preventive Action beserta bukti objective (lampiran) di Aplikasi FTA Online No. " & txtFtaNo.Text & " (kolom follow up by auditee)." & vbCrLf & vbCrLf &
"Aplikasi FTA Online dapat diakses menggunakan link http://ice.enseval.com." & vbCrLf &
"Login menggunakan user dan password yang sama saat login di Aplikasi Check List ICE." & vbCrLf & vbCrLf &
"Adapun pengisian tanggapan di Aplikasi FTA Online tersebut, maksimal 7 hari kerja sejak notifikasi (WhatsApp) ini diterima oleh Bapak/ Ibu." & vbCrLf & vbCrLf &
"Untuk informasi lebih lanjut, Bapak/ Ibu dapat menghubungi kami di nomor Ext. 2501/ 2506." & vbCrLf & vbCrLf &
"Demikian informasi yang dapat kami sampaikan, atas perhatian dan kerjasamanya kami ucapkan terima kasih." & vbCrLf & vbCrLf &
"Hormat kami," & vbCrLf & vbCrLf & vbCrLf &
"Rustam Tan" & vbCrLf &
"Internal Audit GM" & vbCrLf & vbCrLf &
"*Pesan ini Dikirim Secara Otomatis Oleh System, Tidak Untuk Di reply*"

            sendWA_ToTable(receiver_wa, textbody)

            For Each no_hp As String In no_hp_cc
                If no_hp <> "" Then
                    If no_hp.Substring(0, 1) = "0" Then
                        no_hp = "62" + no_hp.Remove(0, 1)
                    End If

                    If no_hp.Substring(0, 2) <> "62" Then
                        no_hp = "62" + no_hp
                    End If
                    sendWA_ToTable(no_hp, textbody)
                End If
            Next

        End If

    End Sub

    Protected Sub sendNotif_Subsidiaries()
        Dim start_date As Date
        Dim end_date As Date = Date.Now



        Dim receiver_email As String = ""
        Dim receiver_wa As String = ""
        Dim dt As New DataTable

        dt = GetDataSql("select email,no_wa from MSTUSER where jabatan = 'DIREKTUR' and kodecab = '" & doBranch.SelectedValue & "'")
        If dt.Rows.Count > 0 Then
            receiver_email = dt.Rows(0).Item(0).ToString
            receiver_wa = dt.Rows(0).Item(1).ToString
        End If

        dt = GetDataSql("select start_date from MST_PERIODE where Kode_Prd = '" & doPeriode.SelectedValue & "'")
        If dt.Rows.Count > 0 Then
            start_date = dt.Rows(0).Item(0)
        End If

        dt = GetDataSql("select created_date from CHECKLIST_ENTRYHDR where kodecab='" & doBranch.SelectedValue & "' and Kode_Prd = '" & doPeriode.SelectedValue & "' ")
        If dt.Rows.Count > 0 Then
            end_date = If(IsDBNull(dt.Rows(0).Item(0)), Date.Now, dt.Rows(0).Item(0))
        End If

        Dim sb As String
        sb = "Yth. DIREKTUR " & doBranch.SelectedItem.Text & "<br>" &
"Di tempat,<br><br>" &
"Dengan hormat,<br>" &
"Berdasarkan hasil pemeriksaan periode " & start_date.ToString("MMMM yyyy") & " s.d " & end_date.ToString("MMMM yyyy") & ", kami informasikan perihal permasalahan-permasalahan yang ditemukan oleh Tim Audit dalam Form Tanggapan Auditee (FTA).<br><br>" &
"Mohon Bapak/ Ibu menindaklanjuti dengan memberikan tanggapan berupa Corrective & Preventive Action beserta bukti objective (lampiran) di Aplikasi FTA Online No. " & txtFtaNo.Text & " (kolom follow up by auditee). <br><br>" &
"Aplikasi FTA Online dapat diakses menggunakan link http://ice.enseval.com. <br>" &
"Login menggunakan user dan password sesuai dengan PIC yang melakukan pengisian FTA. <br><br>" &
"Adapun pengisian tanggapan di Aplikasi FTA Online tersebut, maksimal 7 hari kerja sejak notifikasi (email) ini diterima oleh Bapak/ Ibu.<br><br>" &
"Untuk informasi lebih lanjut, Bapak/ Ibu dapat menghubungi kami di nomor Ext. 2508/ 2511/ 2500.<br><br>" &
"Demikian informasi yang dapat kami sampaikan, atas perhatian dan kerjasamanya kami ucapkan terima kasih.<br><br>" &
"Hormat kami,<br><br>" &
"Rustam Tan<br>" &
"Internal Audit GM<br>"

        Dim cc_email_approver As String = getEmailApprover_Subsidiaries()
        'Dim cc_email_approver As String = "kevin190598@gmail.com,kevincristian105@yahoo.com"
        If receiver_email <> "" Then
            kirimEmail("admin_fta@enseval.com", receiver_email, "Form Tanggapan Auditee (FTA) Online", sb, cc_email_approver)
            'kirimEmail("admin_fta@enseval.com", "merrysa.wiradinata@enseval.com", "Form Tanggapan Auditee (FTA) Online", sb, "kevin.then@enseval.com")
            'lblerrorasd.Text = cc_email_approver + receiver
        End If

        Dim cc_wa_approver As String = getWAApprover_Subsidiaries()
        'cc_wa_approver = "6281314940096,6281288599507"
        'receiver_wa = "6285217419052"
        If receiver_wa <> "" Then
            If receiver_wa.Substring(0, 1) = "0" Then
                receiver_wa = "62" + receiver_wa.Remove(0, 1)
            End If

            If receiver_wa.Substring(0, 2) <> "62" Then
                receiver_wa = "62" + receiver_wa
            End If

            Dim no_hp_cc As String() = cc_wa_approver.Split(",")
            Dim textbody As String
            textbody = "Yth. DIREKTUR " & doBranch.SelectedItem.Text & vbCrLf &
"Di tempat," & vbCrLf & vbCrLf &
"Dengan hormat," & vbCrLf &
"Berdasarkan hasil pemeriksaan periode " & start_date.ToString("MMMM yyyy") & " s.d " & end_date.ToString("MMMM yyyy") & ", kami informasikan perihal permasalahan-permasalahan yang ditemukan oleh Tim Audit dalam Form Tanggapan Auditee (FTA)." & vbCrLf & vbCrLf &
"Mohon Bapak/ Ibu menindaklanjuti dengan memberikan tanggapan berupa Corrective %26 Preventive Action beserta bukti objective (lampiran) di Aplikasi FTA Online No. " & txtFtaNo.Text & " (kolom follow up by auditee). " & vbCrLf & vbCrLf &
"Aplikasi FTA Online dapat diakses menggunakan link http://ice.enseval.com. " & vbCrLf &
"Login menggunakan user dan password sesuai dengan PIC yang melakukan pengisian FTA." & vbCrLf & vbCrLf &
"Adapun pengisian tanggapan di Aplikasi FTA Online tersebut, maksimal 7 hari kerja sejak notifikasi (WhatsApp) ini diterima oleh Bapak/ Ibu." & vbCrLf & vbCrLf &
"Untuk informasi lebih lanjut, Bapak/ Ibu dapat menghubungi kami di nomor Ext. 2508/ 2511/ 2500." & vbCrLf & vbCrLf &
"Demikian informasi yang dapat kami sampaikan, atas perhatian dan kerjasamanya kami ucapkan terima kasih." & vbCrLf & vbCrLf &
"Hormat kami," & vbCrLf & vbCrLf &
"Rustam Tan" & vbCrLf &
"Internal Audit GM" & vbCrLf & vbCrLf &
"*Pesan ini Dikirim Secara Otomatis Oleh System, Tidak Untuk Di reply*"



            sendWA_ToTable(receiver_wa, textbody)

            For Each no_hp As String In no_hp_cc
                If no_hp <> "" Then
                    If no_hp.Substring(0, 1) = "0" Then
                        no_hp = "62" + no_hp.Remove(0, 1)
                    End If

                    If no_hp.Substring(0, 2) <> "62" Then
                        no_hp = "62" + no_hp
                    End If
                    sendWA_ToTable(no_hp, textbody)
                End If
            Next

        End If

    End Sub

    Protected Function getEmailApprover() As String
        Dim username_approver As String = "''"
        Dim email_approver As String = ""
        Dim auditor_approver As String = "''"
        Dim dt As DataTable = GetDataSql("select ''''+ approver1 + ''',' +  ''''+approver2 +'''' from MSTKELOMPOKPEMERIKSA where kodekel = '" & ddlKelompokPemeriksa.SelectedValue & "'")
        If dt.Rows.Count > 0 Then
            username_approver = dt.Rows(0).Item(0)
        End If

        If ddlKelompokPemeriksa.SelectedValue = "001" Then
            dt = GetDataSql("select '''' + auditor_1 + ''',''' + auditor_2 + ''',''' + auditor_3 + '''' from FTA_CHECKLIST_AUDITOR where fta_no = '" & txtFtaNo.Text & "' and tahun = (SELECT Parsename(Replace((SELECT nama_prd FROM mst_periode a WHERE a.kode_prd = " & Request.QueryString("kode_prd") & "), ' ', '.'), 1) )")
            If dt.Rows.Count > 0 Then
                auditor_approver = dt.Rows(0).Item(0)
            Else
                dt = GetDataSql("select '''' + auditor_1 + ''',''' + auditor_2 + ''',''' + auditor_3 + '''' from CHECKLIST_ENTRYHDR where kode_tmp = '" & hdnKode_Tmp.Value & "' and Kodecab = '" & doBranch.SelectedValue & "' and Kode_Prd = '" & doPeriode.SelectedValue & "'")
                If dt.Rows.Count > 0 Then
                    auditor_approver = dt.Rows(0).Item(0)
                End If
            End If
        Else
            dt = GetDataSql("select '''' + auditor_1 + ''',''' + auditor_2 + ''',''' + auditor_3 + '''' from FTA_CHECKLIST_AUDITOR where fta_no = '" & txtFtaNo.Text & "' and tahun = (SELECT Parsename(Replace((SELECT nama_prd FROM mst_periode a WHERE a.kode_prd = " & Request.QueryString("kode_prd") & "), ' ', '.'), 1) )")
            If dt.Rows.Count > 0 Then
                auditor_approver = dt.Rows(0).Item(0)
            End If
        End If


        dt = GetDataSql("select email from MSTUSER where username in (" & username_approver & ")")
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                email_approver = email_approver + dt.Rows(i).Item(0) + ","
            Next
        End If

        dt = GetDataSql("select email from MSTUSER where nik in (" & auditor_approver & ")")
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                email_approver = email_approver + dt.Rows(i).Item(0) + ","
            Next
        End If

        dt = GetDataSql("select email from MSTUSER where jabatan in ('SEKRETARIS','WA ABM') and kodecab = '" & doBranch.SelectedValue & "'")
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                email_approver = email_approver + dt.Rows(i).Item(0) + ","
            Next
        End If

        dt = GetDataSql("select email from MSTUSER where username = (select rbm from MSTCAB where kodecab = '" & doBranch.SelectedValue & "')")
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                email_approver = email_approver + dt.Rows(i).Item(0) + ","
            Next
        End If

        dt = GetDataSql("select email from MSTUSER where kodejabatan in (select distinct position from (select position from FTA_CHECKLIST_DTL	where fta_no = '" & txtFtaNo.Text & "' and Kode_Prd = '" & doPeriode.SelectedValue & "' union select position2 from FTA_CHECKLIST_DTL	where fta_no = '" & txtFtaNo.Text & "' and Kode_Prd = '" & doPeriode.SelectedValue & "') a) and kodecab = '" & doBranch.SelectedValue & "'")
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                email_approver = email_approver + dt.Rows(i).Item(0) + ","
            Next
        End If

        If email_approver <> "''" Then
            email_approver = email_approver.Remove(email_approver.Length - 1, 1)
        End If
        Return email_approver
    End Function

    Protected Function getEmailApprover_Approval() As String
        Dim username_approver As String = "''"
        Dim email_approver As String = ""
        Dim auditor_approver As String = "''"
        Dim dt As DataTable = Nothing

        If ddlKelompokPemeriksa.SelectedValue = "001" Then
            dt = GetDataSql("select '''' + auditor_1 + ''',''' + auditor_2 + ''',''' + auditor_3 + '''' from FTA_CHECKLIST_AUDITOR where fta_no = '" & txtFtaNo.Text & "' and tahun = (SELECT Parsename(Replace((SELECT nama_prd FROM mst_periode a WHERE a.kode_prd = " & Request.QueryString("kode_prd") & "), ' ', '.'), 1) )")
            If dt.Rows.Count > 0 Then
                auditor_approver = dt.Rows(0).Item(0)
            Else
                dt = GetDataSql("select '''' + auditor_1 + ''',''' + auditor_2 + ''',''' + auditor_3 + '''' from CHECKLIST_ENTRYHDR where kode_tmp = '" & hdnKode_Tmp.Value & "' and Kodecab = '" & doBranch.SelectedValue & "' and Kode_Prd = '" & doPeriode.SelectedValue & "'")
                If dt.Rows.Count > 0 Then
                    auditor_approver = dt.Rows(0).Item(0)
                End If
            End If
        Else
            dt = GetDataSql("select '''' + auditor_1 + ''',''' + auditor_2 + ''',''' + auditor_3 + '''' from FTA_CHECKLIST_AUDITOR where fta_no = '" & txtFtaNo.Text & "' and tahun = (SELECT Parsename(Replace((SELECT nama_prd FROM mst_periode a WHERE a.kode_prd = " & Request.QueryString("kode_prd") & "), ' ', '.'), 1) )")
            If dt.Rows.Count > 0 Then
                auditor_approver = dt.Rows(0).Item(0)
            End If
        End If


        dt = GetDataSql("select email from MSTUSER where nik in (" & auditor_approver & ")")
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                email_approver = email_approver + dt.Rows(i).Item(0) + ","
            Next
        End If

        If email_approver <> "''" Then
            email_approver = email_approver.Remove(email_approver.Length - 1, 1)
        End If
        Return email_approver
    End Function
    Protected Function getEmailApprover_Subsidiaries() As String
        Dim username_approver As String = "''"
        Dim email_approver As String = ""
        Dim auditor_approver As String = "''"
        Dim dt As DataTable = GetDataSql("select ''''+ approver1 + ''',' +  ''''+approver2 +'''' from MSTKELOMPOKPEMERIKSA where kodekel = '" & ddlKelompokPemeriksa.SelectedValue & "'")
        If dt.Rows.Count > 0 Then
            username_approver = dt.Rows(0).Item(0)
        End If

        If ddlKelompokPemeriksa.SelectedValue = "001" Then
            dt = GetDataSql("select '''' + auditor_1 + ''',''' + auditor_2 + ''',''' + auditor_3 + '''' from FTA_CHECKLIST_AUDITOR where fta_no = '" & txtFtaNo.Text & "' and tahun = (SELECT Parsename(Replace((SELECT nama_prd FROM mst_periode a WHERE a.kode_prd = " & Request.QueryString("kode_prd") & "), ' ', '.'), 1) )")
            If dt.Rows.Count > 0 Then
                auditor_approver = dt.Rows(0).Item(0)
            Else
                dt = GetDataSql("select '''' + auditor_1 + ''',''' + auditor_2 + ''',''' + auditor_3 + '''' from CHECKLIST_ENTRYHDR where kode_tmp = '" & hdnKode_Tmp.Value & "' and Kodecab = '" & doBranch.SelectedValue & "' and Kode_Prd = '" & doPeriode.SelectedValue & "'")
                If dt.Rows.Count > 0 Then
                    auditor_approver = dt.Rows(0).Item(0)
                End If
            End If
        Else
            dt = GetDataSql("select '''' + auditor_1 + ''',''' + auditor_2 + ''',''' + auditor_3 + '''' from FTA_CHECKLIST_AUDITOR where fta_no = '" & txtFtaNo.Text & "' and tahun = (SELECT Parsename(Replace((SELECT nama_prd FROM mst_periode a WHERE a.kode_prd = " & Request.QueryString("kode_prd") & "), ' ', '.'), 1) )")
            If dt.Rows.Count > 0 Then
                auditor_approver = dt.Rows(0).Item(0)
            End If
        End If


        dt = GetDataSql("select email from MSTUSER where username in (" & username_approver & ")")
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                email_approver = email_approver + dt.Rows(i).Item(0) + ","
            Next
        End If

        dt = GetDataSql("select email from MSTUSER where nik in (" & auditor_approver & ")")
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                email_approver = email_approver + dt.Rows(i).Item(0) + ","
            Next
        End If

        dt = GetDataSql("select email from MSTUSER where (jabatan like 'SEKRETARIS' or jabatan like 'FA MANAGER%') and kodecab = '" & doBranch.SelectedValue & "'")
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                email_approver = email_approver + dt.Rows(i).Item(0) + ","
            Next
        End If

        dt = GetDataSql("select email from MSTUSER where kodejabatan in (select distinct position from (select position from FTA_CHECKLIST_DTL	where fta_no = '" & txtFtaNo.Text & "' and Kode_Prd = '" & doPeriode.SelectedValue & "' union select position2 from FTA_CHECKLIST_DTL	where fta_no = '" & txtFtaNo.Text & "' and Kode_Prd = '" & doPeriode.SelectedValue & "') a) and kodecab = '" & doBranch.SelectedValue & "'")
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                email_approver = email_approver + dt.Rows(i).Item(0) + ","
            Next
        End If

        If email_approver <> "''" Then
            email_approver = email_approver.Remove(email_approver.Length - 1, 1)
        End If
        Return email_approver
    End Function
    Protected Function getWAApprover() As String
        Dim username_approver As String = "''"
        Dim wa_approver As String = ""
        Dim auditor_approver As String = "''"
        Dim dt As DataTable = GetDataSql("select ''''+ approver1 + ''',' +  ''''+approver2 +'''' from MSTKELOMPOKPEMERIKSA where kodekel = '" & ddlKelompokPemeriksa.SelectedValue & "'")
        If dt.Rows.Count > 0 Then
            username_approver = dt.Rows(0).Item(0)
        End If

        If ddlKelompokPemeriksa.SelectedValue = "001" Then
            dt = GetDataSql("select '''' + auditor_1 + ''',''' + auditor_2 + ''',''' + auditor_3 + '''' from FTA_CHECKLIST_AUDITOR where fta_no = '" & txtFtaNo.Text & "' and tahun = (SELECT Parsename(Replace((SELECT nama_prd FROM mst_periode a WHERE a.kode_prd = " & Request.QueryString("kode_prd") & "), ' ', '.'), 1) )")
            If dt.Rows.Count > 0 Then
                auditor_approver = dt.Rows(0).Item(0)
            Else
                dt = GetDataSql("select '''' + auditor_1 + ''',''' + auditor_2 + ''',''' + auditor_3 + '''' from CHECKLIST_ENTRYHDR where kode_tmp = '" & hdnKode_Tmp.Value & "' and Kodecab = '" & doBranch.SelectedValue & "' and Kode_Prd = '" & doPeriode.SelectedValue & "'")
                If dt.Rows.Count > 0 Then
                    auditor_approver = dt.Rows(0).Item(0)
                End If
            End If
        Else
            dt = GetDataSql("select '''' + auditor_1 + ''',''' + auditor_2 + ''',''' + auditor_3 + '''' from FTA_CHECKLIST_AUDITOR where fta_no = '" & txtFtaNo.Text & "' and tahun = (SELECT Parsename(Replace((SELECT nama_prd FROM mst_periode a WHERE a.kode_prd = " & Request.QueryString("kode_prd") & "), ' ', '.'), 1) )")
            If dt.Rows.Count > 0 Then
                auditor_approver = dt.Rows(0).Item(0)
            End If
        End If


        dt = GetDataSql("select no_wa from MSTUSER where username in (" & username_approver & ")")
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                wa_approver = wa_approver + dt.Rows(i).Item(0) + ","
            Next
        End If

        dt = GetDataSql("select no_wa from MSTUSER where nik in (" & auditor_approver & ")")
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                wa_approver = wa_approver + dt.Rows(i).Item(0) + ","
            Next
        End If

        dt = GetDataSql("select no_wa from MSTUSER where jabatan in ('SEKRETARIS','WA ABM') and kodecab = '" & doBranch.SelectedValue & "'")
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                wa_approver = wa_approver + dt.Rows(i).Item(0) + ","
            Next
        End If

        dt = GetDataSql("select no_wa from MSTUSER where username = (select rbm from MSTCAB where kodecab = '" & doBranch.SelectedValue & "')")
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                wa_approver = wa_approver + dt.Rows(i).Item(0) + ","
            Next
        End If

        dt = GetDataSql("select no_wa from MSTUSER where kodejabatan in (select distinct position from (select position from FTA_CHECKLIST_DTL	where fta_no = '" & txtFtaNo.Text & "' and Kode_Prd = '" & doPeriode.SelectedValue & "' union select position2 from FTA_CHECKLIST_DTL	where fta_no = '" & txtFtaNo.Text & "' and Kode_Prd = '" & doPeriode.SelectedValue & ") a) and kodecab = '" & doBranch.SelectedValue & "'")
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                wa_approver = wa_approver + dt.Rows(i).Item(0) + ","
            Next
        End If

        If wa_approver <> "''" Then
            wa_approver = wa_approver.Remove(wa_approver.Length - 1, 1)
        End If
        Return wa_approver
    End Function

    Protected Function getWAApprover_Approval() As String
        Dim username_approver As String = "''"
        Dim wa_approver As String = ""
        Dim auditor_approver As String = "''"
        Dim dt As DataTable = Nothing

        If ddlKelompokPemeriksa.SelectedValue = "001" Then
            dt = GetDataSql("select '''' + auditor_1 + ''',''' + auditor_2 + ''',''' + auditor_3 + '''' from FTA_CHECKLIST_AUDITOR where fta_no = '" & txtFtaNo.Text & "' and tahun = (SELECT Parsename(Replace((SELECT nama_prd FROM mst_periode a WHERE a.kode_prd = " & Request.QueryString("kode_prd") & "), ' ', '.'), 1) )")
            If dt.Rows.Count > 0 Then
                auditor_approver = dt.Rows(0).Item(0)
            Else
                dt = GetDataSql("select '''' + auditor_1 + ''',''' + auditor_2 + ''',''' + auditor_3 + '''' from CHECKLIST_ENTRYHDR where kode_tmp = '" & hdnKode_Tmp.Value & "' and Kodecab = '" & doBranch.SelectedValue & "' and Kode_Prd = '" & doPeriode.SelectedValue & "'")
                If dt.Rows.Count > 0 Then
                    auditor_approver = dt.Rows(0).Item(0)
                End If
            End If
        Else
            dt = GetDataSql("select '''' + auditor_1 + ''',''' + auditor_2 + ''',''' + auditor_3 + '''' from FTA_CHECKLIST_AUDITOR where fta_no = '" & txtFtaNo.Text & "' and tahun = (SELECT Parsename(Replace((SELECT nama_prd FROM mst_periode a WHERE a.kode_prd = " & Request.QueryString("kode_prd") & "), ' ', '.'), 1) )")
            If dt.Rows.Count > 0 Then
                auditor_approver = dt.Rows(0).Item(0)
            End If
        End If


        dt = GetDataSql("select no_wa from MSTUSER where nik in (" & auditor_approver & ")")
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                wa_approver = wa_approver + dt.Rows(i).Item(0) + ","
            Next
        End If

        If wa_approver <> "''" Then
            wa_approver = wa_approver.Remove(wa_approver.Length - 1, 1)
        End If
        Return wa_approver
    End Function
    Protected Function getWAApprover_Subsidiaries() As String
        Dim username_approver As String = "''"
        Dim wa_approver As String = ""
        Dim auditor_approver As String = "''"
        Dim dt As DataTable = GetDataSql("select ''''+ approver1 + ''',' +  ''''+approver2 +'''' from MSTKELOMPOKPEMERIKSA where kodekel = '" & ddlKelompokPemeriksa.SelectedValue & "'")
        If dt.Rows.Count > 0 Then
            username_approver = dt.Rows(0).Item(0)
        End If

        If ddlKelompokPemeriksa.SelectedValue = "001" Then
            dt = GetDataSql("select '''' + auditor_1 + ''',''' + auditor_2 + ''',''' + auditor_3 + '''' from FTA_CHECKLIST_AUDITOR where fta_no = '" & txtFtaNo.Text & "' and tahun = (SELECT Parsename(Replace((SELECT nama_prd FROM mst_periode a WHERE a.kode_prd = " & Request.QueryString("kode_prd") & "), ' ', '.'), 1) )")
            If dt.Rows.Count > 0 Then
                auditor_approver = dt.Rows(0).Item(0)
            Else
                dt = GetDataSql("select '''' + auditor_1 + ''',''' + auditor_2 + ''',''' + auditor_3 + '''' from CHECKLIST_ENTRYHDR where kode_tmp = '" & hdnKode_Tmp.Value & "' and Kodecab = '" & doBranch.SelectedValue & "' and Kode_Prd = '" & doPeriode.SelectedValue & "'")
                If dt.Rows.Count > 0 Then
                    auditor_approver = dt.Rows(0).Item(0)
                End If
            End If
        Else
            dt = GetDataSql("select '''' + auditor_1 + ''',''' + auditor_2 + ''',''' + auditor_3 + '''' from FTA_CHECKLIST_AUDITOR where fta_no = '" & txtFtaNo.Text & "' and tahun = (SELECT Parsename(Replace((SELECT nama_prd FROM mst_periode a WHERE a.kode_prd = " & Request.QueryString("kode_prd") & "), ' ', '.'), 1) )")
            If dt.Rows.Count > 0 Then
                auditor_approver = dt.Rows(0).Item(0).ToString
            End If
        End If


        dt = GetDataSql("select no_wa from MSTUSER where username in (" & username_approver & ")")
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                wa_approver = wa_approver + dt.Rows(i).Item(0).ToString + ","
            Next
        End If

        dt = GetDataSql("select no_wa from MSTUSER where nik in (" & auditor_approver & ")")
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                wa_approver = wa_approver + dt.Rows(i).Item(0).ToString + ","
            Next
        End If

        dt = GetDataSql("select no_wa from MSTUSER where (jabatan like 'SEKRETARIS' or jabatan like 'FA MANAGER%') and kodecab = '" & doBranch.SelectedValue & "'")
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                wa_approver = wa_approver + dt.Rows(i).Item(0).ToString + ","
            Next
        End If

        dt = GetDataSql("select no_wa from MSTUSER where kodejabatan in (select distinct position from (select position from FTA_CHECKLIST_DTL	where fta_no = '" & txtFtaNo.Text & "' and Kode_Prd = '" & doPeriode.SelectedValue & "' union select position2 from FTA_CHECKLIST_DTL	where fta_no = '" & txtFtaNo.Text & "' and Kode_Prd = '" & doPeriode.SelectedValue & "') a) and kodecab = '" & doBranch.SelectedValue & "'")
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                wa_approver = wa_approver + dt.Rows(i).Item(0) + ","
            Next
        End If

        If wa_approver <> "''" Then
            wa_approver = wa_approver.Remove(wa_approver.Length - 1, 1)
        End If
        Return wa_approver
    End Function
    Protected Sub sendWA_ToTable(ByVal no_hp As String, ByVal textbody As String)
        InsertUpdateCommandSql("insert into SEND_NOTIF_BY_WA(no_to,msg,status) values('" & no_hp & "','" & textbody & "','P')")
    End Sub
    Protected Sub sendWA(ByVal no_hp As String, ByVal textbody As String)
        Try
            Dim url As String = "https://www.waboxapp.com/api/send/chat?"
            Dim token As String = "9e082ea14d75f82ed3fdeaa45222a7c25e463eff16a12"
            Dim no_telp_from As String = "62895353107000"
            Dim Data As String = "token=" & token & "&uid=" & no_telp_from & "&to=" + no_hp + "&custom_uid=FTAMSG-" & getLastID() & "&text=" + textbody
            Dim client As New HttpClient
            client.Timeout = TimeSpan.FromSeconds(10)
            Dim Response = client.PostAsync(url + Data, Nothing).Result
            Dim responseString = Response.Content.ReadAsStringAsync().Result
            InsertUpdateCommandSql("insert into LogSendWA(last_msg,last_hit) values('" & responseString & "',current_timestamp)")
        Catch ex As Exception
            InsertUpdateCommandSql("insert into LogSendWA(last_msg,last_hit) values('Error',current_timestamp)")
        End Try

    End Sub
    Public Shared Function kirimEmail(ByVal sender As String, ByVal Receiver As String, ByVal subject As String, ByVal contains As String, ccEmail As String) As String
        Dim Smtp_Server As New SmtpClient
        Dim e_mail As New MailMessage()
        Smtp_Server.UseDefaultCredentials = False
        Smtp_Server.Port = 25
        Smtp_Server.EnableSsl = False
        'Smtp_Server.Host = "imss.enseval.com"
        Smtp_Server.Host = "mail.enseval.com"

        e_mail = New MailMessage()
        e_mail.From = New MailAddress(sender, subject)
        e_mail.To.Add(Receiver)
        Dim cc_email As String() = ccEmail.Split(",")
        For Each cc_email_split As String In cc_email
            If cc_email_split <> "" Then
                e_mail.CC.Add(New MailAddress(cc_email_split))
            End If
        Next

        e_mail.Subject = subject
        e_mail.IsBodyHtml = True
        e_mail.Body = contains

        Try
            Smtp_Server.Send(e_mail)

            Return "sukses"
        Catch ex As Exception
            Return "gagal karena " + ex.Message.ToString
        End Try

    End Function

    Protected Sub btnImportChecklist_Click(sender As Object, e As EventArgs)

        'InsertUpdateCommandSql("insert into FTA_CHECKLIST_DTL(fta_no,kel_pemeriksa,manual_insertion_flag,Kode_tmp,No,Kodecab,Kode_Prd,Kode_Prd_Checklist,Hasil_Audit,start_date) select '" & txtFtaNo.Text & "' fta_no, '" & ddlKelompokPemeriksa.SelectedValue & "' kel_pemeriksa, manual_insertion_flag, a.Kode_tmp, No, a.Kodecab, '" & doPeriode.SelectedValue & "' Kode_Prd, Kode_Prd_Checklist, Hasil_Audit, start_date from FTA_CHECKLIST_DTL a inner join FTA_CHECKLIST_HDR b on a.fta_no = b.fta_no where (finish_date is null or finish_date = '') and a.Kodecab = '" & doBranch.SelectedValue & "' and and a.kel_pemeriksa = '" & ddlKelompokPemeriksa.SelectedValue & "' a.fta_no <> '0' and b.status = '2'")
        InsertUpdateCommandSql("update FTA_CHECKLIST_DTL set fta_no = '" & txtFtaNo.Text & "', Kode_Prd = '" & doPeriode.SelectedValue & "' where id in (select a.id from FTA_CHECKLIST_DTL a inner join FTA_CHECKLIST_HDR b on a.fta_no = b.fta_no and a.Kode_Prd = b.Kode_Prd where (finish_date is null or finish_date = '') and a.Kodecab = '" & doBranch.SelectedValue & "' and a.kel_pemeriksa = '" & ddlKelompokPemeriksa.SelectedValue & "' and a.fta_no <> '0' and b.status = '2')")
        InsertUpdateCommandSql("insert into FTA_IMPORT_HISTORY values('" & txtFtaNo.Text & "','" & ddlKelompokPemeriksa.SelectedValue & "','" & doBranch.SelectedValue & "','" & doPeriode.SelectedValue & "',current_timestamp,'" & Session("sUsername") & "')")
        loadTable()
    End Sub
    Protected Sub btnSaveAuditor_Click(sender As Object, e As EventArgs)
        Dim dt As New DataTable
        If ddlAuditor1.Text <> ddlAuditor2.Text And ddlAuditor1.Text <> ddlAuditor3.Text And ddlAuditor3.Text <> ddlAuditor2.Text Then
            dt = GetDataSql("select * from FTA_CHECKLIST_AUDITOR where fta_no = '" & txtFtaNo.Text & "'")
            If dt.Rows.Count > 0 Then
                InsertUpdateCommandSql("update FTA_CHECKLIST_AUDITOR set auditor_1 = '" & ddlAuditor1.SelectedValue & "', auditor_2 = '" & ddlAuditor2.SelectedValue & "', auditor_3 = '" & ddlAuditor3.SelectedValue & "' where fta_no = '" & txtFtaNo.Text & "'")
            Else
                InsertUpdateCommandSql("insert into FTA_CHECKLIST_AUDITOR(fta_no,auditor_1,auditor_2,auditor_3) values ('" & txtFtaNo.Text & "','" & ddlAuditor1.SelectedValue & "','" & ddlAuditor2.SelectedValue & "','" & ddlAuditor3.SelectedValue & "')")
            End If
        Else
            Dim message As String = "Auditor tidak boleh sama"
            Dim sb As New System.Text.StringBuilder()
            sb.Append("<script type = 'text/javascript'>")
            sb.Append("window.onload=function(){")
            sb.Append("alert('")
            sb.Append(message)
            sb.Append("')};")
            sb.Append("</script>")
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "alert", sb.ToString())
            dt = GetDataSql("select * from FTA_CHECKLIST_AUDITOR where fta_no = '" & txtFtaNo.Text & "'")
            If dt.Rows.Count > 0 Then
                ddlAuditor1.SelectedValue = dt.Rows(0).Item("auditor_1")
                ddlAuditor2.SelectedValue = dt.Rows(0).Item("auditor_2")
                ddlAuditor3.SelectedValue = dt.Rows(0).Item("auditor_3")
            End If

        End If
    End Sub
    Protected Sub btnUploadExcel_Click(sender As Object, e As EventArgs)
        lblerrorasd.Text = ""
        Try
            Dim postedFile As HttpPostedFile = fuExcelImport.PostedFile
            Dim lokasifile As String = Nothing
            Dim filePath As String = ""
            Dim extensionfile As String = ""
            Dim datetime_string As String = DateTime.Now.ToString("yyyyMMddHHmmss")
            'Check if File is available.
            If postedFile IsNot Nothing And postedFile.ContentLength > 0 Then
                'Save the File.
                Directory.CreateDirectory(Server.MapPath("/Uploads/file/FTA/" & doBranch.SelectedValue + "_" + doPeriode.SelectedValue & "/"))
                extensionfile = Path.GetExtension(postedFile.FileName)
                filePath = Server.MapPath("/Uploads/file/FTA/" & doBranch.SelectedValue + "_" + doPeriode.SelectedValue & "/") + datetime_string + Path.GetFileName(postedFile.FileName)
                lokasifile = "'/Uploads/file/FTA/" & doBranch.SelectedValue + "_" + doPeriode.SelectedValue & "/" & datetime_string & Path.GetFileName(postedFile.FileName) & "'"
                If Not System.IO.File.Exists(filePath) Then
                    postedFile.SaveAs(filePath)
                Else
                    lblerrorasd.Text = "File sudah pernah terupload"
                End If
            Else
                lokasifile = "''"
            End If

            If lblerrorasd.Text = "" Then
                Dim connString As String = String.Empty
                Select Case extensionfile
                    Case ".xls"
                        'Excel 97-03
                        connString = ConfigurationManager.ConnectionStrings("Excel03ConString").ConnectionString
                        Exit Select
                    Case ".xlsx"
                        'Excel 07 or higher
                        connString = ConfigurationManager.ConnectionStrings("Excel07+ConString").ConnectionString
                        Exit Select

                End Select
                connString = String.Format(connString, filePath)
                Using excel_con As New OleDbConnection(connString)
                    excel_con.Open()
                    Dim sheet1 As String = excel_con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing).Rows(0)("TABLE_NAME").ToString()
                    Dim dtExcelData As New DataTable()

                    Using oda As New OleDbDataAdapter((Convert.ToString("SELECT '" & txtFtaNo.Text & "' as fta_no, '" & ddlKelompokPemeriksa.SelectedValue & "' as kel_pemeriksa, '1' as manual_insertion_flag, '0' as Kode_tmp, '" & doBranch.SelectedValue & "' as Kodecab, '" & doPeriode.SelectedValue & "' as Kode_Prd, * FROM [") & sheet1) + "] WHERE [no] IS NOT NULL", excel_con)
                        oda.Fill(dtExcelData)
                    End Using
                    excel_con.Close()

                    Dim dt_MainActivity As DataTable = GetDataSql("select kode, main_activity from MSTMAIN_ACTIVITY")
                    Dim dt_SubActivity As DataTable = GetDataSql("select kode_sub_activity, sub_activity from MSTSUB_ACTIVITY")

                    Dim dtExcelData_Clone As DataTable = dtExcelData.Clone()
                    dtExcelData_Clone.Columns("main_activity").DataType = GetType(String)
                    dtExcelData_Clone.Columns("sub_activity").DataType = GetType(String)

                    For Each dr As DataRow In dtExcelData.Rows
                        dtExcelData_Clone.ImportRow(dr)
                    Next

                    For i = 0 To dtExcelData.Rows.Count - 1
                        dtExcelData_Clone.Rows(i)("main_activity") = getMainActivity_Excel(dtExcelData.Rows(i)("main_activity").ToString(), dt_MainActivity)
                        dtExcelData_Clone.Rows(i)("sub_activity") = getSubActivity_Excel(dtExcelData.Rows(i)("sub_activity").ToString(), dt_SubActivity)
                    Next

                    Using con As New SqlConnection(strConn)
                        Using sqlBulkCopy As New SqlBulkCopy(con)
                            'Set the database table name
                            sqlBulkCopy.DestinationTableName = "dbo.FTA_CHECKLIST_DTL"

                            '[OPTIONAL]: Map the Excel columns with that of the database table
                            sqlBulkCopy.ColumnMappings.Add("fta_no", "fta_no")
                            sqlBulkCopy.ColumnMappings.Add("kel_pemeriksa", "kel_pemeriksa")
                            sqlBulkCopy.ColumnMappings.Add("manual_insertion_flag", "manual_insertion_flag")
                            sqlBulkCopy.ColumnMappings.Add("Kode_tmp", "Kode_tmp")
                            sqlBulkCopy.ColumnMappings.Add("Kodecab", "Kodecab")
                            sqlBulkCopy.ColumnMappings.Add("Kode_Prd", "Kode_Prd")
                            sqlBulkCopy.ColumnMappings.Add("No", "No")
                            sqlBulkCopy.ColumnMappings.Add("main_activity", "main_activity")
                            sqlBulkCopy.ColumnMappings.Add("sub_activity", "sub_activity")
                            sqlBulkCopy.ColumnMappings.Add("kode_prd_checklist", "Kode_Prd_Checklist")
                            sqlBulkCopy.ColumnMappings.Add("fin_category", "fin_category")
                            sqlBulkCopy.ColumnMappings.Add("case_category", "case_category")
                            sqlBulkCopy.ColumnMappings.Add("hasil_audit", "Hasil_Audit")
                            sqlBulkCopy.ColumnMappings.Add("suspect", "suspect")
                            sqlBulkCopy.ColumnMappings.Add("responsibility", "position")
                            sqlBulkCopy.ColumnMappings.Add("corrective_action", "corrective_action")
                            sqlBulkCopy.ColumnMappings.Add("pic_auditee", "pic_auditee")
                            sqlBulkCopy.ColumnMappings.Add("recommendation", "recommendation")
                            sqlBulkCopy.ColumnMappings.Add("start_date", "start_date")
                            sqlBulkCopy.ColumnMappings.Add("due_date", "due_date")
                            sqlBulkCopy.ColumnMappings.Add("verif_result1", "verif_result1")
                            sqlBulkCopy.ColumnMappings.Add("verif_result2", "verif_result2")
                            sqlBulkCopy.ColumnMappings.Add("status", "status")
                            sqlBulkCopy.ColumnMappings.Add("finish_date", "finish_date")
                            sqlBulkCopy.ColumnMappings.Add("pic_auditor", "pic_auditor")

                            con.Open()
                            sqlBulkCopy.WriteToServer(dtExcelData_Clone)
                            con.Close()
                        End Using
                    End Using
                End Using
                loadTable()
            End If
        Catch ex As Exception
            lblerrorasd.Text = ex.Message.ToString
        End Try

    End Sub

    Function getMainActivity_Excel(ByVal kode As String, ByVal dt_MainActivity As DataTable) As String
        Dim dt_MainActivity_Result As DataTable = dt_MainActivity.Clone()
        Dim queryResult = dt_MainActivity.AsEnumerable().Where(Function(myRow) myRow.Field(Of String)("kode") = kode)
        dt_MainActivity_Result = queryResult.CopyToDataTable()
        Return dt_MainActivity_Result.Rows(0)("main_activity")
    End Function

    Function getSubActivity_Excel(ByVal kode As String, ByVal dt_SubActivity As DataTable) As String
        Dim dt_SubActivity_Result As DataTable = dt_SubActivity.Clone()
        Dim queryResult = dt_SubActivity.AsEnumerable().Where(Function(myRow) myRow.Field(Of String)("kode_sub_activity") = kode)
        dt_SubActivity_Result = queryResult.CopyToDataTable()
        Return dt_SubActivity_Result.Rows(0)("sub_activity")
    End Function

    Protected Sub btnReject_Click(sender As Object, e As EventArgs)
        InsertUpdateCommandSql("update FTA_CHECKLIST_HDR set status = '-1', reject_status = '1', reject_date = current_timestamp, reject_by = '" & Session("sUsername") & "', reject_reason = '" & txtRejectReason.Text & "' where fta_no = '" & txtFtaNo.Text & "' and kode_prd = '" & Request.QueryString("kode_prd") & "'")
        btnReject.Visible = False
        btnApproveChecklist.Visible = False
        btnRejectModal.Visible = False
        loadTable()
    End Sub
    Protected Sub btnUpdateHasilAudit_Click(sender As Object, e As EventArgs)
        Dim postedFile As HttpPostedFile = audit_result_attachment.PostedFile
        Dim lokasifile As String = Nothing
        Dim filePath As String
        Dim extensionfile As String

        'Check if File is available.
        If postedFile IsNot Nothing And postedFile.ContentLength > 0 Then
            'Save the File.
            Directory.CreateDirectory(Server.MapPath("/Uploads/file/FTA/" & doBranch.SelectedValue + "_" + doPeriode.SelectedValue & "/"))
            extensionfile = Path.GetExtension(postedFile.FileName)
            filePath = Server.MapPath("/Uploads/file/FTA/" & doBranch.SelectedValue + "_" + doPeriode.SelectedValue & "/") + Format(Now(), "yyyyMMddHHmmss") + extensionfile
            lokasifile = "'/Uploads/file/FTA/" & doBranch.SelectedValue + "_" + doPeriode.SelectedValue & "/" & Format(Now(), "yyyyMMddHHmmss") & extensionfile & "'"
            postedFile.SaveAs(filePath)
        Else
            lokasifile = "''"
        End If
        If lokasifile = "''" Then
            InsertUpdateCommandSql("update FTA_CHECKLIST_DTL set hasil_audit = '" & txtHasilAudit.Text & "' where id = '" & hdnIdChklist.Value & "'")
        Else
            InsertUpdateCommandSql("update FTA_CHECKLIST_DTL set hasil_audit = '" & txtHasilAudit.Text & "', hasil_audit_attachment = " & lokasifile & " where id = '" & hdnIdChklist.Value & "'")
        End If

        loadTable()
    End Sub

    Protected Sub btnUpdateHasilAuditAttachment_Click(sender As Object, e As EventArgs)
        Dim postedFile As HttpPostedFile = audit_result_attachment_edit.PostedFile
        Dim lokasifile As String = Nothing
        Dim filePath As String
        Dim extensionfile As String

        'Check if File is available.
        If postedFile IsNot Nothing And postedFile.ContentLength > 0 Then
            'Save the File.
            Directory.CreateDirectory(Server.MapPath("/Uploads/file/FTA/" & doBranch.SelectedValue + "_" + doPeriode.SelectedValue & "/"))
            extensionfile = Path.GetExtension(postedFile.FileName)
            filePath = Server.MapPath("/Uploads/file/FTA/" & doBranch.SelectedValue + "_" + doPeriode.SelectedValue & "/") + Format(Now(), "yyyyMMddHHmmss") + extensionfile
            lokasifile = "'/Uploads/file/FTA/" & doBranch.SelectedValue + "_" + doPeriode.SelectedValue & "/" & Format(Now(), "yyyyMMddHHmmss") & extensionfile & "'"
            postedFile.SaveAs(filePath)
        Else
            lokasifile = "''"
        End If
        If lokasifile <> Nothing Then
            InsertUpdateCommandSql("update FTA_CHECKLIST_DTL set hasil_audit_attachment = " & lokasifile & " where id = '" & hdnIdChklist.Value & "'")
        End If

        loadTable()
    End Sub

    <System.Web.Services.WebMethod()>
    Public Shared Function LoadSubActivity(ByVal kode As String) As String

        Dim ConnStrAudit As String = ConfigurationManager.ConnectionStrings("SQLICE").ConnectionString
        Dim dt As New DataTable

        Dim i As Double
        Dim Chrs As String = Chr(34)

        Dim sb As New StringBuilder()
        sb.Append("[")
        Using con As New SqlConnection(ConnStrAudit)
                con.Open()
            Dim da As New SqlDataAdapter("select * from MSTSUB_ACTIVITY where kode_main_activity = '" & kode & "' order by kode_sub_activity", con)
            da.Fill(dt)
                If dt.Rows.Count > 0 Then
                    For i = 0 To dt.Rows.Count - 1
                    sb.Append("{")
                    System.Threading.Thread.Sleep(50)
                    sb.Append(String.Format("""value"":""{0}"", ""text"":""{1}""", dt.Rows(i).Item("kode_sub_activity"), dt.Rows(i).Item("kode_sub_activity") & " - " & dt.Rows(i).Item("sub_activity")))
                    sb.Append("},")
                Next
                End If
                con.Close()
            End Using

        sb = sb.Remove(sb.Length - 1, 1)
        sb.Append("]")
        Return sb.ToString()
    End Function
    Protected Sub ddlMainActivity_NewChecklist_SelectedIndexChanged(sender As Object, e As EventArgs)
        ddlSubActivity_NewChecklist.Items.Clear()
        Dim dt As DataTable = GetDataSql("select * from MSTSUB_ACTIVITY where kode_main_activity = '" & ddlMainActivity_NewChecklist.SelectedValue & "' order by kode_sub_activity")
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                ddlSubActivity_NewChecklist.Items.Add(New ListItem(dt.Rows(i).Item("kode_sub_activity") & "-" & dt.Rows(i).Item("sub_activity"), dt.Rows(i).Item("kode_sub_activity")))
            Next

        End If
    End Sub
    Protected Sub btnUpdatePosition2_Click(sender As Object, e As EventArgs)
        InsertUpdateCommandSql("update FTA_CHECKLIST_DTL set position2 = '" & ddlPosition2.SelectedValue & "' where id = '" & hdnIdChklist.Value & "'")
        loadTable()
    End Sub
    Protected Sub btnUpdateCorrective2_Click(sender As Object, e As EventArgs)
        InsertUpdateCommandSql("update FTA_CHECKLIST_DTL set corrective_action2 = '" & txtCorrective2.Text & "' where id = '" & hdnIdChklist.Value & "'")
        loadTable()
    End Sub
    Protected Sub btnUpdatePIC_Auditee2_Click(sender As Object, e As EventArgs)
        InsertUpdateCommandSql("update FTA_CHECKLIST_DTL set pic_auditee2 = '" & txtPIC_Auditee2.Text & "' where id = '" & hdnIdChklist.Value & "'")
        loadTable()
    End Sub
    Protected Sub btnUpdateFileEvidence2_Click(sender As Object, e As EventArgs)
        Dim postedFile As HttpPostedFile = file_evidence2.PostedFile
        Dim lokasifile As String = Nothing
        Dim filePath As String
        Dim extensionfile As String

        'Check if File is available.
        If postedFile IsNot Nothing And postedFile.ContentLength > 0 Then
            'Save the File.
            Directory.CreateDirectory(Server.MapPath("/Uploads/file/FTA/" & doBranch.SelectedValue + "_" + doPeriode.SelectedValue & "/"))
            extensionfile = Path.GetExtension(postedFile.FileName)
            filePath = Server.MapPath("/Uploads/file/FTA/" & doBranch.SelectedValue + "_" + doPeriode.SelectedValue & "/") + Format(Now(), "yyyyMMddHHmmss") + extensionfile
            lokasifile = "'/Uploads/file/FTA/" & doBranch.SelectedValue + "_" + doPeriode.SelectedValue & "/" & Format(Now(), "yyyyMMddHHmmss") & extensionfile & "'"
            postedFile.SaveAs(filePath)
        Else
            lokasifile = "''"
        End If

        InsertUpdateCommandSql("update FTA_CHECKLIST_DTL set evidence2 = " & lokasifile & " where id = '" & hdnIdChklist.Value & "'")
        loadTable()
    End Sub

    Protected Function getLastID() As String
        Dim dt As DataTable = GetDataSql("select count(1) + 1 from LogSendWA")
        If dt.Rows.Count > 0 Then
            Return dt.Rows(0).Item(0).ToString
        Else
            Return "0"
        End If
    End Function
End Class
