﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class fta_list
    Inherits System.Web.UI.Page
    Public Shared strConn As String = ConfigurationManager.ConnectionStrings("SQLICE").ConnectionString
    Dim myChef As New Chef
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim myData As Data.DataSet
            Dim j As Integer
            Dim dt As New DataTable
            myData = myChef.SQLMSTPERIODEAKTIF

            j = 1
            doPeriode.Items.Add("")
            For i = 0 To myData.Tables(0).Rows.Count - 1

                doPeriode.Items.Add(myData.Tables(0).Rows(i).Item("nama_prd"))
                doPeriode.Items(j).Value = myData.Tables(0).Rows(i).Item("kode_prd")
                j = j + 1

            Next
            myData = myChef.SQLLISTCAB
            j = 1
            doBranch.Items.Add("")
            For i = 0 To myData.Tables(0).Rows.Count - 1

                If Session("sCabang") = "PST" Or Session("sCabang") = myData.Tables(0).Rows(i).Item("kodecab") Then

                    doBranch.Items.Add(myData.Tables(0).Rows(i).Item("namacab"))
                    doBranch.Items(j).Value = myData.Tables(0).Rows(i).Item("kodecab")
                    j = j + 1

                End If
            Next

            dt = GetDataSql("select * from MSTKELOMPOKPEMERIKSA order by kodekel")
            ddlKelompokPemeriksa.Items.Add(New ListItem("", ""))
            If dt.Rows.Count > 0 Then
                For i = 0 To dt.Rows.Count - 1
                    ddlKelompokPemeriksa.Items.Add(New ListItem(dt.Rows(i).Item(1), dt.Rows(i).Item(0)))
                Next
            End If
            loadAuditor()
            loadTable()
            If Session("levelakses") > 3 Then
                btnNewFta.Visible = False
            End If
        End If

    End Sub
    Protected Sub loadAuditor()
        ddlAuditor1.Items.Add(" ")
        ddlAuditor2.Items.Add(" ")
        ddlAuditor3.Items.Add(" ")

        Try
            Using con As New SqlConnection(strConn)
                con.Open()
                Dim da As New SqlDataAdapter("select * from mstauditor order by nama", con)
                Dim dt As New DataTable
                da.Fill(dt)
                If dt.Rows.Count > 0 Then
                    For i = 0 To dt.Rows.Count - 1
                        ddlAuditor1.Items.Add(New ListItem(dt.Rows(i).Item("nama"), dt.Rows(i).Item("nik")))
                        ddlAuditor2.Items.Add(New ListItem(dt.Rows(i).Item("nama"), dt.Rows(i).Item("nik")))
                        ddlAuditor3.Items.Add(New ListItem(dt.Rows(i).Item("nama"), dt.Rows(i).Item("nik")))
                    Next
                End If
                con.Close()
            End Using
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub loadTable()
        Dim kode_cab As String = Session("sCabang")
        Dim kel_pemeriksa As String = getKelPemeriksa()
        Dim strsql As String = ""
        If Session("jabatan") = "AUDITOR MGR" Or Session("jabatan") = "AUDITOR DEVELOPMENT" Or Session("sUsername") = "SUPERADMIN" Or Session("kodejabatan") = "J0015" Or Session("levelakses") = "2" Then
            kode_cab = "%"
        End If
        strsql = "SELECT fta_no,tahun,b.kode_prd, (SELECT namakel FROM mstkelompokpemeriksa WHERE kodekel = kel_pemeriksa) kel_pemeriksa, (SELECT namacab FROM mstcab a WHERE a.kodecab = b.kodecab) AS kodecab, (SELECT Parsename(Replace((SELECT nama_prd FROM mst_periode a WHERE a.kode_prd = b.kode_prd), ' ', '.'), 2) ) prd_month, (SELECT Parsename(Replace((SELECT nama_prd FROM mst_periode a WHERE a.kode_prd = b.kode_prd), ' ', '.'), 1) ) prd_year, CASE WHEN status = '-1' AND (reject_status = 0 or reject_status is null) THEN 'Draft' WHEN status = '-1' AND reject_status = '1' THEN 'Rejected' WHEN status = '0' THEN 'Submitted' WHEN status = '1' THEN 'Approved' WHEN status = '2' THEN 'Approved 2' END AS status, approve1_by, approve1_date, approve2_by, approve2_date, created_date FROM fta_checklist_hdr b WHERE EXISTS (SELECT * FROM mst_periode c WHERE flag_aktif = 'A' AND c.kode_prd = b.kode_prd) AND b.kodecab LIKE '%" & kode_cab & "'"
        If kode_cab = "%" Then
            If Session("sUsername") <> "SUPERADMIN" And Session("kodejabatan") <> "J0015" And Session("levelakses") <> "2" Then
                strsql = strsql + " and kel_pemeriksa in (" & kel_pemeriksa & ")"
            End If
        Else
            strsql = strsql + "  and b.status = '2'"
        End If
        strsql = strsql + " ORDER BY fta_no DESC "

        If Session("levelakses") = "3" And Session("jabatan") <> "AUDITOR MGR" And Session("jabatan") <> "AUDITOR DEVELOPMENT" And Session("sUsername") <> "SUPERADMIN" And Session("kodejabatan") <> "J0015" And Session("levelakses") <> "2" Then
            strsql = "select * from (SELECT fta_no, tahun,b.kode_prd, (SELECT namakel FROM mstkelompokpemeriksa WHERE kodekel = kel_pemeriksa) kel_pemeriksa, (SELECT namacab FROM mstcab a WHERE a.kodecab = b.kodecab) AS kodecab, (SELECT Parsename(Replace((SELECT nama_prd FROM mst_periode a WHERE a.kode_prd = b.kode_prd), ' ', '.'), 2) ) prd_month, (SELECT Parsename(Replace((SELECT nama_prd FROM mst_periode a WHERE a.kode_prd = b.kode_prd), ' ', '.'), 1) ) prd_year, CASE WHEN status = '-1' AND (reject_status = 0 or reject_status is null) THEN 'Draft' WHEN status = '-1' AND reject_status = '1' THEN 'Rejected' WHEN status = '0' THEN 'Submitted' WHEN status = '1' THEN 'Approved' WHEN status = '2' THEN 'Approved 2' END AS status, approve1_by, approve1_date, approve2_by, approve2_date, created_date, (select count(1) from CHECKLIST_ENTRYHDR a where (auditor_1 = '" & Session("nik") & "' or auditor_2 = '" & Session("nik") & "' or auditor_3 = '" & Session("nik") & "') and a.Kodecab = b.Kodecab and a.Kode_Prd = b.Kode_Prd) auditor_status_checklist, (select count(1) from FTA_CHECKLIST_AUDITOR a where (auditor_1 = '" & Session("nik") & "' or auditor_2 = '" & Session("nik") & "' or auditor_3 = '" & Session("nik") & "') and a.fta_no = b.fta_no and a.tahun=b.tahun) auditor_status_fta FROM fta_checklist_hdr b WHERE EXISTS (SELECT * FROM mst_periode c WHERE flag_aktif = 'A' AND c.kode_prd = b.kode_prd) ) fta_list where (auditor_status_checklist = '1' and kel_pemeriksa = 'Team Internal Audit EPM') or auditor_status_fta = '1'"
        End If
        lblMsg.Text = Session("levelakses")
        Dim dt As DataTable = GetDataSql(strsql)
        GridView1.DataSource = dt
        GridView1.DataBind()
        If dt.Rows.Count > 0 Then
            GridView1.HeaderRow.TableSection = TableRowSection.TableHeader
        End If

    End Sub

    Protected Function getKelPemeriksa() As String
        Dim kode_kel As String = ""
        Dim dt As DataTable = GetDataSql("select kodekel from MSTKELOMPOKPEMERIKSA where approver1 = '" & Session("sUsername") & "' or approver2 = '" & Session("sUsername") & "'")
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                kode_kel = kode_kel + "'" + dt.Rows(i).Item(0).ToString + "',"
            Next
            kode_kel = kode_kel.Remove(kode_kel.Length - 1, 1)
            Return kode_kel
        Else
            Return "''"
        End If

    End Function

    Protected Sub GridView1_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim prd_month As String = ""
            Dim prd_year As String = ""

            'Dim dt2 As DataTable = GetDataSql("select Nama_Prd from MST_PERIODE where kode_prd = '" & e.Row.Cells(3).Text & "'")
            'If dt2.Rows.Count > 0 Then
            '    prd_month = dt2.Rows(0).Item(0).ToString.Split(" ")(0)
            '    prd_year = dt2.Rows(0).Item(0).ToString.Split(" ")(1)
            'End If
            'e.Row.Cells(3).Text = prd_month
            'e.Row.Cells(4).Text = prd_year

            Dim created_date As Date = e.Row.Cells(11).Text
            e.Row.Cells(11).Text = created_date.ToString("dd-MMM-yyyy")

            Dim btnDeleteFTA As Button = CType(e.Row.FindControl("btnDeleteFTA"), Button)
            btnDeleteFTA.Visible = False
            If Session("sUsername") = "SUPERADMIN" Or Session("levelakses") = "2" Then
                btnDeleteFTA.Visible = True
            End If
        End If
    End Sub

    Protected Sub InsertUpdateCommandSql(ByVal query As String)
        Dim cmd As New SqlCommand
        Try
            Using con As New SqlConnection(strConn)
                con.Open()
                cmd = New SqlCommand(query, con)
                cmd.ExecuteNonQuery()
                con.Close()
            End Using
        Catch ex As Exception
            Dim a As String = ex.Message.ToString
        End Try
    End Sub

    Protected Function GetDataSql(ByVal query As String) As DataTable
        Try
            Using con As New SqlConnection(strConn)
                con.Open()
                Dim da As New SqlDataAdapter(query, con)
                Dim dt As New DataTable
                da.Fill(dt)
                con.Close()
                Return dt
            End Using
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Protected Sub btnInsertFTA_Click(sender As Object, e As EventArgs)
        Dim URL, Script, Message As String
        Dim kel_pemeriksa As String = ddlKelompokPemeriksa.SelectedValue
        Dim kodecab As String = doBranch.SelectedValue
        Dim kode_prd As String = doPeriode.SelectedValue
        Dim auditor_1 As String = ddlAuditor1.SelectedValue
        Dim auditor_2 As String = ddlAuditor2.SelectedValue
        Dim auditor_3 As String = ddlAuditor3.SelectedValue

        Dim dt As DataTable = GetDataSql("select * from FTA_CHECKLIST_HDR where kel_pemeriksa = '" & kel_pemeriksa & "' and Kodecab = '" & kodecab & "' and Kode_Prd = '" & kode_prd & "'")
        If dt.Rows.Count = 0 Then
            'InsertUpdateCommandSql("insert into FTA_CHECKLIST_HDR(kel_pemeriksa,Kodecab, Kode_Prd, status, created_date) values ('" & ddlKelompokPemeriksa.SelectedValue & "','" & doBranch.SelectedValue & "','" & doPeriode.SelectedValue & "','-1',current_timestamp)")
            'InsertUpdateCommandSql("insert into FTA_CHECKLIST_AUDITOR(fta_no,auditor_1,auditor_2,auditor_3) values ('" & getLastInsertedID(ddlKelompokPemeriksa.SelectedValue, doBranch.SelectedValue, doPeriode.SelectedValue) & "','" & ddlAuditor1.SelectedValue & "','" & ddlAuditor2.SelectedValue & "','" & ddlAuditor3.SelectedValue & "')")

            InsertUpdateCommandSql("exec sp_InsertFTA_HDR '" & kel_pemeriksa & "', '" & kodecab & "', '" & kode_prd & "', '-1', '" & auditor_1 & "', '" & auditor_2 & "', '" & auditor_3 & "',''")
        Else
            Message = "Nomor FTA dengan Cabang, Periode, dan Kelompok Pemeriksa yang dipilih sudah ada."
            URL = "fta_list.aspx"
            Script = "window.onload = function(){ alert('" & Message & "'); window.location = '" & URL & "'; }"

            ClientScript.RegisterStartupScript(Me.GetType(), "Redirect", Script, True)
        End If
        loadTable()
    End Sub

    Private Function getLastInsertedID(ByVal kel_pemeriksa As String, ByVal kodecab As String, ByVal kode_prd As String) As String
        Dim dt As DataTable = GetDataSql("select fta_no from fta_checklist_hdr where kel_pemeriksa = '" & kel_pemeriksa & "' and kodecab = '" & kodecab & "' and kode_prd = '" & kode_prd & "'")
        If dt.Rows.Count > 0 Then
            Return dt.Rows(0).Item(0).ToString
        Else
            Return "0"
        End If
    End Function

    Protected Sub btnSaveAuditor_Click(sender As Object, e As EventArgs)

    End Sub
    Protected Sub btnDeleteFTA_Click(sender As Object, e As EventArgs)
        Dim row As GridViewRow = CType(sender.NamingContainer, GridViewRow)
        Dim fta_no As String = row.Cells(1).Text
        Dim kode_prd As String = row.Cells(12).Text
        InsertUpdateCommandSql("delete [FTA_CHECKLIST_HDR] where fta_no = '" & fta_no & "' and kode_prd = '" & kode_prd & "'")
        InsertUpdateCommandSql("delete fta_checklist_dtl where fta_no = '" & fta_no & "' and kode_prd = '" & kode_prd & "'")
        loadTable()
    End Sub
End Class
