﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class kriteria_penilaian
    Inherits System.Web.UI.Page

    Public Shared ConnStrAudit As String = ConfigurationManager.ConnectionStrings("SQLICE").ConnectionString
    Public Shared cmd As New SqlCommand

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        If Not Page.IsPostBack Then
            tampilTable()
        End If
    End Sub

    Public Shared Function GetDataSql(ByVal query As String) As DataTable
        Dim con As New SqlConnection(ConnStrAudit)
        Try
            con.Open()
            Dim da As New SqlDataAdapter(query, con)
            Dim dt As New DataTable
            da.Fill(dt)
            con.Close()
            Return dt
        Catch ex As Exception
            con.Close()
            Return Nothing
        End Try
    End Function

    Protected Sub tampilTable()
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                Dim da As New SqlDataAdapter("select * from MST_KRITERIA_PENILAIAN order by kode_kriteria", con)
                Dim dt As New DataTable
                da.Fill(dt)
                GridView1.DataSource = dt
                GridView1.DataBind()
                If GridView1.Rows.Count > 0 Then
                    GridView1.HeaderRow.TableSection = TableRowSection.TableHeader
                End If
                con.Close()
            End Using
        Catch ex As Exception

        End Try

    End Sub
    Protected Sub GridView1_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        tampilTable()
    End Sub
    Protected Sub clearData_Click(sender As Object, e As EventArgs)
        Response.Redirect("kriteria_penilaian.aspx")
    End Sub
    Protected Sub insertData_Click(sender As Object, e As EventArgs)
        Dim con As New SqlConnection(ConnStrAudit)
        Dim sqlstr As String = "insert into MST_KRITERIA_PENILAIAN(kode_kriteria, nama_kriteria, createdby, createdat, lasteditby, lasteditat) values('" & txtKodeKriteria.Text & "', '" & txtNamaKriteria.Text & "', '" & Session("sUsername") & "', current_timestamp, '" & Session("sUsername") & "', current_timestamp)"
        Try
            con.Open()
            cmd = New SqlCommand(sqlstr, con)
            cmd.ExecuteNonQuery()
            con.Close()

            div_lbl.Visible = True
            lbl_msg.Text = "Insert Successful!"
            tampilTable()
        Catch ex As Exception
            con.Close()
            div_lbl.Visible = True
            lbl_msg.Text = ex.Message.ToString
        End Try
    End Sub
    Protected Sub updateData_Click(sender As Object, e As EventArgs)
        Dim sqlstr As String = "update MST_KRITERIA_PENILAIAN set kode_kriteria = '" & txtKodeKriteria.Text & "', nama_kriteria = '" & txtNamaKriteria.Text & "', lasteditby = '" & Session("sUsername") & "', lasteditat = current_timestamp where kode_kriteria='" & txtKodeKriteria.Text & "'"
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                cmd = New SqlCommand(sqlstr, con)
                cmd.ExecuteNonQuery()
                con.Close()
            End Using

            div_lbl.Visible = True
            lbl_msg.Text = "Update Successful!"
            tampilTable()
        Catch ex As Exception
            div_lbl.Visible = True
            lbl_msg.Text = ex.Message.ToString
        End Try
    End Sub
    Protected Sub deleteData_Click(sender As Object, e As EventArgs)
        Dim sqlstr As String = "delete from MST_KRITERIA_PENILAIAN where kode_kriteria = '" & txtKodeKriteria.Text & "'"
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                cmd = New SqlCommand(sqlstr, con)
                cmd.ExecuteNonQuery()
                con.Close()
            End Using

            div_lbl.Visible = True
            lbl_msg.Text = "Delete Successful!"
            tampilTable()
        Catch ex As Exception
            div_lbl.Visible = True
            lbl_msg.Text = ex.Message.ToString
        End Try
    End Sub
    Protected Sub GridView1_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GridView1.RowCommand

    End Sub

    <System.Web.Services.WebMethod()>
    Public Shared Function loadDetail(ByVal kode_kriteria As String) As String
        Dim return_string As String = "["
        Using con As New SqlConnection(ConnStrAudit)
            con.Open()
            Dim da As New SqlDataAdapter("select * from MST_KRITERIA_PENILAIAN_DETAIL where kode_kriteria = '" & kode_kriteria & "' order by nilai", con)
            Dim dt As New DataTable
            da.Fill(dt)
            If dt.Rows.Count > 0 Then
                For i = 0 To dt.Rows.Count - 1
                    return_string = return_string & "{""nilai"": """ & dt.Rows(i)("nilai").ToString() & """, ""penjelasan"": """ & dt.Rows(i)("penjelasan").ToString() & """},"
                Next
                return_string = return_string.Remove(return_string.Length - 1, 1)
            End If
            con.Close()
        End Using
        return_string = return_string & "]"
        Return return_string
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function insertDetail(ByVal kode_kriteria As String, ByVal nilai As String, ByVal penjelasan As String) As String
        Dim return_string As String = "{"
        Using con As New SqlConnection(ConnStrAudit)
            con.Open()
            Dim da As New SqlDataAdapter("exec sp_Insert_Kriteria_Penilaian_Detail '" & kode_kriteria & "','" & nilai & "','" & penjelasan & "'", con)
            Dim dt As New DataTable
            da.Fill(dt)
            If dt.Rows.Count > 0 Then
                return_string = return_string & """msg"": """ & dt.Rows(0)(0).ToString() & """"
            End If
            con.Close()
        End Using
        return_string = return_string & "}"
        Return return_string
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function updateDetail(ByVal kode_kriteria As String, ByVal nilai As String, ByVal penjelasan As String) As String
        Dim return_string As String = "{"
        Using con As New SqlConnection(ConnStrAudit)
            con.Open()
            Dim da As New SqlDataAdapter("exec sp_Update_Kriteria_Penilaian_Detail '" & kode_kriteria & "','" & nilai & "','" & penjelasan & "'", con)
            Dim dt As New DataTable
            da.Fill(dt)
            If dt.Rows.Count > 0 Then
                return_string = return_string & """msg"": """ & dt.Rows(0)(0).ToString() & """"
            End If
            con.Close()
        End Using
        return_string = return_string & "}"
        Return return_string
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function deleteDetail(ByVal kode_kriteria As String, ByVal nilai As String) As String
        Dim return_string As String = "{"
        Using con As New SqlConnection(ConnStrAudit)
            con.Open()
            Dim da As New SqlDataAdapter("exec sp_Delete_Kriteria_Penilaian_Detail '" & kode_kriteria & "','" & nilai & "'", con)
            Dim dt As New DataTable
            da.Fill(dt)
            If dt.Rows.Count > 0 Then
                return_string = return_string & """msg"": """ & dt.Rows(0)(0).ToString() & """"
            End If
            con.Close()
        End Using
        return_string = return_string & "}"
        Return return_string
    End Function
End Class
