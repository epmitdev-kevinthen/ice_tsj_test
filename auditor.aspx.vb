﻿Imports System.Data.SqlClient
Imports System.Data

Partial Class auditor
    Inherits System.Web.UI.Page

    Public Shared ConnStrAudit As String = ConfigurationManager.ConnectionStrings("SQLICE").ConnectionString
    Public Shared cmd As New SqlCommand

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        If Not Page.IsPostBack Then
            tampilTable()
            tampilCabang()
        End If

    End Sub

    Protected Sub tampilCabang()
        ddlCabang.Items.Add("")
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                Dim da As New SqlDataAdapter("select * from MSTCAB", con)
                Dim dt As New DataTable
                da.Fill(dt)
                If dt.Rows.Count > 0 Then
                    For i = 0 To dt.Rows.Count - 1
                        ddlCabang.Items.Add(New ListItem(dt.Rows(i).Item("kodecab") & "-" & dt.Rows(i).Item("namacab"), dt.Rows(i).Item("kodecab")))
                    Next
                End If
                con.Close()
            End Using
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub tampilTable()
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                Dim da As New SqlDataAdapter("select * from MSTAUDITOR order by NIK", con)
                Dim dt As New DataTable
                da.Fill(dt)
                GridView1.DataSource = dt
                GridView1.DataBind()
                GridView1.HeaderRow.TableSection = TableRowSection.TableHeader
                con.Close()
            End Using
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub clearData_Click(sender As Object, e As EventArgs)
        Response.Redirect("auditor.aspx")
    End Sub

    Protected Sub insertData_Click(sender As Object, e As EventArgs)
        If txtNIK.Text.Length > 9 Then
            Dim message As String = "NIK tidak boleh lebih dari 9 karakter"
            Dim sb As New System.Text.StringBuilder()
            sb.Append("<script type = 'text/javascript'>")
            sb.Append("window.onload=function(){")
            sb.Append("alert('")
            sb.Append(message)
            sb.Append("')};")
            sb.Append("</script>")
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "alert", sb.ToString())
            Exit Sub
        End If
        Dim sqlstr As String = "insert into mstauditor(nama,nik,jabatan,kodecab,created_by,creation_date,last_update_by,last_update_date) values('" & txtNama.Text & "','" & txtNIK.Text & "','" & txtJabatan.Text & "','" & ddlCabang.SelectedValue & "','" & Session("sUsername") & "',current_timestamp,'" & Session("sUsername") & "',current_timestamp)"
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                cmd = New SqlCommand(sqlstr, con)
                cmd.ExecuteNonQuery()
                con.Close()
            End Using

            div_lbl.Visible = True
            lbl_msg.Text = "Insert Successful!"
            tampilTable()
        Catch ex As Exception
            div_lbl.Visible = True
            lbl_msg.Text = ex.Message.ToString
        End Try
    End Sub

    Protected Sub updateData_Click(sender As Object, e As EventArgs)
        If txtNIK.Text.Length > 9 Then
            Dim message As String = "NIK tidak boleh lebih dari 9 karakter"
            Dim sb As New System.Text.StringBuilder()
            sb.Append("<script type = 'text/javascript'>")
            sb.Append("window.onload=function(){")
            sb.Append("alert('")
            sb.Append(message)
            sb.Append("')};")
            sb.Append("</script>")
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "alert", sb.ToString())
            Exit Sub
        End If

        Dim sqlstr As String = "update mstauditor set nama = '" & txtNama.Text & "', nik = '" & txtNIK.Text & "', jabatan = '" & txtJabatan.Text & "', kodecab='" & ddlCabang.SelectedValue & "', last_update_by = '" & Session("sUsername") & "', last_update_date = current_timestamp where id = '" & txtId.Text & "'"
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                cmd = New SqlCommand(sqlstr, con)
                cmd.ExecuteNonQuery()
                con.Close()
            End Using

            div_lbl.Visible = True
            lbl_msg.Text = "Update Successful!"
            tampilTable()
        Catch ex As Exception
            div_lbl.Visible = True
            lbl_msg.Text = ex.Message.ToString
        End Try
    End Sub

    Protected Sub deleteData_Click(sender As Object, e As EventArgs)
        Dim sqlstr As String = "delete from MSTAUDITOR where id = '" & txtId.Text & "'"
        Try
            Using con As New SqlConnection(ConnStrAudit)
                con.Open()
                cmd = New SqlCommand(sqlstr, con)
                cmd.ExecuteNonQuery()
                con.Close()
            End Using

            div_lbl.Visible = True
            lbl_msg.Text = "Delete Successful!"
            tampilTable()
        Catch ex As Exception
            div_lbl.Visible = True
            lbl_msg.Text = ex.Message.ToString
        End Try
    End Sub
End Class
