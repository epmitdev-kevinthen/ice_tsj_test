﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CheckListAudit_byPIC_Rekap.aspx.vb" Inherits="CheckListAudit_byPIC_Rekap" MasterPageFile="~/MasterPage.master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .large-width {
            min-width: 500px !important;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />


    <asp:HiddenField ID="hdnMaxNilai" runat="server" />
    <asp:Label ID="lblHead" runat="server" Text="CheckList ICE"
        Style="font-size: 20px;"></asp:Label>
    <br />
    <div style="font-size: 20px;">
        <b>
            <table>
                <tr>
                    <td>
                        <asp:Label ID="Lblbranch" runat="server">Cabang </asp:Label>&nbsp;&nbsp;</td>
                    <td>
                        <asp:DropDownList ID="doBranch" runat="server" Width="250px"></asp:DropDownList></td>
                    <td rowspan="3">&nbsp;&nbsp;&nbsp;
                        <asp:Button ID="btnFind" CssClass="button2" runat="server" Text="Find" /></td>

                    <td rowspan="3">&nbsp;&nbsp;&nbsp;
                        <button id="btnExport" type="button" class="button2" onclick="fnExcelReport();">Export to Excel</button></td>


                    <td rowspan="3">&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="infoerror1" runat="server" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblTemplate" runat="server">CheckList </asp:Label>&nbsp;&nbsp;</td>
                    <td>
                        <asp:DropDownList ID="doTemplate" runat="server" Width="400px"></asp:DropDownList></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblPeriode" runat="server">Periode </asp:Label>&nbsp;&nbsp;</td>
                    <td>
                        <asp:DropDownList ID="doPeriode" runat="server" Width="400px"></asp:DropDownList></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label1" runat="server">Status </asp:Label>&nbsp;&nbsp;</td>
                    <td>
                        <asp:DropDownList ID="doStatusFind" runat="server" Width="200px"></asp:DropDownList></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label2" runat="server">Entry </asp:Label>&nbsp;&nbsp;</td>
                    <td>
                        <asp:DropDownList ID="doEntryFind" runat="server" Width="200px"></asp:DropDownList></td>
                </tr>


            </table>
        </b>
        <div id="divAuditor" runat="server" style="border: double; display: inline-block;">
            <b>AUDITOR</b>
            <br />
            <asp:Label ID="Label3" runat="server">Auditor - 1 </asp:Label>&nbsp;&nbsp;
                    <asp:DropDownList ID="ddlAuditor1" runat="server" Width="200px"></asp:DropDownList>
            <br />


            <asp:Label ID="Label4" runat="server">Auditor - 2 </asp:Label>&nbsp;&nbsp;
                    <asp:DropDownList ID="ddlAuditor2" runat="server" Width="200px"></asp:DropDownList>
            <br />


            <asp:Label ID="Label5" runat="server">Auditor - 3 </asp:Label>&nbsp;&nbsp;
                    <asp:DropDownList ID="ddlAuditor3" runat="server" Width="200px"></asp:DropDownList>
            <br />
            <asp:Button ID="btnSaveAuditor" runat="server" OnClick="btnSaveAuditor_Click" Text="Save Auditor" CssClass="button2" Visible="false" />

        </div>
    </div>



    <script type="text/javascript">
        $(document).ready(function () {
            var table = $('#mytable').DataTable({
                scrollY: "1000px",
                scrollX: "800px",
                scrollCollapse: true,
                bProcessing: true, // shows 'processing' label
                bStateSave: true, // presumably saves state for reloads
                paging: true,
                pagelangth: 5,
                lengthMenu: [
                    [5, 10, 25, 50, -1],
                    ['5 rows', '10 rows', '25 rows', '50 rows', 'Show all']
                ],
                fixedColumns: {
                    leftColumns: 2
                }
            });
        });

        function btnClick(args) {
            window.location.href = "r.aspx?a=1&b=" + args;
        }

        function openw(p) {
            window.open('brwfolder.aspx?p=' + p, 'mywindow', 'left=100,top=100,width=1300,height=1000,scrollbars=yes,resizable=no');
        };

        function fncBrowseF() {
            window.open('frmBrowseFile.aspx', 'mywindow', 'left=100,top=100,width=450,height=300,scrollbars=yes,resizable=no');
        }

        function fncBrowseClear() {
            document.getElementById("txtBuktiObyektif").value = "";
        }


        function fncBrowseF__3() {
            window.open('frmBrowseFile__3.aspx', 'mywindow', 'left=100,top=100,width=450,height=300,scrollbars=yes,resizable=no');
        }

        function fncBrowseClear__3() {
            document.getElementById("txtBuktiObyektif__3").value = "";
        }

        function fncBrowseF__4() {
            window.open('frmBrowseFile__4.aspx', 'mywindow', 'left=100,top=100,width=450,height=300,scrollbars=yes,resizable=no');
        }

        function fncBrowseClear__4() {
            document.getElementById("txtBuktiObyektif__4").value = "";
        }


        function fncBrowseF__6() {
            window.open('frmBrowseFile__6.aspx', 'mywindow', 'left=100,top=100,width=450,height=300,scrollbars=yes,resizable=no');
        }

        function fncBrowseClear__6() {
            document.getElementById("txtBuktiObyektif__6").value = "";
        }




        function DropOnChange1() {
            if (document.getElementById("txtDone1").value == "N/A") { document.getElementById("txtDone2").value = "N/A" };
        }

        function DropOnChange2() {
            if (document.getElementById("txtDone2").value == "N/A") { document.getElementById("txtDone1").value = "N/A" };
        }

        function fnExcelReport() {
            var table = $("#tbl2");
            if (table && table.length) {
                $(table).table2excel({
                    exclude: ".noExl",
                    name: "Excel Document Name",
                    filename: "ICE by PIC Rekap.xls",
                        fileext: ".xls",
                        exclude_img: true,
                        exclude_links: true,
                        exclude_inputs: true,
                        preserveColors: true
                    });
            }
            return false;
        }

    </script>

    <div style="margin: 30px;">

        <!--<button type='button' class='button2xx' data-book-id='||||A|A' data-target='#formModalNew' data-toggle='modal'>Add New</button>
    -->

        <asp:Label ID="_periode" Style="display: none" runat="server"></asp:Label>
        <asp:Label ID="_cabang" Style="display: none" runat="server"></asp:Label>
        <asp:Label ID="_kode" Style="display: none" runat="server"></asp:Label>

        <asp:Label ID="_statusFind" Style="display: none" runat="server"></asp:Label>
        <asp:Label ID="_entryFind" Style="display: none" runat="server"></asp:Label>

        <asp:PlaceHolder ID="myPlaceH" runat="server"></asp:PlaceHolder>

        <asp:Repeater ID="rptTable1" runat="server" OnItemDataBound="rptTable1_ItemDataBound">
            <HeaderTemplate>
                <table id="tbl1">
                    <thead>
                        <tr>
                            <th rowspan="2">No. </th>
                            <th rowspan="2">PIC</th>
                            <th colspan="4">1st Defense</th>
                            <th colspan="4">2nd Defense</th>
                            <th colspan="4">Total</th>
                            <th rowspan="2">Category</th>
                        </tr>
                        <tr>
                            <th>Element Control</th>
                            <th>Poin Element Control</th>
                            <th>Poin Done Control</th>
                            <th>Score</th>
                            <th>Element Control</th>
                            <th>Poin Element Control</th>
                            <th>Poin Done Control</th>
                            <th>Score</th>
                            <th>Element Control</th>
                            <th>Poin Element Control</th>
                            <th>Poin Done Control</th>
                            <th>Score</th>
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td><%# Container.ItemIndex + 1 %></td>
                    <td><%# Eval("PIC").ToString() %></td>
                    <td style="text-align: right;"><%# Eval("Element_Control_1st").ToString() %></td>
                    <td style="text-align: right;"><%# Eval("Poin_Element_Control_1st").ToString() %></td>
                    <td style="text-align: right;"><%# Eval("Poin_Done_Control_1st").ToString() %></td>
                    <td style="text-align: right;"><%# Eval("score_1st").ToString() %></td>
                    <td style="text-align: right;"><%# Eval("Element_Control_2nd").ToString() %></td>
                    <td style="text-align: right;"><%# Eval("Poin_Element_Control_2nd").ToString() %></td>
                    <td style="text-align: right;"><%# Eval("Poin_Done_Control_2nd").ToString() %></td>
                    <td style="text-align: right;"><%# Eval("score_2nd").ToString() %></td>
                    <td style="text-align: right;"><%# Eval("total_Element_Control").ToString() %></td>
                    <td style="text-align: right;"><%# Eval("total_Poin_Element_Control").ToString() %></td>
                    <td style="text-align: right;"><%# Eval("total_Poin_Done_Control").ToString() %></td>
                    <td style="text-align: right;"><asp:label ID="lblTotal_Score" runat='server' Text='<%# Eval("total_score").ToString() %>' /></td>
                    <asp:PlaceHolder ID="plhCategory" runat="server"></asp:PlaceHolder>                    
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                
                    </tbody>
                </table>
            </FooterTemplate>
        </asp:Repeater>

        <table id="tbl2" style="display:none;">
            <thead>
                <tr>
                    <th rowspan="2">No. </th>
                    <th rowspan="2">PIC</th>
                    <th colspan="4">1st Defense</th>
                    <th colspan="4">2nd Defense</th>
                    <th colspan="4">Total</th>
                    <th rowspan="2">Category</th>
                </tr>
                <tr>
                    <th>Element Control</th>
                    <th>Poin Element Control</th>
                    <th>Poin Done Control</th>
                    <th>Score</th>
                    <th>Element Control</th>
                    <th>Poin Element Control</th>
                    <th>Poin Done Control</th>
                    <th>Score</th>
                    <th>Element Control</th>
                    <th>Poin Element Control</th>
                    <th>Poin Done Control</th>
                    <th>Score</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

    <asp:Label ID="infoerror" runat="server" Style="color: red"></asp:Label>



    <script type="text/javascript">

        $(function () {
            var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "personal";
            $('#Tabs a[href="#' + tabName + '"]').tab('show');
            $("#Tabs a").click(function () {
                $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
            });

            $("#tbl1 > tbody").clone().appendTo($("#tbl2 > tbody"));

            $("#tbl1").DataTable({
                paging: true,
                bProcessing: true, // shows 'processing' label
                bStateSave: true, // presumably saves state for reloads
            });
        });


    </script>


    <div style="display: none;">
        <asp:Label ID="myHTMLTable" runat="server"></asp:Label>

        <asp:TextBox ID="sUsername_" runat="server"></asp:TextBox>
        <asp:TextBox ID="levelakses_" runat="server"></asp:TextBox>
        <asp:TextBox ID="jabatan_" runat="server"></asp:TextBox>
        <asp:TextBox ID="sCabang_" runat="server"></asp:TextBox>
        <asp:TextBox ID="kodedept_" runat="server"></asp:TextBox>

    </div>


    <asp:HiddenField ID="hdnPengali" runat="server" />

</asp:Content>

